import numpy as np
import scipy.stats
import math
import re
import pandas as pd
import subprocess
import time
import os
os.chdir('/home/ryan/Documents/meamfit/SampleCalculation/Input/MEAM/Test')
import meamfit_py
meamfit_py.start_mpi()

# Dataframe for recording values at each step
vals = pd.DataFrame(columns=['accepted_b1', 'rejected_b1',
                             'accepted_b2', 'rejected_b2',
                             'accepted_b3', 'rejected_b3',
                             'accepted_b4', 'rejected_b4',
                             'curr_post',
                             'prop_post',
                             'prob',
                             'likelihood',
                             'prior',
                             'change_accepted'])


iterations = 3
meamfit_params = meamfit_py.read_params_py()

theta = [0,0,0,5]
theta[0] = meamfit_params[1][0][1]
theta[1] = meamfit_params[3][0][1]
theta[2] = meamfit_params[5][0][1]


def likelihood(theta):
    likelihoods = np.exp((-meamfit_py.meamfit(theta[0], theta[1], theta[2]))/theta[3])
    return likelihoods


#Priors
b1_dist = scipy.stats.norm(theta[0], 0.1)
#b1_dist = scipy.stats.uniform(0,10)
#b1_dist = scipy.stats.gamma(6, theta[0]-5)

b2_dist = scipy.stats.norm(theta[1], 0.1)
#b2_dist = scipy.stats.uniform(0,10)
#b2_dist = scipy.stats.gamma(6, theta[1]-5)

b3_dist = scipy.stats.norm(theta[2], 0.1)
#b3_dist = scipy.stats.uniform(0,10)
#b3_dist = scipy.stats.gamma(6,theta[2]-5)


b4_dist = scipy.stats.uniform(0,10)

#x = np.linspace(-5,10,31)
#plt.plot(scipy.stats.gamma.pdf(x,theta[0], 2))

#plt.plot(scipy.stats.gamma(6, 5).pdf(x))
#plt.plot(x, scipy.stats.gamma(loc=3.5, scale=5).pdf(x))
#plt.plot(x, scipy.stats.uniform(0,10).pdf(x))

#plt.plot(scipy.stats.norm(theta[0], .1).pdf(x))

# Functions needed for prob calc at each step
def prior(theta):
    b1_prior = b1_dist.pdf(theta[0])
    b2_prior = b2_dist.pdf(theta[1])
    b3_prior = b3_dist.pdf(theta[2])
    b4_prior = b4_dist.pdf(theta[3])
    return b1_prior * b2_prior * b3_prior * b4_prior

def posterior(theta):
    return likelihood(theta) * prior(theta)

# Current posterior
curr_post = posterior(theta)

# MCMC Loop
for i in range(1, iterations):
    print("---------------- \n {}".format(i))
    # Proposed step
    theta_proposal = [np.random.normal(theta[0], 0.05),
                      np.random.normal(theta[1], 0.05),
                      np.random.normal(theta[2], 0.05),#]
                      np.random.normal(theta[3], 0.1)]
    # Proposed posterior
    prop_post = posterior(theta_proposal)
    # If prob > 1 the proposed theta is accepted, if not, value between 0 and 1 is the prob of acceptance
    prob = (prop_post / curr_post)

    if np.random.uniform(0,1) < prob and prob > 0:
        vals = vals.append({'accepted_b1'   :theta_proposal[0], 'rejected_b1'    :theta[0],
                            'accepted_b2'   :theta_proposal[1], 'rejected_b2'    :theta[1],
                            'accepted_b3'   :theta_proposal[2], 'rejected_b3'    :theta[2],
                            'accepted_b4'   :theta_proposal[3], 'rejected_b4'    :theta[3],
                            'curr_post'     :curr_post,
                            'prop_post'     :prop_post,
                            'prob'          :prob,
                            'likelihood'    :likelihood(theta_proposal),
                            'prior'    :prior(theta_proposal),
                            'change_accepted': True}, ignore_index=True)
        theta = theta_proposal
        curr_post = prop_post
    else:
        vals = vals.append({'accepted_b1'   :theta[0], 'rejected_b1'     :theta_proposal[0],
                            'accepted_b2'   :theta[1], 'rejected_b2'     :theta_proposal[1],
                            'accepted_b3'   :theta[2], 'rejected_b3'     :theta_proposal[2],
                            'accepted_b4'   :theta[3], 'rejected_b4'     :theta_proposal[3],
                            'curr_post'     :curr_post,
                            'prop_post'     :prop_post,
                            'prob'          :prob,
                            'likelihood'    :likelihood(theta),
                            'prior'         :prior(theta),
                            'change_accepted': False}, ignore_index=True)


meamfit_py.end_mpi()
vals.to_csv('1.csv')
vals=pd.read_csv('1.csv')


def plot_converg(accept, reject ,string, predicted):
    ax = plt.subplot(1,1,1)
    plt.grid(which='both', color='black', linestyle='-', linewidth=0.5, alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.4)
    ax.plot(range(1,len(accept)+1), accept, color = 'black',  marker=None, alpha=0.8)
    plt.title("coefficient {} (starting {})".format(string, predicted))
    plt.scatter(range(1,len(accept)+1), reject, marker='x', alpha=0.18, color='red')
    plt.xlabel('no. iterations')
    plt.ylabel(string)
    ax.annotate(string, xy=(range(1,len(accept)+1),accept), 
                xytext=(5,0), 
                xycoords=(ax.get_xaxis_transform(),
                          ax.get_yaxis_transform()),
                textcoords='offset points', 
                va='center')
    
    plt.legend(["accepted", "rejected"], 
               loc ="lower right")
    plt.show()
    plt.draw()

import seaborn as sns

burn_in = 0
import matplotlib.pyplot as plt


# Plot of convergence
plot_converg(vals['accepted_b1'], vals['rejected_b1'], 'b1', round(vals['accepted_b1'][0],3))
plot_converg(vals['accepted_b2'], vals['rejected_b2'], 'b2', round(vals['accepted_b2'][0],3))
plot_converg(vals['accepted_b3'], vals['rejected_b3'], 'b3', round(vals['accepted_b3'][0],3))
#plot_converg(vals['accepted_b4'], vals['rejected_b4'], 'b4', round(vals['accepted_b4'][0],3))


# b0
# Overview of coefficient
vals['accepted_b1'][burn_in:].describe()

#%matplotlib inline
#plt.plot(range(1,iterations), vals['accepted_b3'])[0]

# Quick histogram
#sns.histplot(vals['accepted_a'][burn_in:])
# b1
vals['accepted_b2'][burn_in:].describe()
#sns.histplot(vals['accepted_b'][burn_in:])

# sb2
vals['accepted_b3'][burn_in:].describe()
#sns.histplot(data = vals['accepted_sigma'][burn_in:])


#plot_converg(vals['accepted_b4'], vals['rejected_b4'], 'b', 1)

