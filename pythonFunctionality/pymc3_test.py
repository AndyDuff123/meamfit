#!/usr/bin/python3

# PyMC3 Approach
import arviz as az
import numpy as np
import pymc3 as pm
import os
import theano.tensor as t
import theano
theano.config.mode = 'FAST_COMPILE'
os.chdir('/home/ryan/Documents/meamfit/SampleCalculation/Input/MEAM/Test')
import meamfit_py15
meamfit_py15.start_mpi()
#set environment MALLOC_CHECK_ 2

@theano.compile.ops.as_op(itypes=[t.dscalar, t.dscalar, t.dscalar], otypes=[t.dscalar])
def meamfit_liklihood(a1,b1,c1):
    return np.array(np.exp(-1*(meamfit_py15.meamfit(a1,b1,c1))))
    

# MCMC
model = pm.Model()

with model:
    
    meamfit_params = meamfit_py15.read_params_py()
    b0 = pm.Lognormal('b0', mu=float(meamfit_params[1][0][1]), sigma=1)
    b1 = pm.Lognormal('b1', mu=float(meamfit_params[3][0][1]), sigma=1)
    b2 = pm.Lognormal('b2', mu=float(meamfit_params[5][0][1]), sigma=1)

    #pm.DensityDist('likelihood', meamfit_log_liklihood()),
    #               observed={'theta': (m, c), 'x': x, 'data': data, 'sigma': sigma})
    #y_obs = pm.Normal("Y_obs", mu=mu, sigma=sigma)
    liklihood = pm.Potential("liklihood", meamfit_liklihood(b0,b1,b2))
    trace = pm.sample(3, return_inferencedata=False, cores=1)

# Quick val of average result for each var
#pm.find_MAP(model=model)

# 1000 posterior samples
#with model:
    


#MAP and maximum of liklihood of distribution


# Shows values summary
#with model:
#    pm.display(az.summary(trace, round_to=5))

# Plot value distributions
#with model:
#    az.plot_trace(trace);


meamfit_py15.end_mpi()


