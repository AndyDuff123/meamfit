Use modern Fortran free form syntax.

Do not use common blocks, use modules, with the **m_...** naming convention.

Do not use go to statements.

Use the .f90 file extension for any new files.

Use spaces for indentation. Do not use tabs as they are not part of the Fortran standard.

Fortran keywords in lower case (e.g. **end program**, not **End Program** or **END PROGRAM**).

Use lower-case english names for variables, constants and program units. Underscore can be used to separate words.

Use **.eq.**, **.ne.**, **.gt.** etc rather than **==**, **/=**, **>** etc.

Do not use the optional separation space for end constructs, e.g. **endif**, not **end if** and **enddo**, not **end do**.

Use **do ... end do** instead of **do ... label continue**.

Do not use interactive commands, like **Read** from keyboard or **Pause**.

Use **Implicit None** to avoid unwanted side effects.

Write comments starting with upper case then lower case. Use hyphen structure, e.g.: **!---- Set-up structures and fitting database ----** to demarkate distinct functionality in code.

