These subroutines are written by Prashanth Srinivasan - please see manual for citation.

Compatible with LAMMPS version 29Sep2021 or greater.

Copy the files into the LAMMPS src/ directory prior to installation.
