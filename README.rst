
MEAMfit
=======

A Fortran code to fit reference-free modified embedded atom method (RF-MEAM) interatomic potentials to density functional theory (DFT) data (VASP or CASTEP).

License
-------

The project is licensed under the `BSD 3-Clause License <LICENSE>`_

Citation
--------

The code is free to use, but if you use it for publication then please cite:

“MEAMfit: A reference-free modified embedded atom method (RF-MEAM) energy and force-fitting code”, Comp. Phys. Comm. 196 439-445 (2015)

User documentation
------------------

See `MEAMfitUserGuide <MEAMfitUserGuide.pdf>`_ for instructions on installation and usage.
As well as the user-manual, there is also a `video guide <https://implant.fs.cvut.cz/meamfit2-compilation/>`_ to installation by Miroslav Lebeda.

Contributions
-------------

Contributions are very welcome. Please first raise an issue to describe the contribution you are planning to make. Then follow 
the branch, fix, merge model, from your own fork. The merge request will then be reviewed. Be sure to follow the `style guide <StyleGuide.rst>`_.
