#!/bin/bash

#
#conversion code : quantum espresso (QE) to (castep) GEOM format
#
#Written by: Ian Shuttleworth
#            Andrew Ian Duff
#E-mail: ian.shuttleworth@ntu.ac.uk / shuttleworth.ian@gmail.com
#
#
# LAUNCH code by:
# sh convert_QE_geom.sh <QE input file> <QE output file> <GEOM filename>
#
# no need to add '.geom' to GEOM filename - code does that automatically
#
# (NOTE FOR USERS: do a substitution: '=' -> ' = ' in the input file before running this)

QE_IN="$1"
QE_OUT="$2"
if [[ "$3" == *.geom ]] ; then
    OUT="$3"
else
    OUT="${3}.geom"
fi

#
#INPUT FILE REQUIREMENTS:
#
#common species are together in ATOMIC_POSITIONS
#CANNOT have species with forces set to zero
#if forces are going to be used as part of the MEAMfit fitting process
#
#ATOMIC_POSITIONS (alat)
#<species 1>
#<species 2>
#...
#<species ntypes>
#




#
#INPUT FILE REQUIREMENTS:
#lattice parameter specified using A (in Angstrom)
#
# Update by Andy: ibrav ~= 0 means that a space group representation is being used
# (user does not have to specify all inequivalent sites in the input file). Since this
# requires space-group subroutines to then determine all positions, this is beyond
# the scope of this script. HOWEVER, the script should still run, because except for
# the first atomic configuration, all other sets of atomic positions (in the output file) 
# will be of 'alat', or 'crystal' type, and so accessible. Behaviour should be that a warning is given
# that the first set of positions and corresponding observables will not be used, but
# that if these are required then the input file should be manually altered (ibrav changed 
# and atomic positions type changed from crystal_sg) and the script re-run.
# NOTE: the assumption that atomic position types in the output file will not be crystal_sg
# is also checked. If atomic position types ARE crystal_sg here the code WILL exit.
#
if [[ ! `grep ibrav $QE_IN | sed s/,//g | awk '{print $3}'` == "0" ]] ; then

    echo "WARNING: You do not have ibrav = 0. The first atomic configuration will"
    echo "be ignored. If you require the first configuration manually change ibrav to 0"
    echo "and change the positions specification to alat or crystal"

    ignoreFirstConfig=true

else

    ignoreFirstConfig=false
    
fi

if grep -q celldm $QE_IN ; then

    echo "lattice parameter must be specified in terms of A"
    exit 0
    
fi


#
#INPUT FILE REQUIREMENTS:
#ATOMIC_POSITIONS (alat, crystal)
#CELL_PARAMETERS (alat, crystal)
#
if [[ ! ignoreFirstConfig ]]; then
    if [[ `grep "CELL_PARAMETERS" $QE_IN` == *"(alat)"* ]]; then
        echo "Cell parameters of type alat"
    elif [[ `grep "CELL_PARAMETERS" $QE_IN` == *"(bohr)"* || `grep "CELL_PARAMETERS" $QE_IN` == *"(angstrom)"* ]] ; then
        echo "ERROR: bohr or angstrom cell_parameters types not supported, STOPPING"
        exit 0
    fi
    if [[ `grep "ATOMIC_POSITIONS" $QE_IN` == *"(alat)"* ]]; then
        atomicPositionType="alat"
    elif [[ `grep "ATOMIC_POSITIONS" $QE_IN` == *"(crystal)"* ]]; then
        atomicPositionType="crystal"
    else
        echo "ERROR: atomic_positions type not supported, STOPPING"
        exit 0
    fi
fi


#
#remove all blank lines from QE input file
#

sed -i '/^$/d' $QE_IN

#
#CONSTANTS for unit conversion
#
#Andy: MEAMfit is agnostic to units. The potential will adopt the same units as whatever the units of DFT data
#are supplied. The only exception is if we are using EXCLUDECOULOMB mode, in which case we need to convert
#to eV. Variables will need defining below (not completed).
#
#QE/PW uses atomic Rydberg units:
#energy in Ry, lengths in Bohr radii
#hbar=1 ; e^2=2 ; electron mass m=1/2
#QE energy output is in Ry
#QE force output is Ry/au (Rydberg/Bohr)
#
convert_energy=1
convert_distance=1



#CONSTANTS from input files
lat_const=`grep "A " $QE_IN | awk '{print $3}' | sed s/","//g`
lat_const=$(echo "$lat_const * $convert_distance" | bc)

natoms_total=`grep nat  $QE_IN | sed s/,//g | awk '{print $3}'`

ntypes=`grep ntyp $QE_IN | sed s/,//g | awk '{print $3}'`

nstructs=`grep ATOMIC_POSITIONS $QE_OUT | cat -n | tail -1 | awk '{print $1}'`

echo "ntypes="${ntypes}
species=`awk -v nlines="${ntypes}"  '/ATOMIC_SPECIES/ \
{for(j=1;j<=nlines;j++) {getline; print $1}}' $QE_IN `
echo "species="${species}

i=0
echo "${species[@]}="${species[@]}
for var in ${species[@]} ; do
    ((i++))
    echo "i="$i", var="$var
    n_species[$i]=`awk -v nlines="${natoms_total}"  '/ATOMIC_POSITIONS/ \
{for(j=1;j<=nlines;j++) {getline; print $1}}' $QE_IN | \
grep "${var}" | cat -n | tail -1 | awk '{print $1}'`
    echo $n_species[$i]
done



#
#Determine start lines in QE output file of structures and then forces
#associated with those structures determined after the SCF calculation
#

istart_R=(`grep -n ATOMIC_POSITIONS $QE_OUT | sed s/":"/" "/g | awk '{ sum = $1 ; print sum}'`)
istart_F=(`grep -n "Forces acting on atoms" $QE_OUT | sed s/":"/" "/g | \
awk '{ sum = $1 + 1; print sum}'`)



#
#NOTES:
#first set of forces are from structure given in input file
#
#first structure in output file is the relaxed according to the
#forces listed immediately above it (the first set of forces)
#
#second last two structures in output file are identical
#i.e. once forces are all below a particular level no need for further movement
# "Begin final coordinates" just restates structure immediately above it
#

#remove any previous output file.
if [ -f $OUT ]; then rm $OUT ; fi

#MEAMfit does not need GEOM header so next line is optional
#printf "BEGIN header\n\nEND header\n" > $OUT

if [[ ! ignoreFirstConfig ]]; then
       
    #first structure is stored in $QE_IN 
    #system energy
    E=`grep "total energy" $QE_OUT | grep "!" | \
    awk -v conv=$convert_energy  '{printf "%.8f\n", $5 * conv }' | \
    head -1 | tail -1`
    
    printf "%44s%27s%35s\n" $E $E "<-- E" >> $OUT
    
    #lattice vectors    
    for j in 1 2 3 ; do
        
        grep -A "$j" CELL_PARAMETERS $QE_IN | \
    	tail -1 | awk -v lconst=$lat_const \
    		      '{printf "%44s%27s%27s%8s\n", \
    		      $1 * lconst, \
    		      $2 * lconst, \
    		      $3 * lconst, \
    		      "<-- h"}' >> $OUT
	#store these as we will need them if we have atomicPositionType=crystal
        latticeVector_x[$j]=`tail -n1 $OUT | awk '{print $1}'`
        latticeVector_y[$j]=`tail -n1 $OUT | awk '{print $2}'`
        latticeVector_z[$j]=`tail -n1 $OUT | awk '{print $3}'`
    done
    
    #structure - coordinates

    finish=`echo "$(grep -n ATOMIC_POSITIONS $QE_IN | sed s/:/" "/g | \
    awk '{print $1}') + 1" | bc`
    
    for j in $(seq 1 $ntypes) ; do
    
        finish=$(($finish + ${n_species[$j]} ))
   
        if [[ $atomicPositionType == "alat" ]]; then
            head -n "${finish}" $QE_IN | tail -n  ${n_species[$j]} | \
    	    awk '{print $1" "$2" "$3" "$4}' | cat -n | \
    	    awk -v lconst=$lat_const \
    	       '{printf " %-2s%15s%26s%27s%27s%8s\n", $2, $1, \
    	       $3 * lconst, $4 * lconst, $5 * lconst, "<-- R"}' \
    	       >> $OUT
        elif [[ $atomicPositionType == "crystal" ]]; then
            head -n "${finish}" $QE_IN | tail -n  ${n_species[$j]} | \
    	    awk '{print $1" "$2" "$3" "$4}' | cat -n | \
    	    awk -v lconst=$lat_const \
    	       '{printf " %-2s%15s%26s%27s%27s%8s\n", $2, $1, \
    	       $3 * latticeVector_x[1] + $4 * latticeVector_x[2] + $5 * latticeVector_x[3], \
    	       $3 * latticeVector_y[1] + $4 * latticeVector_y[2] + $5 * latticeVector_y[3], \
    	       $3 * latticeVector_z[1] + $4 * latticeVector_z[2] + $5 * latticeVector_z[3], \
                "<-- R"}' \
    	       >> $OUT
        fi
    	
    done
    
    
    #structure - forces
    finish=${istart_F[0]}
    j=1
    for spec in ${species} ; do
        
        finish=$(($finish + ${n_species[$j]} ))
    	
        head -n "${finish}" $QE_OUT | tail -n  ${n_species[$j]} | \
    	cat -n | sed s/^/"$spec"/ | \
    	awk -v conv=$convert_energy  \
    	    '{printf " %-2s%15s%26s%27s%27s%8s\n", $1, $2, \
    	    $9 * conv, $10 * conv, $11 * conv, "<-- F"}' \
    	    >> $OUT
    
        ((j++))
       	    
    done
    
    echo "" >> $OUT
    
fi

#terminate here if there were no relaxation steps
#i.e only one SCF calculation
if [[ `grep "iteration #  1" $QE_OUT | cat -n | tail -1 | awk '{print $1}'` == "1" ]] ; then

    echo "no relaxation steps"
    exit 0
    
fi

# Check the atomic positions type in the output file (I do not have much experience with QE, but I have
# seen at least one instance where the type is different between input and output, so include this to be
# safe).
if [[ `grep "ATOMIC_POSITIONS" $QE_OUT` == *"(alat)"* ]]; then
    atomicPositionType="alat"
elif [[ `grep "ATOMIC_POSITIONS" $QE_OUT` == *"(crystal)"* ]]; then
    atomicPositionType="crystal"
else
    echo "ERROR: atomic_positions type not supported, STOPPING"
    echo "(EXITING during reading QE output file. atomicPositionType="$atomicPositionType")"
    exit 0
fi

i_R=0
i_F=1

#second and subsequent structures are stored in $QE_OUT
#for i in $(seq 2 ${#istart_R[@]}) ; do
for i in 2 ; do

    #system energy
    E=`grep "total energy" $QE_OUT | grep "!" | \
awk -v conv=$convert_energy  '{printf "%.8f\n", $5 * conv }' | \
head -"$i" | tail -1`

    printf "%44s%27s%35s\n" $E $E "<-- E" >> $OUT


    #lattice vectors    
    for j in 1 2 3 ; do

	grep -A "$j" CELL_PARAMETERS $QE_IN | \
	    tail -1 | awk -v lconst=$lat_const \
			  '{printf "%44s%27s%27s%8s\n",
			  $1 * lconst, \
			  $2 * lconst, \
			  $3 * lconst, \
			  "<-- h"}' >> $OUT
	
    done

    #structure - coordinates
    finish=${istart_R[$i_R]}
    echo "finish="$finish
    for j in $(seq 1 $ntypes) ; do

        echo "n_species["$j"]="${n_species[$j]}
	finish=$(($finish + ${n_species[$j]} ))

        if [[ $atomicPositionType == "alat" ]]; then
            head -n "${finish}" $QE_OUT | tail -n  ${n_species[$j]} | \
    	    awk '{print $1" "$2" "$3" "$4}' | cat -n | \
    	    awk -v lconst=$lat_const \
    	       '{printf " %-2s%15s%26s%27s%27s%8s\n", $2, $1, \
    	       $3 * lconst, $4 * lconst, $5 * lconst, "<-- R"}' \
    	       >> $OUT
        elif [[ $atomicPositionType == "crystal" ]]; then
            head -n "${finish}" $QE_OUT | tail -n  ${n_species[$j]} | \
    	    awk '{print $1" "$2" "$3" "$4}' | cat -n | \
    	    awk -v lconst=$lat_const \
    	       '{printf " %-2s%15s%26s%27s%27s%8s\n", $2, $1, \
    	       $3 * latticeVector_x[1] + $4 * latticeVector_x[2] + $5 * latticeVector_x[3], \
    	       $3 * latticeVector_y[1] + $4 * latticeVector_y[2] + $5 * latticeVector_y[3], \
    	       $3 * latticeVector_z[1] + $4 * latticeVector_z[2] + $5 * latticeVector_z[3], \
                "<-- R"}' \
    	       >> $OUT
        fi
	
    done

    ((i_R++))

    #structure - forces
    finish=${istart_F[$i_F]}
    j=1
    for spec in ${species} ; do

	finish=$(($finish + ${n_species[$j]} ))
	
	head -n "${finish}" $QE_OUT | tail -n  ${n_species[$j]} | \
	    cat -n | sed s/^/"$spec"/ | \
	    awk -v conv=$convert_energy  \
		'{printf " %-2s%15s%26s%27s%27s%8s\n", $1, $2, \
	    	$9 * conv, $10 * conv, $11 * conv, "<-- F"}' \
		>> $OUT

	((j++))
	    
    done

    ((i_F++))

    echo "" >> $OUT
    
done
