        program outcar2vasprun

        ! Andy Duff
        ! Code to 'convert' an OUTCAR to vasprun file. Only those data
        ! relevant to MEAMfit are read in. These include:
        ! number of atoms and species, species per atoms, and then for
        ! each snapshot in the trajectory:
        ! energy, forces, stress tensor, volume.
        ! 
        ! Note that OUTCAR stores positions in Cartesian rather than
        ! Direct coordinates, while vasprun stores as latter. To avoid
        ! having to convert, instead of writing:
        !    <varray name="positions">
        ! to vasprun we write:
        !    <varray name="cartesian">
        ! then in MEAMfit we just interpret the coordinates as Cartesian
        !
        ! Further note: OUTCAR doesn't record the species of atoms,
        ! therefore this needs to be provided as input to this code.

        implicit none

        logical finished
        integer natoms,nspecies,IOstatus,ispc,iatom,i,iconf
        integer, allocatable :: natoms_spc(:)
        real(8) stress(6),energy,energyWE,volume,lattvectors(3,3)
        real(8), allocatable :: xyz(:,:),forces(:,:)
        character*2, allocatable :: species(:)
        character*106 string,string2

        open(50,file='OUTCAR')
        open(51,file='vasprun_new.xml')

        ! Read in species
        print *,'no. species? : '
        read *,nspecies
        allocate(species(nspecies),natoms_spc(nspecies))
        do ispc=1,nspecies
          print *,'species name for ',ispc,'? (e.g., Na) :'
          read *,species(ispc)
          print *,'number of atoms for species ',species(ispc),'? :'
          read *,natoms_spc(ispc)
        enddo

        ! Locate in OUTCAR number of atoms
        do
          read(50,'(a106)',IOSTAT=IOstatus) string
          if (len(string)>=65) then
             !if (trim(string(59:65)).ne.'') then
             !   print *,'string(59:65) = ',string(59:65)
             !endif
             if (string(59:65).eq.'NIONS =') then
               string2=string(66:)
               read(string2,*) natoms
               exit
             endif
          endif
        enddo
        print *,'natoms= ',natoms
        allocate(xyz(3,natoms),forces(3,natoms))
        
        ! Write initial data to vasprun
        write(51,"(A)") '<modeling>'
        write(51,"(A,I2,A)") '  <atoms>      ',natoms,' </atoms>'
        write(51,"(A,I2,A)") '  <types>      ',nspecies,' </types>'
        do i=1,5
          write(51,*)
        enddo
        do ispc=1,nspecies
          do iatom=1,natoms_spc(ispc)
            write(51,"(A,A,A,I4,A)") '    <rc><c>',species(ispc), &
       '</c><c>',ispc,'</c></rc>'
          enddo
        enddo

        ! Read positions, forces, energies, stresses

        iconf=0
        finished=.false.
        do 

           ! Stress
           do
             read(50,'(a106)',IOSTAT=IOstatus) string

             if (IOstatus.lt.0) then
                finished=.true.
                exit
             endif

             if (len(string)>=7) then
                if (string(1:7).eq.'  in kB') then
                  string2=string(8:)
                  !print *,'string2=',string2
                  read(string2,*) stress(1:6)
                  exit
                endif
             endif
           enddo
           ! print *,'stress= ',stress(1:6)
           if (finished.eqv..true.) then
             print *,'Final configuration read in, after ',iconf, &
          ' configs'
             exit
           endif

           ! Volume
           do
             read(50,'(a106)',IOSTAT=IOstatus) string
             if (len(string)>=18) then
                if (string(1:18).eq.'  volume of cell :') then
                  string2=string(19:)
                  !print *,'string2=',string2
                  read(string2,*) volume
                  exit
                endif
             endif
           enddo
           ! print *,'volume of cell= ',volume

           ! Lattice vectors
           do
             read(50,'(a106)',IOSTAT=IOstatus) string
             if (len(string)>=28) then
                if (string(1:28).eq.'      direct lattice vectors') then
                  exit
                endif
             endif
           enddo
           read(50,*) lattvectors(1:3,1)
           read(50,*) lattvectors(1:3,2)
           read(50,*) lattvectors(1:3,3)
           ! print *,'lattice vectors= ',lattvectors

           ! Positions and forces
           do
             read(50,'(a106)',IOSTAT=IOstatus) string
             if (len(string)>=9) then
                if (string(1:9).eq.' POSITION') then
                  exit
                endif
             endif
           enddo
           read(50,*)
           do iatom=1,natoms
             read(50,*) xyz(1:3,iatom),forces(1:3,iatom)
           !   print *,'xyz,frc=',xyz(1:3,iatom),forces(1:3,iatom)
           enddo

           ! Energy
           do
             read(50,'(a106)',IOSTAT=IOstatus) string
             if (len(string)>=22) then
                if (string(1:22).eq.'  free  energy   TOTEN') then
                  exit
                endif
             endif
           enddo
           string2=string(26:)
           read(string2,*) energy
           read(50,'(a106)',IOSTAT=IOstatus) string
           read(50,'(a106)',IOSTAT=IOstatus) string
           string2=string(27:)
           read(string2,*) energyWE
           print *,'free energy = ',energy
           print *,'energy WO entropy = ',energyWE

           ! Write to vasprun
           write(51,*) '   <varray name="basis" >'
           write(51,*) '    <v>   ',lattvectors(1:3,1)
           write(51,*) '    <v>   ',lattvectors(1:3,2)
           write(51,*) '    <v>   ',lattvectors(1:3,3)
           write(51,*) '   </varray>'
           write(51,*) '   <i name="volume">  ',volume
           do i=1,6
              write(51,*) ' dummy line'
           enddo
           write(51,*) '  <varray name="cartesian" >'
           do iatom=1,natoms
              write(51,*) '   <v>   ',xyz(1:3,iatom)
           enddo
           write(51,*) ' <varray name="forces" >'
           do iatom=1,natoms
              write(51,*) '  <v>   ',forces(1:3,iatom)
           enddo
           write(51,*) ' <varray name="stress" >'
           write(51,*) '  <v>   ',stress(1),stress(4),stress(6)
           write(51,*) '  <v>   ',stress(4),stress(2),stress(5)
           write(51,*) '  <v>   ',stress(6),stress(5),stress(3)
           write(51,*) '  <i name="e_fr_energy">  ',energy
           write(51,*) '  <i name="e_wo_entrp">  ',energyWE

           iconf=iconf+1

        enddo

        close(50)
        close(51)

        end program

