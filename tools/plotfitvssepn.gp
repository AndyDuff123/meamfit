set key font ",8"
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 3
unset xlabel
unset xtics
set multiplot layout 4,1
unset title
set ytics scale 0 font ",8"
set ylabel font ",8"
set ylabel "Total energy of supercell (eV)"
p './fitted_quantities.out' u 1:2 t "Potential", './fitted_quantities.out' u 1:3 w l t "DFT"
p './fitted_quantities.out' u 1:7 t "Smallest sepn, X-X"
p './fitted_quantities.out' u 1:8 t "Smallest sepn, X-Y"
p './fitted_quantities.out' u 1:9 t "Smallest sepn, Y-Y"

