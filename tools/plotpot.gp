#set terminal postscript eps size 4.5,3.4 enhanced color \
#    font 'Helvetica,20' linewidth 1
#set output "potvssepns.eps"
set tmargin 0.5
set bmargin 0.5
set lmargin 10
set rmargin 3

# first check the spacing we need for the ytics:
#
ntics=4
#
# plot 1:
p './pairpotXX' w l t "PairPot 1,1", './pairpotXY' w l t "PairPot 1,2", './pairpotYY' w l t "PairPot 2,2"
dy1=floor((GPVAL_Y_MAX-GPVAL_Y_MIN)/ntics*1000.)/1000.
xmin=GPVAL_X_MIN
xmax=GPVAL_X_MAX
# plot 2:
p './elecdensXX' w l t "AtomDens 1" , './elecdensYY' w l t "AtomDens 2"
dy2=floor((GPVAL_Y_MAX-GPVAL_Y_MIN)/ntics*1000.)/1000.
# plot 3:
p 'sepnHistogram.out' u 1:2 w l t "SepnDist 1,1", 'sepnHistogram.out' u 1:3 w l t "SepnDist 1,2", 'sepnHistogram.out' u 1:4 w l t "SepnDist 2,2"
dy3=floor((GPVAL_Y_MAX-GPVAL_Y_MIN)/ntics*1000.)/1000.

unset xtics
set multiplot layout 4, 1
set key font ",8"
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 3
unset title
set ytics scale 0 font ",8"
set ytics dy1
set xrange[xmin:xmax]
p './pairpotXX' w l t "PairPot 1,1", './pairpotXY' w l t "PairPot 1,2", './pairpotYY' w l t "PairPot 2,2"
set ytics dy2
set xrange[xmin:xmax]
p './elecdensXX' w l t "AtomDens 1" , './elecdensYY' w l t "AtomDens 2"
set xlabel font ",8"
set xlabel "Seperation"
set xtics scale 0 font ",8"
set xtics nomirror
set ytics dy3
p 'sepnHistogram.out' u 1:2 w l t "SepnDist 1,1", 'sepnHistogram.out' u 1:3 w l t "SepnDist 1,2", 'sepnHistogram.out' u 1:4 w l t "SepnDist 2,2"
unset multiplot
