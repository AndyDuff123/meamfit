
      program selectforcesforopt

      ! This program reads through an OUTCAR file, with a name which can
      ! be specified by the user, and produces a file ('outputfile')
      ! containing the energy line and the force lines, with the force
      ! lines given a 't' in the first column if any of the components
      ! have a magnitude greater than that specified by the user, and
      ! provided no other symmetrically equivalent force has already
      ! been given a 't'.

      implicit none

      logical allforcessmall,noeqvforcefound,settozero
      integer i,j,nargs,natoms,nforcesopt,nforcesopt_tot
      character*1 snglchar
      character*80 line,inputfile,aux(6),preenergy(6)
      character*80, allocatable:: words(:)
      real(8) trueenergy,minimumforce,truedata_readin(9000),
     +        posns(9000),xcompmag,ycompmag,xcomp,ycomp,zcomp,
     +        zcompmag,xcompmagj,ycompmagj,zcompmagj,xcompj,
     +        ycompj,zcompj,zeroforcethres

      zeroforcethres=0.05d0 !If all forces are smaller than this, offer to
                      !automatically set them all to zero

      print *
      print *,'Please enter name of OUTCAR file for which'
      print *,'you want to select which forces to optimize.'
      read(*,*) inputfile
      print *
      print *,'Please enter number of atoms.'
      read(*,*) natoms
      if (dble(natoms).gt.(3000d0)) then
         print *,'ERROR: Number of atoms too large, stopping.'
         stop
      endif
      print *
      print *,'Please enter the minimum force component'
      print *,'for which force should be included in'
      print *,'optimization.'
      read(*,*) minimumforce

      open(unit=1,file=inputfile)

      !First locate the energy
      do
         read(1,'(a80)') line
         call optionalarg(line,nargs)
         allocate(words(nargs))
         read(line,*) words(1:nargs)
         if (line(1:26).eq.'  energy  without entropy=') then
            read(line,*) preenergy(1:6),trueenergy
            deallocate(words)
            exit
         endif
         deallocate(words)
      enddo
      print *,'energy=',trueenergy

      !Next read in the forces
      allforcessmall=.true.
      do
         read(1,'(a80)') line
         call optionalarg(line,nargs)
         if (line(1:9).eq.' POSITION') then
            !Now we have found the part of the file containing the
            !forces
            print *,'found forces...'
            read(1,*)
            do i=1,natoms
               !Read in the forces (need to check first if there is
               !already a 't' at the start
               read(1,*) line
               if ((line(1:1).eq.'T').or.(line(1:1).eq.'t')) then
                  backspace(unit=1)
                  read(1,*) snglchar,posns(3*i-2:3*i),
     +                   truedata_readin(3*i-2:3*i)
               else
                  backspace(unit=1)
                  read(1,*) posns(3*i-2:3*i),
     +                   truedata_readin(3*i-2:3*i)
               endif
               if ((sqrt(truedata_readin(3*i-2)**2).gt.zeroforcethres)
     +      .or.(sqrt(truedata_readin(3*i-1)**2).gt.zeroforcethres)
     +      .or.(sqrt(truedata_readin(3*i)**2).gt.zeroforcethres)) then
                  allforcessmall=.false.
c                  print *,sqrt(truedata_readin(3*i-2)**2),
c     + sqrt(truedata_readin(3*i-1)**2),sqrt(truedata_readin(3*i)**2),
c     +     zeroforcethres
               endif
            enddo
            exit
         endif
      enddo
      close(unit=1)

      settozero=.false.
      if (allforcessmall) then
         print *
         print *,'All forces are small (<',zeroforcethres,').'
         print *,'Would you like to set them to zero? (T/F)'
         read *,settozero
      endif

      !Write out to new file
      open(unit=1,file='outputfile')

      write(1,*)
      write(1,*)
      write(1,*)
      write(1,*)
      write(1,*) ' energy  without entropy= blah energy(sigma->0) =',
     +          trueenergy
      write(1,*)
      write(1,*) 'POSITION                                       ',
     +           'TOTAL-FORCE (eV/Angst)'
      write(1,*) '------------------------------------------------',
     +           '----------------------'
      nforcesopt_tot=0
      nforcesopt=0
      do i=1,natoms
         !First check if at least one of the force components is greater
         !than the user specified value.
         xcomp=truedata_readin(3*i-2)
         ycomp=truedata_readin(3*i-1)
         zcomp=truedata_readin(3*i)
         xcompmag=sqrt(truedata_readin(3*i-2)**2)
         ycompmag=sqrt(truedata_readin(3*i-1)**2)
         zcompmag=sqrt(truedata_readin(3*i)**2)
         if ((xcompmag.ge.minimumforce).or.(ycompmag.ge.minimumforce)
     +       .or.(zcompmag.ge.minimumforce)) then
           nforcesopt_tot=nforcesopt_tot+1
           !Now check that we haven't already tagged a symmetrically
           !equivalent force
           noeqvforcefound=.true.
           do j=1,i-1
              xcompj=truedata_readin(3*j-2)
              ycompj=truedata_readin(3*j-1)
              zcompj=truedata_readin(3*j)
              xcompmagj=sqrt(truedata_readin(3*j-2)**2)
              ycompmagj=sqrt(truedata_readin(3*j-1)**2)
              zcompmagj=sqrt(truedata_readin(3*j)**2)
              if ( ((xcompmag.eq.xcompmagj).and.(ycompmag.eq.ycompmagj)
     +         .and.(zcompmag.eq.zcompmagj)) .or.
     +             ((xcompmag.eq.ycompmagj).and.(ycompmag.eq.zcompmagj)
     +         .and.(zcompmag.eq.xcompmagj)).or.
     +             ((xcompmag.eq.zcompmagj).and.(ycompmag.eq.xcompmagj)
     +         .and.(zcompmag.eq.ycompmagj)) .or.
     +             ((xcompmag.eq.ycompmagj).and.(ycompmag.eq.xcompmagj)
     +         .and.(zcompmag.eq.zcompmagj)) .or.   
     +             ((xcompmag.eq.zcompmagj).and.(ycompmag.eq.ycompmagj)
     +         .and.(zcompmag.eq.xcompmagj)) .or.   
     +             ((xcompmag.eq.xcompmagj).and.(ycompmag.eq.zcompmagj)
     +         .and.(zcompmag.eq.ycompmagj)) .or.   
     +             ((xcompmag.eq.zcompmagj).and.(ycompmag.eq.ycompmagj)
     +         .and.(zcompmag.eq.xcompmagj)) .or.  
     +             ((xcompmag.eq.xcompmagj).and.(ycompmag.eq.zcompmagj)
     +         .and.(zcompmag.eq.ycompmagj)) .or.   
     +             ((xcompmag.eq.ycompmagj).and.(ycompmag.eq.xcompmagj)
     +         .and.(zcompmag.eq.zcompmagj)) ) then
                  noeqvforcefound=.false.
               endif
           enddo
           if (settozero) then
            xcomp=0d0
            ycomp=0d0
            zcomp=0d0
           endif
           if (noeqvforcefound) then         
            nforcesopt=nforcesopt+1
            write(1,'(a,f10.5,a,f10.5,a,f10.5,a,f10.6,a,f10.6,a,f10.6)')
     +      't  ',posns(3*i-2),'   ',posns(3*i-1),'   ',posns(3*i),
     +      '       ',xcomp,'    ',ycomp,'    ',zcomp
           else
            write(1,'(a,f10.5,a,f10.5,a,f10.5,a,f10.6,a,f10.6,a,f10.6)')
     +      '   ',posns(3*i-2),'   ',posns(3*i-1),'   ',posns(3*i),
     +      '       ',xcomp,'    ',ycomp,'    ',zcomp
           endif
         else
           write(1,'(a,f10.5,a,f10.5,a,f10.5,a,f10.6,a,f10.6,a,f10.6)')
     +     '   ',posns(3*i-2),'   ',posns(3*i-1),'   ',posns(3*i),
     +     '       ',xcomp,'    ',ycomp,'    ',zcomp
         endif
      enddo

      close(unit=1)

      print *
      print *,nforcesopt_tot,'forces have components greater'
      print *,'than the value you specified. Of these,'
      print *,nforcesopt,'are symmetrically unique, and have'
      print *,'therefore been tagged with a t in the first'
      print *,'column.'
      print *
      print *,'Written contents to outputfile'

      end program selectforcesforopt





      subroutine optionalarg(line,nargs)

c--------------------------------------------------------------c
c
c     Parses the string 'line', searches for number of strings 
c     separated by spaces or commas.
c
c     Called by:     readposcar,readoutcarenergy,
c                 readoutcarforces
c     Calls:         -
c     Arguments:     line
c     Returns:       nargs
c     Files read:    -
c     Files written: -   
c
c     Marcel Sluiter, Dec 28 2005 
c
c--------------------------------------------------------------c

      implicit none

      integer nargs,length,i
      character line*(*)
      logical newarg

      intent(in) line
      intent(out) nargs

      !Analyze LINE 
      length=len_trim(line)
      !Count number of sections in LINE that are separated by commas or
      !spaces 
      nargs=0
      newarg=.true.
      do i=1,length
        if ((line(i:i).eq.',').or.(line(i:i).eq.' ').or.
     +        (line(i:i).eq.CHAR(9))) then
          newarg=.true.
        else
          if(newarg) nargs=nargs+1
          newarg=.false.
        endif
      enddo

      end subroutine optionalarg

