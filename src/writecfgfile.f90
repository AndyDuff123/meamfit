subroutine writecfgfile

    !---------------------------------------------------------------------c
    !
    !     Writes DFT energies/forces/stresses and positions to a .cfg file
    !     as used in the MLIP code.
    !
    !     Called by:     program MEAMfit
    !     Calls:         -
    !     Arguments:     -
    !     Returns:       -
    !     Files read:    -
    !     Files written: -
    !
    !     Andy Duff, Feb 2023.
    !
    !     Copyright (c) 2023, STFC
    !
    !---------------------------------------------------------------------c

    use m_datapoints
    use m_optimization
    use m_geometry
    use m_filenames
    use m_atomproperties

    implicit none

    logical extraOutput,fileExists
    integer idatapoint,idatapointEn,idatapointFr,idatapointSt,idatapointdummy,i,j,previous,iRef,nRef,num_atoms,iat
    integer ref_datapoint(9),ref_strucnum(9)
    real(8) intEn_fit,intEn_true

    !See 'objective function' for description of following variables
    ref_datapoint(1)=1
    ref_strucnum(1)=1
    ref_datapoint(2:9)=0
    ref_strucnum(2:9)=0

    nRef=1     !Number of reference energies
    idatapoint=1
    previous=0

    print *,'Writing config file to: config.cfg'
    inquire(file='config.cfg', exist=fileExists)
    if (fileExists.eqv..true.) then
       print *,'ERROR: config.cfg file already exists, please delete or rename and then rerun MEAMfit, STOPPING.'
       stop
    endif
    open(2,file='config.cfg')
    do i=1,nstruct
        write(2,'(A)') 'BEGIN_CFG' !poss use e.g. write(outch,'(A1,A35,F25.15,A2,F25.15)') style for formatiing?
        write(2,'(A)') ' Size'
        if (computeForces(i)) then
           num_atoms=gn_forces(i)
        else
           num_atoms=gn_inequivalentsites(i)
        endif
        write(2,'(A3,I3)') '   ',num_atoms
        write(2,'(A)') ' Supercell'
        write(2,'(A3,3F14.6)') '   ',gtr(1,1,i), gtr(2,1,i), gtr(3,1,i)
        write(2,'(A3,3F14.6)') '   ',gtr(1,2,i), gtr(2,2,i), gtr(3,2,i)
        write(2,'(A3,3F14.6)') '   ',gtr(1,3,i), gtr(2,3,i), gtr(3,3,i)

        !In MEAMfit I use: energy; force; stress tensor ordering in the
        !fitdata and truedata arrays. config files instead write in the order:
        !forces, energy, stress tensor. Use following to set starting point for each
        !quantity:
        if (weightsEn(i).ne.0d0) then
           idatapointEn=idatapoint
        endif
        if (weightsFr(i).gt.0d0) then
           if (weightsEn(i).ne.0d0) then
              idatapointFr=idatapoint+1
           else
              idatapointFr=idatapoint
           endif
        endif
        if (weightsSt(i).gt.0d0) then
           if (weightsFr(i).gt.0d0) then
              idatapointSt=idatapointFr+3*num_atoms
           else
              if (weightsEn(i).ne.0d0) then
                 idatapointSt=idatapoint+1
              else
                 idatapointSt=idatapoint
              endif
           endif
        endif

        if (weightsFr(i).ne.0d0) then
            ! formatting notes: species label starts at 0 for config files
            idatapointdummy=idatapointFr
            write(2,'(A)') ' AtomData:  id type       cartes_x      cartes_y      cartes_z           fx          fy          fz'
            do iat=1,num_atoms
               !write(2, '(8X, I1, 4X, I1, 5X, F13.6, F13.6, F13.6, 1X, F12.6, F12.6, F12.6)') 1, 0, 0.000000, 0.000000, 0.000000, -0.000000, -0.000011, 0.000007
               write(2, '(11X, I3, 4X, I1, 1X, F14.6, F14.6, F14.6, 1X, F12.6, F12.6, F12.6)') iat, gspecies(iat,i)-1, gxyz(1,iat,i), gxyz(2,iat,i), gxyz(3,iat,i), truedata(idatapointdummy), truedata(idatapointdummy+1), truedata(idatapointdummy+2)
               idatapointdummy=idatapointdummy+3
            enddo
        else
            write(2,'(A)') ' AtomData:  id type       cartes_x      cartes_y      cartes_z'
            do iat=1,num_atoms
               write(2, '(8X, I1, 4X, I1, 5X, F13.6, F13.6, F13.6, 1X, F12.6, F12.6, F12.6)') iat, gspecies(iat,i)-1, gxyz(1,iat,i), gxyz(2,iat,i), gxyz(3,iat,i)
            enddo
        endif

        if (weightsEn(i).ne.0d0) then
           ! Need to convert energies back to total energy per cell (MEAMfit
           ! uses energy/atom internally (c.f. readtargetdata.f90: 'MEAMfit fits
           ! energies per atom')
           truedata(idatapointEn)=truedata(idatapointEn)*num_atoms
           write(2,'(A)') ' Energy'
           write(2, '(F24.12)') truedata(idatapointEn)
        endif

        if (weightsSt(i).gt.0d0) then
           ! Stresses read-in are actual stresses. What .cfg needs is stress*volume (ditto below for castep)
           write(2,'(A)') ' PlusStress:  xx          yy          zz          yz          xz          xy'
           write(2, '(4X,F12.5, F12.5, F12.5, F12.5, F12.5, F12.5)') truedata(idatapointSt)*g_vol(i), truedata(idatapointSt+1)*g_vol(i), truedata(idatapointSt+2)*g_vol(i), truedata(idatapointSt+5)*g_vol(i), truedata(idatapointSt+4)*g_vol(i), truedata(idatapointSt+3)*g_vol(i)
        endif

        if (DFTdata(i).eq.0) then 
           write(2,'(A)') ' Feature   EFS_by       VASP'
           !print *,'ERROR: config file writing for VASP data not yet supported, STOPPING'
           !stop
        elseif ((DFTdata(i).eq.1).or.(DFTdata(i).eq.3)) then
           write(2,'(A)') ' Feature   EFS_by       CASTEP'
        elseif (DFTdata(i).eq.2) then
           write(2,'(A)') ' Feature   EFS_by       QE'
           print *,'ERROR: config file writing for QE data not yet supported, STOPPING'
           stop
        elseif (DFTdata(i).eq.4) then
           write(2,'(A)') ' Feature   EFS_by       QE-PWscf'
           !print *,'ERROR: config file writing for QE data not yet supported, STOPPING'
           !stop
        endif
        write(2,'(A)') 'END_CFG'
        write(2,'(A)')

        if (weightsEn(i).ne.0d0) then
           idatapoint=idatapoint+1
        endif
        if (weightsFr(i).gt.0d0) then
           idatapoint=idatapoint+3*num_atoms
        endif
        if (weightsSt(i).gt.0d0) then
           idatapoint=idatapoint+6
        endif

    enddo


end subroutine writecfgfile
