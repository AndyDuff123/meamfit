subroutine writemeamp

    !---------------------------------------------------------------c
    !
    !     Writes MEAM parameters from the 'p' array into ouput
    !     channel 2 (should be defined w.r.t a filename prior to
    !     call)
    !
    !     Called by:     optimizeparameters
    !     Calls:         -
    !     Arguments:     maxspecies,lmax,workparameterfile,p
    !     Returns:       -
    !     Files read:    -
    !     Files written: workparameterfile
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization
    use m_meamparameters

    implicit none

    integer i,j,paracounter

    !First check that non of the values are NaN
    do i=1,2*m3+(5+lmax)*m1+34*m2+12*lm1*m2
        if ((p(i).le.0d0).or.(p(i).ge.0d0)) then
            !ok...
        else
            close(2)
            open(2,file='FaultyPotPAras')
            print *,'NaN detected -not going to write', &
                'work_meam_parameters',p(i)
            !stop
        endif
    enddo

    write(2,*) lmax,' # lmax'
    write(2,*) p(1:m3),' # cmin' !cmin(maxspecies,maxspecies,maxspecies)
    write(2,*) p(1+m3:2*m3),' # cmax'    !cmax(maxspecies,maxspecies,maxspecies)
    write(2,*) envdepmeamt,' # environ dep meamts? ..followed by meamtaus:'
    write(2,*) p(2*m3+1:2*m3+lmax*m1) !meamtau(1:lmax,maxspecies)
    write(2,*) thiaccptindepndt,' # acceptor independant thi?'
    paracounter=2*m3+lmax*m1
    do i=1,maxspecies
        do j=1,maxspecies
            if ((i.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                write(2,*) typethi(i,j),' #type of thi, followed by thi spec:'
                write(2,*) p(paracounter+1:paracounter+12*lm1)
            endif
            paracounter=paracounter+12*lm1
        enddo
    enddo
    write(2,*) embfunctype,' # embedding function type, followed by spec:'
    write(2,*) p(paracounter+1:paracounter+m1) !meame0(maxspecies)
    write(2,*) p(paracounter+m1+1:paracounter+2*m1)
    !meamrho0(maxspecies)
    write(2,*) p(paracounter+2*m1+1:paracounter+3*m1) !meamemb3
    write(2,*) p(paracounter+3*m1+1:paracounter+4*m1) !meamemb4
    paracounter=paracounter+4*m1
    do i=1,maxspecies
        do j=1,maxspecies
            if (j.ge.i) then
                write(2,*) typepairpot(i,j),' # pair-potential type, followed',&
                               'by spec:'
                write(2,*) p(paracounter+1:paracounter+32)
            endif
            paracounter=paracounter+32
        enddo
    enddo
    write(2,*) p(paracounter+1:paracounter+m2),' # rs' !rs(maxspecies,
    !maxspecies)
    write(2,*) p(paracounter+m2+1:paracounter+2*m2),' # rc' !rc(maxspecies,
    !maxspecies)
    write(2,*) p(paracounter+2*m2+1:paracounter+2*m2+m1),' enconst (fixed offset per atom)' !enconst
    !(maxspecies)

    if (paracounter+2*m2+m1.ne.m4+2*m2+m1) then
        print *,'Error in writemeamp subroutine regarding array limits'
        print *,'number of parameters in p=',paracounter+2*m2+m1, &
            ' not equal ',m4+2*m2+m1
        stop
    endif

end subroutine writemeamp
