subroutine generaterandomstartparas

    !--------------------------------------------------------------c
    !
    !     Randomly initializes the MEAM parameters according to the
    !     limits previously determined.
    !
    !     Called by: MEAMfit
    !     Calls: RANDOM_NUMBER
    !     Returns: cmin, cmax, meamtau, meamrhodecay, meame0,
    !         meamrho0, meamemb3, meamemb4, pairpotparameter,
    !         enconst
    !     Files read: -
    !     Files written: -
    !
    !     Andy Duff, Feb 2013.
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization
    use m_geometry

    implicit none

    !logical sorted
    !integer iseed
    integer i,j,ii,ll,isp,jsp,spc_cnt
    real(8) tmp,maxradius_current,randChng

    !Variables just used for reading in start_meam_parameters file:
    integer nfreepThi

    !---- cmin and cmax ----
    j=1
    isp=1
    jsp=1
    do i=1,m3
        if (freep(i).ge.2) then
            if (freep(i+m3).lt.2) then
                do
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    cmin(j,isp,jsp)=5d0*tmp
                    if (cmin(j,isp,jsp).lt.cmax(j,isp,jsp)) then
                        exit
                    endif
                enddo
            else
                !tmp=ran(iseed)
                call RANDOM_NUMBER(tmp)
                cmin(j,isp,jsp)=5d0*tmp
            endif
        endif
        if (j.lt.m1) then
            j=j+1
        else
            if (isp.lt.m1) then
                j=1
                isp=isp+1
            else
                j=1
                isp=1
                jsp=jsp+1
            endif
        endif
    enddo
    j=1
    isp=1
    jsp=1
    do i=m3+1,2*m3
        if (freep(i).ge.2) then
            do
                !tmp=ran(iseed)
                call RANDOM_NUMBER(tmp)
                cmax(j,isp,jsp)=5d0*tmp
                if (cmin(j,isp,jsp).lt.cmax(j,isp,jsp)) then
                    exit
                endif
            enddo
        endif
        if (j.lt.m1) then
            j=j+1
        else
            if (isp.lt.m1) then
                j=1
                isp=isp+1
            else
                j=1
                isp=1
                jsp=jsp+1
            endif
        endif
    enddo
    !----------------------

    !---- meamtau ----
    if (lmax.gt.0) then
       do ll=1,lmax
           do isp=1,maxspecies
               !Check if we need to generate random paramaters for
               !meamtau(ll,isp)
               if (freep(2*m3+isp+(ll-1)*m1).ge.2) then
                   !tmp=ran(iseed)
                   call RANDOM_NUMBER(tmp)
                   !print *,'rand nmr=',tmp
                   !Original logarithmic based meamtau generation:
                   meamtau(ll,isp)=10d0**(meamtau_minorder(ll,isp)+tmp* &
                       (meamtau_maxorder(ll,isp)-meamtau_minorder(ll,isp)))
                   !Trialing a former that encourages higher numbers (for the
                   !SiC work)
                   meamtau(ll,isp)=tmp*( 10d0**(meamtau_maxorder(ll,isp)) )

                   !tmp=ran(iseed)
                   call RANDOM_NUMBER(tmp)
                   !print *,'rand nmr=',tmp
                   if (tmp.gt.0.5d0) then
                       meamtau(ll,isp)=-meamtau(ll,isp)
                   endif
                   !print *,'meamtau(',isp,',',ll,')=',meamtau(ll,isp)
               endif
           enddo
       enddo
    endif
    !-----------------

    !---- electron density functions ----
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    !Check if we need to generate random paramaters for
                    !thi(isp,jsp,ll)
                    nfreepThi=0
                    do i=1,12
                        if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i).ge.2) then
                            nfreepThi=nfreepThi+1
                        endif
                    enddo
                    if (nfreepThi.gt.0) then
                        if (typethi(isp,jsp).eq.1) then !Cubic form

                            !Generate randomly generated parameters.
                            !Define an overall maximum, maxradius_current, for which
                            !all cut-off radii must be less than (avoids
                            !thi(l,isp,jsp) preferentially initializing with a large
                            !radial extent.
                            !tmp=ran(iseed)       !meamrhodecay_minradius(ll,isp,jsp)
                            call RANDOM_NUMBER(tmp)
                            !print *,'rand nmr=',tmp
                            maxradius_current=meamrhodecay_minradius(ll,isp,jsp)+ &
                                0.1d0*(meamrhodecay_maxradius(ll,isp,jsp)- &
                                       meamrhodecay_minradius(ll,isp,jsp)) + &
                                      (meamrhodecay_maxradius(ll,isp,jsp)- &
                                       (meamrhodecay_minradius(ll,isp,jsp)+ &
                                        0.1d0*(meamrhodecay_maxradius(ll,isp,jsp)- &
                                        meamrhodecay_minradius(ll,isp,jsp))))*tmp
                            do i=1,6
                                if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i).ge.2) &
                                    then
                                    !tmp=ran(iseed)
                                    call RANDOM_NUMBER(tmp)
                                    !print *,'rand nmr=',tmp
                                    meamrhodecay(2*i,ll,isp,jsp)= &
                                       meamrhodecay_minradius(ll,isp,jsp)+ &
                                  !     (maxradius_current- &
                                        (meamrhodecay_maxradius(ll,isp,jsp)- &
                                        meamrhodecay_minradius(ll,isp,jsp))*tmp
                                    !print *,'meamrhodecay(',2*i,',',ll,',',isp,',',jsp,')=',meamrhodecay(2*i,ll,isp,jsp)
                                endif
                                !Put -1d0 placeholders into the coefficient slots that
                                !need to be randomly generated, then after re-ordering
                                !we know which coefficients to generate.
                                if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i-1).ge.2) &
                                    then
                                    meamrhodecay(2*i-1,ll,isp,jsp)=-1d0
                                endif
                            enddo
                          ! print *,'random cutoffs for elec_Zr(',isp,',',jsp,'):',meamrhodecay(2,0,isp,jsp),meamrhodecay(4,0,isp,jsp)
                          ! stop

                            !Generate random coefficients
                            ii=1
                            do i=1,6
                                if (meamrhodecay(2*i-1,ll,isp,jsp).eq.-1d0) then
                                    !tmp=ran(iseed)
                                    call RANDOM_NUMBER(tmp)
                                    !print *,'rand nmr=',tmp
                                    meamrhodecay(2*i-1,ll,isp,jsp)= &
                                        10d0**(meamrhodecay_minorder(ii,ll,isp,jsp)+ &
                                               tmp*(meamrhodecay_maxorder(ii,ll,isp,jsp)- &
                                                    (meamrhodecay_minorder(ii,ll,isp,jsp))))
                                    if (meamrhodecay_negvals(ll,isp,jsp).eq.2) then
                                        !tmp=ran(iseed)
                                        call RANDOM_NUMBER(tmp)
                                        if (tmp.gt.0.5d0) then
                                            meamrhodecay(2*i-1,ll,isp,jsp)= &
                                                -meamrhodecay(2*i-1,ll,isp,jsp)
                                        endif
                                    endif
                                    !print *,'meamrhodecay(',2*i-1,',',ll,',',isp,',',jsp,')=',meamrhodecay(2*i-1,ll,isp,jsp)
                                    ii=ii+1
                                endif
                            enddo
                        else
                            print *,'     typethi(',isp,',',jsp,')=', &
                                typethi(isp,jsp),'not supported, stopping'
                            stop
                        endif
                    endif
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    !------------------------------------


    !---- embedding functions ----
    do isp=1,maxspecies
        do i=1,4
            !print *,'For the embedding functions, freep(',2*m3+lm1*m1+12*lm1*m2+isp+(i-1)*m1,', which corresponds to isp=',isp,' and i=',i,' has the value:',freep(2*m3+lm1*m1+12*lm1*m2+isp+(i-1)*m1),' (with embfuncRandType=',embfuncRandType,')'
            if (freep(2*m3+lmax*m1+12*lm1*m2+isp+(i-1)*m1).ge.2) then
               if (embfuncRandType.eq.1) then
                if (i.eq.1) then
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    !print *,'rand nmr=',tmp
                    meame0(isp)=tmp*(meame0_maxorder(isp)- &
                        meame0_minorder(isp))+meame0_minorder(isp)
                    !print *,'meame0(',isp,')=',meame0(isp)
                elseif (i.eq.2) then
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    !print *,'rand nmr=',tmp
                    meamrho0(isp)=tmp*(meamrho0_maxorder(isp)- &
                        meamrho0_minorder(isp))+meamrho0_minorder(isp)
                    !print *,'meamrho0(',isp,')=',meamrho0(isp)
                elseif (i.eq.3) then
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    !print *,'rand nmr=',tmp
                    meamemb3(isp)=tmp*(meamemb3_maxorder(isp)- &
                        meamemb3_minorder(isp))+meamemb3_minorder(isp)
                    !print *,'meamemb3(',isp,')=',meamemb3(isp)
                elseif (i.eq.4) then
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    meamemb4(isp)=tmp*(meamemb4_maxorder(isp)- &
                        meamemb4_minorder(isp))+meamemb4_minorder(isp)
                endif
               elseif (embfuncRandType.eq.2) then
                if (i.eq.1) then
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    !print *,'rand nmr=',tmp
                    meame0(isp)=10d0**( tmp*(meame0_maxorder(isp)- &
                        meame0_minorder(isp))+meame0_minorder(isp) )
                    !print *,'meame0(',isp,')=',meame0(isp)
                elseif (i.eq.2) then
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    !print *,'rand nmr=',tmp
                    meamrho0(isp)=10d0**( tmp*(meamrho0_maxorder(isp)- &
                        meamrho0_minorder(isp))+meamrho0_minorder(isp) )
                    !print *,'meamerho0(',isp,')=',meame0(isp)
                elseif (i.eq.3) then
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    !print *,'rand nmr=',tmp
                    meamemb3(isp)=10d0**( tmp*(meamemb3_maxorder(isp)- &
                        meamemb3_minorder(isp))+meamemb3_minorder(isp) )
                    !print *,'meamemb3(',isp,')=',meamemb3(isp)
                elseif (i.eq.4) then
                    !tmp=ran(iseed)
                    call RANDOM_NUMBER(tmp)
                    meamemb4(isp)=10d0**( tmp*(meamemb4_maxorder(isp)- &
                        meamemb4_minorder(isp))+meamemb4_minorder(isp) )
                endif
               else
                print *,'ERROR: method for randomly generating embfuncs not supported (embfuncRandType=',embfuncRandType,')'
               endif
            endif
        enddo
    enddo
    !----------------------------

    !---- pair-potentials ----
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                if (nfreeppairpot(isp,jsp).gt.0) then
                    if (typepairpot(isp,jsp).eq.2) then
                        !Generate randomly generated parameters.
                        !Define an overall maximum, maxradius_current, for which
                        !all cut-off radii must be less than (avoids
                        !thi(l,isp,jsp) preferentially initializing with a
                        !large radial extent.
                        !tmp=ran(iseed)
                        call RANDOM_NUMBER(tmp)
                        !print *,'rand nmr=',tmp
                        maxradius_current=pairpotparameter_minradius(isp,jsp)+ &
                            0.1d0*(pairpotparameter_maxradius(isp,jsp)- &
                                   pairpotparameter_minradius(isp,jsp)) + &
                            (pairpotparameter_maxradius(isp,jsp)- &
                             (pairpotparameter_minradius(isp,jsp)+0.1d0* &
                              (pairpotparameter_maxradius(isp,jsp)- &
                               pairpotparameter_minradius(isp,jsp)) ) )*tmp
                        do i=1,16
                            if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i+32*spc_cnt) &
                                .ge.2) then
                                !tmp=ran(iseed)
                                call RANDOM_NUMBER(tmp)
                                !print *,'rand nmr=',tmp
                                pairpotparameter(2*i,isp,jsp)= &
                                    pairpotparameter_minradius(isp,jsp)+ &
                            !        (maxradius_current-pairpotparameter_minradius(isp,jsp))*tmp
                                    (pairpotparameter_maxradius(isp,jsp)-pairpotparameter_minradius(isp,jsp))*tmp
                            ! print *,'random cutoffs for pairpot cutoff(',2*i,',',isp,',',jsp,'):',pairpotparameter(2*i,isp,jsp)
                                !print *,'pairpotparameter(',2*i,',',isp,',',jsp,')=',pairpotparameter(2*i,isp,jsp)
                            endif

                            !Put -1d0 placeholders into the coefficient slots that
                            !need
                            !to be randomly generated. This is so that, after
                            !re-ordering, we know which coefficients to generate.
                            if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i-1+32*spc_cnt) &
                                .ge.2) then
                                pairpotparameter(2*i-1,isp,jsp)=-1d0
                            endif
                        enddo
                        !Sort the terms in order of increasing cut-off radii
                !       do
                !           sorted=.true.
                !           do i=2,30,2
                !               if ((pairpotparameter(i+2,isp,jsp).lt. &
                !                   pairpotparameter(i,isp,jsp)).and. &
                !                   (pairpotparameter(i+2,isp,jsp).ne.0d0)) then
                !                   tmp=pairpotparameter(i-1,isp,jsp)
                !                   tmp2=pairpotparameter(i,isp,jsp)
                !                   pairpotparameter(i-1,isp,jsp)= &
                !                       pairpotparameter(i+1,isp,jsp)
                !                   pairpotparameter(i,isp,jsp)= &
                !                       pairpotparameter(i+2,isp,jsp)
                !                   pairpotparameter(i+1,isp,jsp)=tmp
                !                   pairpotparameter(i+2,isp,jsp)=tmp2
                !                   sorted=.false.
                !               endif
                !           enddo
                !           if (sorted.eqv..true.) then
                !               exit
                !           endif
                !       enddo
                        !Generate random coefficients
                        ii=1
                        do i=1,16
                            if (pairpotparameter(2*i-1,isp,jsp).eq.-1d0) then
                                !tmp=ran(iseed)
                                call RANDOM_NUMBER(tmp)
                                !print *,'rand nmr=',tmp
                                pairpotparameter(2*i-1,isp,jsp)= &
                                    10d0**(pairpotparameter_minorder(ii,isp,jsp)+ &
                                    tmp*(pairpotparameter_maxorder(ii,isp,jsp)- &
                                    (pairpotparameter_minorder(ii,isp,jsp))))
                                if (pairpotparameter_negvals(isp,jsp).eq.2) then
                                    !tmp=ran(iseed)
                                    call RANDOM_NUMBER(tmp)
                                    !print *,'rand nmr=',tmp
                                    if (tmp.gt.0.5d0) then
                                        pairpotparameter(2*i-1,isp,jsp)= &
                                            -pairpotparameter(2*i-1,isp,jsp)
                                    endif
                                endif
                                !print *,'pairpotparameter(',2*i-1,',',isp,',',jsp,')=',pairpotparameter(2*i-1,isp,jsp)
                                ii=ii+1
                            endif
                        enddo
                    else
                        print *,'typepairpot=',typepairpot(isp,jsp), &
                            'not supported,stopping'
                        stop
                    endif
                endif
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    !--------------------------------------------------------
!    stop

    !---- enconst ----
    do isp=1,maxspecies
        if (freep(m4+2*m2+isp).ge.2) then
            !tmp=ran(iseed)
            call RANDOM_NUMBER(tmp)
            !print *,'rand nmr=',tmp
            enconst(isp)=10d0**( tmp*(enconst_maxorder(isp)-enconst_minorder(isp)) + &
                               enconst_minorder(isp) )
            !tmp=ran(iseed)
            call RANDOM_NUMBER(tmp)
            !print *,'rand nmr=',tmp
            if (tmp.gt.0.5d0) then
                enconst(isp)=-enconst(isp)
            endif
            !print *,'enconst(',isp,')=',enconst(isp)
        endif
    enddo
    !------------------

    if (holdcutoffs) then

       !assign cutoffs (calculated earlier in 'findsmallestsepn'), with random
       !kicks; with magntiude governed by:
       fracCutoffChng=0.1

       !---- electron density functions ----
       spc_cnt=0
       do isp=1,maxspecies
           do jsp=1,maxspecies
               if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                   do ll=0,lmax
                       if (ncutoffDens(isp,jsp).gt.1) then
                          do i=1,ncutoffDens(isp,jsp)-1
                              call RANDOM_NUMBER(tmp)
                              randChng=(tmp-0.5d0)*fracCutoffChng*(lrgsepn_overall-smlsepn_overall)/ncutoffDens(1,isp)
                              meamrhodecay(2*i,ll,isp,jsp)=MIN(lrgsepn_overall,cutoffDens(i,isp,jsp)+randChng)
                          enddo
                       endif
                       meamrhodecay(2*ncutoffDens(isp,jsp),ll,isp,jsp)=cutoffDens(ncutoffDens(isp,jsp),isp,jsp)
                   enddo
               endif
               spc_cnt=spc_cnt+1
           enddo
       enddo
       !------------------------------------


       !---- pair-potentials ----
       spc_cnt=0
       do isp=1,maxspecies
           do jsp=1,maxspecies
               if (jsp.ge.isp) then
                   if (nfreeppairpot(isp,jsp).gt.0) then
                       if (typepairpot(isp,jsp).eq.2) then
                           if (ncutoffPairpot(isp,jsp).gt.1) then
                              do i=1,ncutoffPairpot(isp,jsp)-1
                                  call RANDOM_NUMBER(tmp)
                                  randChng=(tmp-0.5d0)*fracCutoffChng*(lrgsepn_overall-smlsepn_overall)/ncutoffDens(1,isp)
                                  pairpotparameter(2*i,isp,jsp)=MIN(lrgsepn_overall,cutoffPairpot(i,isp,jsp)+randChng)
                              enddo
                           endif
                           pairpotparameter(2*ncutoffPairpot(isp,jsp),isp,jsp)=cutoffPairpot(ncutoffPairpot(isp,jsp),isp,jsp)
                       endif
                   endif
               endif
               spc_cnt=spc_cnt+1
           enddo
       enddo
       !--------------------------------------------------------

    endif

end subroutine generaterandomstartparas
