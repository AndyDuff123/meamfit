subroutine setupdefaultlmax

    !--------------------------------------------------------------c
    !
    !     Use values of 'maxspecies' and 'lmax' to set up the    
    !     variables to be used to reference the potential parameter
    !     array, p().
    !
    !     Called by:     program MEAMfit
    !     Calls:         
    !     Arguments:     
    !     Returns:       
    !     Files read:    
    !     Files written: -
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters

    implicit none

    integer, parameter :: lmaxDefault = 0 ! Number of angular momentum terms to set
                                          ! up for in the electron density terms

    lmax=lmaxDefault

end subroutine setupdefaultlmax
