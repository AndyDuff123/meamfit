subroutine map_icoords_to_its(it1,it2,it3,iface,ishell, &
        icoord1,icoord2)

    !---------------------------------------------------------------c
    !
    !     Takes the variables 'icoord1' and 'icoord2' (which are
    !     varied as:     do icoord1=-ishell,ishell
    !                       do icoord2=-ishell,ishell
    !     in the subroutine 'calculateshells') and convert them
    !     into coordinates, (it1,it2,it3), on the face of a cube.
    !     The cube is of dimension: (2 * ishell) and is centred on
    !     the origin. The value of iface denotes which face we want
    !     the coordinates to lie on.
    !
    !     Called by:     calculateshells
    !     Calls:         -
    !     Arguments:     icoord1,icoord2,iface,ishell
    !     Returns:       it1,it2,it3
    !     Files read:    -
    !     Files written: -
    !
    !     Andy Duff, Dec 2007
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    implicit none

    integer iface,it1,it2,it3,icoord1,icoord2,ishell

    if (iface.eq.1) then      !Map icoord1 and icoord2 onto
        it1=icoord1            !direct coordinates. iface=1 is the
        it2=icoord2            !bottom face, 2 is the top face, 3
        it3=-ishell            !the left face, 4 the right face,
    elseif (iface.eq.2) then  !5 the back face and 6 is the
        it1=icoord1            !front face.
        it2=icoord2
        it3=ishell
    elseif (iface.eq.3) then
        it1=icoord1
        it2=-ishell
        it3=icoord2
    elseif (iface.eq.4) then
        it1=icoord1
        it2=ishell
        it3=icoord2
    elseif (iface.eq.5) then
        it1=-ishell
        it2=icoord1
        it3=icoord2
    elseif (iface.eq.6) then
        it1=ishell
        it2=icoord1
        it3=icoord2
    endif

end subroutine map_icoords_to_its
