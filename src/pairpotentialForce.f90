

    subroutine pairpotentialForce

    !------------------------------------------------------------------c
    !
    !     Calculates the pair-potential contribution to the total
    !     energy and its derivatives w.r.t coordinates and potential
    !     parameters, by repeated calls to 'radpairpotForce'. Note that
    !     to obtain forces we need to consider the expanded supercell
    !     (gn_inequivalentsites(istr) no. of atoms), since each atomic
    !     force requires the spatial derivatives of the energy of all
    !     of its neighboring atoms. For energies and derivatives of
    !     energies w.r.t potential parameters we just need the reduced
    !     cell (gn_forces(istr) atoms).
    !
    !     Called by:     meamforce
    !     Calls:         -
    !     Arguments:     
    !     Returns:       
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_atomproperties
    use m_meamparameters
    use m_geometry
    use m_observables

    implicit none
    integer iat,isp,jj,j,jsp,alph,ip
    real(8) distComp(3),pairpot

    if (allocated(meam_paire)) deallocate(meam_paire)
    if (allocated(dmeam_paire_dxyz)) deallocate(dmeam_paire_dxyz)
    if (allocated(d2meam_paire_dxyz_dpara)) deallocate(d2meam_paire_dxyz_dpara)
    allocate( meam_paire(gn_inequivalentsites(istr)), &
              dmeam_paire_dxyz(1:3,0:maxneighbors,gn_inequivalentsites(istr)), &
              d2meam_paire_dxyz_dpara(1:3,32,maxspecies,maxspecies,0:maxneighbors,gn_inequivalentsites(istr)) )

    meam_paire=0d0
    summeam_paire=0d0
    dsummeam_paire_dpara=0d0
    dmeam_paire_dxyz=0d0
    d2meam_paire_dxyz_dpara=0d0

    !First compute quantities for the original cell (I.e. prior to the expansion for force calculation purposes), 
    !this will provide meam_paire, summeam_paire and dmeam_paire_dpara.
    do iat=1,gn_forces(istr)

       isp=gspecies(iat,istr)
       do jj=1,gn_neighbors(iat,istr)
           j=gneighborlist(jj,iat,istr)
           jsp=gspecies(j,istr)
           !Change the dx, dy etc later to come from one array with 1:3 index
           distComp(1)=dxstr(jj,iat,0,0,istr)
           distComp(2)=dystr(jj,iat,0,0,istr)
           distComp(3)=dzstr(jj,iat,0,0,istr)
           call radpairpotForce(isp,jsp,diststr(jj,iat,0,0,istr), &
              distComp,pairpot,dsummeam_paire_dpara,dmeam_paire_dxyz(1:3,jj,iat), &
              d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat) )
           meam_paire(iat)=meam_paire(iat)+pairpot
           dmeam_paire_dxyz(1:3,0,iat)=dmeam_paire_dxyz(1:3,0,iat) - &
              dmeam_paire_dxyz(1:3,jj,iat)
           d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,0,iat) = &
              d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,0,iat) - &
              d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat)
       enddo
       meam_paire(iat)=0.5d0*meam_paire(iat)
       summeam_paire=summeam_paire+meam_paire(iat)
       dmeam_paire_dxyz(1:3,0:gn_neighbors(iat,istr),iat)= 0.5d0* &
               dmeam_paire_dxyz(1:3,0:gn_neighbors(iat,istr),iat)
       d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,0:gn_neighbors(iat,istr),iat)= 0.5d0* &
               d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,0:gn_neighbors(iat,istr),iat)

    enddo
    dsummeam_paire_dpara=0.5d0*dsummeam_paire_dpara

    !Loop over additional atoms (of the expanded cell) needed to compute atomic
    !force based quantities: dmeam_paire_dxyz, d2meam_paire_dxyz_dpara
    do iat=gn_forces(istr)+1,gn_inequivalentsites(istr)

       isp=gspecies(iat,istr)
       do jj=1,gn_neighbors(iat,istr)
           j=gneighborlist(jj,iat,istr)
           jsp=gspecies(j,istr)
           !Change the dx, dy etc later to come from one array with 1:3 index
           distComp(1)=dxstr(jj,iat,0,0,istr)
           distComp(2)=dystr(jj,iat,0,0,istr)
           distComp(3)=dzstr(jj,iat,0,0,istr)
           call radpairpotForce(isp,jsp,diststr(jj,iat,0,0,istr), &
              distComp,pairpot,dsummeam_paire_dpara_dummy,dmeam_paire_dxyz(1:3,jj,iat), &
              d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat) )
           meam_paire(iat)=meam_paire(iat)+pairpot
           dmeam_paire_dxyz(1:3,0,iat)=dmeam_paire_dxyz(1:3,0,iat) - &
              dmeam_paire_dxyz(1:3,jj,iat)
           d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,0,iat) = &
              d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,0,iat) - &
              d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat)
       enddo
       meam_paire(iat)=0.5d0*meam_paire(iat)
       dmeam_paire_dxyz(1:3,0:gn_neighbors(iat,istr),iat)= 0.5d0* &
               dmeam_paire_dxyz(1:3,0:gn_neighbors(iat,istr),iat)
       d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,0:gn_neighbors(iat,istr),iat)= 0.5d0* &
               d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,0:gn_neighbors(iat,istr),iat)

    enddo

    summeam_paire=summeam_paire/gn_forces(istr) ! careful, use gn_forces as this is the orignal nmr of atoms. gn_inequivalentsites(istr) is the 'expanded' cell
    dsummeam_paire_dpara=dsummeam_paire_dpara/gn_forces(istr)
    !print *,'gn_forces(',istr,')=',gn_forces(istr),' in pairpotentialForce'

    end subroutine pairpotentialForce


    subroutine pairpotentialForceSpline

    !------------------------------------------------------------------c
    !
    !     Calculates the pair-potential contribution to the total
    !     energy and its derivatives w.r.t coordinates and potential
    !     parameters, by repeated calls to 'radpairpotForce'. Note that
    !     to obtain forces we need to consider the expanded supercell
    !     (gn_inequivalentsites(istr) no. of atoms), since each atomic
    !     force requires the spatial derivatives of the energy of all
    !     of its neighboring atoms. For energies and derivatives of
    !     energies w.r.t potential parameters we just need the reduced
    !     cell (gn_forces(istr) atoms).
    !
    !     Called by:     meamforce
    !     Calls:         -
    !     Arguments:     
    !     Returns:       
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_atomproperties
    use m_meamparameters
    use m_geometry
    use m_observables

    implicit none
    integer iat,isp,jj,j,jsp,alph,ip
    real(8) distComp(3),pairpot

    if (allocated(meam_paire)) deallocate(meam_paire)
    if (allocated(dmeam_paire_dxyz)) deallocate(dmeam_paire_dxyz)
    if (allocated(d2meam_paire_dxyz_dpara)) deallocate(d2meam_paire_dxyz_dpara)
    allocate( meam_paire(gn_inequivalentsites(istr)), &
              dmeam_paire_dxyz(1:3,0:maxneighbors,gn_inequivalentsites(istr)), &
              d2meam_paire_dxyz_dpara(1:3,32,maxspecies,maxspecies,0:maxneighbors,gn_inequivalentsites(istr)))

    meam_paire=0d0
    summeam_paire=0d0
    dsummeam_paire_dpara=0d0
    dmeam_paire_dxyz=0d0
    d2meam_paire_dxyz_dpara=0d0

    !First compute quantities for the original cell (I.e. prior to the expansion
    !for force calculation purposes), 
    !this will provide meam_paire, summeam_paire and dmeam_paire_dpara.
    do iat=1,gn_forces(istr)

       isp=gspecies(iat,istr)
       do jj=1,gn_neighbors(iat,istr)
           j=gneighborlist(jj,iat,istr)
           jsp=gspecies(j,istr)
           !Change the dx, dy etc later to come from one array with 1:3 index
           distComp(1)=dxstr(jj,iat,0,0,istr)
           distComp(2)=dystr(jj,iat,0,0,istr)
           distComp(3)=dzstr(jj,iat,0,0,istr)
           if (useSpline2PartFns(min(isp,jsp),max(isp,jsp)).eqv..false.) then
              call radpairpotForce(isp,jsp,diststr(jj,iat,0,0,istr), &
                 distComp,pairpot,dsummeam_paire_dpara,dmeam_paire_dxyz(1:3,jj,iat), &
                 d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat) )
           else
              ! diststr(jj,iat,0,0,istr)=3.52352352352352d0
              ! print *,'For rij=',diststr(jj,iat,0,0,istr)
              ! print *,'Calling radpairpotForceSpline for interaction: ',min(isp,jsp),'-',max(isp,jsp)
              ! call radpairpotForce(isp,jsp,diststr(jj,iat,0,0,istr), &
              !    distComp,pairpot,dsummeam_paire_dpara,dmeam_paire_dxyz(1:3,jj,iat), &
              !    d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat) )
              ! print *,'analytic, pairpot=',pairpot,', dmeam_paire_dxyz(1:3,',jj,',',iat,')=',dmeam_paire_dxyz(1:3,jj,iat)
              call radpairpotForceSpline(isp,jsp,diststr(jj,iat,0,0,istr), &
                 distComp,pairpot,dsummeam_paire_dpara,dmeam_paire_dxyz(1:3,jj,iat), &
                 d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat) )
              ! print *,'numeric, pairpot=',pairpot,', dmeam_paire_dxyz(1:3,',jj,',',iat,')=',dmeam_paire_dxyz(1:3,jj,iat)

              ! stop
           endif

           meam_paire(iat)=meam_paire(iat)+pairpot
           dmeam_paire_dxyz(1:3,0,iat)=dmeam_paire_dxyz(1:3,0,iat) - &
              dmeam_paire_dxyz(1:3,jj,iat)
       enddo
       meam_paire(iat)=0.5d0*meam_paire(iat)
       summeam_paire=summeam_paire+meam_paire(iat)
       dmeam_paire_dxyz(1:3,0:gn_neighbors(iat,istr),iat)= 0.5d0* &
               dmeam_paire_dxyz(1:3,0:gn_neighbors(iat,istr),iat)

    enddo

    !Loop over additional atoms (of the expanded cell) needed to compute atomic
    !force based quantities: dmeam_paire_dxyz, d2meam_paire_dxyz_dpara
    do iat=gn_forces(istr)+1,gn_inequivalentsites(istr)

       isp=gspecies(iat,istr)
       do jj=1,gn_neighbors(iat,istr)
           j=gneighborlist(jj,iat,istr)
           jsp=gspecies(j,istr)
           !Change the dx, dy etc later to come from one array with 1:3 index
           distComp(1)=dxstr(jj,iat,0,0,istr)
           distComp(2)=dystr(jj,iat,0,0,istr)
           distComp(3)=dzstr(jj,iat,0,0,istr)
           if (useSpline2PartFns(min(isp,jsp),max(isp,jsp)).eqv..false.) then
              call radpairpotForce(isp,jsp,diststr(jj,iat,0,0,istr), &
                 distComp,pairpot,dsummeam_paire_dpara_dummy,dmeam_paire_dxyz(1:3,jj,iat), &
                 d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat) )
           else
              call radpairpotForceSpline(isp,jsp,diststr(jj,iat,0,0,istr), &
                 distComp,pairpot,dsummeam_paire_dpara,dmeam_paire_dxyz(1:3,jj,iat), &
                 d2meam_paire_dxyz_dpara(1:3,1:32,1:maxspecies,1:maxspecies,jj,iat))
           endif
           meam_paire(iat)=meam_paire(iat)+pairpot
           dmeam_paire_dxyz(1:3,0,iat)=dmeam_paire_dxyz(1:3,0,iat) - &
              dmeam_paire_dxyz(1:3,jj,iat)
       enddo
       meam_paire(iat)=0.5d0*meam_paire(iat)
       dmeam_paire_dxyz(1:3,0:gn_neighbors(iat,istr),iat)= 0.5d0* &
               dmeam_paire_dxyz(1:3,0:gn_neighbors(iat,istr),iat)

    enddo


    end subroutine pairpotentialForceSpline

