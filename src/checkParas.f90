subroutine checkPotType

    !----------------------------------------------------------------------c
    !
    !
    !
    !
    !     Called by: MEAMfit
    !     Calls: -
    !     Returns: cmin_cmax_zero, pairpotonly 
    !     Files read:   
    !     Files written:
    !
    !     Andrew Duff, 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_meamparameters
    use m_generalinfo
    use m_atomproperties
    use m_optimization
    use m_filenames

    implicit none

    logical cutoffsOk,firstoccurance
    integer i,j,k,isp,jsp,ll,spc_cnt

    !Check to see if all the cmin and cmax equal zero and if they are
    !not free parameters.
    cmin_cmax_zero=.true.
    if ((freep(1).eq.1).or.(freep(2).eq.1).or.(freep(1).eq.2).or.(freep(2).eq.2)) then
        cmin_cmax_zero=.false.
    endif
    do i=1,maxspecies
        do j=1,maxspecies
            do k=1,maxspecies
                if ((cmin(i,j,k).ne.0).and.(cmax(i,j,k).ne.0)) then
                    cmin_cmax_zero=.false.
                endif
            enddo
        enddo
    enddo
    if (cmin_cmax_zero.eqv..false.) then
        print *,'Baskes angular screening is on'
    endif

    !Check if we have only a pair-potential
    if ((cmin_cmax_zero.eqv..true.).and.(typethi(1,1).eq.0).and. &
        (typethi(1,2).eq.0).and.(typethi(2,2).eq.0).and. &
        (embfunctype.eq.0)) then
        pairpotonly=.true.
        print *,'Pair-potential only'
        print *,'WARNING: output file for LAMMPS needs to be adjusted: stopping)'
        stop
    endif

end subroutine checkPotType


subroutine checkParas

    !----------------------------------------------------------------------c
    !
    !     Check potential parameters and variables used to set up these
    !     parameters. Display information about potential to user.
    !
    !     Called by: MEAMfit
    !     Calls: -
    !     Returns: cmin_cmax_zero, pairpotonly 
    !     Files read:   
    !     Files written:
    !
    !     Andrew Duff, 2014
    !
    !----------------------------------------------------------------------c

    use m_meamparameters
    use m_generalinfo
    use m_atomproperties
    use m_optimization
    use m_filenames

    implicit none

    logical cutoffsOk,firstoccurance
    integer i,j,k,isp,jsp,ll,spc_cnt

    if (cutoffopt.eqv..true.) then
       print *,'Cut-off radii are being optimized, within range CUTOFF_MIN ', &
              '< cut-off radius < CUTOFF_MAX'
       print *
       print *,'Checking cut-off radii:'
       !Check that p_rmax is larger than all of the cut-offs which are to be
       !initialized from the start_meam_parameters file. Also, for those cut-offs 
       !which are to be randomly generated, check that p_rmax is larger than the 
       !corresponding upper limits for the random generation.
       cutoffsOk=.true.
       spc_cnt=0
       do isp=1,maxspecies
           do jsp=1,maxspecies
               if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                   do ll=0,lmax
                       if (randgenparas) then
                          !Check upper-limits for the cut-off random generation
                          !(those upper-limits not in use were set to zero in 
                          !initBoundsForRandParas, so will not wrongly trigger 
                          !the following if/then condition)
                          if (meamrhodecay_maxradius(ll,isp,jsp).gt.p_rmax) then
                              if (cutoffsOk.eqv..true.) then
                                 print *,'ERROR:'
                              endif
                              print *,'The upper-limit for random-generation of the electron density cut-off'
                              write(*,'(A17,I1,A7,I1,A4,I1,A24)') ' radii (for isp=',isp,', jsp=',jsp, &
                                         ', l=',ll,') is larger than p_rmax.'
                              cutoffsOk=.false.
                          endif
                       endif
                       !Check cut-offs which are not to be randomly generated
                       do i=2,12,2
                           if ((freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i) &
                                .lt.2).and.(meamrhodecay(i,ll,isp,jsp) &
                                .gt.p_rmax)) then
                               if (cutoffsOk.eqv..true.) then
                                  print *,'ERROR:'
                               endif
                               write(*,'(A46,A10,I1,A17)') ' The starting value of the cut-off radius for ', & 
                                    'parameter ',i,' of the electron-'
                               write(*,'(A18,I1,A7,I1,A4,I1,A24)') ' density (for isp=',isp, &
                                    ', jsp=',jsp,', l=',ll,') is larger than p_rmax.'
                               cutoffsOk=.false.
                           endif
                       enddo
                  enddo
              endif
              spc_cnt=spc_cnt+1
           enddo
        enddo

       spc_cnt=0
       do isp=1,maxspecies
           do jsp=1,maxspecies
               if (jsp.ge.isp) then
                   if (randgenparas) then
                      !Check that upper-limit for cut-off random generation is
                      !less than p_rmax
                      if (pairpotparameter_maxradius(isp,jsp).gt. &
                          p_rmax) then
                          if (cutoffsOk.eqv..true.) then
                             print *,'ERROR:'
                          endif
                          print *,'The upper-limit for random-generation of the pair-potential cut-off'
                          write(*,'(A17,I1,A7,I1,A24)') ' radii (for isp=',isp,', jsp=',jsp, &
                                     ') is larger than p_rmax.'
                          cutoffsOk=.false.
                      endif
                   endif
                   !Also check that those cut-offs which are not to be randomly
                   !generated are also less than p_rmax
                   do i=2,32,2
                       if ((freep(2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt) &
                           .lt.2).and.(pairpotparameter(i,isp,jsp).gt.p_rmax)) then
                           if (cutoffsOk.eqv..true.) then
                              print *,'ERROR:'
                           endif
                           write(*,'(A46,A10,I1,A13)') ' The starting value of the cut-off radius for ', &
                                'parameter ',i,' of the pair-'
                           write(*,'(A20,I1,A7,I1,A24)') ' potential (for isp=',isp, &
                                ', jsp=',jsp,') is larger than p_rmax.'
                           cutoffsOk=.false.
                       endif
                   enddo
               endif
               spc_cnt=spc_cnt+1
           enddo
        enddo

        if (cutoffsOk.eqv..false.) then
           print *
           print *,'Please increase p_rmax or decrease the values of the cutoffs'
           print *,'(or their upper limits) and run again.'
           print *
           print *,'STOPPING.'
           stop
        else
           print *,'...all good'
        endif

    endif
    !--------------------------------------------------------------------


    !---- Check if any finite value potential-parameters have not been set to optimize ----
    !If so: warn the user. Note that these warnings are less relevant than when
    !I originally wrote the code, and so are currently only produced when
    !verbosity>1
    if ((nsteps.gt.1).and.(verbosity.gt.1)) then

        firstoccurance=.true.

        !Check the coefficients of the electron-density
        do i=2*m3+lmax*m1+1,2*m3+lmax*m1+12*lm1*m2,2
           !If acceptor independant densities, then only read half this array
           if (i.le.2*m3+lmax*m1+12*lm1*m1) then
              if ((freep(i).eq.0).and.(p(i).ne.0d0).and.(p(i+1).ne.0d0)) then
                 if (firstoccurance) then
                    print *
                    print *,'WARNING:'
                    firstoccurance=.false.
                 endif
                 write(*,*) 'You have a coefficient/cutoff radius pair in your electron-density (parameter'
                 write(*,'(A1,I2,A5,I2,A53)') ' ',i,' and ',i+1,' in the start_meam_parameters file) which is non-zero'
                 write(*,*) '(',p(i),',',p(i+1),')'
                 write(*,'(A61)') 'and yet which is set to remain fixed during the optimization'
              endif
           endif
        enddo

        !Check the coefficients of the embedding function
        do i=2*m3+lmax*m1+12*lm1*m2+1,2*m3+4*m1+lmax*m1+12*lm1*m2
           if ((freep(i).eq.0).and.(p(i).ne.0d0)) then
               if (firstoccurance) then
                  print *
                  print *,'WARNING:'
                  firstoccurance=.false.
               endif
               write(*,'(A53,I3,A8)') ' You have an embedding function parameter (parameter ',i,') in the'
               write(*,'(A46)') ' start_meam_parameters file) which is non-zero'
               write(*,*) '(',p(i),')'
               write(*,'(A61)') 'and yet which is set to remain fixed during the optimization'
            endif
        enddo 

        !Check the coefficients of the pair-potential
        spc_cnt=0
        do isp=1,maxspecies
            do jsp=1,maxspecies
                if (jsp.ge.isp) then
                   do i=2*m3+(4+lmax)*m1+12*lm1*m2+1+spc_cnt*32,2*m3+(4+lmax)*m1+12*lm1*m2+32+spc_cnt*32,2
                      if ((freep(i).eq.0).and.(p(i).ne.0d0).and.(p(i+1).ne.0d0)) then
                         if (firstoccurance) then
                            print *
                            print *,'WARNING:'
                            firstoccurance=.false.
                         endif
                         write(*,*) 'You have a coefficient/cutoff radius pair in your pair-potential (parameter'
                         write(*,'(A1,I3,A5,I3,A53)') ' ',i,' and ',i+1,' in the start_meam_parameters file) which is non-zero'
                         write(*,*) '(',p(i),',',p(i+1),')'
                         write(*,'(A61)') 'and yet which is set to remain fixed during the optimization'
                      endif
                   enddo
                endif
                spc_cnt=spc_cnt+1
            enddo
        enddo

        if (firstoccurance.eqv..false.) then
           print *
           print *,'...is this what you intended?'
        endif

    endif
    !--------------------------------------------------------------------------------------

end subroutine checkParas
