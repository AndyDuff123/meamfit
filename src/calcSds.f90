subroutine calcSds

    !----------------------------------------------------------------------c
    !
    !     Calculate the standard-deviations of the input energies and
    !     forces for rescaling of the optimization function.
    !
    !     Called by:     program MEAMfit
    !     Returns:       
    !     Files read:    
    !     Files written: 
    !
    !     Andy Duff, Oct 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_geometry
    use m_datapoints
    use m_optimization
    use m_generalinfo
    use m_objectiveFunction

    implicit none

    integer iat,istruc,idatapoint

    !Calculate averages of energies and forces
    avgEn=0d0
    avgFrc=0d0
    avgStr=0d0
    nEn=0
    nFrcComp=0
    nStrComp=0
    idatapoint=1
    do istruc=1,nstruct
        if (weightsEn(istruc).ne.0d0) then
            avgEn=avgEn+truedata(idatapoint)
          !print *,'truedata(',idatapoint,')=',truedata(idatapoint)
            idatapoint=idatapoint+1
            nEn=nEn+1
        endif
        if (weightsFr(istruc).gt.0d0) then
            do iat=1,gn_forces(istruc)
               if (optforce(iat,istruc).eqv..true.) then
                  avgFrc = avgFrc + truedata(idatapoint) + truedata(idatapoint+1) + truedata(idatapoint+2)
                  nFrcComp=nFrcComp+3
               endif
               !truedata and fitdata contain zero entries for where forces are
               !not to be fit (so, there are 3*gn_forces(istr) entries to these
               !arrays, per structure, irrespective of whether all forces are
               !fit).
               idatapoint=idatapoint+3
            enddo
        endif
        if (weightsSt(istruc).gt.0d0) then
           avgStr = avgStr + truedata(idatapoint) + truedata(idatapoint+1) + truedata(idatapoint+2) + &
                    truedata(idatapoint+3) + truedata(idatapoint+4) + truedata(idatapoint+5)
           nStrComp=nStrComp+6
           idatapoint=idatapoint+6
        endif
    enddo
    if (nEn.gt.0) then
       avgEn=avgEn/nEn 
       !print *,'avgEn=',avgEn
    endif
    if (nFrcComp.gt.0) then
       avgFrc=avgFrc/nFrcComp
    endif
    if (nStrComp.gt.0) then
       avgStr=avgStr/nStrComp
    endif

    !Calculate variances of energies and forces
    idatapoint=1
    varEn=0d0
    varFrc=0d0
    varStr=0d0
    do istruc=1,nstruct
        if (weightsEn(istruc).ne.0d0) then
            varEn=varEn+(truedata(idatapoint)-avgEn)**2
            idatapoint=idatapoint+1
        endif
        if (weightsFr(istruc).gt.0d0) then
            do iat=1,gn_forces(istruc)
               if (optforce(iat,istruc).eqv..true.) then
                  varFrc=varFrc+(truedata(idatapoint)-avgFrc)**2 + &
                                (truedata(idatapoint+1)-avgFrc)**2 + &
                                (truedata(idatapoint+2)-avgFrc)**2
               endif
               idatapoint=idatapoint+3
            enddo
        endif
        if (weightsSt(istruc).gt.0d0) then
            varStr=varStr+(truedata(idatapoint)-avgStr)**2 + &
                          (truedata(idatapoint+1)-avgStr)**2 + &
                          (truedata(idatapoint+2)-avgStr)**2 + &
                          (truedata(idatapoint+3)-avgStr)**2 + &
                          (truedata(idatapoint+4)-avgStr)**2 + &
                          (truedata(idatapoint+5)-avgStr)**2
            idatapoint=idatapoint+6
        endif
    enddo
    if (nEn.gt.0) then
       varEn=varEn/nEn
    endif
    if (scaleObyVar.eqv..false.) then
       !We don't want to scale by the variance of the energies (for when we consider interaction energies)
       varEn=1d0
    endif
    if (nFrcComp.gt.0) then
       varFrc=varFrc/nFrcComp
       !print *,'varFrc before division=',varFrc,', nFrcComp=',nFrcComp
    endif
    if (scaleOFrcbyVar.eqv..false.) then
       !We don't want to scale by the variance of the forces (for when we consider only zero forces)
       varFrc=1d0
    endif
    if (nStrComp.gt.0) then
       varStr=varStr/nStrComp
       !print *,'varFrc before division=',varFrc,', nFrcComp=',nFrcComp
    endif

    !print *,'idatapoint=',idatapoint,', ndatapoints=',ndatapoints
    if (verbosity.gt.1) then
       write(*,*) '   Average value of energies, forces and stress tensor components:'
       write(*,*) '      ',avgEn,avgFrc,avgStr
       write(*,*) '   Variances of energies, forces and stress tensor components:'
       write(*,*) '      ',varEn,varFrc,varStr
    endif

end subroutine calcSds
