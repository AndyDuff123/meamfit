subroutine initialize_noptp

    !--------------------------------------------------------------c
    !
    !     Uses freep() to determine the variables noptp... (eg
    !     noptpairpot) to store how many variables in each part of
    !     the potential to optimize. These variables are used to
    !     set up the do loops used to calculate the analytic
    !     derivatives. If no analytic derivatives are being
    !     computed these should be set to zero (as below).
    !     Also determine ioptdensityCoeff, etc, for storing which 
    !     parameters are to be optimized.
    !
    !     Called by: MEAMfit 
    !     Calls:
    !     Arguments:
    !     Returns:
    !     Files read:
    !     Files written:
    !
    !     Andy Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_atomproperties
    use m_optimization
    use m_meamparameters

    implicit none

    integer i,isp,jsp,spc_cnt,ll,iiCoeff,iiCutoff,&
        max_noptpairpotCoeff,max_noptpairpotCutoff,&
        max_noptdensityCoeff,max_noptdensityCutoff

    allocate( optmeamtau(1:lmax,maxspecies) )
    allocate( noptpairpotCoeff(maxspecies,maxspecies), noptpairpotCutoff(maxspecies,maxspecies) )
    allocate( noptdensityCoeff(0:lmax,maxspecies,maxspecies), &
              noptdensityCutoff(0:lmax,maxspecies,maxspecies) )

    if (analyticDerivs.eqv..true.) then

       !---- meamtau ----

       !Record which meamtau parameters are to be optimized
       optmeamtau=.false.
       do ll=1,lmax
          do isp=1,maxspecies
             if (freep(2*m3+isp+(ll-1)*m1).ge.1) then
                optmeamtau(ll,isp)=.true.
             else
                optmeamtau(ll,isp)=.false.
             endif
          enddo
       enddo

       !----------------

       !Determine how many pair-potential parameters are to be optimized for each
       !isp, jsp
       spc_cnt=0
       do isp=1,maxspecies
           do jsp=1,maxspecies
               if (jsp.ge.isp) then !need this (rather than do jsp=isp,maxspecies) because potential 
                                    !files currently formatted with 0's for isp,jsp = 2,1, etc
                  noptpairpotCoeff(isp,jsp)=0
                  noptpairpotCutoff(isp,jsp)=0
                  do i=1,16
                      if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i-1+32*spc_cnt).ge.1) then
                          noptpairpotCoeff(isp,jsp)=noptpairpotCoeff(isp,jsp)+1
                      endif
                      if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i+32*spc_cnt).ge.1) then
                          noptpairpotCutoff(isp,jsp)=noptpairpotCutoff(isp,jsp)+1
                      endif
                  enddo
               endif
               spc_cnt=spc_cnt+1
           enddo
       enddo

       !Record which pair-potential parameters are to be optimized (n values)
       max_noptpairpotCoeff=0
       max_noptpairpotCutoff=0
       do isp=1,maxspecies
          do jsp=isp,maxspecies
             max_noptpairpotCoeff=MAX(noptpairpotCoeff(isp,jsp),max_noptpairpotCoeff)
             max_noptpairpotCutoff=MAX(noptpairpotCutoff(isp,jsp),max_noptpairpotCutoff)
          enddo
       enddo
       allocate( ioptpairpotCoeff(max_noptpairpotCoeff,maxspecies,maxspecies), &
                 ioptpairpotCutoff(max_noptpairpotCutoff,maxspecies,maxspecies) )
       spc_cnt=0
       do isp=1,maxspecies
           do jsp=1,maxspecies
               if (jsp.ge.isp) then
                  iiCoeff=1
                  iiCutoff=1
                  do i=1,16
                      if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i-1+32*spc_cnt).ge.1) then
                          ioptpairpotCoeff(iiCoeff,isp,jsp)=2*i-1
                          iiCoeff=iiCoeff+1
                      endif
                      if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i+32*spc_cnt).ge.1) then
                          ioptpairpotCutoff(iiCutoff,isp,jsp)=2*i
                          iiCutoff=iiCutoff+1
                      endif
                  enddo
               endif
               spc_cnt=spc_cnt+1
           enddo
       enddo
       ! do isp=1,maxspecies
       !     do jsp=isp,maxspecies
       !        print *,'noptpairpotCoeff(',isp,',',jsp,')=',noptpairpotCoeff(isp,jsp)
       !        if (noptpairpotCoeff(isp,jsp).gt.0) then
       !           do iiCoeff=1,noptpairpotCoeff(isp,jsp)
       !              print *,'ioptpairpotCoeff(',iiCoeff,',',isp,',',jsp,')=',ioptpairpotCoeff(iiCoeff,isp,jsp)
       !           enddo
       !        endif
       !        print *,'noptpairpotCutoff(',isp,',',jsp,')=',noptpairpotCutoff(isp,jsp)
       !        if (noptpairpotCutoff(isp,jsp).gt.0) then
       !           do iiCutoff=1,noptpairpotCutoff(isp,jsp)
       !              print *,'ioptpairpotCutoff(',iiCutoff,',',isp,',',jsp,')=',ioptpairpotCutoff(iiCutoff,isp,jsp)
       !           enddo
       !        endif
       !     enddo
       ! enddo

       !---- set up variables relevant to radial density functions ----
       spc_cnt=0
       do isp=1,maxspecies
           do jsp=1,maxspecies
               if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                   do ll=0,lmax
                       noptdensityCoeff(ll,isp,jsp)=0
                       noptdensityCutoff(ll,isp,jsp)=0
                       do i=1,6
                           if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i-1).ge.1) then
                               noptdensityCoeff(ll,isp,jsp)=noptdensityCoeff(ll,isp,jsp)+1
                           endif
                           if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i).ge.1) then
                               noptdensityCutoff(ll,isp,jsp)=noptdensityCutoff(ll,isp,jsp)+1
                           endif
                       enddo
                   enddo
               endif
               spc_cnt=spc_cnt+1
           enddo
       enddo

       !Record which density parameters are to be optimized (n values)
       max_noptdensityCoeff=0
       max_noptdensityCutoff=0
       do isp=1,maxspecies
          do jsp=isp,maxspecies
             if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                  max_noptdensityCoeff=MAX(noptdensityCoeff(ll,isp,jsp),max_noptdensityCoeff)
                  max_noptdensityCutoff=MAX(noptdensityCutoff(ll,isp,jsp),max_noptdensityCutoff)
                enddo
             endif
          enddo
       enddo
       allocate( ioptdensityCoeff(max_noptdensityCoeff,0:lmax,maxspecies,maxspecies), &
                 ioptdensityCutoff(max_noptdensityCutoff,0:lmax,maxspecies,maxspecies) )
       ! print *,'ioptdensity Coeff arrays initialized with size:',max_noptdensityCoeff,'0:',lmax,maxspecies,maxspecies
       ! print *,'ioptdensity Cutoff arrays initialized with size:',max_noptdensityCutoff,'0:',lmax,maxspecies,maxspecies

       spc_cnt=0
       do isp=1,maxspecies
           do jsp=1,maxspecies
               if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                   do ll=0,lmax
                      iiCoeff=1
                      iiCutoff=1
                       do i=1,6
                           if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i-1).ge.1) then
                              ioptdensityCoeff(iiCoeff,ll,isp,jsp)=2*i-1
                              iiCoeff=iiCoeff+1
                           endif
                           if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i).ge.1) then
                              ioptdensityCutoff(iiCutoff,ll,isp,jsp)=2*i
                              iiCutoff=iiCutoff+1
                           endif
                       enddo
                   enddo
               endif
               spc_cnt=spc_cnt+1
           enddo
       enddo

       ! do isp=1,maxspecies
       !     do jsp=1,maxspecies
       !        if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
       !           do ll=0,lmax
       !              print *,'isp=',isp,' jsp=',jsp,' ll=',ll
       !              if (noptdensityCoeff(ll,isp,jsp).gt.0) then
       !                 do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
       !                    print *,'iiCoeff=',iiCoeff,'/',noptdensityCoeff(ll,isp,jsp)
       !                    print *,'ioptdensityCoeff(',iiCoeff,',0:,',lmax,isp,',',jsp,')=',ioptdensityCoeff(iiCoeff,0:lmax,isp,jsp)
       !                 enddo
       !              endif
       !              if (noptdensityCutoff(ll,isp,jsp).gt.0) then
       !                 do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
       !                    print *,'iiCutoff=',iiCutoff,'/',noptdensityCutoff(ll,isp,jsp)
       !                    print *,'ioptdensityCutoff(',iiCutoff,',0:,',lmax,isp,',',jsp,')=',ioptdensityCutoff(iiCutoff,0:lmax,isp,jsp)
       !                 enddo
       !              endif
       !           enddo
       !        endif
       !     enddo
       ! enddo

     else

        optmeamtau=.false.
        noptpairpotCoeff=0
        noptpairpotCutoff=0
        noptdensityCoeff=0
        noptdensityCutoff=0

     endif

end subroutine initialize_noptp
