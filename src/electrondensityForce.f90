subroutine electrondensityForce

    !----------------------------------------------------------------c
    !
    !     Fills the module m_electrondensity with derivatives of 
    !     rhol's and meamt's w.r.t the atomic coordinates
    !     (drhol_dxyz, dmeam_t_dxyz)
    !
    !     Development notes: there are some ways I could increase the
    !     efficiency of the dmeamt_dmeamtau and d2meamt_dxyz_dmeamtau
    !     calcs, by using the fact that, summed over species, the 
    !     former sums to 1 and the second sums to 0. See MEAMfit
    !     hand written notes 22/05/2017.
    !
    !     Called by:     meamforce
    !     Calls:         
    !     Returns:       drhol_dxyz,dmeam_t_dxyz
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------c

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_screening       !S_ij
    use m_electrondensity !f_i_L_ij,rho_i_l
    use m_optimization
    use m_objectiveFunction

    implicit none

    integer i,j,jj,l,nni,isp,jsp,iatom,cart,ip,iiCoeff,iiCutoff,alph,alph2,alph3,alph4
    real(8) aux1(3),aux2(3,3),aux3(3,3,3),aux3a(3), &
        aux2a,aux,tmp1,tmp2,fjlijFact,dxstr2,dystr2,dzstr2, &
        dxstrdystr,dxstrdzstr,dystrdzstr,screen_over_rij,sum_auxSqr,sum_aux3aSqr, &
        screen_over_rij2,screen_over_rij3,screen_over_rij4, &
        screen_over_rij5,dmeamt_drhol0, &
        dalph(3),fjlijPrefact(3,3),dfjlij_dxyzPrefact(3), &
        fjlijPrefact2(3,3,3),dfjlij_dxyzPrefact2(3,3), &
        fjlijPrefact3(3,3,3,3),dfjlij_dxyzPrefact3(3,3,3)

    !  if(allocated(drhol_dxyz)) deallocate(drhol_dxyz,d2rhol_dxyz_dpara)
    !  if(allocated(rhol)) deallocate(rhol,drhol_dpara)
    !  allocate( rhol(0:lmax,gn_inequivalentsites(istr)), &
    !        drhol_dxyz(1:3,0:maxneighbors,0:lmax,gn_inequivalentsites(istr))  , &
    !        drhol_dpara(12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)), &
    !        d2rhol_dxyz_dpara(1:3,0:maxneighbors,12,0:lmax,1:maxspecies,gn_inequivalentsites(istr))  )

    rhol=0d0
    drhol_dpara=0d0
    drhol_dxyz=0d0
    d2rhol_dxyz_dpara=0d0


    ! ----------------------- rhol and derivatives for l = 0 -----------------------
    !Compute derivatives of rhol(0,iatom) w.r.t parameters and atomic coordinates
    do iatom=1,gn_inequivalentsites(istr)
       !print *,iatom,'/',gn_inequivalentsites(istr)
       isp=1
       if (thiaccptindepndt.eqv..false.) isp=gspecies(iatom,istr)

       !Compute spatial derivatives of rhol(l=0,iatom) w.r.t all atomic
       !coordinates (including its own)
       do jj=1,gn_neighbors(iatom,istr)

          j=gneighborlist(jj,iatom,istr)
          jsp=gspecies(j,istr)

          rhol(0,iatom)=rhol(0,iatom)+screening(jj,iatom)*fjlij(0,jj,iatom)

          !Derivatives of rhol(l=0,iatom) w.r.t potential parameters
          do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
             ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
             drhol_dpara(ip,0,jsp,iatom) = drhol_dpara(ip,0,jsp,iatom) + &
                  screening(jj,iatom)*dfjlij_dpara(ip,0,jj,iatom)
          enddo
          do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
             ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
             drhol_dpara(ip,0,jsp,iatom) = drhol_dpara(ip,0,jsp,iatom) + &
                  screening(jj,iatom)*dfjlij_dpara(ip,0,jj,iatom)
          enddo

          !Derivatives of rhol(l=0,iatom) w.r.t ionic coordinates
          ![the following will need modifying once the screening function
          !comes into effect (for now screening=1 in all cases)]
          do alph=1,3
             !Derivatives w.r.t on site atomic coordinates
             drhol_dxyz(alph,0,0,iatom) = drhol_dxyz(alph,0,0,iatom) + &
                  screening(jj,iatom)*dfjlij_dxyz(alph,0,jj,iatom)
             !Derivatives w.r.t off site atomic coordinates
             drhol_dxyz(alph,jj,0,iatom) = &
                  -screening(jj,iatom)*dfjlij_dxyz(alph,0,jj,iatom)
          enddo

          !Derivatives of drhol_dxyz w.r.t potential parameters
          do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
             ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
             do alph=1,3
                d2rhol_dxyz_dpara(alph,0,ip,0,jsp,iatom) = d2rhol_dxyz_dpara(alph,0,ip,0,jsp,iatom) + &
                     screening(jj,iatom)*d2fjlij_dxyz_dpara(alph,ip,0,jj,iatom)
                d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,iatom) = &
                     -screening(jj,iatom)*d2fjlij_dxyz_dpara(alph,ip,0,jj,iatom)

             enddo
          enddo
          do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
             ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
             do alph=1,3
                d2rhol_dxyz_dpara(alph,0,ip,0,jsp,iatom) = d2rhol_dxyz_dpara(alph,0,ip,0,jsp,iatom) + &
                     screening(jj,iatom)*d2fjlij_dxyz_dpara(alph,ip,0,jj,iatom)
                d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,iatom) = &
                     -screening(jj,iatom)*d2fjlij_dxyz_dpara(alph,ip,0,jj,iatom)
             enddo
          enddo

       enddo

       !print *,'rhol(0,',iatom,')=',rhol(0,iatom)
       if (rhol(0,iatom).lt.0d0) then
          !Since we have a negative density we have essentially reached a
          !dead-end in the optimization
          negBackDens=.true.
          return
       endif

    enddo
    ! ------------------------------------------------------------------------------


    if (lmax.ge.1) then

       ! ----------------- meam_t and derivatives (for all l ) ------------------------
       !  if(allocated(meam_t)) deallocate(meam_t)
       !  if(allocated(dmeamt_dmeamtau)) deallocate(dmeamt_dmeamtau)
       !  if(allocated(dmeamt_dpara)) deallocate(dmeamt_dpara)
       !  if(allocated(dmeamt_dxyz)) deallocate(dmeamt_dxyz)
       !  if(allocated(d2meamt_dxyz_dmeamtau)) deallocate(d2meamt_dxyz_dmeamtau)
       !  if(allocated(d2meamt_dxyz_dpara)) deallocate(d2meamt_dxyz_dpara)
       !  allocate( meam_t(1:lmax,gn_inequivalentsites(istr)), &
       !            dmeamt_dmeamtau(1:lmax,1:maxspecies,gn_inequivalentsites(istr)), &
       !            dmeamt_dpara(12,1:lmax,1:maxspecies,gn_inequivalentsites(istr)), &
       !            dmeamt_dxyz(3,0:maxneighbors,1:lmax,gn_inequivalentsites(istr)), &
       !            d2meamt_dxyz_dmeamtau(3,0:maxneighbors,1:lmax,1:maxspecies,gn_inequivalentsites(istr)), &
       !            d2meamt_dxyz_dpara(3,0:maxneighbors,1:lmax,12,1:maxspecies,gn_inequivalentsites(istr)) )
       meam_t=0d0
       dmeamt_dmeamtau=0d0
       dmeamt_dpara=0d0
       dmeamt_dxyz=0d0
       d2meamt_dxyz_dmeamtau=0d0
       d2meamt_dxyz_dpara=0d0

       !Calculate meam_t, and first and second order derivatives w.r.t meamtau,
       !para and xyz
       if (envdepmeamt.eqv..false.) then
          do l=1,lmax
             do iatom=1,gn_inequivalentsites(istr)
                !note, here we use the _actual_ species of i (not isp)
                meam_t(l,iatom)=meamtau(l,gspecies(iatom,istr))
                dmeamt_dmeamtau(l,1:maxspecies,iatom)=0d0
                dmeamt_dmeamtau(l,gspecies(iatom,istr),iatom)=1d0
             enddo
          enddo
       else
          do l=1,lmax
             !build meam_t (and derivatives) from surrounding contributions
             do iatom=1,gn_inequivalentsites(istr)
                !first meam_t, and the part of dmeamt/dxyz containing dfjlij/dxyz
                do jj=1,gn_neighbors(iatom,istr)
                   j=gneighborlist(jj,iatom,istr)
                   jsp=gspecies(j,istr)
                   meam_t(l,iatom)=meam_t(l,iatom)+meamtau(l,jsp)* &
                       screening(jj,iatom)*fjlij(0,jj,iatom)
                   if (optmeamtau(l,jsp).eqv..true.) then
                      dmeamt_dmeamtau(l,jsp,iatom) = dmeamt_dmeamtau(l,jsp,iatom) + &
                             screening(jj,iatom)*fjlij(0,jj,iatom)
                   endif
                   !In the following, the derivative are w.r.t l=0 density parameters
                   do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                      ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                      dmeamt_dpara(ip,l,jsp,iatom)=dmeamt_dpara(ip,l,jsp,iatom)+ &
                             meamtau(l,jsp)*screening(jj,iatom)*dfjlij_dpara(ip,0,jj,iatom)
                   enddo
                   do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                      ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                      dmeamt_dpara(ip,l,jsp,iatom)=dmeamt_dpara(ip,l,jsp,iatom)+ &
                             meamtau(l,jsp)*screening(jj,iatom)*dfjlij_dpara(ip,0,jj,iatom) !note, l=0 on RHS
                   enddo
                   if (abs(fjlij(0,jj,iatom)).gt.0d0) then
                      do alph=1,3
                         !Derivatives of meamt w.r.t off site atomic coordinates [Note the minus sign, 
                         !because dfjlij_dxyz stores the on-site derivative value (with the off-site being
                         !minus the on-site)]
                         dmeamt_dxyz(alph,jj,l,iatom)= -meamtau(l,jsp)*screening(jj,iatom)* &
                             dfjlij_dxyz(alph,0,jj,iatom)
                         !Derivatives of meamt w.r.t on site atomic coordinates
                         dmeamt_dxyz(alph,0,l,iatom)= dmeamt_dxyz(alph,0,l,iatom) - dmeamt_dxyz(alph,jj,l,iatom)
                         !Derivatives of dmeamt_dxyz w.r.t meamtau
                         d2meamt_dxyz_dmeamtau(alph,jj,l,jsp,iatom)=-screening(jj,iatom)*dfjlij_dxyz(alph,0,jj,iatom)
                         d2meamt_dxyz_dmeamtau(alph,0,l,jsp,iatom)=d2meamt_dxyz_dmeamtau(alph,0,l,jsp,iatom)-d2meamt_dxyz_dmeamtau(alph,jj,l,jsp,iatom)
                      enddo
                   endif
                enddo
                if (abs(rhol(0,iatom)).gt.0d0) then
                   meam_t(l,iatom)=meam_t(l,iatom)/rhol(0,iatom)
                   dmeamt_dxyz(1:3,0:gn_neighbors(iatom,istr),l,iatom)= &
                      dmeamt_dxyz(1:3,0:gn_neighbors(iatom,istr),l,iatom) / rhol(0,iatom)
                   !dmeamt_dmeamtau, dmeamt_dpara and d2meamt_dxyz_dmeamtau
                   do jsp=1,maxspecies
                      dmeamt_dmeamtau(l,jsp,iatom)=dmeamt_dmeamtau(l,jsp,iatom)/rhol(0,iatom)
                      do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                         ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                         !Below, in 2nd term we only have one rhol on the denominator
                         !since meam_t has already been divided by rhol
                         dmeamt_dpara(ip,l,jsp,iatom)=dmeamt_dpara(ip,l,jsp,iatom)/rhol(0,iatom) &
                            - meam_t(l,iatom)/rhol(0,iatom) * &
                              drhol_dpara(ip,0,jsp,iatom)
                      enddo
                      do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                         ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                         dmeamt_dpara(ip,l,jsp,iatom)=dmeamt_dpara(ip,l,jsp,iatom)/rhol(0,iatom) &
                            - meam_t(l,iatom)/rhol(0,iatom) * &
                              drhol_dpara(ip,0,jsp,iatom)
                      enddo
                      d2meamt_dxyz_dmeamtau(1:3,0:gn_neighbors(iatom,istr),l,jsp,iatom)= &
                         d2meamt_dxyz_dmeamtau(1:3,0:gn_neighbors(iatom,istr),l,jsp,iatom) / rhol(0,iatom)
                   enddo
                   !Add remaining part to dmeamt_dxyz and d2meamt_dxyz_dmeamtau
                   dmeamt_drhol0= -meam_t(l,iatom)/rhol(0,iatom)
                   do jj=0,gn_neighbors(iatom,istr)
                      do alph=1,3
                         dmeamt_dxyz(alph,jj,l,iatom)= dmeamt_dxyz(alph,jj,l,iatom) + &
                            dmeamt_drhol0*drhol_dxyz(alph,jj,0,iatom)
                         do jsp=1,maxspecies
                            d2meamt_dxyz_dmeamtau(alph,jj,l,jsp,iatom)= &
                               d2meamt_dxyz_dmeamtau(alph,jj,l,jsp,iatom) - &
                               dmeamt_dmeamtau(l,jsp,iatom)/rhol(0,iatom)*drhol_dxyz(alph,jj,0,iatom)
                         enddo
                      enddo
                   enddo
                endif
                !d2meamt_dxyz_dpara
                do jj=1,gn_neighbors(iatom,istr)
                   j=gneighborlist(jj,iatom,istr)
                   jsp=gspecies(j,istr)
                   do alph=1,3
                      do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                         ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                         d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,iatom)= &
                            ( - meamtau(l,jsp)*screening(jj,iatom)*d2fjlij_dxyz_dpara(alph,ip,0,jj,iatom) ) / rhol(0,iatom)
                         d2meamt_dxyz_dpara(alph,0,l,ip,jsp,iatom)=d2meamt_dxyz_dpara(alph,0,l,ip,jsp,iatom)-d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,iatom)
                      enddo
                      do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                         ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                         d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,iatom)= &
                            ( - meamtau(l,jsp)*screening(jj,iatom)*d2fjlij_dxyz_dpara(alph,ip,0,jj,iatom) ) / rhol(0,iatom)
                         d2meamt_dxyz_dpara(alph,0,l,ip,jsp,iatom)=d2meamt_dxyz_dpara(alph,0,l,ip,jsp,iatom)-d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,iatom)
                      enddo
                   enddo
                enddo
                do jj=0,gn_neighbors(iatom,istr)
                   do alph=1,3
                      do jsp=1,maxspecies
                         do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                            ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                            d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,iatom) = d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,iatom)+ &
                               ( - dmeamt_dpara(ip,l,jsp,iatom)*drhol_dxyz(alph,jj,0,iatom) &
                                 - meam_t(l,iatom)*d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,iatom) &
                                 - dmeamt_dxyz(alph,jj,l,iatom)*drhol_dpara(ip,0,jsp,iatom) ) &
                               / rhol(0,iatom)
                         enddo
                         do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                            ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)

                            d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,iatom) = d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,iatom)+ &
                               ( - dmeamt_dpara(ip,l,jsp,iatom)*drhol_dxyz(alph,jj,0,iatom) &
                                 - meam_t(l,iatom)*d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,iatom) &
                                 - dmeamt_dxyz(alph,jj,l,iatom)*drhol_dpara(ip,0,jsp,iatom) ) &
                               / rhol(0,iatom)
                         enddo
                      enddo
                   enddo
                enddo
             enddo
          enddo
       endif
       ! ------------------------------------------------------------------------------


       ! -------------------- rhol and derivatives for l = 1 --------------------------
       !Calculate aux1 and its derivatives w.r.t xyz and para
       !  allocate( daux1_dxyz(3,0:maxneighbors,3),daux1_dpara(3,1:12,1:maxspecies), &
       !     d2aux1_dxyz_dpara(3,0:maxneighbors,3,12,maxspecies) )

       do iatom=1,gn_inequivalentsites(istr)

          isp=1
          if (thiaccptindepndt.eqv..false.) isp=gspecies(iatom,istr)

          aux1=0d0
          daux1_dxyz=0d0
          daux1_dpara=0d0
          d2aux1_dxyz_dpara=0d0

          do jj=1,gn_neighbors(iatom,istr)
              j=gneighborlist(jj,iatom,istr)
              jsp=gspecies(j,istr)
              rij=diststr(jj,iatom,0,0,istr)

              if (abs(fjlij(1,jj,iatom)).gt.0d0) then
                 screen_over_rij=screening(jj,iatom)/rij
                 fjlijFact=screen_over_rij*fjlij(1,jj,iatom)
                 aux1(1)=aux1(1)+fjlijFact* &
                     dxstr(jj,iatom,0,0,istr)
                 aux1(2)=aux1(2)+fjlijFact* &
                     dystr(jj,iatom,0,0,istr)
                 aux1(3)=aux1(3)+fjlijFact* &
                     dzstr(jj,iatom,0,0,istr)

                 !Derivatives of aux1 w.r.t atomic coordinates
                 screen_over_rij3=screening(jj,iatom)/(rij**3)
                 !Here I introduce dalph(1-3), as it makes the subsequent alculations tidier. At some point it would make sense to
                 !merge dxstr, dystr and dzstr into a single array (with extra index 1-3) across the whole code.
                 dalph(1)=dxstr(jj,iatom,0,0,istr)
                 dalph(2)=dystr(jj,iatom,0,0,istr)
                 dalph(3)=dzstr(jj,iatom,0,0,istr)
                 do alph=1,3
                    do alph2=1,3
                       fjlijPrefact(alph2,alph) = -screen_over_rij3*dalph(alph)*dalph(alph2)
                    enddo
                    fjlijPrefact(alph,alph) = fjlijPrefact(alph,alph) + screen_over_rij
                    dfjlij_dxyzPrefact(alph) = -screen_over_rij*dalph(alph)
                 enddo
                 do alph=1,3
                    do alph2=1,3
                       !Derivatives of aux1 w.r.t off site atomic coordinates
                       daux1_dxyz(alph2,jj,alph) = fjlijPrefact(alph2,alph)*fjlij(1,jj,iatom) + &
                            dfjlij_dxyzPrefact(alph)*dfjlij_dxyz(alph2,1,jj,iatom)
                       !Derivatives of daux1_dxyz w.r.t potential parameters
                       do iiCoeff=1,noptdensityCoeff(1,isp,jsp)
                          ip = ioptdensityCoeff(iiCoeff,1,isp,jsp)
                          d2aux1_dxyz_dpara(alph2,jj,alph,ip,jsp)= &
                             fjlijPrefact(alph2,alph)*dfjlij_dpara(ip,1,jj,iatom)+ &
                             dfjlij_dxyzPrefact(alph)*d2fjlij_dxyz_dpara(alph2,ip,1,jj,iatom) !alph2 or alph???
                       enddo
                       do iiCutoff=1,noptdensityCutoff(1,isp,jsp)
                          ip = ioptdensityCutoff(iiCutoff,1,isp,jsp)
                          d2aux1_dxyz_dpara(alph2,jj,alph,ip,jsp)=&
                             fjlijPrefact(alph2,alph)*dfjlij_dpara(ip,1,jj,iatom)+ &
                             dfjlij_dxyzPrefact(alph)*d2fjlij_dxyz_dpara(alph2,ip,1,jj,iatom) !alph2 or alph???
                       enddo
                       !Derivatives of aux1 w.r.t on site atomic coordinates
                       daux1_dxyz(alph2,0,alph)  = daux1_dxyz(alph2,0,alph) - daux1_dxyz(alph2,jj,alph)
                       !Derivatives of daux1_dxyz w.r.t potential parameters
                       do iiCoeff=1,noptdensityCoeff(1,isp,jsp)
                          ip = ioptdensityCoeff(iiCoeff,1,isp,jsp)
                          d2aux1_dxyz_dpara(alph2,0,alph,ip,jsp)=d2aux1_dxyz_dpara(alph2,0,alph,ip,jsp)- &
                             d2aux1_dxyz_dpara(alph2,jj,alph,ip,jsp)
                     !       fjlijPrefact(alph2,alph)*dfjlij_dpara(ip,1,jj,iatom)- &
                     !       dfjlij_dxyzPrefact(alph)*d2fjlij_dxyz_dpara(alph2,ip,1,jj,iatom)
                       enddo
                       do iiCutoff=1,noptdensityCutoff(1,isp,jsp)
                          ip = ioptdensityCutoff(iiCutoff,1,isp,jsp)
                          d2aux1_dxyz_dpara(alph2,0,alph,ip,jsp)=d2aux1_dxyz_dpara(alph2,0,alph,ip,jsp)- &
                             d2aux1_dxyz_dpara(alph2,jj,alph,ip,jsp)
                     !        fjlijPrefact(alph2,alph)*dfjlij_dpara(ip,1,jj,iatom)- &
                     !        dfjlij_dxyzPrefact(alph)*d2fjlij_dxyz_dpara(alph2,ip,1,jj,iatom)
                       enddo
                    enddo
                 enddo

                 !---- altered code, not tested ----
                 ! !Derivatives of aux1 w.r.t off site atomic coordinates
                 ! screen_over_rij3=screening(jj,iatom)/(rij**3)
                 ! dalph(1)=dxstr(jj,iatom,0,0,istr)
                 ! dalph(2)=dystr(jj,iatom,0,0,istr)
                 ! dalph(3)=dystr(jj,iatom,0,0,istr)
                 ! dfjlij_dxyzPrefact(1) = -screen_over_rij*dalph(1)
                 ! fjlijPrefact(1,1) = -screen_over_rij3*dalph(1)*dalph(1) + screen_over_rij
                 ! daux1_dxyz(1,jj,1) = fjlijPrefact(1,1)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(1)*dfjlij_dxyz(1,1,jj,iatom)
                 ! fjlijPrefact(2,1) = -screen_over_rij3*dalph(1)*dalph(2)
                 ! daux1_dxyz(2,jj,1) = fjlijPrefact(2,1)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(1)*dfjlij_dxyz(2,1,jj,iatom)
                 ! fjlijPrefact(3,1) = -screen_over_rij3*dalph(1)*dalph(3)
                 ! daux1_dxyz(3,jj,1) = fjlijPrefact(3,1)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(1)*dfjlij_dxyz(3,1,jj,iatom)

                 ! dfjlij_dxyzPrefact(2) = -screen_over_rij*dalph(2)
                 ! fjlijPrefact(1,2) = -screen_over_rij3*dalph(2)*dalph(1)
                 ! daux1_dxyz(1,jj,2) = fjlijPrefact(1,2)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(2)*dfjlij_dxyz(1,1,jj,iatom)
                 ! fjlijPrefact(2,2) = -screen_over_rij3*dalph(2)*dalph(2) + screen_over_rij
                 ! daux1_dxyz(2,jj,2) = fjlijPrefact(2,2)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(2)*dfjlij_dxyz(2,1,jj,iatom)
                 ! fjlijPrefact(3,2) = -screen_over_rij3*dalph(2)*dalph(3)
                 ! daux1_dxyz(3,jj,2) = fjlijPrefact(3,2)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(2)*dfjlij_dxyz(3,1,jj,iatom)

                 ! dfjlij_dxyzPrefact(3) = -screen_over_rij*dalph(3)
                 ! fjlijPrefact(1,3) = -screen_over_rij3*dalph(3)*dalph(1)
                 ! daux1_dxyz(1,jj,3) = fjlijPrefact(1,3)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(3)*dfjlij_dxyz(1,1,jj,iatom)
                 ! fjlijPrefact(2,3) = -screen_over_rij3*dalph(3)*dalph(2) + screen_over_rij
                 ! daux1_dxyz(2,jj,3) = fjlijPrefact(2,3)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(3)*dfjlij_dxyz(2,1,jj,iatom)
                 ! fjlijPrefact(3,3) = -screen_over_rij3*dalph(3)*dalph(3) 
                 ! daux1_dxyz(3,jj,3) = fjlijPrefact(3,3)*fjlij(1,jj,iatom) + dfjlij_dxyzPrefact(3)*dfjlij_dxyz(3,1,jj,iatom)

                 ! !Derivatives of aux1 w.r.t on site atomic coordinates
                 ! do alph=1,3
                 !    do alph2=1,3
                 !       daux1_dxyz(alph2,0,alph) = daux1_dxyz(alph2,0,alph) - daux1_dxyz(alph2,jj,alph)
                 !    enddo
                 ! enddo
                 !------------------------------------

                 ! ---- Original code, works: ----
                 ! !Derivatives of aux1 w.r.t off site atomic coordinates
                 ! screen_over_rij3=screening(jj,iatom)/(rij**3)
                 ! daux1_dxyz(1,jj,1) = -screen_over_rij3*fjlij(1,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2) - &
                 !   screen_over_rij*dfjlij_dxyz(1,1,jj,iatom)*dxstr(jj,iatom,0,0,istr) + &
                 !   screen_over_rij*fjlij(1,jj,iatom)
                 ! daux1_dxyz(2,jj,1) = -screen_over_rij3*fjlij(1,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr) - &
                 !   screen_over_rij*dfjlij_dxyz(2,1,jj,iatom)*dxstr(jj,iatom,0,0,istr)
                 ! daux1_dxyz(3,jj,1) = -screen_over_rij3*fjlij(1,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) - &
                 !   screen_over_rij*dfjlij_dxyz(3,1,jj,iatom)*dxstr(jj,iatom,0,0,istr)
                 ! daux1_dxyz(1,jj,2) = -screen_over_rij3*fjlij(1,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr) - &
                 !   screen_over_rij*dfjlij_dxyz(1,1,jj,iatom)*dystr(jj,iatom,0,0,istr)
                 ! daux1_dxyz(2,jj,2) = -screen_over_rij3*fjlij(1,jj,iatom)*(dystr(jj,iatom,0,0,istr)**2) - &
                 !   screen_over_rij*dfjlij_dxyz(2,1,jj,iatom)*dystr(jj,iatom,0,0,istr) + &
                 !   screen_over_rij*fjlij(1,jj,iatom)
                 ! daux1_dxyz(3,jj,2) = -screen_over_rij3*fjlij(1,jj,iatom)*dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) - &
                 !   screen_over_rij*dfjlij_dxyz(3,1,jj,iatom)*dystr(jj,iatom,0,0,istr)
                 ! daux1_dxyz(1,jj,3) = -screen_over_rij3*fjlij(1,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) - &
                 !   screen_over_rij*dfjlij_dxyz(1,1,jj,iatom)*dzstr(jj,iatom,0,0,istr)
                 ! daux1_dxyz(2,jj,3) = -screen_over_rij3*fjlij(1,jj,iatom)*dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) - &
                 !   screen_over_rij*dfjlij_dxyz(2,1,jj,iatom)*dzstr(jj,iatom,0,0,istr)
                 ! daux1_dxyz(3,jj,3) = -screen_over_rij3*fjlij(1,jj,iatom)*(dzstr(jj,iatom,0,0,istr)**2) - &
                 !   screen_over_rij*dfjlij_dxyz(3,1,jj,iatom)*dzstr(jj,iatom,0,0,istr) + &
                 !   screen_over_rij*fjlij(1,jj,iatom)
                 ! !Derivatives of aux1 w.r.t on site atomic coordinates
                 ! daux1_dxyz(1,0,1) = daux1_dxyz(1,0,1) - daux1_dxyz(1,jj,1)
                 ! daux1_dxyz(2,0,1) = daux1_dxyz(2,0,1) - daux1_dxyz(2,jj,1)
                 ! daux1_dxyz(3,0,1) = daux1_dxyz(3,0,1) - daux1_dxyz(3,jj,1)
                 ! daux1_dxyz(1,0,2) = daux1_dxyz(1,0,2) - daux1_dxyz(1,jj,2)
                 ! daux1_dxyz(2,0,2) = daux1_dxyz(2,0,2) - daux1_dxyz(2,jj,2)
                 ! daux1_dxyz(3,0,2) = daux1_dxyz(3,0,2) - daux1_dxyz(3,jj,2)
                 ! daux1_dxyz(1,0,3) = daux1_dxyz(1,0,3) - daux1_dxyz(1,jj,3)
                 ! daux1_dxyz(2,0,3) = daux1_dxyz(2,0,3) - daux1_dxyz(2,jj,3)
                 ! daux1_dxyz(3,0,3) = daux1_dxyz(3,0,3) - daux1_dxyz(3,jj,3)
                 ! -----------------------------------------

                 !Calculate derivatives of aux1 w.r.t parameters
                 !Coefficients:
                 do iiCoeff=1,noptdensityCoeff(1,isp,jsp)
                    ip = ioptdensityCoeff(iiCoeff,1,isp,jsp)
                    fjlijFact=screen_over_rij*dfjlij_dpara(ip,1,jj,iatom)
                    daux1_dpara(1,ip,jsp) = daux1_dpara(1,ip,jsp) + &
                         fjlijFact*dxstr(jj,iatom,0,0,istr)
                    daux1_dpara(2,ip,jsp) = daux1_dpara(2,ip,jsp) + &
                         fjlijFact*dystr(jj,iatom,0,0,istr)
                    daux1_dpara(3,ip,jsp) = daux1_dpara(3,ip,jsp) + &
                         fjlijFact*dzstr(jj,iatom,0,0,istr)
                 enddo
                 !Cutoffs:
                 do iiCutoff=1,noptdensityCutoff(1,isp,jsp)
                    ip = ioptdensityCutoff(iiCutoff,1,isp,jsp)
                    fjlijFact=screen_over_rij*dfjlij_dpara(ip,1,jj,iatom)
                    daux1_dpara(1,ip,jsp) = daux1_dpara(1,ip,jsp) + &
                         fjlijFact*dxstr(jj,iatom,0,0,istr)
                    daux1_dpara(2,ip,jsp) = daux1_dpara(2,ip,jsp) + &
                         fjlijFact*dystr(jj,iatom,0,0,istr)
                    daux1_dpara(3,ip,jsp) = daux1_dpara(3,ip,jsp) + &
                         fjlijFact*dzstr(jj,iatom,0,0,istr)
                 enddo

              endif

              !    if (envdepmeamt.eqv..true.) then
              !       meam_t(1,iatom)=meam_t(1,iatom)+meamtau(1,jsp)* &
              !           screening(jj,iatom)*fjlij(0,jj,iatom)
              !       !Calculate derivatives of meam_t w.r.t parameters
              !       !w.r.t meamtau parameters:
              !       if (optmeamtau(1,jsp).eqv..true.) then
              !          dmeamt_dmeamtau(1,jsp,iatom) = dmeamt_dmeamtau(1,jsp,iatom) + &
              !                 screening(jj,iatom)*fjlij(0,jj,iatom)
              !       endif
              !       !w.r.t density coefficients:
              !       !(note here the derivative are w.r.t l=0 density parameters)
              !       do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
              !          ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
              !          dmeamt_dpara(ip,1,jsp,iatom)=dmeamt_dpara(ip,1,jsp,iatom)+ &
              !                 meamtau(1,jsp)*screening(jj,iatom)*dfjlij_dpara(ip,0,jj,iatom)
              !       enddo
              !       !w.r.t density cutoffs:
              !       do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
              !          ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
              !          dmeamt_dpara(ip,1,jsp,iatom)=dmeamt_dpara(ip,1,jsp,iatom)+ &
              !                 meamtau(1,jsp)*screening(jj,iatom)*dfjlij_dpara(ip,0,jj,iatom) !note, l=0 on RHS
              !       enddo
              !    endif

              !    if (envdepmeamt.eqv..true.) then
              !       if (rhol(0,iatom).ne.0d0) then
              !           meam_t(1,iatom)=meam_t(1,iatom)/rhol(0,iatom)
              !           !Calculate analytic derivatives of meam_t with respect to
              !           !meamtau and the radial electron density parameters
              !           do jsp=1,maxspecies
              !              dmeamt_dmeamtau(1,jsp,iatom)=dmeamt_dmeamtau(1,jsp,iatom)/rhol(0,iatom)
              !              do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
              !                 ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
              !                 !Below, in 2nd term we only have one rhol on the denominator
              !                 !since meam_t has already been divided by rhol
              !                 dmeamt_dpara(ip,1,jsp,iatom)=dmeamt_dpara(ip,1,jsp,iatom)/rhol(0,iatom) &
              !                    - meam_t(1,iatom)/rhol(0,iatom) * &
              !                      drhol_dpara(ip,0,jsp,iatom)
              !              enddo
              !              do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
              !                 ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
              !                 dmeamt_dpara(ip,1,jsp,iatom)=dmeamt_dpara(ip,1,jsp,iatom)/rhol(0,iatom) &
              !                    - meam_t(1,iatom)/rhol(0,iatom) * &
              !                      drhol_dpara(ip,0,jsp,iatom)
              !              enddo
              !           enddo
              !       endif
              !    endif

          enddo

          sum_auxSqr=aux1(1)**2+aux1(2)**2+aux1(3)**2

          rhol(1,iatom)=sqrt(sum_auxSqr)

          !Calculate derivative of rhol w.r.t parameters
          !Coefficients:
          if (sum_auxSqr.ne.0d0) then !if =0, should have drhol_dpara=0,
                                      !however the eqns below will return = NaN
             !Calculate derivatives of rhol w.r.t coordinates of atoms (jj=0
             !gives onsite)
             do alph=1,3
                do jj=0,gn_neighbors(iatom,istr)
                   drhol_dxyz(alph,jj,1,iatom) = (1d0/sqrt(sum_auxSqr)) * &
                      ( aux1(1)*daux1_dxyz(alph,jj,1) + &
                        aux1(2)*daux1_dxyz(alph,jj,2) + &
                        aux1(3)*daux1_dxyz(alph,jj,3) )
                   ! print *,'drhol_dxyz(',alph,jj,1,iatom,')=',drhol_dxyz(alph,jj,1,iatom)
                enddo
             enddo

             !Calculate derivatives of rhol w.r.t potential parameters
             do jsp=1,maxspecies
                do iiCoeff=1,noptdensityCoeff(1,isp,jsp)

                   ip = ioptdensityCoeff(iiCoeff,1,isp,jsp)
                   !rhol(1,i)/sum_auxSqr used in place of
                   !0.5d0*(sum_auxSqr**(-0.5d0)) as it should be faster
                   drhol_dpara(ip,1,jsp,iatom) = (rhol(1,iatom)/sum_auxSqr) * &
                         ( aux1(1)*daux1_dpara(1,ip,jsp) + &
                           aux1(2)*daux1_dpara(2,ip,jsp) + &
                           aux1(3)*daux1_dpara(3,ip,jsp) )
                enddo

                do iiCutoff=1,noptdensityCutoff(1,isp,jsp)
                   ip = ioptdensityCutoff(iiCutoff,1,isp,jsp)
                   !rhol(1,i)/sum_auxSqr used in place of
                   !0.5d0*(sum_auxSqr**(-0.5d0)) as it should be faster
                   drhol_dpara(ip,1,jsp,iatom) = (rhol(1,iatom)/sum_auxSqr) * &
                         ( aux1(1)*daux1_dpara(1,ip,jsp) + & 
                           aux1(2)*daux1_dpara(2,ip,jsp) + & 
                           aux1(3)*daux1_dpara(3,ip,jsp) ) 
                enddo

             enddo

             !Calculate derivatives of d2rhol_dxyz_dpara w.r.t potential parameters
             !d2rhol_dxyz_dpara(1:3,0:maxneighbors,12,0:lmax,gn_inequivalentsites(istr))
             !using:  d2aux1_dxyz_dpara(alph2,jj,alph,ip,lmax,jsp)
             do alph=1,3
                do jj=0,gn_neighbors(iatom,istr)

                   do jsp=1,maxspecies
                      do iiCoeff=1,noptdensityCoeff(1,isp,jsp)
                         ip = ioptdensityCoeff(iiCoeff,1,isp,jsp)
                         d2rhol_dxyz_dpara(alph,jj,ip,1,jsp,iatom)= &
                           (-1d0/(sum_auxSqr**1.5d0)) * &
                           ( aux1(1)*daux1_dpara(1,ip,jsp) + &
                             aux1(2)*daux1_dpara(2,ip,jsp) + &
                             aux1(3)*daux1_dpara(3,ip,jsp) ) * &
                           ( aux1(1)*daux1_dxyz(alph,jj,1) + &
                             aux1(2)*daux1_dxyz(alph,jj,2) + &
                             aux1(3)*daux1_dxyz(alph,jj,3) ) &
                         + (1d0/sqrt(sum_auxSqr)) * &
                           ( daux1_dpara(1,ip,jsp)*daux1_dxyz(alph,jj,1) + &
                             daux1_dpara(2,ip,jsp)*daux1_dxyz(alph,jj,2) + &
                             daux1_dpara(3,ip,jsp)*daux1_dxyz(alph,jj,3) + &
                             aux1(1)*d2aux1_dxyz_dpara(alph,jj,1,ip,jsp) + &
                             aux1(2)*d2aux1_dxyz_dpara(alph,jj,2,ip,jsp) + &
                             aux1(3)*d2aux1_dxyz_dpara(alph,jj,3,ip,jsp) )
                      enddo
                      do iiCutoff=1,noptdensityCutoff(1,isp,jsp)
                         ip = ioptdensityCutoff(iiCutoff,1,isp,jsp)
                         d2rhol_dxyz_dpara(alph,jj,ip,1,jsp,iatom)= &
                           (-1d0/(sum_auxSqr**1.5d0)) * &
                           ( aux1(1)*daux1_dpara(1,ip,jsp) + &
                             aux1(2)*daux1_dpara(2,ip,jsp) + &
                             aux1(3)*daux1_dpara(3,ip,jsp) )* &
                           ( aux1(1)*daux1_dxyz(alph,jj,1) + &
                             aux1(2)*daux1_dxyz(alph,jj,2) + &
                             aux1(3)*daux1_dxyz(alph,jj,3) ) &
                         + (1d0/sqrt(sum_auxSqr)) * &
                           ( daux1_dpara(1,ip,jsp)*daux1_dxyz(alph,jj,1) + &
                             daux1_dpara(2,ip,jsp)*daux1_dxyz(alph,jj,2) + &
                             daux1_dpara(3,ip,jsp)*daux1_dxyz(alph,jj,3) + &
                             aux1(1)*d2aux1_dxyz_dpara(alph,jj,1,ip,jsp) + &
                             aux1(2)*d2aux1_dxyz_dpara(alph,jj,2,ip,jsp) + &
                             aux1(3)*d2aux1_dxyz_dpara(alph,jj,3,ip,jsp) )
                      enddo
                   enddo
                enddo
             enddo

            !if (iatom.eq.1) then
            !   print *,'from electrondensityForce:'
            !   print *,'--------------------------'
            !   print *,'... and aux1(1)=',aux1(1)
            !   print *,'... and aux1(2)=',aux1(2)
            !   print *,'... and aux1(3)=',aux1(3)
            !   print *,'... and daux1_dxyz(1,0,1)=',daux1_dxyz(1,0,1)
            !   print *,'... and daux1_dxyz(1,0,2)=',daux1_dxyz(1,0,2)
            !   print *,'... and daux1_dxyz(1,0,3)=',daux1_dxyz(1,0,3)
            !   print *,'... and daux1_dpara(1,1,1)=',daux1_dpara(1,1,1)
            !   print *,'... and daux1_dpara(2,1,1)=',daux1_dpara(2,1,1)
            !   print *,'... and daux1_dpara(3,1,1)=',daux1_dpara(3,1,1)
            !   print *,'... and d2aux1_dxyz_dpara(1,0,1,1,1)=',d2aux1_dxyz_dpara(1,0,1,1,1)
            !   print *,'... and d2aux1_dxyz_dpara(1,0,2,1,1)=',d2aux1_dxyz_dpara(1,0,2,1,1)
            !   print *,'... and d2aux1_dxyz_dpara(1,0,3,1,1)=',d2aux1_dxyz_dpara(1,0,3,1,1)
            !   print *,'... and drhol_dxyz(1,0,1,1)=',drhol_dxyz(1,0,1,1)
            !endif

          endif

       enddo

       ! do iiCoeff=1,noptdensityCoeff(1,isp,jsp)
       !    ip = ioptdensityCoeff(iiCoeff,1,isp,jsp)
       !    do alph=1,3
       !       print *,'d2rhol_dxyz_dpara(',alph,',',ip,',1,',i,')
       !       =',d2rhol_dxyz_dpara(alph,ip,1,i)
       !    enddo
       ! enddo
       ! do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
       !    ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
       !    do alph=1,3
       !       print *,'d2rhol_dxyz_dpara(',alph,',',ip,',1,',i,')
       !       =',d2rhol_dxyz_dpara(alph,ip,1,i)
       !    enddo
       ! enddo

       !Print out analytic derivatives

       ! do iatom=1,gn_inequivalentsites(istr)
       !    do jj=0,gn_neighbors(iatom,istr)
       !       do alph=1,3
       !           print *,'drhol_dxyz(',alph,',jj=',jj,',1,',iatom,')=',drhol_dxyz(alph,jj,1,iatom)
       !       enddo
       !    enddo
       ! enddo

       ! ------------------------------------------------------------------------------


       ! -------------------- rhol and derivatives for l = 2 --------------------------
       if (lmax.ge.2) then

          !Calculate aux2 and its derivatives w.r.t xyz and para
          !  allocate( daux2_dxyz(3,0:maxneighbors,3,3),daux2_dpara(3,3,12,1:maxspecies), &
          !            daux2a_dxyz(3,0:maxneighbors),daux2a_dpara(12,maxspecies), &
          !            d2aux2_dxyz_dpara(3,0:maxneighbors,3,3,12,maxspecies), &
          !            d2aux2a_dxyz_dpara(3,0:maxneighbors,12,maxspecies) )

          do iatom=1,gn_inequivalentsites(istr)

             aux2=0d0
             aux2a=0d0
             daux2_dxyz=0d0
             daux2_dpara=0d0
             daux2a_dxyz=0d0
             daux2a_dpara=0d0
             d2aux2_dxyz_dpara=0d0
             d2aux2a_dxyz_dpara=0d0

             do jj=1,gn_neighbors(iatom,istr)

                j=gneighborlist(jj,iatom,istr)
                jsp=gspecies(j,istr)
                rij=diststr(jj,iatom,0,0,istr)

                if (abs(fjlij(2,jj,iatom)).gt.0d0) then
                   !New code (see commented out code below for old code)
                   dalph(1)=dxstr(jj,iatom,0,0,istr)
                   dalph(2)=dystr(jj,iatom,0,0,istr)
                   dalph(3)=dzstr(jj,iatom,0,0,istr)
                   screen_over_rij2=screening(jj,iatom)/(rij**2)
                   fjlijFact=screen_over_rij2*fjlij(2,jj,iatom)
                   do alph=1,3
                      do alph2=alph,3
                         aux2(alph,alph2)=aux2(alph,alph2)+fjlijFact* &
                             dalph(alph)*dalph(alph2)
                      enddo
                   enddo
                   aux2a=aux2a+screening(jj,iatom)*fjlij(2,jj,iatom)
                   !New code ends
                   !Old code:
                   ! screen_over_rij2=screening(jj,iatom)/(rij**2)
                   ! fjlijFact=screen_over_rij2*fjlij(2,jj,iatom)
                   ! aux2(1,1)=aux2(1,1)+fjlijFact* &
                   !     dxstr(jj,iatom,0,0,istr)**2
                   ! aux2(1,2)=aux2(1,2)+fjlijFact* &
                   !     dxstr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr)
                   ! aux2(1,3)=aux2(1,3)+fjlijFact* &
                   !     dxstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr)
                   ! aux2(2,2)=aux2(2,2)+fjlijFact* &
                   !     dystr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr)
                   ! aux2(2,3)=aux2(2,3)+fjlijFact* &
                   !     dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr)
                   ! aux2(3,3)=aux2(3,3)+fjlijFact* &
                   !     dzstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr)
                   ! aux2a=aux2a+screening(jj,iatom)*fjlij(2,jj,iatom)
                   !Old code ends

                   !Derivatives of aux2 w.r.t atomic coordinates
                   !Currently recoding to reduce number of lines:
                   !New code (see commented out old code below):
                   screen_over_rij4=screening(jj,iatom)/(rij**4)
                   do alph=1,3
                      do alph2=alph,3
                         dfjlij_dxyzPrefact2(alph2,alph) = -screen_over_rij2*dalph(alph2)*dalph(alph)
                         do alph3=1,3
                            fjlijPrefact2(alph3,alph2,alph) = -2d0*screen_over_rij4* &
                                dalph(alph3)*dalph(alph2)*dalph(alph)
                         enddo
                      enddo
                      fjlijPrefact2(alph,alph,alph)=fjlijPrefact2(alph,alph,alph) + &
                           2d0*screen_over_rij2*dalph(alph)
                   enddo
                   do alph=1,2
                      do alph2=alph+1,3
                         fjlijPrefact2(alph,alph2,alph)=fjlijPrefact2(alph,alph2,alph) + &
                           screen_over_rij2*dalph(alph2)
                         fjlijPrefact2(alph2,alph2,alph)=fjlijPrefact2(alph2,alph2,alph) + &
                           screen_over_rij2*dalph(alph)
                      enddo
                   enddo

                   do alph=1,3
                      do alph2=alph,3
                         do alph3=1,3
                            !Derivatives of aux1 w.r.t off site atomic coordinates
                            daux2_dxyz(alph3,jj,alph,alph2) = fjlijPrefact2(alph3,alph2,alph)*fjlij(2,jj,iatom) + &
                                 dfjlij_dxyzPrefact2(alph2,alph)*dfjlij_dxyz(alph3,2,jj,iatom)
                            daux2_dxyz(alph3,0,alph,alph2) = daux2_dxyz(alph3,0,alph,alph2) - daux2_dxyz(alph3,jj,alph,alph2)
                            !print *,'daux2_dxyz(',alph3,',',jj,',',alph,',',alph2,')=',daux2_dxyz(alph3,jj,alph,alph2)
                            do iiCoeff=1,noptdensityCoeff(2,isp,jsp)
                               ip = ioptdensityCoeff(iiCoeff,2,isp,jsp)
                               d2aux2_dxyz_dpara(alph3,jj,alph,alph2,ip,jsp)= &
                                  fjlijPrefact2(alph3,alph2,alph)*dfjlij_dpara(ip,2,jj,iatom) + &
                                  dfjlij_dxyzPrefact2(alph2,alph)*d2fjlij_dxyz_dpara(alph3,ip,2,jj,iatom)
                               d2aux2_dxyz_dpara(alph3,0,alph,alph2,ip,jsp)=d2aux2_dxyz_dpara(alph3,0,alph,alph2,ip,jsp) - &
                                  d2aux2_dxyz_dpara(alph3,jj,alph,alph2,ip,jsp)
                            enddo
                            do iiCutoff=1,noptdensityCutoff(2,isp,jsp)
                               ip = ioptdensityCutoff(iiCutoff,2,isp,jsp)
                               d2aux2_dxyz_dpara(alph3,jj,alph,alph2,ip,jsp)= &
                                  fjlijPrefact2(alph3,alph2,alph)*dfjlij_dpara(ip,2,jj,iatom) + &
                                  dfjlij_dxyzPrefact2(alph2,alph)*d2fjlij_dxyz_dpara(alph3,ip,2,jj,iatom)
                               d2aux2_dxyz_dpara(alph3,0,alph,alph2,ip,jsp)=d2aux2_dxyz_dpara(alph3,0,alph,alph2,ip,jsp) - &
                                  d2aux2_dxyz_dpara(alph3,jj,alph,alph2,ip,jsp)
                            enddo
                         enddo
                      enddo
                   enddo
                   do alph=1,3
                      daux2a_dxyz(alph,jj) = -screening(jj,iatom)*dfjlij_dxyz(alph,2,jj,iatom)
                      daux2a_dxyz(alph,0) = daux2a_dxyz(alph,0) - daux2a_dxyz(alph,jj)
                      do iiCoeff=1,noptdensityCoeff(2,isp,jsp)
                         ip = ioptdensityCoeff(iiCoeff,2,isp,jsp)
                         d2aux2a_dxyz_dpara(alph,jj,ip,jsp)= &
                            -screening(jj,iatom)*d2fjlij_dxyz_dpara(alph,ip,2,jj,iatom)
                         d2aux2a_dxyz_dpara(alph,0,ip,jsp)=d2aux2a_dxyz_dpara(alph,0,ip,jsp) - &
                            d2aux2a_dxyz_dpara(alph,jj,ip,jsp)
                      enddo
                      do iiCutoff=1,noptdensityCutoff(2,isp,jsp)
                         ip = ioptdensityCutoff(iiCutoff,2,isp,jsp)
                         d2aux2a_dxyz_dpara(alph,jj,ip,jsp)= &
                            -screening(jj,iatom)*d2fjlij_dxyz_dpara(alph,ip,2,jj,iatom)
                         d2aux2a_dxyz_dpara(alph,0,ip,jsp)=d2aux2a_dxyz_dpara(alph,0,ip,jsp) - &
                            d2aux2a_dxyz_dpara(alph,jj,ip,jsp)
                      enddo

                   enddo

                   !print *,'daux2_dxyz(1,jj,1,1)   new=',daux2_dxyz(1,jj,1,1)
                   !print *,'   fjlij part=',fjlijPrefact2(1,1,1)*fjlij(2,jj,iatom)
                   !print *,'      (first part=',-2d0*screen_over_rij4* &
                   !             dalph(1)*dalph(1)*dalph(1)*fjlij(2,jj,iatom),')'
                   !print *,'   dfjlij part=',dfjlij_dxyzPrefact2(1,1)*dfjlij_dxyz(1,2,jj,iatom)
                   !End of new code

                   !  ! !Old code:
                   !   screen_over_rij4=screening(jj,iatom)/(rij**4)
                   !  daux2_dxyz(1,jj,1,1) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**3) - &
                   !    screen_over_rij2*dfjlij_dxyz(1,2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2) + &
                   !    2d0*screen_over_rij2*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)
                   !  !    print *,'daux2_dxyz(1,jj,1,1)   old=',daux2_dxyz(1,jj,1,1)
                   !  !    print *,'   fjlij part=',-2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**3) + &
                   !  !      2d0*screen_over_rij2*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)
                   !  !    print *,'      (first part=',-2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**3),')'
                   !  !    print *,'   dfjlij part=',- &
                   !  !      screen_over_rij2*dfjlij_dxyz(1,2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2) 
                   !  !    stop
                   !  daux2_dxyz(2,jj,1,1) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2)* &
                   !    dystr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(2,2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2)
                   !  daux2_dxyz(3,jj,1,1) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2)* &
                   !    dzstr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(3,2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2) 
                   !  daux2_dxyz(1,jj,1,2) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2)* &
                   !    dystr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(1,2,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr) + &
                   !    screen_over_rij2*fjlij(2,jj,iatom)*dystr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(2,jj,1,2) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)* & 
                   !    (dystr(jj,iatom,0,0,istr)**2) - &
                   !    screen_over_rij2*dfjlij_dxyz(2,2,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr) + &
                   !    screen_over_rij2*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(3,jj,1,2) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)* &     
                   !    dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(3,2,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(1,jj,1,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dxstr(jj,iatom,0,0,istr)**2)* &
                   !    dzstr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(1,2,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) + &
                   !    screen_over_rij2*fjlij(2,jj,iatom)*dzstr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(2,jj,1,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)* &
                   !    dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(2,2,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(3,jj,1,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)* &
                   !    (dzstr(jj,iatom,0,0,istr)**2) - &
                   !    screen_over_rij2*dfjlij_dxyz(3,2,jj,iatom)*dxstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) + &
                   !    screen_over_rij2*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(1,jj,2,2) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)* &
                   !     (dystr(jj,iatom,0,0,istr)**2) - &
                   !    screen_over_rij2*dfjlij_dxyz(1,2,jj,iatom)*(dystr(jj,iatom,0,0,istr)**2)
                   !  daux2_dxyz(2,jj,2,2) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dystr(jj,iatom,0,0,istr)**3) - &
                   !    screen_over_rij2*dfjlij_dxyz(2,2,jj,iatom)*(dystr(jj,iatom,0,0,istr)**2) + &
                   !    2d0*screen_over_rij2*fjlij(2,jj,iatom)*dystr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(3,jj,2,2) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dystr(jj,iatom,0,0,istr)**2)* &
                   !    dzstr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(3,2,jj,iatom)*(dystr(jj,iatom,0,0,istr)**2)
                   !  daux2_dxyz(1,jj,2,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)* &
                   !     dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(1,2,jj,iatom)*dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) 
                   !  daux2_dxyz(2,jj,2,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dystr(jj,iatom,0,0,istr)**2)* &
                   !    dzstr(jj,iatom,0,0,istr) - &
                   !    screen_over_rij2*dfjlij_dxyz(2,2,jj,iatom)*dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) + &
                   !    screen_over_rij2*fjlij(2,jj,iatom)*dzstr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(3,jj,2,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dystr(jj,iatom,0,0,istr)* &
                   !    (dzstr(jj,iatom,0,0,istr)**2) - &
                   !    screen_over_rij2*dfjlij_dxyz(3,2,jj,iatom)*dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr) + &
                   !    screen_over_rij2*fjlij(2,jj,iatom)*dystr(jj,iatom,0,0,istr)
                   !  daux2_dxyz(1,jj,3,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dxstr(jj,iatom,0,0,istr)* &
                   !    (dzstr(jj,iatom,0,0,istr)**2) - &
                   !    screen_over_rij2*dfjlij_dxyz(1,2,jj,iatom)*(dzstr(jj,iatom,0,0,istr)**2)
                   !  daux2_dxyz(2,jj,3,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*dystr(jj,iatom,0,0,istr)* &
                   !    (dzstr(jj,iatom,0,0,istr)**2) - &
                   !    screen_over_rij2*dfjlij_dxyz(2,2,jj,iatom)*(dzstr(jj,iatom,0,0,istr)**2)
                   !  daux2_dxyz(3,jj,3,3) = -2d0*screen_over_rij4*fjlij(2,jj,iatom)*(dzstr(jj,iatom,0,0,istr)**3) - &
                   !    screen_over_rij2*dfjlij_dxyz(3,2,jj,iatom)*(dzstr(jj,iatom,0,0,istr)**2) + &
                   !    2d0*screen_over_rij2*fjlij(2,jj,iatom)*dzstr(jj,iatom,0,0,istr)
                   !  daux2a_dxyz(1,jj) = -screening(jj,iatom)*dfjlij_dxyz(1,2,jj,iatom)
                   !  daux2a_dxyz(2,jj) = -screening(jj,iatom)*dfjlij_dxyz(2,2,jj,iatom)
                   !  daux2a_dxyz(3,jj) = -screening(jj,iatom)*dfjlij_dxyz(3,2,jj,iatom)
                   !  !  print *,'old:'
                   !  !  do alph=1,3
                   !  !     do alph2=alph,3
                   !  !        do alph3=1,3
                   !  !           print *,'daux2_dxyz(',alph3,',',jj,',',alph,',',alph2,')=',daux2_dxyz(alph3,jj,alph,alph2)
                   !  !        enddo
                   !  !     enddo
                   !  !  enddo
                   !  !  stop
                   !
                   !  !Derivatives of aux1 w.r.t on site atomic coordinates
                   !  daux2_dxyz(1,0,1,1) = daux2_dxyz(1,0,1,1) - daux2_dxyz(1,jj,1,1)
                   !  daux2_dxyz(2,0,1,1) = daux2_dxyz(2,0,1,1) - daux2_dxyz(2,jj,1,1)
                   !  daux2_dxyz(3,0,1,1) = daux2_dxyz(3,0,1,1) - daux2_dxyz(3,jj,1,1)
                   !  daux2_dxyz(1,0,1,2) = daux2_dxyz(1,0,1,2) - daux2_dxyz(1,jj,1,2)
                   !  daux2_dxyz(2,0,1,2) = daux2_dxyz(2,0,1,2) - daux2_dxyz(2,jj,1,2)
                   !  daux2_dxyz(3,0,1,2) = daux2_dxyz(3,0,1,2) - daux2_dxyz(3,jj,1,2)
                   !  daux2_dxyz(1,0,1,3) = daux2_dxyz(1,0,1,3) - daux2_dxyz(1,jj,1,3)
                   !  daux2_dxyz(2,0,1,3) = daux2_dxyz(2,0,1,3) - daux2_dxyz(2,jj,1,3)
                   !  daux2_dxyz(3,0,1,3) = daux2_dxyz(3,0,1,3) - daux2_dxyz(3,jj,1,3)
                   !  daux2_dxyz(1,0,2,2) = daux2_dxyz(1,0,2,2) - daux2_dxyz(1,jj,2,2)
                   !  daux2_dxyz(2,0,2,2) = daux2_dxyz(2,0,2,2) - daux2_dxyz(2,jj,2,2)
                   !  daux2_dxyz(3,0,2,2) = daux2_dxyz(3,0,2,2) - daux2_dxyz(3,jj,2,2)
                   !  daux2_dxyz(1,0,2,3) = daux2_dxyz(1,0,2,3) - daux2_dxyz(1,jj,2,3)
                   !  daux2_dxyz(2,0,2,3) = daux2_dxyz(2,0,2,3) - daux2_dxyz(2,jj,2,3)
                   !  daux2_dxyz(3,0,2,3) = daux2_dxyz(3,0,2,3) - daux2_dxyz(3,jj,2,3)
                   !  daux2_dxyz(1,0,3,3) = daux2_dxyz(1,0,3,3) - daux2_dxyz(1,jj,3,3)
                   !  daux2_dxyz(2,0,3,3) = daux2_dxyz(2,0,3,3) - daux2_dxyz(2,jj,3,3)
                   !  daux2_dxyz(3,0,3,3) = daux2_dxyz(3,0,3,3) - daux2_dxyz(3,jj,3,3)
                   !  daux2a_dxyz(1,0) = daux2a_dxyz(1,0) - daux2a_dxyz(1,jj)
                   !  daux2a_dxyz(2,0) = daux2a_dxyz(2,0) - daux2a_dxyz(2,jj)
                   !  daux2a_dxyz(3,0) = daux2a_dxyz(3,0) - daux2a_dxyz(3,jj)
                   !  !  !Old code ends

                   !Calculate derivatives of aux2 and aux2a w.r.t parameters
                   !Coefficients:
                   do iiCoeff=1,noptdensityCoeff(2,isp,jsp)
                      ip = ioptdensityCoeff(iiCoeff,2,isp,jsp)
                      fjlijFact=screen_over_rij2*dfjlij_dpara(ip,2,jj,iatom)
                      daux2_dpara(1,1,ip,jsp) = daux2_dpara(1,1,ip,jsp) + fjlijFact* &
                           (dxstr(jj,iatom,0,0,istr)**2)
                      daux2_dpara(1,2,ip,jsp) = daux2_dpara(1,2,ip,jsp) + fjlijFact* &
                           dxstr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr)
                      daux2_dpara(1,3,ip,jsp) = daux2_dpara(1,3,ip,jsp) + fjlijFact* &
                           dxstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr)
                      daux2_dpara(2,2,ip,jsp) = daux2_dpara(2,2,ip,jsp) + fjlijFact* &
                           (dystr(jj,iatom,0,0,istr)**2)
                      daux2_dpara(2,3,ip,jsp) = daux2_dpara(2,3,ip,jsp) + fjlijFact* &
                           dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr)
                      daux2_dpara(3,3,ip,jsp) = daux2_dpara(3,3,ip,jsp) + fjlijFact* &
                           (dzstr(jj,iatom,0,0,istr)**2)
                      daux2a_dpara(ip,jsp)=daux2a_dpara(ip,jsp)+ &
                           screening(jj,iatom)*dfjlij_dpara(ip,2,jj,iatom)
                   enddo
                   !Cutoffs:
                   do iiCutoff=1,noptdensityCutoff(2,isp,jsp)
                      ip = ioptdensityCutoff(iiCutoff,2,isp,jsp)
                      fjlijFact=screen_over_rij2*dfjlij_dpara(ip,2,jj,iatom)
                      daux2_dpara(1,1,ip,jsp) = daux2_dpara(1,1,ip,jsp) + fjlijFact* &
                           (dxstr(jj,iatom,0,0,istr)**2)
                      daux2_dpara(1,2,ip,jsp) = daux2_dpara(1,2,ip,jsp) + fjlijFact* &
                           dxstr(jj,iatom,0,0,istr)*dystr(jj,iatom,0,0,istr)
                      daux2_dpara(1,3,ip,jsp) = daux2_dpara(1,3,ip,jsp) + fjlijFact* &
                           dxstr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr)
                      daux2_dpara(2,2,ip,jsp) = daux2_dpara(2,2,ip,jsp) + fjlijFact* &
                           (dystr(jj,iatom,0,0,istr)**2)
                      daux2_dpara(2,3,ip,jsp) = daux2_dpara(2,3,ip,jsp) + fjlijFact* &
                           dystr(jj,iatom,0,0,istr)*dzstr(jj,iatom,0,0,istr)
                      daux2_dpara(3,3,ip,jsp) = daux2_dpara(3,3,ip,jsp) + fjlijFact* &
                           (dzstr(jj,iatom,0,0,istr)**2)
                      daux2a_dpara(ip,jsp)=daux2a_dpara(ip,jsp)+ &
                           screening(jj,iatom)*dfjlij_dpara(ip,2,jj,iatom)
                   enddo

                endif

                ! if (envdepmeamt.eqv..true.) then
                !    meam_t(2,iatom)=meam_t(2,iatom)+meamtau(2,jsp)* &
                !        screening(jj,iatom)*fjlij(0,jj,iatom)
                !    !Calculate derivatives of meam_t w.r.t parameters
                !    !w.r.t meamtau parameters:
                !    if (optmeamtau(2,jsp).eqv..true.) then
                !       dmeamt_dmeamtau(2,jsp,iatom) = dmeamt_dmeamtau(2,jsp,iatom) + &
                !              screening(jj,iatom)*fjlij(0,jj,iatom)
                !    endif
                !    !w.r.t density coefficients:
                !    !(note here the derivative are w.r.t l=0 density parameters)
                !    do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                !       ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                !       dmeamt_dpara(ip,2,jsp,iatom)=dmeamt_dpara(ip,2,jsp,iatom)+ &
                !              meamtau(2,jsp)*screening(jj,iatom)*dfjlij_dpara(ip,0,jj,iatom)
                !    enddo
                !    !w.r.t density cutoffs:
                !    do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                !       ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                !       dmeamt_dpara(ip,2,jsp,iatom)=dmeamt_dpara(ip,2,jsp,iatom)+ &
                !              meamtau(2,jsp)*screening(jj,iatom)*dfjlij_dpara(ip,0,jj,iatom) !note, l=0 on RHS
                !    enddo
                ! endif

             enddo

             ! if (envdepmeamt.eqv..true.) then
             !    if (rhol(0,iatom).ne.0d0) then
             !        meam_t(2,iatom)=meam_t(2,iatom)/rhol(0,iatom)
             !        !Calculate analytic derivatives of meam_t with respect to
             !        !meamtau and the radial electron density parameters
             !        do jsp=1,maxspecies
             !           dmeamt_dmeamtau(2,jsp,iatom)=dmeamt_dmeamtau(2,jsp,iatom)/rhol(0,iatom)
             !           do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
             !              ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
             !              !Below, in 2nd term we only have one rhol on the denominator
             !              !since meam_t has already been divided by rhol
             !              dmeamt_dpara(ip,2,jsp,iatom)=dmeamt_dpara(ip,2,jsp,iatom)/rhol(0,iatom) &
             !                 - meam_t(2,iatom)/rhol(0,iatom) * &
             !                   drhol_dpara(ip,0,jsp,iatom)
             !           enddo
             !           do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
             !              ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
             !              dmeamt_dpara(ip,2,jsp,iatom)=dmeamt_dpara(ip,2,jsp,iatom)/rhol(0,iatom) &
             !                 - meam_t(2,iatom)/rhol(0,iatom) * &
             !                   drhol_dpara(ip,0,jsp,iatom)
             !           enddo
             !        enddo

             !    endif
             ! endif
          tmp1=aux2(1,1)**2+2d0*aux2(1,2)**2+2d0*aux2(1,3)**2+ &
              aux2(2,2)**2+2d0*aux2(2,3)**2+aux2(3,3)**2
          tmp2=aux2a*aux2a/3d0
          !If tmp1 and tmp2 are equal to 15 s.f set tmp1-tmp2 to zero
          !by hand to avoid rounding error (sqr root = 10^-8 would otherwise
          !result). Note, if either tmp1 or tmp2=0 then we are ok to take
          !aux=tmp1-tmp2 (see my written notes, 08/08/2017).
          if ((tmp1.eq.0d0).or.(tmp2.eq.0d0)) then
             aux=tmp1-tmp2
          else
             if (abs((tmp1-tmp2)/tmp1).lt.10d-14) then
                !If the two numbers are equal within precision:
                aux=0d0
             else
                aux=tmp1-tmp2
             endif
          endif
          rhol(2,iatom)=sqrt(abs(aux)) !I use BASKES formula 8c PRB 46, 2727 (1992)
          if (aux.ne.0d0) then
             do alph=1,3
                do jj=0,gn_neighbors(iatom,istr)
                   drhol_dxyz(alph,jj,2,iatom) = (1d0/sqrt(abs(aux))) * &
                      ( aux2(1,1)*daux2_dxyz(alph,jj,1,1) + 2d0*aux2(1,2)*daux2_dxyz(alph,jj,1,2) + &
                        2d0*aux2(1,3)*daux2_dxyz(alph,jj,1,3) + aux2(2,2)*daux2_dxyz(alph,jj,2,2) + &
                        2d0*aux2(2,3)*daux2_dxyz(alph,jj,2,3) + aux2(3,3)*daux2_dxyz(alph,jj,3,3) - &
                        aux2a*daux2a_dxyz(alph,jj)/3d0 )
                enddo
             enddo
          endif

          !Calculate derivative of drhol_dxyz w.r.t parameters
          if (aux.ne.0d0) then
             do alph=1,3

                do jj=0,gn_neighbors(iatom,istr)

                   do jsp=1,maxspecies
                      do iiCoeff=1,noptdensityCoeff(2,isp,jsp)
                         ip = ioptdensityCoeff(iiCoeff,2,isp,jsp)
                         d2rhol_dxyz_dpara(alph,jj,ip,2,jsp,iatom)= (-1d0/(abs(aux)**1.5d0))  * &
                           ( aux2(1,1)*daux2_dpara(1,1,ip,jsp) + &
                             2d0*aux2(1,2)*daux2_dpara(1,2,ip,jsp) + &
                             2d0*aux2(1,3)*daux2_dpara(1,3,ip,jsp) + &
                             aux2(2,2)*daux2_dpara(2,2,ip,jsp) + &
                             2d0*aux2(2,3)*daux2_dpara(2,3,ip,jsp) + &
                             aux2(3,3)*daux2_dpara(3,3,ip,jsp) - &
                             aux2a*daux2a_dpara(ip,jsp)/3d0 )  * &
                           ( aux2(1,1)*daux2_dxyz(alph,jj,1,1) + &
                             2d0*aux2(1,2)*daux2_dxyz(alph,jj,1,2) + &
                             2d0*aux2(1,3)*daux2_dxyz(alph,jj,1,3) + &
                             aux2(2,2)*daux2_dxyz(alph,jj,2,2) + &
                             2d0*aux2(2,3)*daux2_dxyz(alph,jj,2,3) + &
                             aux2(3,3)*daux2_dxyz(alph,jj,3,3) - &
                             aux2a*daux2a_dxyz(alph,jj)/3d0 ) &
                         + (1d0/sqrt(abs(aux))) * &
                           ( daux2_dpara(1,1,ip,jsp)*daux2_dxyz(alph,jj,1,1) + &
                             2d0*daux2_dpara(1,2,ip,jsp)*daux2_dxyz(alph,jj,1,2) + &
                             2d0*daux2_dpara(1,3,ip,jsp)*daux2_dxyz(alph,jj,1,3) + &
                             daux2_dpara(2,2,ip,jsp)*daux2_dxyz(alph,jj,2,2) + &
                             2d0*daux2_dpara(2,3,ip,jsp)*daux2_dxyz(alph,jj,2,3) + &
                             daux2_dpara(3,3,ip,jsp)*daux2_dxyz(alph,jj,3,3) - &
                             daux2a_dpara(ip,jsp)*daux2a_dxyz(alph,jj)/3d0 + &
                             aux2(1,1)*d2aux2_dxyz_dpara(alph,jj,1,1,ip,jsp) + &
                             2d0*aux2(1,2)*d2aux2_dxyz_dpara(alph,jj,1,2,ip,jsp) + &
                             2d0*aux2(1,3)*d2aux2_dxyz_dpara(alph,jj,1,3,ip,jsp) + &
                             aux2(2,2)*d2aux2_dxyz_dpara(alph,jj,2,2,ip,jsp) + &
                             2d0*aux2(2,3)*d2aux2_dxyz_dpara(alph,jj,2,3,ip,jsp) + &
                             aux2(3,3)*d2aux2_dxyz_dpara(alph,jj,3,3,ip,jsp) - &
                             aux2a*d2aux2a_dxyz_dpara(alph,jj,ip,jsp)/3d0 )
                      enddo
                      do iiCutoff=1,noptdensityCutoff(2,isp,jsp)
                         ip = ioptdensityCutoff(iiCutoff,2,isp,jsp)
                         d2rhol_dxyz_dpara(alph,jj,ip,2,jsp,iatom)= (-1d0/(abs(aux)**1.5d0))  * &
                           ( aux2(1,1)*daux2_dpara(1,1,ip,jsp) + &
                             2d0*aux2(1,2)*daux2_dpara(1,2,ip,jsp) + &
                             2d0*aux2(1,3)*daux2_dpara(1,3,ip,jsp) + &
                             aux2(2,2)*daux2_dpara(2,2,ip,jsp) + &
                             2d0*aux2(2,3)*daux2_dpara(2,3,ip,jsp) + &
                             aux2(3,3)*daux2_dpara(3,3,ip,jsp) - &
                             aux2a*daux2a_dpara(ip,jsp)/3d0 )  * &
                           ( aux2(1,1)*daux2_dxyz(alph,jj,1,1) + &
                             2d0*aux2(1,2)*daux2_dxyz(alph,jj,1,2) + &
                             2d0*aux2(1,3)*daux2_dxyz(alph,jj,1,3) + &
                             aux2(2,2)*daux2_dxyz(alph,jj,2,2) + &
                             2d0*aux2(2,3)*daux2_dxyz(alph,jj,2,3) + &
                             aux2(3,3)*daux2_dxyz(alph,jj,3,3) - &
                             aux2a*daux2a_dxyz(alph,jj)/3d0 ) &
                         + (1d0/sqrt(abs(aux))) * &
                           ( daux2_dpara(1,1,ip,jsp)*daux2_dxyz(alph,jj,1,1) + &
                             2d0*daux2_dpara(1,2,ip,jsp)*daux2_dxyz(alph,jj,1,2) + &
                             2d0*daux2_dpara(1,3,ip,jsp)*daux2_dxyz(alph,jj,1,3) + &
                             daux2_dpara(2,2,ip,jsp)*daux2_dxyz(alph,jj,2,2) + &
                             2d0*daux2_dpara(2,3,ip,jsp)*daux2_dxyz(alph,jj,2,3) + &
                             daux2_dpara(3,3,ip,jsp)*daux2_dxyz(alph,jj,3,3) - &
                             daux2a_dpara(ip,jsp)*daux2a_dxyz(alph,jj)/3d0 + &
                             aux2(1,1)*d2aux2_dxyz_dpara(alph,jj,1,1,ip,jsp) + &
                             2d0*aux2(1,2)*d2aux2_dxyz_dpara(alph,jj,1,2,ip,jsp) + &
                             2d0*aux2(1,3)*d2aux2_dxyz_dpara(alph,jj,1,3,ip,jsp) + &
                             aux2(2,2)*d2aux2_dxyz_dpara(alph,jj,2,2,ip,jsp) + &
                             2d0*aux2(2,3)*d2aux2_dxyz_dpara(alph,jj,2,3,ip,jsp) + &
                             aux2(3,3)*d2aux2_dxyz_dpara(alph,jj,3,3,ip,jsp) - &
                             aux2a*d2aux2a_dxyz_dpara(alph,jj,ip,jsp)/3d0 )
                      enddo
                   enddo


                enddo
             enddo
          endif

          !Calculate derivative of rhol w.r.t parameters
          !Coefficients:
          if (aux.ne.0d0) then !if =0, should have drhol_dpara=0,
                                      !however the eqns below will return =NaN
             do jsp=1,maxspecies
                do iiCoeff=1,noptdensityCoeff(2,isp,jsp)
                   ip = ioptdensityCoeff(iiCoeff,2,isp,jsp)
                   !Note, there is an abs() in the above equation for rhol(2,i).
                   !Hence below there is an extra factor d abs(aux) / d aux
                   != abs(aux)/aux below (so that 1/abs(aux) -> 1/aux)
                   drhol_dpara(ip,2,jsp,iatom) = (rhol(2,iatom)/aux) * &
                        ( aux2(1,1)*daux2_dpara(1,1,ip,jsp) + &
                          2d0*aux2(1,2)*daux2_dpara(1,2,ip,jsp) + &
                          2d0*aux2(1,3)*daux2_dpara(1,3,ip,jsp) + &
                          aux2(2,2)*daux2_dpara(2,2,ip,jsp) + &
                          2d0*aux2(2,3)*daux2_dpara(2,3,ip,jsp) + &
                          aux2(3,3)*daux2_dpara(3,3,ip,jsp) - &
                          aux2a*daux2a_dpara(ip,jsp)/3d0 )
                enddo
                do iiCutoff=1,noptdensityCutoff(2,isp,jsp)
                   ip = ioptdensityCutoff(iiCutoff,2,isp,jsp)
                   drhol_dpara(ip,2,jsp,iatom) = (rhol(2,iatom)/aux) * &
                        ( aux2(1,1)*daux2_dpara(1,1,ip,jsp) + &
                          2d0*aux2(1,2)*daux2_dpara(1,2,ip,jsp) + &
                          2d0*aux2(1,3)*daux2_dpara(1,3,ip,jsp) + &
                          aux2(2,2)*daux2_dpara(2,2,ip,jsp) + &
                          2d0*aux2(2,3)*daux2_dpara(2,3,ip,jsp) + &
                          aux2(3,3)*daux2_dpara(3,3,ip,jsp) - &
                          aux2a*daux2a_dpara(ip,jsp)/3d0 )
                enddo
             enddo
          endif

          enddo

          !Print out analytic derivatives

          ! do iatom=1,gn_inequivalentsites(istr)
          !    do jj=0,gn_neighbors(iatom,istr)
          !       do alph=1,3
          !           print *,'drhol_dxyz(',alph,',jj=',jj,',2,',iatom,')=',drhol_dxyz(alph,jj,2,iatom)
          !       enddo
          !    enddo
          ! enddo

       endif
       ! ------------------------------------------------------------------------------


       ! -------------------- rhol and derivatives for l = 3 --------------------------
       if (lmax.ge.3) then

          !Calculate aux3 and its derivatives w.r.t xyz and para
          !  allocate( daux3_dxyz(3,0:maxneighbors,3,3,3), &
          !            daux3_dpara(3,3,3,12,1:maxspecies), &
          !            d2aux3_dxyz_dpara(3,0:maxneighbors,3,3,3,12,maxspecies))
          !  if (orthogElecDens) then
          !     allocate( daux3a_dxyz(3,0:maxneighbors,3), &
          !               daux3a_dpara(3,1:12,1:maxspecies), &
          !               d2aux3a_dxyz_dpara(3,0:maxneighbors,3,12,maxspecies) )
          !  endif

          do iatom=1,gn_inequivalentsites(istr)
             aux3=0d0
             daux3_dpara=0d0
             daux3_dxyz=0d0
             d2aux3_dxyz_dpara=0d0
             if (orthogElecDens) then
                aux3a=0d0
                daux3a_dxyz=0d0
                daux3a_dpara=0d0
                d2aux3a_dxyz_dpara=0d0
             endif

             do jj=1,gn_neighbors(iatom,istr)
                 j=gneighborlist(jj,iatom,istr)
                 jsp=gspecies(j,istr)
                 rij=diststr(jj,iatom,0,0,istr)

                 !new code:
                 if (abs(fjlij(3,jj,iatom)).gt.0d0) then

                    dalph(1)=dxstr(jj,iatom,0,0,istr)
                    dalph(2)=dystr(jj,iatom,0,0,istr)
                    dalph(3)=dzstr(jj,iatom,0,0,istr)
                    screen_over_rij3=screening(jj,iatom)/(rij**3)
                    fjlijFact=screen_over_rij3*fjlij(3,jj,iatom)
                    do alph=1,3
                       do alph2=alph,3
                          do alph3=alph2,3
                             aux3(alph,alph2,alph3)=aux3(alph,alph2,alph3)+fjlijFact* &
                                dalph(alph)*dalph(alph2)*dalph(alph3)
                          enddo
                       enddo
                    enddo

                    if (orthogElecDens) then
                       screen_over_rij=screening(jj,iatom)/rij
                       fjlijFact=screen_over_rij*fjlij(3,jj,iatom)
                       aux3a(1)=aux3a(1)+fjlijFact* &
                           dxstr(jj,iatom,0,0,istr)
                       aux3a(2)=aux3a(2)+fjlijFact* &
                           dystr(jj,iatom,0,0,istr)
                       aux3a(3)=aux3a(3)+fjlijFact* &
                           dzstr(jj,iatom,0,0,istr)

                       !Derivatives of aux3a w.r.t atomic coordinates
                       screen_over_rij3=screening(jj,iatom)/(rij**3)
                       !Here I introduce dalph(1-3), as it makes the subsequent
                       !alculations tidier. At some point it would make sense to
                       !merge dxstr, dystr and dzstr into a single array (with
                       !extra index 1-3) across the whole code.
                       dalph(1)=dxstr(jj,iatom,0,0,istr)
                       dalph(2)=dystr(jj,iatom,0,0,istr)
                       dalph(3)=dzstr(jj,iatom,0,0,istr)
                       do alph=1,3
                          do alph2=1,3
                             fjlijPrefact(alph2,alph) = -screen_over_rij3*dalph(alph)*dalph(alph2)
                          enddo
                          fjlijPrefact(alph,alph) = fjlijPrefact(alph,alph) + screen_over_rij
                          dfjlij_dxyzPrefact(alph) = -screen_over_rij*dalph(alph)
                       enddo
                       do alph=1,3
                          do alph2=1,3
                             !Derivatives of aux3a w.r.t off site atomic coordinates
                             daux3a_dxyz(alph2,jj,alph) = fjlijPrefact(alph2,alph)*fjlij(3,jj,iatom) + &
                                  dfjlij_dxyzPrefact(alph)*dfjlij_dxyz(alph2,3,jj,iatom)
                             !Derivatives of daux3a_dxyz w.r.t potential parameters
                             do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                                ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                                d2aux3a_dxyz_dpara(alph2,jj,alph,ip,jsp)= &
                                   fjlijPrefact(alph2,alph)*dfjlij_dpara(ip,3,jj,iatom)+ &
                                   dfjlij_dxyzPrefact(alph)*d2fjlij_dxyz_dpara(alph2,ip,3,jj,iatom) !alph2 or alph???
                             enddo
                             do iiCutoff=1,noptdensityCutoff(3,isp,jsp)
                                ip = ioptdensityCutoff(iiCutoff,3,isp,jsp)
                                d2aux3a_dxyz_dpara(alph2,jj,alph,ip,jsp)=&
                                   fjlijPrefact(alph2,alph)*dfjlij_dpara(ip,3,jj,iatom)+ &
                                   dfjlij_dxyzPrefact(alph)*d2fjlij_dxyz_dpara(alph2,ip,3,jj,iatom) !alph2 or alph???
                             enddo
                             !Derivatives of aux1 w.r.t on site atomic coordinates
                             daux3a_dxyz(alph2,0,alph)  = daux3a_dxyz(alph2,0,alph) - daux3a_dxyz(alph2,jj,alph)
                             !Derivatives of daux3a_dxyz w.r.t potential parameters
                             do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                                ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                                d2aux3a_dxyz_dpara(alph2,0,alph,ip,jsp)=d2aux3a_dxyz_dpara(alph2,0,alph,ip,jsp)- &
                                   d2aux3a_dxyz_dpara(alph2,jj,alph,ip,jsp)
                             enddo
                             do iiCutoff=1,noptdensityCutoff(3,isp,jsp)
                                ip = ioptdensityCutoff(iiCutoff,3,isp,jsp)
                                d2aux3a_dxyz_dpara(alph2,0,alph,ip,jsp)=d2aux3a_dxyz_dpara(alph2,0,alph,ip,jsp)- &
                                   d2aux3a_dxyz_dpara(alph2,jj,alph,ip,jsp)
                             enddo
                          enddo
                       enddo

                       !Derivatives of aux3a w.r.t parameters
                       !Coefficients:
                       do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                          ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                          fjlijFact=screen_over_rij*dfjlij_dpara(ip,3,jj,iatom)
                          daux3a_dpara(1,ip,jsp) = daux3a_dpara(1,ip,jsp) + &
                               fjlijFact*dxstr(jj,iatom,0,0,istr)
                          daux3a_dpara(2,ip,jsp) = daux3a_dpara(2,ip,jsp) + &
                               fjlijFact*dystr(jj,iatom,0,0,istr)
                          daux3a_dpara(3,ip,jsp) = daux3a_dpara(3,ip,jsp) + &
                               fjlijFact*dzstr(jj,iatom,0,0,istr)
                       enddo

                       !Cutoffs:
                       do iiCutoff=1,noptdensityCutoff(3,isp,jsp)
                          ip = ioptdensityCutoff(iiCutoff,3,isp,jsp)
                          fjlijFact=screen_over_rij*dfjlij_dpara(ip,3,jj,iatom)
                          daux3a_dpara(1,ip,jsp) = daux3a_dpara(1,ip,jsp) + &
                               fjlijFact*dxstr(jj,iatom,0,0,istr)
                          daux3a_dpara(2,ip,jsp) = daux3a_dpara(2,ip,jsp) + &
                               fjlijFact*dystr(jj,iatom,0,0,istr)
                          daux3a_dpara(3,ip,jsp) = daux3a_dpara(3,ip,jsp) + &
                               fjlijFact*dzstr(jj,iatom,0,0,istr)
                       enddo

                    endif

                    !Derivatives of aux3 w.r.t off site atomic coordinates
                    screen_over_rij5=screening(jj,iatom)/(rij**5)
                    do alph=1,3
                       do alph2=alph,3
                          do alph3=alph2,3
                             dfjlij_dxyzPrefact3(alph3,alph2,alph) = -screen_over_rij3*dalph(alph3)*dalph(alph2)*dalph(alph)
                             do alph4=1,3
                                fjlijPrefact3(alph4,alph3,alph2,alph) = -3d0*screen_over_rij5*&
                                    dalph(alph4)*dalph(alph3)*dalph(alph2)*dalph(alph)
                             enddo
                          enddo
                       enddo
                       fjlijPrefact3(alph,alph,alph,alph)=fjlijPrefact3(alph,alph,alph,alph) + &
                            3d0*screen_over_rij3*dalph(alph)*dalph(alph)
                    enddo
                    fjlijPrefact3(1,2,1,1)=fjlijPrefact3(1,2,1,1)+2d0*screen_over_rij3*dalph(1)*dalph(2)
                    fjlijPrefact3(2,2,1,1)=fjlijPrefact3(2,2,1,1)+screen_over_rij3*dalph(1)*dalph(1)
                    fjlijPrefact3(1,3,1,1)=fjlijPrefact3(1,3,1,1)+2d0*screen_over_rij3*dalph(1)*dalph(3)
                    fjlijPrefact3(3,3,1,1)=fjlijPrefact3(3,3,1,1)+screen_over_rij3*dalph(1)*dalph(1)
                    fjlijPrefact3(1,2,2,1)=fjlijPrefact3(1,2,2,1)+screen_over_rij3*dalph(2)*dalph(2)
                    fjlijPrefact3(2,2,2,1)=fjlijPrefact3(2,2,2,1)+2d0*screen_over_rij3*dalph(1)*dalph(2)
                    fjlijPrefact3(1,3,2,1)=fjlijPrefact3(1,3,2,1)+screen_over_rij3*dalph(2)*dalph(3)
                    fjlijPrefact3(2,3,2,1)=fjlijPrefact3(2,3,2,1)+screen_over_rij3*dalph(1)*dalph(3)
                    fjlijPrefact3(3,3,2,1)=fjlijPrefact3(3,3,2,1)+screen_over_rij3*dalph(1)*dalph(2)
                    fjlijPrefact3(1,3,3,1)=fjlijPrefact3(1,3,3,1)+screen_over_rij3*dalph(3)*dalph(3)
                    fjlijPrefact3(3,3,3,1)=fjlijPrefact3(3,3,3,1)+2d0*screen_over_rij3*dalph(1)*dalph(3)
                    fjlijPrefact3(2,3,2,2)=fjlijPrefact3(2,3,2,2)+2d0*screen_over_rij3*dalph(2)*dalph(3)
                    fjlijPrefact3(3,3,2,2)=fjlijPrefact3(3,3,2,2)+screen_over_rij3*dalph(2)*dalph(2)
                    fjlijPrefact3(2,3,3,2)=fjlijPrefact3(2,3,3,2)+screen_over_rij3*dalph(3)*dalph(3)
                    fjlijPrefact3(3,3,3,2)=fjlijPrefact3(3,3,3,2)+2d0*screen_over_rij3*dalph(2)*dalph(3)

                    do alph=1,3
                       do alph2=alph,3
                          do alph3=alph2,3
                             do alph4=1,3
                                !Derivatives of aux3 w.r.t dxyz
                                daux3_dxyz(alph4,jj,alph,alph2,alph3) = fjlijPrefact3(alph4,alph3,alph2,alph)*fjlij(3,jj,iatom) + &
                                     dfjlij_dxyzPrefact3(alph3,alph2,alph)*dfjlij_dxyz(alph4,3,jj,iatom) !(off site atomic coordinates)
                                daux3_dxyz(alph4,0,alph,alph2,alph3) = daux3_dxyz(alph4,0,alph,alph2,alph3) - &
                                     daux3_dxyz(alph4,jj,alph,alph2,alph3) !(on site atomic coordinates)
                                !Derivatives of daux3/dxyz w.r.t paras
                                do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                                   ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                                   d2aux3_dxyz_dpara(alph4,jj,alph,alph2,alph3,ip,jsp) = & !(off site atomic coordinates)
                                      fjlijPrefact3(alph4,alph3,alph2,alph)*dfjlij_dpara(ip,3,jj,iatom) + &
                                      dfjlij_dxyzPrefact3(alph3,alph2,alph)*d2fjlij_dxyz_dpara(alph4,ip,3,jj,iatom)
                                   d2aux3_dxyz_dpara(alph4,0,alph,alph2,alph3,ip,jsp) = & !(on site atomic coordinates)
                                      d2aux3_dxyz_dpara(alph4,0,alph,alph2,alph3,ip,jsp) - &
                                      d2aux3_dxyz_dpara(alph4,jj,alph,alph2,alph3,ip,jsp)
                                enddo
                                do iiCutoff=1,noptdensityCutoff(3,isp,jsp)
                                   ip = ioptdensityCutoff(iiCutoff,3,isp,jsp)
                                   d2aux3_dxyz_dpara(alph4,jj,alph,alph2,alph3,ip,jsp) = & !(off site atomic coordinates)
                                      fjlijPrefact3(alph4,alph3,alph2,alph)*dfjlij_dpara(ip,3,jj,iatom) + &
                                      dfjlij_dxyzPrefact3(alph3,alph2,alph)*d2fjlij_dxyz_dpara(alph4,ip,3,jj,iatom)
                                   d2aux3_dxyz_dpara(alph4,0,alph,alph2,alph3,ip,jsp) = & !(on site atomic coordinates)
                                      d2aux3_dxyz_dpara(alph4,0,alph,alph2,alph3,ip,jsp) - &
                                      d2aux3_dxyz_dpara(alph4,jj,alph,alph2,alph3,ip,jsp)
                                enddo
                             enddo
                          enddo
                       enddo
                    enddo
                    ! ------------------------------------------------

                    !Derivatives of aux1 w.r.t parameters
                    ! --- currently debugging the following ----
                    !Coefficients:
                    do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                       ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                       fjlijFact=screen_over_rij3*dfjlij_dpara(ip,3,jj,iatom)
                       do alph=1,3       
                          do alph2=alph,3
                             do alph3=alph2,3
                                daux3_dpara(alph,alph2,alph3,ip,jsp) = daux3_dpara(alph,alph2,alph3,ip,jsp) + &
                                   fjlijFact*dalph(alph)*dalph(alph2)*dalph(alph3)
                             enddo
                          enddo
                       enddo
                    enddo
                    !Cutoffs:
                    do iiCutoff=1,noptdensityCutoff(3,isp,jsp)
                       ip = ioptdensityCutoff(iiCutoff,3,isp,jsp)
                       fjlijFact=screen_over_rij3*dfjlij_dpara(ip,3,jj,iatom)
                       do alph=1,3
                          do alph2=alph,3
                             do alph3=alph2,3
                                daux3_dpara(alph,alph2,alph3,ip,jsp) = daux3_dpara(alph,alph2,alph3,ip,jsp) + &
                                   fjlijFact*dalph(alph)*dalph(alph2)*dalph(alph3)
                             enddo
                          enddo
                       enddo
                    enddo
                    ! ------------------------------------------------

                 endif
                 !---------

              enddo  !sum over neighbors complete

              aux=aux3(1,1,1)**2+3d0*aux3(1,1,2)**2+3d0*aux3(1,1,3)**2+ &
                  3d0*aux3(1,2,2)**2+6d0*aux3(1,2,3)**2+3d0*aux3(1,3,3)**2+ &
                  aux3(2,2,2)**2+3d0*aux3(2,2,3)**2+3d0*aux3(2,3,3)**2+ &
                  aux3(3,3,3)**2

              if (orthogElecDens.eqv..false.) then

                 rhol(3,iatom)=sqrt(abs(aux))    !BASKES formula 8d PRB 46

                 !Calculate derivative of rhol w.r.t parameters
                 !Coefficients:
                 if (aux.ne.0d0) then !if =0, should have drhol_dpara=0,
                                           !however the eqns below will return
                                           !=NaN
                    do jsp=1,maxspecies
                       do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                          ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                          !Note, there is an abs() in the above equation for
                          !rhol(3,i).
                          !Hence below there is an extra factor d abs(aux) / d aux
                          != abs(aux)/aux below (so that 1/abs(aux) -> 1/aux)
                          drhol_dpara(ip,3,jsp,iatom) = (rhol(3,iatom)/aux) * &
                               ( aux3(1,1,1)*daux3_dpara(1,1,1,ip,jsp) + &
                                 3d0*aux3(1,1,2)*daux3_dpara(1,1,2,ip,jsp) + &
                                 3d0*aux3(1,1,3)*daux3_dpara(1,1,3,ip,jsp) + &
                                 3d0*aux3(1,2,2)*daux3_dpara(1,2,2,ip,jsp) + &
                                 6d0*aux3(1,2,3)*daux3_dpara(1,2,3,ip,jsp) + &
                                 3d0*aux3(1,3,3)*daux3_dpara(1,3,3,ip,jsp) + &
                                 aux3(2,2,2)*daux3_dpara(2,2,2,ip,jsp) + &
                                 3d0*aux3(2,2,3)*daux3_dpara(2,2,3,ip,jsp) + &
                                 3d0*aux3(2,3,3)*daux3_dpara(2,3,3,ip,jsp) + &
                                 aux3(3,3,3)*daux3_dpara(3,3,3,ip,jsp) )
                       enddo
                       do iiCutoff=1,noptdensityCutoff(3,isp,jsp)
                          ip = ioptdensityCutoff(iiCutoff,3,isp,jsp)
                          drhol_dpara(ip,3,jsp,iatom) = (rhol(3,iatom)/aux) * &
                               ( aux3(1,1,1)*daux3_dpara(1,1,1,ip,jsp) + &
                                 3d0*aux3(1,1,2)*daux3_dpara(1,1,2,ip,jsp) + &
                                 3d0*aux3(1,1,3)*daux3_dpara(1,1,3,ip,jsp) + &
                                 3d0*aux3(1,2,2)*daux3_dpara(1,2,2,ip,jsp) + &
                                 6d0*aux3(1,2,3)*daux3_dpara(1,2,3,ip,jsp) + &
                                 3d0*aux3(1,3,3)*daux3_dpara(1,3,3,ip,jsp) + &
                                 aux3(2,2,2)*daux3_dpara(2,2,2,ip,jsp) + &
                                 3d0*aux3(2,2,3)*daux3_dpara(2,2,3,ip,jsp) + &
                                 3d0*aux3(2,3,3)*daux3_dpara(2,3,3,ip,jsp) + &
                                 aux3(3,3,3)*daux3_dpara(3,3,3,ip,jsp) )
                       enddo
                    enddo

                    do alph=1,3
                       do jj=0,gn_neighbors(iatom,istr)
                          drhol_dxyz(alph,jj,3,iatom) = (1d0/sqrt(abs(aux))) * &
                             ( aux3(1,1,1)*daux3_dxyz(alph,jj,1,1,1) + 3d0*aux3(1,1,2)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*aux3(1,1,3)*daux3_dxyz(alph,jj,1,1,3) + 3d0*aux3(1,2,2)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*aux3(1,2,3)*daux3_dxyz(alph,jj,1,2,3) + 3d0*aux3(1,3,3)*daux3_dxyz(alph,jj,1,3,3) + &
                               aux3(2,2,2)*daux3_dxyz(alph,jj,2,2,2) + 3d0*aux3(2,2,3)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*aux3(2,3,3)*daux3_dxyz(alph,jj,2,3,3) + aux3(3,3,3)*daux3_dxyz(alph,jj,3,3,3) )
                       enddo
                    enddo

                    do alph=1,3
                       do jj=0,gn_neighbors(iatom,istr)

                          do jsp=1,maxspecies
                             do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                                ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                                d2rhol_dxyz_dpara(alph,jj,ip,3,jsp,iatom)= (-1d0/(abs(aux)**1.5d0))  * &
                             ( aux3(1,1,1)*daux3_dpara(1,1,1,ip,jsp) + 3d0*aux3(1,1,2)*daux3_dpara(1,1,2,ip,jsp) + &
                               3d0*aux3(1,1,3)*daux3_dpara(1,1,3,ip,jsp) + 3d0*aux3(1,2,2)*daux3_dpara(1,2,2,ip,jsp) + &
                               6d0*aux3(1,2,3)*daux3_dpara(1,2,3,ip,jsp) + 3d0*aux3(1,3,3)*daux3_dpara(1,3,3,ip,jsp) + &
                               aux3(2,2,2)*daux3_dpara(2,2,2,ip,jsp) + 3d0*aux3(2,2,3)*daux3_dpara(2,2,3,ip,jsp)+ &
                               3d0*aux3(2,3,3)*daux3_dpara(2,3,3,ip,jsp) + aux3(3,3,3)*daux3_dpara(3,3,3,ip,jsp) ) * &
                             ( aux3(1,1,1)*daux3_dxyz(alph,jj,1,1,1) + 3d0*aux3(1,1,2)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*aux3(1,1,3)*daux3_dxyz(alph,jj,1,1,3) + 3d0*aux3(1,2,2)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*aux3(1,2,3)*daux3_dxyz(alph,jj,1,2,3) + 3d0*aux3(1,3,3)*daux3_dxyz(alph,jj,1,3,3) + &
                               aux3(2,2,2)*daux3_dxyz(alph,jj,2,2,2) + 3d0*aux3(2,2,3)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*aux3(2,3,3)*daux3_dxyz(alph,jj,2,3,3) + aux3(3,3,3)*daux3_dxyz(alph,jj,3,3,3) ) &
                           + (1d0/sqrt(abs(aux))) * &
                             ( daux3_dpara(1,1,1,ip,jsp)*daux3_dxyz(alph,jj,1,1,1) + &
                               3d0*daux3_dpara(1,1,2,ip,jsp)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*daux3_dpara(1,1,3,ip,jsp)*daux3_dxyz(alph,jj,1,1,3) + &
                               3d0*daux3_dpara(1,2,2,ip,jsp)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*daux3_dpara(1,2,3,ip,jsp)*daux3_dxyz(alph,jj,1,2,3) + &
                               3d0*daux3_dpara(1,3,3,ip,jsp)*daux3_dxyz(alph,jj,1,3,3) + &
                               daux3_dpara(2,2,2,ip,jsp)*daux3_dxyz(alph,jj,2,2,2) + &
                               3d0*daux3_dpara(2,2,3,ip,jsp)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*daux3_dpara(2,3,3,ip,jsp)*daux3_dxyz(alph,jj,2,3,3) + &
                               daux3_dpara(3,3,3,ip,jsp)*daux3_dxyz(alph,jj,3,3,3) + &
                               aux3(1,1,1)*d2aux3_dxyz_dpara(alph,jj,1,1,1,ip,jsp) + &
                               3d0*aux3(1,1,2)*d2aux3_dxyz_dpara(alph,jj,1,1,2,ip,jsp) + &
                               3d0*aux3(1,1,3)*d2aux3_dxyz_dpara(alph,jj,1,1,3,ip,jsp) + &
                               3d0*aux3(1,2,2)*d2aux3_dxyz_dpara(alph,jj,1,2,2,ip,jsp) + &
                               6d0*aux3(1,2,3)*d2aux3_dxyz_dpara(alph,jj,1,2,3,ip,jsp) + &
                               3d0*aux3(1,3,3)*d2aux3_dxyz_dpara(alph,jj,1,3,3,ip,jsp) + &
                               aux3(2,2,2)*d2aux3_dxyz_dpara(alph,jj,2,2,2,ip,jsp) + &
                               3d0*aux3(2,2,3)*d2aux3_dxyz_dpara(alph,jj,2,2,3,ip,jsp)+ &
                               3d0*aux3(2,3,3)*d2aux3_dxyz_dpara(alph,jj,2,3,3,ip,jsp) + &
                               aux3(3,3,3)*d2aux3_dxyz_dpara(alph,jj,3,3,3,ip,jsp) )
                             enddo
                             do iiCutoff=1,noptdensityCutoff(3,isp,jsp)
                                ip = ioptdensityCutoff(iiCutoff,3,isp,jsp)
                                d2rhol_dxyz_dpara(alph,jj,ip,3,jsp,iatom)= (-1d0/(abs(aux)**1.5d0))  * &
                             ( aux3(1,1,1)*daux3_dpara(1,1,1,ip,jsp) + 3d0*aux3(1,1,2)*daux3_dpara(1,1,2,ip,jsp) + &
                               3d0*aux3(1,1,3)*daux3_dpara(1,1,3,ip,jsp) + 3d0*aux3(1,2,2)*daux3_dpara(1,2,2,ip,jsp) + &
                               6d0*aux3(1,2,3)*daux3_dpara(1,2,3,ip,jsp) + 3d0*aux3(1,3,3)*daux3_dpara(1,3,3,ip,jsp) + &
                               aux3(2,2,2)*daux3_dpara(2,2,2,ip,jsp) + 3d0*aux3(2,2,3)*daux3_dpara(2,2,3,ip,jsp)+ &
                               3d0*aux3(2,3,3)*daux3_dpara(2,3,3,ip,jsp) + aux3(3,3,3)*daux3_dpara(3,3,3,ip,jsp) ) * &
                             ( aux3(1,1,1)*daux3_dxyz(alph,jj,1,1,1) + 3d0*aux3(1,1,2)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*aux3(1,1,3)*daux3_dxyz(alph,jj,1,1,3) + 3d0*aux3(1,2,2)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*aux3(1,2,3)*daux3_dxyz(alph,jj,1,2,3) + 3d0*aux3(1,3,3)*daux3_dxyz(alph,jj,1,3,3) + &
                               aux3(2,2,2)*daux3_dxyz(alph,jj,2,2,2) + 3d0*aux3(2,2,3)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*aux3(2,3,3)*daux3_dxyz(alph,jj,2,3,3) + aux3(3,3,3)*daux3_dxyz(alph,jj,3,3,3) ) &
                           + (1d0/sqrt(abs(aux))) * &
                             ( daux3_dpara(1,1,1,ip,jsp)*daux3_dxyz(alph,jj,1,1,1) + &
                               3d0*daux3_dpara(1,1,2,ip,jsp)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*daux3_dpara(1,1,3,ip,jsp)*daux3_dxyz(alph,jj,1,1,3) + &
                               3d0*daux3_dpara(1,2,2,ip,jsp)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*daux3_dpara(1,2,3,ip,jsp)*daux3_dxyz(alph,jj,1,2,3) + &
                               3d0*daux3_dpara(1,3,3,ip,jsp)*daux3_dxyz(alph,jj,1,3,3) + &
                               daux3_dpara(2,2,2,ip,jsp)*daux3_dxyz(alph,jj,2,2,2) + &
                               3d0*daux3_dpara(2,2,3,ip,jsp)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*daux3_dpara(2,3,3,ip,jsp)*daux3_dxyz(alph,jj,2,3,3) + &
                               daux3_dpara(3,3,3,ip,jsp)*daux3_dxyz(alph,jj,3,3,3) + &
                               aux3(1,1,1)*d2aux3_dxyz_dpara(alph,jj,1,1,1,ip,jsp) + &
                               3d0*aux3(1,1,2)*d2aux3_dxyz_dpara(alph,jj,1,1,2,ip,jsp) + &
                               3d0*aux3(1,1,3)*d2aux3_dxyz_dpara(alph,jj,1,1,3,ip,jsp) + &
                               3d0*aux3(1,2,2)*d2aux3_dxyz_dpara(alph,jj,1,2,2,ip,jsp) + &
                               6d0*aux3(1,2,3)*d2aux3_dxyz_dpara(alph,jj,1,2,3,ip,jsp) + &
                               3d0*aux3(1,3,3)*d2aux3_dxyz_dpara(alph,jj,1,3,3,ip,jsp) + &
                               aux3(2,2,2)*d2aux3_dxyz_dpara(alph,jj,2,2,2,ip,jsp) + &
                               3d0*aux3(2,2,3)*d2aux3_dxyz_dpara(alph,jj,2,2,3,ip,jsp)+ &
                               3d0*aux3(2,3,3)*d2aux3_dxyz_dpara(alph,jj,2,3,3,ip,jsp) + &
                               aux3(3,3,3)*d2aux3_dxyz_dpara(alph,jj,3,3,3,ip,jsp) )
                             enddo
                          enddo
                       enddo
                    enddo
                 endif

              else
                 !Updated formula (to get orthogonal elec densities, I.e. elec
                 !densities that are Legendre polynomials) - formula Mat Sci and
                 !Eng A261 165
                 sum_aux3aSqr=aux3a(1)**2+aux3a(2)**2+aux3a(3)**2
                 rhol(3,iatom)=sqrt(abs(aux-0.6d0*sum_aux3aSqr))

                 !Calculate derivative of rhol w.r.t parameters
                 !Coefficients:
                 if ((aux-0.6d0*sum_aux3aSqr).ne.0d0) then

                    do jsp=1,maxspecies
                       do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                          ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                          drhol_dpara(ip,3,jsp,iatom) = (rhol(3,iatom)/(aux-0.6d0*sum_aux3aSqr)) * &
                               ( aux3(1,1,1)*daux3_dpara(1,1,1,ip,jsp) + &
                                 3d0*aux3(1,1,2)*daux3_dpara(1,1,2,ip,jsp) + &
                                 3d0*aux3(1,1,3)*daux3_dpara(1,1,3,ip,jsp) + &
                                 3d0*aux3(1,2,2)*daux3_dpara(1,2,2,ip,jsp) + &
                                 6d0*aux3(1,2,3)*daux3_dpara(1,2,3,ip,jsp) + &
                                 3d0*aux3(1,3,3)*daux3_dpara(1,3,3,ip,jsp) + &
                                 aux3(2,2,2)*daux3_dpara(2,2,2,ip,jsp) + &
                                 3d0*aux3(2,2,3)*daux3_dpara(2,2,3,ip,jsp) + &
                                 3d0*aux3(2,3,3)*daux3_dpara(2,3,3,ip,jsp) + &
                                 aux3(3,3,3)*daux3_dpara(3,3,3,ip,jsp) - &
                                 0.6d0*( aux3a(1)*daux3a_dpara(1,ip,jsp) + &
                                         aux3a(2)*daux3a_dpara(2,ip,jsp) + &
                                         aux3a(3)*daux3a_dpara(3,ip,jsp) ) )
                       enddo
                       do iiCutoff=1,noptdensityCutoff(3,isp,jsp)
                          ip = ioptdensityCutoff(iiCutoff,3,isp,jsp)
                             drhol_dpara(ip,3,jsp,iatom) = (rhol(3,iatom)/(aux-0.6d0*sum_aux3aSqr)) * &
                               ( aux3(1,1,1)*daux3_dpara(1,1,1,ip,jsp) + &
                                 3d0*aux3(1,1,2)*daux3_dpara(1,1,2,ip,jsp) + &
                                 3d0*aux3(1,1,3)*daux3_dpara(1,1,3,ip,jsp) + &
                                 3d0*aux3(1,2,2)*daux3_dpara(1,2,2,ip,jsp) + &
                                 6d0*aux3(1,2,3)*daux3_dpara(1,2,3,ip,jsp) + &
                                 3d0*aux3(1,3,3)*daux3_dpara(1,3,3,ip,jsp) + &
                                 aux3(2,2,2)*daux3_dpara(2,2,2,ip,jsp) + &
                                 3d0*aux3(2,2,3)*daux3_dpara(2,2,3,ip,jsp) + &
                                 3d0*aux3(2,3,3)*daux3_dpara(2,3,3,ip,jsp) + &
                                 aux3(3,3,3)*daux3_dpara(3,3,3,ip,jsp) - &
                                 0.6d0*( aux3a(1)*daux3a_dpara(1,ip,jsp) + &
                                         aux3a(2)*daux3a_dpara(2,ip,jsp) + &
                                         aux3a(3)*daux3a_dpara(3,ip,jsp) ) )
                       enddo
                    enddo

                    do alph=1,3
                       do jj=0,gn_neighbors(iatom,istr)
                          drhol_dxyz(alph,jj,3,iatom) = (1d0/sqrt(abs(aux-0.6d0*sum_aux3aSqr))) * &
                             ( aux3(1,1,1)*daux3_dxyz(alph,jj,1,1,1) + 3d0*aux3(1,1,2)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*aux3(1,1,3)*daux3_dxyz(alph,jj,1,1,3) + 3d0*aux3(1,2,2)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*aux3(1,2,3)*daux3_dxyz(alph,jj,1,2,3) + 3d0*aux3(1,3,3)*daux3_dxyz(alph,jj,1,3,3) + &
                               aux3(2,2,2)*daux3_dxyz(alph,jj,2,2,2) + 3d0*aux3(2,2,3)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*aux3(2,3,3)*daux3_dxyz(alph,jj,2,3,3) + aux3(3,3,3)*daux3_dxyz(alph,jj,3,3,3) - & 
                               0.6d0* ( aux3a(1)*daux3a_dxyz(alph,jj,1) + &
                                        aux3a(2)*daux3a_dxyz(alph,jj,2) + &
                                        aux3a(3)*daux3a_dxyz(alph,jj,3) ) ) 
                       enddo
                    enddo

                    do alph=1,3
                       do jj=0,gn_neighbors(iatom,istr)

                          do jsp=1,maxspecies
                             do iiCoeff=1,noptdensityCoeff(3,isp,jsp)
                                ip = ioptdensityCoeff(iiCoeff,3,isp,jsp)
                                d2rhol_dxyz_dpara(alph,jj,ip,3,jsp,iatom)= (-1d0/(abs(aux-0.6d0*sum_aux3aSqr)**1.5d0))  * &
                             ( aux3(1,1,1)*daux3_dpara(1,1,1,ip,jsp) + 3d0*aux3(1,1,2)*daux3_dpara(1,1,2,ip,jsp) + &
                               3d0*aux3(1,1,3)*daux3_dpara(1,1,3,ip,jsp) + 3d0*aux3(1,2,2)*daux3_dpara(1,2,2,ip,jsp) + &
                               6d0*aux3(1,2,3)*daux3_dpara(1,2,3,ip,jsp) + 3d0*aux3(1,3,3)*daux3_dpara(1,3,3,ip,jsp) + &
                               aux3(2,2,2)*daux3_dpara(2,2,2,ip,jsp) + 3d0*aux3(2,2,3)*daux3_dpara(2,2,3,ip,jsp)+ &
                               3d0*aux3(2,3,3)*daux3_dpara(2,3,3,ip,jsp) + aux3(3,3,3)*daux3_dpara(3,3,3,ip,jsp) - &
                               0.6d0*( aux3a(1)*daux3a_dpara(1,ip,jsp) + &
                                         aux3a(2)*daux3a_dpara(2,ip,jsp) + &
                                         aux3a(3)*daux3a_dpara(3,ip,jsp) ) ) * &
                             ( aux3(1,1,1)*daux3_dxyz(alph,jj,1,1,1) + 3d0*aux3(1,1,2)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*aux3(1,1,3)*daux3_dxyz(alph,jj,1,1,3) + 3d0*aux3(1,2,2)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*aux3(1,2,3)*daux3_dxyz(alph,jj,1,2,3) + 3d0*aux3(1,3,3)*daux3_dxyz(alph,jj,1,3,3) + &
                               aux3(2,2,2)*daux3_dxyz(alph,jj,2,2,2) + 3d0*aux3(2,2,3)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*aux3(2,3,3)*daux3_dxyz(alph,jj,2,3,3) + aux3(3,3,3)*daux3_dxyz(alph,jj,3,3,3) - &
                               0.6d0* ( aux3a(1)*daux3a_dxyz(alph,jj,1) + &
                                        aux3a(2)*daux3a_dxyz(alph,jj,2) + &
                                        aux3a(3)*daux3a_dxyz(alph,jj,3) ) ) &
                           + (1d0/sqrt(abs(aux-0.6d0*sum_aux3aSqr))) * &
                             ( daux3_dpara(1,1,1,ip,jsp)*daux3_dxyz(alph,jj,1,1,1) + &
                               3d0*daux3_dpara(1,1,2,ip,jsp)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*daux3_dpara(1,1,3,ip,jsp)*daux3_dxyz(alph,jj,1,1,3) + &
                               3d0*daux3_dpara(1,2,2,ip,jsp)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*daux3_dpara(1,2,3,ip,jsp)*daux3_dxyz(alph,jj,1,2,3) + &
                               3d0*daux3_dpara(1,3,3,ip,jsp)*daux3_dxyz(alph,jj,1,3,3) + &
                               daux3_dpara(2,2,2,ip,jsp)*daux3_dxyz(alph,jj,2,2,2) + &
                               3d0*daux3_dpara(2,2,3,ip,jsp)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*daux3_dpara(2,3,3,ip,jsp)*daux3_dxyz(alph,jj,2,3,3) + &
                               daux3_dpara(3,3,3,ip,jsp)*daux3_dxyz(alph,jj,3,3,3) + &
                               aux3(1,1,1)*d2aux3_dxyz_dpara(alph,jj,1,1,1,ip,jsp) + &
                               3d0*aux3(1,1,2)*d2aux3_dxyz_dpara(alph,jj,1,1,2,ip,jsp) + &
                               3d0*aux3(1,1,3)*d2aux3_dxyz_dpara(alph,jj,1,1,3,ip,jsp) + &
                               3d0*aux3(1,2,2)*d2aux3_dxyz_dpara(alph,jj,1,2,2,ip,jsp) + &
                               6d0*aux3(1,2,3)*d2aux3_dxyz_dpara(alph,jj,1,2,3,ip,jsp) + &
                               3d0*aux3(1,3,3)*d2aux3_dxyz_dpara(alph,jj,1,3,3,ip,jsp) + &
                               aux3(2,2,2)*d2aux3_dxyz_dpara(alph,jj,2,2,2,ip,jsp) + &
                               3d0*aux3(2,2,3)*d2aux3_dxyz_dpara(alph,jj,2,2,3,ip,jsp)+ &
                               3d0*aux3(2,3,3)*d2aux3_dxyz_dpara(alph,jj,2,3,3,ip,jsp) + &
                               aux3(3,3,3)*d2aux3_dxyz_dpara(alph,jj,3,3,3,ip,jsp)  - &
                               0.6d0* ( daux3a_dpara(1,ip,jsp)*daux3a_dxyz(alph,jj,1) + &
                                        daux3a_dpara(2,ip,jsp)*daux3a_dxyz(alph,jj,2) + &
                                        daux3a_dpara(3,ip,jsp)*daux3a_dxyz(alph,jj,3) + &
                                        aux3a(1)*d2aux3a_dxyz_dpara(alph,jj,1,ip,jsp) + &
                                        aux3a(2)*d2aux3a_dxyz_dpara(alph,jj,2,ip,jsp) + &
                                        aux3a(3)*d2aux3a_dxyz_dpara(alph,jj,3,ip,jsp) ) )
                             enddo
                             do iiCutoff=1,noptdensityCutoff(1,isp,jsp)
                                ip = ioptdensityCutoff(iiCutoff,1,isp,jsp)
                                d2rhol_dxyz_dpara(alph,jj,ip,3,jsp,iatom)= (-1d0/(abs(aux-0.6d0*sum_aux3aSqr)**1.5d0))  * &
                             ( aux3(1,1,1)*daux3_dpara(1,1,1,ip,jsp) + 3d0*aux3(1,1,2)*daux3_dpara(1,1,2,ip,jsp) + &
                               3d0*aux3(1,1,3)*daux3_dpara(1,1,3,ip,jsp) + 3d0*aux3(1,2,2)*daux3_dpara(1,2,2,ip,jsp) + &
                               6d0*aux3(1,2,3)*daux3_dpara(1,2,3,ip,jsp) + 3d0*aux3(1,3,3)*daux3_dpara(1,3,3,ip,jsp) + &
                               aux3(2,2,2)*daux3_dpara(2,2,2,ip,jsp) + 3d0*aux3(2,2,3)*daux3_dpara(2,2,3,ip,jsp)+ &
                               3d0*aux3(2,3,3)*daux3_dpara(2,3,3,ip,jsp) + aux3(3,3,3)*daux3_dpara(3,3,3,ip,jsp) - &
                               0.6d0*( aux3a(1)*daux3a_dpara(1,ip,jsp) + &
                                         aux3a(2)*daux3a_dpara(2,ip,jsp) + &
                                         aux3a(3)*daux3a_dpara(3,ip,jsp) ) ) * &
                             ( aux3(1,1,1)*daux3_dxyz(alph,jj,1,1,1) + 3d0*aux3(1,1,2)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*aux3(1,1,3)*daux3_dxyz(alph,jj,1,1,3) + 3d0*aux3(1,2,2)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*aux3(1,2,3)*daux3_dxyz(alph,jj,1,2,3) + 3d0*aux3(1,3,3)*daux3_dxyz(alph,jj,1,3,3) + &
                               aux3(2,2,2)*daux3_dxyz(alph,jj,2,2,2) + 3d0*aux3(2,2,3)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*aux3(2,3,3)*daux3_dxyz(alph,jj,2,3,3) + aux3(3,3,3)*daux3_dxyz(alph,jj,3,3,3) - &
                               0.6d0* ( aux3a(1)*daux3a_dxyz(alph,jj,1) + &
                                        aux3a(2)*daux3a_dxyz(alph,jj,2) + &
                                        aux3a(3)*daux3a_dxyz(alph,jj,3) ) ) &
                           + (1d0/sqrt(abs(aux-0.6d0*sum_aux3aSqr))) * &
                             ( daux3_dpara(1,1,1,ip,jsp)*daux3_dxyz(alph,jj,1,1,1) + &
                               3d0*daux3_dpara(1,1,2,ip,jsp)*daux3_dxyz(alph,jj,1,1,2) + &
                               3d0*daux3_dpara(1,1,3,ip,jsp)*daux3_dxyz(alph,jj,1,1,3) + &
                               3d0*daux3_dpara(1,2,2,ip,jsp)*daux3_dxyz(alph,jj,1,2,2) + &
                               6d0*daux3_dpara(1,2,3,ip,jsp)*daux3_dxyz(alph,jj,1,2,3) + &
                               3d0*daux3_dpara(1,3,3,ip,jsp)*daux3_dxyz(alph,jj,1,3,3) + &
                               daux3_dpara(2,2,2,ip,jsp)*daux3_dxyz(alph,jj,2,2,2) + &
                               3d0*daux3_dpara(2,2,3,ip,jsp)*daux3_dxyz(alph,jj,2,2,3)+ &
                               3d0*daux3_dpara(2,3,3,ip,jsp)*daux3_dxyz(alph,jj,2,3,3) + &
                               daux3_dpara(3,3,3,ip,jsp)*daux3_dxyz(alph,jj,3,3,3) + &
                               aux3(1,1,1)*d2aux3_dxyz_dpara(alph,jj,1,1,1,ip,jsp) + &
                               3d0*aux3(1,1,2)*d2aux3_dxyz_dpara(alph,jj,1,1,2,ip,jsp) + &
                               3d0*aux3(1,1,3)*d2aux3_dxyz_dpara(alph,jj,1,1,3,ip,jsp) + &
                               3d0*aux3(1,2,2)*d2aux3_dxyz_dpara(alph,jj,1,2,2,ip,jsp) + &
                               6d0*aux3(1,2,3)*d2aux3_dxyz_dpara(alph,jj,1,2,3,ip,jsp) + &
                               3d0*aux3(1,3,3)*d2aux3_dxyz_dpara(alph,jj,1,3,3,ip,jsp) + &
                               aux3(2,2,2)*d2aux3_dxyz_dpara(alph,jj,2,2,2,ip,jsp) + &
                               3d0*aux3(2,2,3)*d2aux3_dxyz_dpara(alph,jj,2,2,3,ip,jsp)+ &
                               3d0*aux3(2,3,3)*d2aux3_dxyz_dpara(alph,jj,2,3,3,ip,jsp) + &
                               aux3(3,3,3)*d2aux3_dxyz_dpara(alph,jj,3,3,3,ip,jsp)  - &
                               0.6d0* ( daux3a_dpara(1,ip,jsp)*daux3a_dxyz(alph,jj,1) + &
                                        daux3a_dpara(2,ip,jsp)*daux3a_dxyz(alph,jj,2) + &
                                        daux3a_dpara(3,ip,jsp)*daux3a_dxyz(alph,jj,3) + &
                                        aux3a(1)*d2aux3a_dxyz_dpara(alph,jj,1,ip,jsp) + &
                                        aux3a(2)*d2aux3a_dxyz_dpara(alph,jj,2,ip,jsp) + &
                                        aux3a(3)*d2aux3a_dxyz_dpara(alph,jj,3,ip,jsp) ) )
                             enddo
                          enddo
                       enddo
                    enddo
                 endif

              endif

          enddo !sum over atoms

          !Print out analytic derivatives

          ! do iatom=1,gn_inequivalentsites(istr)
          !    do jj=0,gn_neighbors(iatom,istr)
          !       do alph=1,3
          !           print *,'drhol_dxyz(',alph,',jj=',jj,',3,',iatom,')=',drhol_dxyz(alph,jj,3,iatom)
          !       enddo
          !    enddo
          ! enddo

       endif
       ! ------------------------------------------------------------------------------


       !deallocate(daux1_dxyz)

    endif

end subroutine electrondensityForce

