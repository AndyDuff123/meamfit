subroutine signRadFuncs

    !--------------------------------------------------------------c
    !
    !     Initializes the signs (positve+negative, or just positve)
    !     that the radial functions are allowed to take
    !
    !     Called by: MEAMfit
    !     Calls:
    !     Returns:
    !     Files read:
    !     Files written:
    !
    !     Andy Duff, 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
!    use m_filenames
    use m_atomproperties
!    use m_generalinfo
    use m_optimization
!    use m_geometry

    implicit none

    integer i,ii,ll,isp,jsp,spc_cnt,l

    allocate( meamrhodecay_negvals(0:lmax,maxspecies,maxspecies), &
        pairpotparameter_negvals(maxspecies,maxspecies) )

    !electron density functions
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                !Check whether coefficients of thi(isp,jsp,0) are allowed to 
                !be negative (the default only applies to l=0 because l>0
                !contributions can never result in a negative background
                !density, no matter whether
                if (typethi(isp,jsp).eq.1) then !Cubic form
                    meamrhodecay_negvals(0,isp,jsp)=meamrhodecay_negvals_default
                else
                   print *,'ERROR, typethi(',isp,',',jsp,') =',typethi(isp,jsp),' /= 1'
                   stop
                endif
                !note, for l>0, parameters can always be positive or negative
                do ll=1,lmax
                    if (typethi(isp,jsp).eq.1) then !Cubic form
                        meamrhodecay_negvals(ll,isp,jsp)=2
                    else
                       print *,'ERROR, typethi(',isp,',',jsp,') =',typethi(isp,jsp),' /= 1'
                       stop
                    endif
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !pair-potentials
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                !Check if we need to generate random paramaters for
                !pairpot(isp,jsp)
                if (typepairpot(isp,jsp).eq.2) then
                    pairpotparameter_negvals(isp,jsp)=pairpotparameter_negvals_default
                else
                    print *,'typepairpot=',typepairpot(isp,jsp), &
                        'not supported,stopping'
                    stop
                endif
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

end subroutine signRadFuncs
