subroutine meamenergy

    !--------------------------------------------------------------c
    !
    !     Computes the MEAM energy for structure, istr (global
    !     variable).
    !
    !     NOTE: do not use this if the structure has been set up
    !     for force-fitting. (c.f. 'meamforces')
    !
    !     Called by:     Fnew, meamforce
    !     Calls:         readmeamparam,screening_ij,
    !                 radialdensityfunction,
    !                 radialdensityfunction_onlyiatom
    !                 electrondensity,
    !                 backgrounddensity,embeddingfunction,
    !                 pairpotential,distance
    !     Arguments:     is,gn_inequivalentsites,gspecies,
    !                 gn_neighbors,gneighborlist,screening
    !     Returns:       meam_energy
    !     Files read:    none
    !     Files written: none
    !
    !     Andrew Duff and Marcel Sluiter.
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_geometry
    use m_screening   !S_ij
    use m_filenames
    use m_electrondensity!can remove
    use m_generalinfo
    use m_atomproperties !AnalyticDeriv
    use m_optimization
    use m_observables
    use m_objectiveFunction

    implicit none

    integer i,j,jj,ll,isp,jsp,iiCoeff,iiCutoff,ip
    integer k,iat !delete
    integer ipara,ispecies1,ispecies2 !AnalyticDeriv
    real(8) pairpot,SI_to_eVamg
    real(8) summeam_paire_disp !AnalyticDeriv
    !real(8), allocatable:: rho_i_backup(:), meam_f_backup(:)
    !real(8), allocatable:: rhol_prev(:,:) !delete
    !real(8), allocatable:: meam_t_prev(:,:) !delete

    parameter(SI_to_eVamg=14.4375d0)
    energy=0d0
    call screening_ij

    !print *,'pairpotential:'
    if (useSplines.eqv..true.) then
       print *,'WARNING: Using splines for some or all pair-potentials - currently in testing phase'
       call pairpotentialSpline
    else
       call pairpotential
    endif

    !write(*,'(A9,F25.20)') 'energy(1)',meam_energy
  ! print *,'dsummeam_paire_dpara (analytic)=',dsummeam_paire_dpara!AnalyticDeriv
  ! !print *,'dsummeam_paire_dpara(3,1,1) (analytic)=',dsummeam_paire_dpara(3,1,1)

  ! !-------- enclosed code AnalyticDeriv START ----
  ! do ipara=1,32 !AnalyticDeriv
  !    do ispecies1=1,maxspecies !AnalyticDeriv
  !       do ispecies2=ispecies1,maxspecies !AnalyticDeriv

  !       summeam_paire=0d0
  !       !Non-displaced
  !       do i=1,gn_inequivalentsites(istr)
  !           isp=gspecies(i,istr)
  !           meam_paire(i)=0d0
  !           do jj=1,gn_neighbors(i,istr)
  !               j=gneighborlist(jj,i,istr)
  !               jsp=gspecies(j,istr)
  !               rij=diststr(jj,i,0,0,istr)
  !         !rij=1.4d0  !0.9-1.5
  !               call pairpotential(isp,jsp,rij, &
  !                   pairpot,dsummeam_paire_dpara) !AnalyticDeriv
  !               meam_paire(i)=meam_paire(i)+pairpot* &
  !                   screening(jj,i)
  !           enddo
  !           meam_paire(i)=meam_paire(i)*0.5d0
  !           summeam_paire=summeam_paire+meam_paire(i)
  !           energy=energy+meam_paire(i)
  !       enddo
  !       pairpotparameter(ipara,ispecies1,ispecies2)=pairpotparameter(ipara,ispecies1,ispecies2)+0.000001d0
  !       summeam_paire_disp=0d0
  !       !Non-displaced
  !       do i=1,gn_inequivalentsites(istr)
  !           isp=gspecies(i,istr)
  !           meam_paire(i)=0d0
  !           do jj=1,gn_neighbors(i,istr)
  !               j=gneighborlist(jj,i,istr)
  !               jsp=gspecies(j,istr)
  !               rij=diststr(jj,i,0,0,istr)
  !         !rij=1.4d0  !0.9-1.5
  !               call pairpotential(isp,jsp,rij, &
  !                   pairpot,dsummeam_paire_dpara) !AnalyticDeriv
  !               meam_paire(i)=meam_paire(i)+pairpot* &
  !                   screening(jj,i)
  !           enddo
  !           meam_paire(i)=meam_paire(i)*0.5d0
  !           summeam_paire_disp=summeam_paire_disp+meam_paire(i)
  !           energy=energy+meam_paire(i)
  !       enddo

  !       pairpotparameter(ipara,ispecies1,ispecies2)=pairpotparameter(ipara,ispecies1,ispecies2)-0.000001d0
  !       !print *,'summeam_paire_disp=',summeam_paire_disp,', summeam_paire=',summeam_paire
  !       print *,'dsummeam_paire_dpara(',ipara,',',ispecies1,',',ispecies2,') (numerical)=',(summeam_paire_disp-summeam_paire)/0.000001d0

  !       enddo
  !    enddo
  ! enddo
  ! stop
  ! !-------- enclosed code AnalyticDeriv ENDS -----

    !print *,'radialdensityfunction:'
    if (useSplines.eqv..true.) then
       print *,'WARNING: Using splines for some or all pair-potentials - currently in testing phase'
       call radialdensityfunctionSpline
    else
       call radialdensityfunction
    endif
    !if ((noOpt.eqv..false.).and.(lowDensPen)) then
    if ((noOpt.eqv..false.).and.(negElecDensAllow.eqv..false.)) then
       call checkSignDensities
       if (negElecDens) then
          !print *,'negative electron density encountered'
          return
       else
          !print *,'no negative elec dens'
       endif
    endif

    !print *,'electrondensity'
    call electrondensity
    if (negBackDens) then
       !print *,'negative density encountered'
       return
    endif

    !print *,'analytic derivatives:'
    !l=0:
    !do iat=1,gn_inequivalentsites(istr)
    !   print *,'rhol(0,1)=',rhol(0,1),', drhol_dpara(1:4,l=0,jsp=1,iat=',iat,')=',drhol_dpara(1:4,0,1,iat)
    !   print *,'          drhol_dpara(1:4,l=0,jsp=2,iat=1)=',drhol_dpara(1:4,0,2,iat)
    !enddo
    !l=1:
    !do iat=1,gn_inequivalentsites(istr)
    !   print *,'rhol(1,1)=',rhol(1,1),', drhol_dpara(1:4,l=1,jsp=1,iat=',iat,')=',drhol_dpara(1:4,1,1,iat)
    !   print *,' drhol_dpara(1:4,l=1,jsp=2,iat=1)=',drhol_dpara(1:4,1,2,iat)
    !enddo
    !print *,'rhol(0,1)=',rhol(0,1),', drhol_dpara(1:4,l=0,jsp=1,iat=1)=',drhol_dpara(1:4,0,1,1)
    !do iat=1,gn_inequivalentsites(istr)
    !   print *,'dmeamt_dmeamtau(l=1,jsp=1,iat=',iat,')=',dmeamt_dmeamtau(1,1,iat), &
    !           'dmeamt_dmeamtau(l=1,jsp=2,iat=',iat,')=',dmeamt_dmeamtau(1,2,iat)
    !   print *,'dmeamt_dpara(1:12,l=1,jsp=1,iat=',iat,')=',dmeamt_dpara(1:12,1,1,iat), &
    !           ' dmeamt_dpara(1:12,l=1,jsp=2,iat=',iat,')=',dmeamt_dpara(1:12,1,2,iat)
    !enddo
    !print *,'meam_t(l=1,iat=1)=',meam_t(1,1),', dmeamt_dpara(1:12,l=1,jsp=1,iat=1)=',dmeamt_dpara(1:12,1,1,1)
    !l=2:
    !do iat=1,gn_inequivalentsites(istr)
    !   print *,'drhol_dpara(1:4,l=2,jsp=1,iat=',iat,')=',drhol_dpara(1:4,2,1,iat)
    !   print *,'drhol_dpara(1:4,l=2,jsp=2,iat=',iat,')=',drhol_dpara(1:4,2,2,iat)
    !enddo
    !do iat=1,gn_inequivalentsites(istr)
    !   print *,'dmeamt_dmeamtau(l=2,jsp=1,iat=',iat,')=',dmeamt_dmeamtau(2,1,iat), &
    !           'dmeamt_dmeamtau(l=2,jsp=2,iat=',iat,')=',dmeamt_dmeamtau(2,2,iat)
    !   print *,'dmeamt_dpara(1:4,l=2,jsp=1,iat=',iat,')=',dmeamt_dpara(1:4,2,1,iat), &
    !           ' dmeamt_dpara(1:4,l=2,jsp=2,iat=',iat,')=',dmeamt_dpara(1:4,2,2,iat)
    !enddo
    !l=3:
    !do iat=1,gn_inequivalentsites(istr)
    !   print *,'drhol_dpara(1:4,l=3,jsp=1,iat=',iat,')=',drhol_dpara(1:4,3,1,iat)
    !   print *,'drhol_dpara(1:4,l=3,jsp=2,iat=',iat,')=',drhol_dpara(1:4,3,2,iat)
    !enddo
    !do iat=1,gn_inequivalentsites(istr)
    !   print *,'dmeamt_dmeamtau(l=3,jsp=1,iat=',iat,')=',dmeamt_dmeamtau(3,1,iat), &
    !           'dmeamt_dmeamtau(l=3,jsp=2,iat=',iat,')=',dmeamt_dmeamtau(3,2,iat)
    !   print *,'dmeamt_dpara(1:4,l=3,jsp=1,iat=',iat,')=',dmeamt_dpara(1:4,3,1,iat), &
    !           ' dmeamt_dpara(1:4,l=3,jsp=2,iat=',iat,')=',dmeamt_dpara(1:4,3,2,iat)
    !enddo

    !if(allocated(rhol_prev)) deallocate(rhol_prev)
    !allocate( rhol_prev(0:lmax,gn_inequivalentsites(istr)) )
    !if(allocated(meam_t_prev)) deallocate(meam_t_prev)
    !allocate( meam_t_prev(1:lmax,gn_inequivalentsites(istr)) )
    !rhol_prev=rhol
    !meam_t_prev=meam_t
    !print *,'numerical derivatives:'
    !print *
    ! print *,'l=0:'
    ! do iat=1,gn_inequivalentsites(istr)

    !    do k=1,4
    !       meamrhodecay(k,0,1,1)=meamrhodecay(k,0,1,1)+0.000001d0
    !       call radialdensityfunction
    !       call electrondensity(0,0)
    !       meamrhodecay(k,0,1,1)=meamrhodecay(k,0,1,1)-0.000001d0
    !       print *,'drhol_dpara(',k,',l=0,jsp=1,iat=',iat,')=',(rhol(0,iat)-rhol_prev(0,iat))/0.000001d0
    !    enddo
    !    exit
    !    do k=1,4
    !       meamrhodecay(k,0,1,2)=meamrhodecay(k,0,1,2)+0.000001d0
    !       call radialdensityfunction
    !       call electrondensity(0,0)
    !       meamrhodecay(k,0,1,2)=meamrhodecay(k,0,1,2)-0.000001d0
    !       print *,'drhol_dpara(',k,',l=0,jsp=2,iat=',iat,')=',(rhol(0,iat)-rhol_prev(0,iat))/0.000001d0
    !    enddo

    ! enddo
    ! print *
    ! print *,'l=1:'
    ! do iat=1,gn_inequivalentsites(istr)

     ! do k=1,4
     !    meamrhodecay(k,1,1,1)=meamrhodecay(k,1,1,1)+0.000001d0
     !    call radialdensityfunction
     !    call electrondensity(0,0)
     !    meamrhodecay(k,1,1,1)=meamrhodecay(k,1,1,1)-0.000001d0
     !    print *,'drhol_dpara(',k,',l=1,jsp=1,iat=',iat,')=',(rhol(1,iat)-rhol_prev(1,iat))/0.000001d0
     ! enddo
     ! do k=1,4
     !    meamrhodecay(k,1,1,2)=meamrhodecay(k,1,1,2)+0.000001d0
     !    call radialdensityfunction
     !    call electrondensity(0,0)
     !    meamrhodecay(k,1,1,2)=meamrhodecay(k,1,1,2)-0.000001d0
     !    print *,'drhol_dpara(',k,',l=1,jsp=2,iat=',iat,')=',(rhol(1,iat)-rhol_prev(1,iat))/0.000001d0
     ! enddo
     ! meamtau(1,1)=meamtau(1,1)+0.000001d0
     ! call radialdensityfunction
     ! call electrondensity(0,0)
     ! meamtau(1,1)=meamtau(1,1)-0.000001d0
     ! print *,'dmeamt_dmeamtau(l=1,jsp=1,iat=',iat,')=',(meam_t(1,iat)-meam_t_prev(1,iat))/0.000001d0
     ! !print *,'meam_t(1,',iat,')=',meam_t(1,iat),', meam_t_prev(1,',iat,')=',meam_t_prev(1,iat)
     ! !stop
     ! meamtau(1,2)=meamtau(1,2)+0.000001d0
     ! call radialdensityfunction
     ! call electrondensity(0,0)
     ! meamtau(1,2)=meamtau(1,2)-0.000001d0
     ! print *,'dmeamt_dmeamtau(l=1,jsp=2,iat=',iat,')=',(meam_t(1,iat)-meam_t_prev(1,iat))/0.000001d0
     ! do k=1,4
     !    meamrhodecay(k,0,1,1)=meamrhodecay(k,0,1,1)+0.000001d0
     !    call radialdensityfunction
     !    call electrondensity(0,0)
     !    meamrhodecay(k,0,1,1)=meamrhodecay(k,0,1,1)-0.000001d0
     !    print *,'dmeamt_dpara(',k,',l=1,jsp=1,iat=',iat,')=',(meam_t(1,iat)-meam_t_prev(1,iat))/0.000001d0
     ! enddo
     ! exit
     ! do k=1,4
     !    meamrhodecay(k,0,1,2)=meamrhodecay(k,0,1,2)+0.000001d0
     !    call radialdensityfunction
     !    call electrondensity(0,0)
     !    meamrhodecay(k,0,1,2)=meamrhodecay(k,0,1,2)-0.000001d0
     !    print *,'dmeamt_dpara(',k,',l=1,jsp=2,iat=',iat,')=',(meam_t(1,iat)-meam_t_prev(1,iat))/0.000001d0
     ! enddo

    !enddo

    !print *
    !print *,'l=2:'
    !
    !do iat=1,gn_inequivalentsites(istr)
    !iat=1

       ! do k=1,4
       !    meamrhodecay(k,2,1,1)=meamrhodecay(k,2,1,1)+0.000001d0
       !    call radialdensityfunction
       !    call electrondensity(0,0)
       !    meamrhodecay(k,2,1,1)=meamrhodecay(k,2,1,1)-0.000001d0
       !    print *,'drhol_dpara(',k,',l=2,jsp=1,iat=',iat,')=',(rhol(2,iat)-rhol_prev(2,iat))/0.000001d0
       ! enddo
       ! do k=1,4
       !    meamrhodecay(k,2,1,2)=meamrhodecay(k,2,1,2)+0.000001d0
       !    call radialdensityfunction
       !    call electrondensity(0,0)
       !    meamrhodecay(k,2,1,2)=meamrhodecay(k,2,1,2)-0.000001d0
       !    print *,'drhol_dpara(',k,',l=2,jsp=2,iat=',iat,')=',(rhol(2,iat)-rhol_prev(2,iat))/0.000001d0
       ! enddo

       ! meamtau(2,1)=meamtau(2,1)+0.000001d0
       ! call radialdensityfunction
       ! call electrondensity(0,0)
       ! meamtau(2,1)=meamtau(2,1)-0.000001d0
       ! print *,'dmeamt_dmeamtau(l=2,jsp=1,iat=',iat,')=',(meam_t(2,iat)-meam_t_prev(2,iat))/0.000001d0
       ! !print
       ! !*,'meam_t(1,',iat,')=',meam_t(1,iat),',meam_t_prev(1,',iat,')=',meam_t_prev(1,iat)
       ! !stop
       ! meamtau(2,2)=meamtau(2,2)+0.000001d0
       ! call radialdensityfunction
       ! call electrondensity(0,0)
       ! meamtau(2,2)=meamtau(2,2)-0.000001d0
       ! print *,'dmeamt_dmeamtau(l=2,jsp=2,iat=',iat,')=',(meam_t(2,iat)-meam_t_prev(2,iat))/0.000001d0

       ! do k=1,4
       !    meamrhodecay(k,0,1,1)=meamrhodecay(k,0,1,1)+0.000001d0
       !    call radialdensityfunction
       !    call electrondensity(0,0)
       !    meamrhodecay(k,0,1,1)=meamrhodecay(k,0,1,1)-0.000001d0
       !    print *,'dmeamt_dpara(',k,',l=2,jsp=1,iat=',iat,')=',(meam_t(2,iat)-meam_t_prev(2,iat))/0.000001d0
       ! enddo
       ! do k=1,4
       !    meamrhodecay(k,0,1,2)=meamrhodecay(k,0,1,2)+0.000001d0
       !    call radialdensityfunction
       !    call electrondensity(0,0)
       !    meamrhodecay(k,0,1,2)=meamrhodecay(k,0,1,2)-0.000001d0
       !    print *,'dmeamt_dpara(',k,',l=2,jsp=2,iat=',iat,')=',(meam_t(2,iat)-meam_t_prev(2,iat))/0.000001d0
       ! enddo


    !enddo

     !print *
     !print *,'l=3:'
     !
     !do iat=1,gn_inequivalentsites(istr)
     !!iat=1

     !   do k=1,4
     !      meamrhodecay(k,3,1,1)=meamrhodecay(k,3,1,1)+0.000001d0
     !      call radialdensityfunction
     !      call electrondensity(0,0)
     !      meamrhodecay(k,3,1,1)=meamrhodecay(k,3,1,1)-0.000001d0
     !      print *,'drhol_dpara(',k,',l=3,jsp=1,iat=',iat,')=',(rhol(3,iat)-rhol_prev(3,iat))/0.000001d0
     !   enddo
     !   do k=1,4
     !      meamrhodecay(k,3,1,2)=meamrhodecay(k,3,1,2)+0.000001d0
     !      call radialdensityfunction
     !      call electrondensity(0,0)
     !      meamrhodecay(k,3,1,2)=meamrhodecay(k,3,1,2)-0.000001d0
     !      print *,'drhol_dpara(',k,',l=3,jsp=2,iat=',iat,')=',(rhol(3,iat)-rhol_prev(3,iat))/0.000001d0
     !   enddo

     !   meamtau(3,1)=meamtau(3,1)+0.000001d0
     !   call radialdensityfunction
     !   call electrondensity(0,0)
     !   meamtau(3,1)=meamtau(3,1)-0.000001d0
     !   print *,'dmeamt_dmeamtau(l=3,jsp=1,iat=',iat,')=',(meam_t(3,iat)-meam_t_prev(3,iat))/0.000001d0
     !   !print
     !   !*,'meam_t(1,',iat,')=',meam_t(1,iat),',meam_t_prev(1,',iat,')=',meam_t_prev(1,iat)
     !   !stop
     !   meamtau(3,2)=meamtau(3,2)+0.000001d0
     !   call radialdensityfunction
     !   call electrondensity(0,0)
     !   meamtau(3,2)=meamtau(3,2)-0.000001d0
     !   print *,'dmeamt_dmeamtau(l=3,jsp=2,iat=',iat,')=',(meam_t(3,iat)-meam_t_prev(3,iat))/0.000001d0

     !   do k=1,4
     !      meamrhodecay(k,0,1,1)=meamrhodecay(k,0,1,1)+0.000001d0
     !      call radialdensityfunction
     !      call electrondensity(0,0)
     !      meamrhodecay(k,0,1,1)=meamrhodecay(k,0,1,1)-0.000001d0
     !      print *,'dmeamt_dpara(',k,',l=3,jsp=1,iat=',iat,')=',(meam_t(3,iat)-meam_t_prev(3,iat))/0.000001d0
     !   enddo
     !   do k=1,4
     !      meamrhodecay(k,0,1,2)=meamrhodecay(k,0,1,2)+0.000001d0
     !      call radialdensityfunction
     !      call electrondensity(0,0)
     !      meamrhodecay(k,0,1,2)=meamrhodecay(k,0,1,2)-0.000001d0
     !      print *,'dmeamt_dpara(',k,',l=3,jsp=2,iat=',iat,')=',(meam_t(3,iat)-meam_t_prev(3,iat))/0.000001d0
     !   enddo

     !enddo


     !stop
    !print *,'backgrounddensity:'
    call backgrounddensity
    if ((gammaOutOfBounds).and.(noOpt.eqv..false.).and.(debug.eqv..true.)) then
       print *,'gamma too small'
       return
    endif

    ! allocate( rho_i_backup(gn_inequivalentsites(istr)) )
    ! do i=1,gn_inequivalentsites(istr)
    !     rho_i_backup(i)=rho_i(i)
    ! enddo
    ! print *,'analytic derivatives:'
    ! do i=1,gn_inequivalentsites(istr)
    ! !i=1
    ! do jsp=1,maxspecies
    !    do ll=1,lmax
    !       print *,'drhoi_dmeamtau(',ll,',',jsp,',',i,')=', &
    !              drhoi_dmeamtau(ll,jsp,i)
    !    enddo
    !    do ll=0,lmax
    !       print *,'drhoi_dpara(',1,',',ll,',',jsp,',',i,')=', &
    !              drhoi_dpara(1,ll,jsp,i)
    !       print *,'drhoi_dpara(',3,',',ll,',',jsp,',',i,')=', &
    !              drhoi_dpara(3,ll,jsp,i)
    !       print *,'drhoi_dpara(',2,',',ll,',',jsp,',',i,')=', &
    !              drhoi_dpara(2,ll,jsp,i)
    !       print *,'drhoi_dpara(',4,',',ll,',',jsp,',',i,')=', &
    !              drhoi_dpara(4,ll,jsp,i)
    !    enddo
    ! enddo
    ! enddo

    ! print *,'numerical derivatives:'
    ! 
    ! do i=1,gn_inequivalentsites(istr)
    ! !i=1
    ! do jsp=1,maxspecies
    !    do ll=1,lmax
    !        meamtau(ll,jsp)=meamtau(ll,jsp)+0.0000001d0
    !        call radialdensityfunction
    !        call electrondensity(0,0)
    !        call backgrounddensity
    !        meamtau(ll,jsp)=meamtau(ll,jsp)-0.0000001d0
    !        print *,'drhoi_dmeamtau(',ll,',',jsp,',',i,')=', &
    !               ( rho_i(i) - rho_i_backup(i) ) / 0.0000001d0
    !    enddo
    !    do ll=0,lmax
    !        do iiCoeff=1,noptdensityCoeff(ll,1,jsp)
    !           ip = ioptdensityCoeff(iiCoeff,ll,1,jsp)
    !           meamrhodecay(ip,ll,1,jsp)=meamrhodecay(ip,ll,1,jsp)+0.0000001d0
    !           call radialdensityfunction
    !           call electrondensity(0,0)
    !           call backgrounddensity
    !           meamrhodecay(ip,ll,1,jsp)=meamrhodecay(ip,ll,1,jsp)-0.0000001d0
    !           print *,'drhoi_dpara(',ip,',',ll,',',jsp,',',i,')=', &
    !                  ( rho_i(i) - rho_i_backup(i) ) / 0.0000001d0
    !        enddo
    !        do iiCutoff=1,noptdensityCutoff(ll,1,jsp)
    !           ip = ioptdensityCutoff(iiCutoff,ll,1,jsp)
    !           meamrhodecay(ip,ll,1,jsp)=meamrhodecay(ip,ll,1,jsp)+0.0000001d0
    !           call radialdensityfunction
    !           call electrondensity(0,0)
    !           call backgrounddensity
    !           meamrhodecay(ip,ll,1,jsp)=meamrhodecay(ip,ll,1,jsp)-0.0000001d0
    !           print *,'drhoi_dpara(',ip,',',ll,',',jsp,',',i,')=', &
    !                  ( rho_i(i) - rho_i_backup(i) ) / 0.0000001d0
    !        enddo
    !    enddo
    ! enddo

    ! enddo
    ! stop




!--------------embedding function------------------------
    !print *,'embeddingfunction:'
    if (useSplines.eqv..true.) then
       print *,'WARNING: Using splines for some or all embedding functions - currently in testing phase'
       call embeddingfunctionSpline
    else
       call embeddingfunction
    endif

    ! allocate( meam_f_backup(gn_inequivalentsites(istr)) )
    ! do i=1,gn_inequivalentsites(istr)
    !     meam_f_backup(i)=meam_f(i)
    ! enddo
    ! print *,'analytic derivatives:'
    ! do i=1,gn_inequivalentsites(istr)
    ! !i=1
    ! print *,'dmeamf_dmeame0=',dmeamf_dmeame0(i)
    ! print *,'dmeamf_dmeamrho0=',dmeamf_dmeamrho0(i)
    ! print *,'dmeamf_dmeamemb3=',dmeamf_dmeamemb3(i)
    ! print *,'dmeamf_dmeamemb4=',dmeamf_dmeamemb4(i)
    ! do jsp=1,maxspecies
    !    do ll=1,lmax
    !       print *,'dmeamf_dmeamtau(',ll,',',jsp,',',i,')=', &
    !              dmeamf_dmeamtau(ll,jsp,i)
    !    enddo
    !    do ll=0,lmax
    !       print *,'dmeamf_dpara(',1,',',ll,',',jsp,',',i,')=', &
    !              dmeamf_dpara(1,ll,jsp,i)
    !       print *,'dmeamf_dpara(',3,',',ll,',',jsp,',',i,')=', &
    !              dmeamf_dpara(3,ll,jsp,i)
    !       print *,'dmeamf_dpara(',2,',',ll,',',jsp,',',i,')=', &
    !              dmeamf_dpara(2,ll,jsp,i)
    !       print *,'dmeamf_dpara(',4,',',ll,',',jsp,',',i,')=', &
    !              dmeamf_dpara(4,ll,jsp,i)
    !    enddo
    ! enddo
    ! enddo

    ! print *,'numerical derivatives:'
    ! do i=1,gn_inequivalentsites(istr)
    ! !i=1
    ! isp=gspecies(i,istr)

    ! meame0(isp)=meame0(isp)+0.0000001d0
    ! call radialdensityfunction
    ! call electrondensity(0,0)
    ! call backgrounddensity
    ! call embeddingfunction
    ! meame0(isp)=meame0(isp)-0.0000001d0
    ! print *,'dmeamf_dmeame0(',isp,',',i,')=', &
    !        ( meam_f(i) - meam_f_backup(i) ) / 0.0000001d0

    ! meamrho0(isp)=meamrho0(isp)+0.0000001d0
    ! call radialdensityfunction
    ! call electrondensity(0,0)
    ! call backgrounddensity
    ! call embeddingfunction
    ! meamrho0(isp)=meamrho0(isp)-0.0000001d0
    ! print *,'dmeamf_dmeamrho0(',isp,',',i,')=', &
    !        ( meam_f(i) - meam_f_backup(i) ) / 0.0000001d0

    ! meamemb3(isp)=meamemb3(isp)+0.0000001d0
    ! call radialdensityfunction
    ! call electrondensity(0,0)
    ! call backgrounddensity
    ! call embeddingfunction
    ! meamemb3(isp)=meamemb3(isp)-0.0000001d0
    ! print *,'dmeamf_dmeamemb3(',isp,',',i,')=', &
    !        ( meam_f(i) - meam_f_backup(i) ) / 0.0000001d0

    ! meamemb4(isp)=meamemb4(isp)+0.0000001d0
    ! call radialdensityfunction
    ! call electrondensity(0,0)
    ! call backgrounddensity
    ! call embeddingfunction
    ! meamemb4(isp)=meamemb4(isp)-0.0000001d0
    ! print *,'dmeamf_dmeamemb4(',isp,',',i,')=', &
    !        ( meam_f(i) - meam_f_backup(i) ) / 0.0000001d0

    ! do jsp=1,maxspecies
    !    do ll=1,lmax
    !        meamtau(ll,jsp)=meamtau(ll,jsp)+0.0000001d0
    !        call radialdensityfunction
    !        call electrondensity(0,0)
    !        call backgrounddensity
    !        call embeddingfunction
    !        meamtau(ll,jsp)=meamtau(ll,jsp)-0.0000001d0
    !        print *,'dmeamf_dmeamtau(',ll,',',jsp,',',i,')=', &
    !               ( meam_f(i) - meam_f_backup(i) ) / 0.0000001d0
    !    enddo
    !    do ll=0,lmax
    !        do iiCoeff=1,noptdensityCoeff(ll,1,jsp)
    !           ip = ioptdensityCoeff(iiCoeff,ll,1,jsp)
    !           meamrhodecay(ip,ll,1,jsp)=meamrhodecay(ip,ll,1,jsp)+0.0000001d0
    !           call radialdensityfunction
    !           call electrondensity(0,0)
    !           call backgrounddensity
    !           call embeddingfunction
    !           meamrhodecay(ip,ll,1,jsp)=meamrhodecay(ip,ll,1,jsp)-0.0000001d0
    !           print *,'dmeamf_dpara(',ip,',',ll,',',jsp,',',i,')=', &
    !                  ( meam_f(i) - meam_f_backup(i) ) / 0.0000001d0
    !        enddo
    !        do iiCutoff=1,noptdensityCutoff(ll,1,jsp)
    !           ip = ioptdensityCutoff(iiCutoff,ll,1,jsp)
    !           meamrhodecay(ip,ll,1,jsp)=meamrhodecay(ip,ll,1,jsp)+0.0000001d0
    !           call radialdensityfunction
    !           call electrondensity(0,0)
    !           call backgrounddensity
    !           call embeddingfunction
    !           meamrhodecay(ip,ll,1,jsp)=meamrhodecay(ip,ll,1,jsp)-0.0000001d0
    !           print *,'dmeamf_dpara(',ip,',',ll,',',jsp,',',i,')=', &
    !                  ( meam_f(i) - meam_f_backup(i) ) / 0.0000001d0
    !        enddo
    !    enddo
    ! enddo

    ! enddo
    ! stop



!--------------total energy------------------------------

    !energy = summeam_paire + summeamf

    dsummeamf_dpara=0d0
    dsummeamf_dmeamtau=0d0
    dsummeamf_dmeame0=0d0
    dsummeamf_dmeamrho0=0d0
    dsummeamf_dmeamemb3=0d0
    dsummeamf_dmeamemb4=0d0
    !print *,'derivs:'
    !summeamf=0d0
    do i=1,gn_inequivalentsites(istr)
        !summeamf=summeamf+meam_f(i)
        !Caution: dsummeamf_dpara has jsp,isp rather than
        !isp,jsp in meamrhodecay [meamrhodecay(np,l,isp,jsp)]
        isp=1
        if (thiaccptindepndt.eqv..false.) isp=gspecies(i,istr)
        ! dsummeamf_dpara(1:12,0:lmax,1:maxspecies,isp) = &
        !       dsummeamf_dpara(1:12,0:lmax,1:maxspecies,isp) + &
        !       dmeamf_dpara(1:12,0:lmax,1:maxspecies,i)
        !We swap the order of the last two indices here. For calculation
        !efficiency purposes, 'i' was the last index. Now, for consistency
        !with
        !meamrhodecay, we use the order: isp, jsp.
        dsummeamf_dpara(1:12,0:lmax,isp,1:maxspecies) = &
              dsummeamf_dpara(1:12,0:lmax,isp,1:maxspecies) + &
              dmeamf_dpara(1:12,0:lmax,1:maxspecies,i)
        isp=gspecies(i,istr)
        dsummeamf_dmeame0(isp)=dsummeamf_dmeame0(isp)+dmeamf_dmeame0(i)
        dsummeamf_dmeamrho0(isp)=dsummeamf_dmeamrho0(isp)+dmeamf_dmeamrho0(i)
        dsummeamf_dmeamemb3(isp)=dsummeamf_dmeamemb3(isp)+dmeamf_dmeamemb3(i)
        dsummeamf_dmeamemb4(isp)=dsummeamf_dmeamemb4(isp)+dmeamf_dmeamemb4(i)
    enddo

    if (lmax.gt.0) then
       do i=1,gn_inequivalentsites(istr)
           dsummeamf_dmeamtau(1:lmax,1:maxspecies)= &
                 dsummeamf_dmeamtau(1:lmax,1:maxspecies) + &
                 dmeamf_dmeamtau(1:lmax,1:maxspecies,i)
       enddo
    endif

    energy = summeam_paire + summeamf + enconst(1) !summeam_paire and summeamf have already been normalized to 'per atom'. enconst(1) is already 'per atom'.

    dsummeamf_dpara=dsummeamf_dpara/gn_inequivalentsites(istr)
    dsummeamf_dmeamtau=dsummeamf_dmeamtau/gn_inequivalentsites(istr)
    dsummeamf_dmeame0=dsummeamf_dmeame0/gn_inequivalentsites(istr)
    dsummeamf_dmeamrho0=dsummeamf_dmeamrho0/gn_inequivalentsites(istr)
    dsummeamf_dmeamemb3=dsummeamf_dmeamemb3/gn_inequivalentsites(istr)
    dsummeamf_dmeamemb4=dsummeamf_dmeamemb4/gn_inequivalentsites(istr)

    !Once we've checked this, we can instead call calcEnergy (From meamforce) to
    !get the energy and its derivatives w.r.t parameters.
    !Also write a subroutine to do the pairpotential stuff above
    !print *,'summeam_paire=',summeam_paire,', summeamf=',summeamf
    !print *,'energy=',energy
    !stop

end subroutine meamenergy
