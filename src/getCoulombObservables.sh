#!/bin/sh

# Calculates Coulom energy, forces and stress tensors using LAMMPS. This is run externally from within MEAMfit.
# (Andrew Ian Duff 2020)
#
# Change LAMMPS executable and location as required:
LMP_DIR="/home/vol01/scarf781/progs/MEAMfit_RFMEAM_LAMMPS/for_LAMMPS_compilation/lammps-11Aug17/src/"
LMP_EXE="lmp_serial"
#
# Ensure:
# i) lammpsIn_energyForce and lammpsIn_stress are present in the current directory
# ii) lammps is loaded before running. E.g.: module load lammps


# echo
# echo "Running LAMMPS"
# echo

# Energy
/$LMP_DIR/$LMP_EXE < lammpsIn_energyForce | grep '^ *0' > LAMMPSenergy
energy=`awk '{print $3}' LAMMPSenergy`
# echo "Energy:"
# echo $energy
# echo
echo "Energy:" > coulombObservables
echo $energy >> coulombObservables

# Forces
sed -i 1,9d dump.myforce.0
cat dump.myforce.0 | sort -n > LAMMPSforces
cut -d " " -f 3- LAMMPSforces > LAMMPSforces.tmp
mv LAMMPSforces.tmp LAMMPSforces
# echo "Forces:"
# cat LAMMPSforces
# echo
echo "Forces:" >> coulombObservables
cat coulombObservables LAMMPSforces > coulombObservables2
mv coulombObservables2 coulombObservables

# Stress tensor
/$LMP_DIR/$LMP_EXE < lammpsIn_stress > lammps.out
grep -A1 'stens\[1\]' lammps.out > lammps_stress.out
tail -n1 lammps_stress.out  > tmp
mv tmp lammps_stress.out
# Lammps stress tensor in bar. 1 bar = 10^-4 GPa. 1 GPa = 1 / 160.21766208 eV / Ang^3.
# Thus factor to convert LAMMPS values (in bars) to my units (eV/Ang^3) is: 10^-4/160.217...
lmpStress1=`awk '{print $1}' lammps_stress.out`
lmpStress1="${lmpStress1/e/*10^}" #replace 'e' with '*10^', otherwise bc will fault up
lmpStress1_converted=`bc -l <<< "scale=5; $lmpStress1*10^-4/160.21766208"`
lmpStress2=`awk '{print $2}' lammps_stress.out`
lmpStress2="${lmpStress2/e/*10^}" #replace 'e' with '*10^', otherwise bc will fault up
lmpStress2_converted=`bc -l <<< "scale=5; $lmpStress2*10^-4/160.21766208"`
lmpStress3=`awk '{print $3}' lammps_stress.out`
lmpStress3="${lmpStress3/e/*10^}" #replace 'e' with '*10^', otherwise bc will fault up
lmpStress3_converted=`bc -l <<< "scale=5; $lmpStress3*10^-4/160.21766208"`
lmpStress4=`awk '{print $4}' lammps_stress.out`
lmpStress4="${lmpStress4/e/*10^}" #replace 'e' with '*10^', otherwise bc will fault up
lmpStress4_converted=`bc -l <<< "scale=5; $lmpStress4*10^-4/160.21766208"`
lmpStress5=`awk '{print $5}' lammps_stress.out`
lmpStress5="${lmpStress5/e/*10^}" #replace 'e' with '*10^', otherwise bc will fault up
lmpStress5_converted=`bc -l <<< "scale=5; $lmpStress5*10^-4/160.21766208"`
lmpStress6=`awk '{print $6}' lammps_stress.out`
lmpStress6="${lmpStress6/e/*10^}" #replace 'e' with '*10^', otherwise bc will fault up
lmpStress6_converted=`bc -l <<< "scale=5; $lmpStress6*10^-4/160.21766208"`
# The same units as those produced by MEAMfit (search for 'Stress tensor' in the MEAMfit output)
# echo "Stress tensor (eV/Angstrom):"
# echo $lmpStress1_converted $lmpStress2_converted $lmpStress3_converted $lmpStress4_converted $lmpStress5_converted $lmpStress6_converted
echo "Stress tensor (eV/Angstrom):" >> coulombObservables
echo $lmpStress1_converted $lmpStress2_converted $lmpStress3_converted $lmpStress4_converted $lmpStress5_converted $lmpStress6_converted >> coulombObservables
