subroutine svbksb(u,w,v,m,n,b, &
        tmp, &
        x)
    ! based on numerical recipes
    ! Marcel Sluiter, May 10 2002
    !
    !     Copyright (c) 2018, STFC

    implicit none
    integer m,n,j
    real(8) b(m),u(m,n),v(n,n),w(n),x(n),tmp(n)
    intent(in) u,w,v,m,n,b
    intent(out) x,tmp

    tmp=0d0
    do j=1,n
        if(w(j).ne.0d0) tmp(j)=sum(u(:,j)*b(:))/w(j)
    enddo
    do j=1,n
        x(j)=sum(v(j,:)*tmp(:))
    enddo
end subroutine svbksb
