subroutine initializeLammpsPot

    !-------------------------------------------------------------------c
    !
    !     Reads in the LAMMPS potential files specified in the settings
    !     file. Determines which species/species-pair each contribution
    !     corresponds. (In MEAMfit species are labelled 1,2,3,...
    !     according to which order they are first read in from the DFT
    !     input files) Throw up error if: i) all the functions of a given LAMMPS
    !     input file are defined for
    !     atoms with Z-values that have not appeared in the DFT files;
    !     ii) the same function is defined in two separated LAMMPS
    !     input files.
    !
    !     Unlike the potparas read-in functionality, ALL interactions and single
    !     particle functions are taken equal (and not to be optimized) to their
    !     values from the LAMMPS files.
    !     (although functions and interactions for species and species pairs not in
    !     the DFT files are ignored)
    !     
    !     Possible use cases:
    !     i) fitting cross terms. E.g., user can supplied Fe.eam.alloy and
    !     C.eam.alloy. The resulting fit will then target only the Fe-C terms.
    !     ii) Using an existing potential for a unary (or higher order component in
    !     multicomponent alloys). Simply input X.eam.alloy where X is the element to
    !     fix to an existing potential.
    !
    !     Andrew Duff 2018
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_atomproperties
    use m_meamparameters
    use m_filenames

    implicit none

    logical notASpecies,notAnySpecies,notASpecies_iSp,notASpecies_iSp2
    integer i,iLmp,iSp,iSp2,iSp3,iLines,nLines,nLeftOver,nSpeciesLmps,nwords,NrhoLmps_tmp,Nr_tmp,elementTmp,iSpTmp1,iSpTmp2,rhoIndex,rIndex
    real(8) drhoLmps_tmp,drLmps_tmp,rmaxLmps_tmp,embfuncExtract,rSample,rhoSample,embFuncDerivExtract,embFuncExtractPrev,densExtract,densDerivExtract,densExtractPrev,pairpotExtract,pairpotExtractPrev,pairpotDerivExtract
    real(8) embfuncSplineTmp(nRhoMaxSpline)
    character*2 elementLmps(10)
    character*80 string
    character*80, allocatable:: words(:)

    allocate(embfuncSpline(nRhoMaxSpline,maxspecies),embfuncSplineSecDer(nRhoMaxSpline,maxspecies),NrhoLmps(maxspecies),drhoLmps(maxspecies),NrLmps(maxspecies,maxspecies),drLmps(maxspecies,maxspecies),rmaxLmps(maxspecies,maxspecies),useSpline1PartFns(maxspecies),useSpline2PartFns(maxspecies,maxspecies),densSpline(nRMaxSpline,maxspecies),densSplineSecDer(nRMaxSpline,maxspecies),pairpotSpline(nRMaxSpline,maxspecies,maxspecies),pairpotSplineSecDer(nRMaxSpline,maxspecies,maxspecies))

    useSplines=.false.
    useSpline1PartFns=.false.
    useSpline2PartFns=.false.

    do iLmp=1,nLammpsFiles

       print *,'Opening LAMMPS file:',lammpsPotIn(iLmp)
       open(55,file=lammpsPotIn(iLmp))
       read(55,*)
       read(55,*)
       read(55,*)

       read(55,'(a80)') string
       call optionalarg(string,nwords)
       if (nwords.gt.0) then
          allocate(words(1:nwords))
          read(string,*) words
       else
          print *,'ERROR: reading line 4, STOPPING'
          stop
       endif
       string=trim(words(1))
       read(string,*) nSpeciesLmps
       do iSp=1,nSpeciesLmps
          elementLmps(iSp)=words(iSp+1)
       enddo
       print *,'Elements from Lammps file 1:',elementLmps(1),', 2:',elementLmps(2)
       read(55,*) NrhoLmps_tmp,drhoLmps_tmp,Nr_tmp,drLmps_tmp,rmaxLmps_tmp
       ! read(55,'(I13,F25.16,I13,E25.16,F25.16)') NrhoLmps_tmp,drhoLmps_tmp,Nr_tmp,drLmps_tmp,rmaxLmps_tmp

       ! No need to store Nr. To evaluate the densities and pair-potentials we
       ! just need rsepn/drLmps(iLmp) to index the position, and then evaluate
       ! the spline fitted to the function at that point.
       print *,NrhoLmps_tmp,drhoLmps_tmp,Nr_tmp,drLmps_tmp,rmaxLmps_tmp
       print *,'Nr*dr=',(Nr_tmp-1)*drLmps_tmp,' should equal cutoff=',rmaxLmps_tmp
      !if ((((Nr_tmp-1)*drLmps_tmp-rmaxLmps_tmp).lt.-10d-13).or.(((Nr_tmp-1)*drLmps_tmp-rmaxLmps_tmp).gt.10d-13)) then
      !   print *,'ERROR: Nr*dr /= p_rmax, STOPPING'
      !   stop
      !endif
       if (NrhoLmps_tmp.gt.nRhoMaxSpline) then
          print *,'ERROR: need to increase nRhoMaxSpline, STOPPING'
          stop
       endif
       
       ! Read in embedding functions for species present in DFT files
       notAnySpecies=.true. ! At least one of the DFT species should be in the LAMMPS file!
       do iSp=1,nSpeciesLmps
          ! Check if this is a species in our DFT files
          notASpecies=.true.
          do iSp2=1,maxspecies
             print *,'iSp=',iSp,' iSp2=',iSp2,' speciestoZ(',iSp2,')=',speciestoZ(iSp2),' element(...)=',element(speciestoZ(iSp2)),' elementLmps(iSp)=',elementLmps(iSp)
             if ( element(speciestoZ(iSp2)).eq.elementLmps(iSp) ) then
                print *,elementLmps(iSp),' (the ',iSp,'th species from the LAMMPS file), equals the ',iSp2,'th species from the DFT files'
                notASpecies=.false.
                notAnySpecies=.false.
                exit
             endif
          enddo
          if (notASpecies.eqv..false.) then
             ! Check that the atomic number in the next line corresponds to the species name
             read(55,*) elementTmp
             if (elementTmp.ne.speciestoZ(iSp2)) then
                print *,'ERROR: the atomic number in the embedding function block for ',elementLmps(iSp),' is inconsistent with the element, STOPPING.'
                stop
             else
                print *,'Element number consistent with element name in embedding function block for ',elementLmps(iSp),' - all set'
             endif
             ! Check that this single particle function hasn't been read in already from another LAMMPS file
             if (useSpline1PartFns(iSp2).eqv..true.) then
                print *,'ERROR: attempting to read embedding function for ',elementLmps(iSp),' from ',trim(lammpsPotIn(iLmp)),' however this has already been read in'
                print *,'from a previous LAMMPS file, STOPPING.'
                stop
             endif
             useSpline1PartFns(iSp2)=.true.
             useSplines=.true.
             ! ---- Read embedding function: ----
             NrhoLmps(iSp2)=NrhoLmps_tmp
             drhoLmps(iSp2)=drhoLmps_tmp
             ! Read in the embedding function into: embfuncSpline(nRhoMaxSpline,iSp2)
             nLines=floor(dble(NrhoLmps(iSp2))/5d0)
             print *,'NrhoLmps(',iSp2,')=',NrhoLmps(iSp2)
             nLeftOver=mod(NrhoLmps(iSp2),5)
             print *,'about to read embfunc. nLines=',nLines,' nLeftOver=',nLeftOver
             do iLines=1,nLines
                !print *,'iLine=',iLines
                read(55,*) embfuncSpline((iLines-1)*5+1:iLines*5,iSp2)
             enddo
             if (nLeftOver.gt.0) then
                !Sweep up the remaining data points
                read(55,*) embfuncSpline(nLines*5+1:nLines*5+nLeftOver,iSp2)
                print *,'embfuncSpline(',nLines*5+1,':',nLines*5+nLeftOver,',',iSp2,')=',embfuncSpline(nLines*5+1:nLines*5+nLeftOver,iSp2)
             endif
             print *,'first two and final two embfunc values:'
             print *,embfuncSpline(1,iSp2),embfuncSpline(2,iSp2),'...',embfuncSpline(NrhoLmps(iSp2)-1,iSp2),embfuncSpline(NrhoLmps(iSp2),iSp2)
             
             call fitSpline(drhoLmps(iSp2),embfuncSpline(1:nRhoMaxSpline,iSp2),NrhoLmps(iSp2),nRhoMaxSpline,embfuncSplineSecDer(1:nRhoMaxSpline,iSp2))
             !  ! Test of the spline:
             !  do i=1,20*NrhoLmps(iSp2)
             !     rhoSample=dble(i-1)*drhoLmps(iSp2)/20
             !     rhoIndex=1+FLOOR(rhoSample/drhoLmps(iSp2))
             !     call interpolateSplineDeriv(drhoLmps(iSp2),embfuncSpline(1:nRhoMaxSpline,iSp2),embfuncSplineSecDer(1:nRhoMaxSpline,iSp2),nRhoMaxSpline,rhoIndex,rhoSample,embfuncExtract,embfuncDerivExtract)
             !     if (i.eq.1) then
             !        write(51,*) rhoSample,embfuncExtract,embfuncDerivExtract
             !     else
             !        write(51,*) rhoSample,embfuncExtract,embfuncDerivExtract,(embfuncExtract-embfuncExtractPrev)/(drhoLmps(iSp2)/20)
             !     endif
             !     if (mod(i-1,20).eq.0) then
             !        write(52,*) rhoSample,embfuncExtract,embfuncDerivExtract
             !     endif
             !     embfuncExtractPrev=embfuncExtract
             !  enddo
             
             ! ---- Read electron density: ----
             NrLmps(iSp2,iSp2)=Nr_tmp
             drLmps(iSp2,iSp2)=drLmps_tmp
             rmaxLmps(iSp2,iSp2)=rmaxLmps_tmp
             ! Read in the embedding function into: embfuncSpline(nRhoMaxSpline,iSp2)
             nLines=floor(dble(NrLmps(iSp2,iSp2))/5d0)
             nLeftOver=mod(NrLmps(iSp2,iSp2),5)
             print *,'about to read density. nLines=',nLines,' nLeftOver=',nLeftOver
             do iLines=1,nLines
                !print *,'iLine=',iLines
                read(55,*) densSpline((iLines-1)*5+1:iLines*5,iSp2)
             enddo
             if (nLeftOver.gt.0) then
                !Sweep up the remaining data points
                read(55,*) densSpline(nLines*5+1:nLines*5+nLeftOver,iSp2)
                print *,'densSpline(',nLines*5+1,':',nLines*5+nLeftOver,',',iSp2,')=',densSpline(nLines*5+1:nLines*5+nLeftOver,iSp2)
             endif
             print *,'first two and final two densSpline values:'
             print *,densSpline(1,iSp2),densSpline(2,iSp2),'...',densSpline(NrLmps(iSp2,iSp2)-1,iSp2),densSpline(NrLmps(iSp2,iSp2),iSp2)

             call fitSpline(drLmps(iSp2,iSp2),densSpline(1:nRMaxSpline,iSp2),NrLmps(iSp2,iSp2),nRMaxSpline,densSplineSecDer(1:nRMaxSpline,iSp2))
             !  ! Test of the spline:
             !  do i=1,20*NrLmps(iSp2,iSp2)
             !     rSample=dble(i-1)*drLmps(iSp2,iSp2)/20
             !     rIndex=1+FLOOR(rSample/drLmps(iSp2,iSp2))
             !     call interpolateSplineDeriv(drLmps(iSp2,iSp2),densSpline(1:nRhoMaxSpline,iSp2),densSplineSecDer(1:nRhoMaxSpline,iSp2),nRMaxSpline,rIndex,rSample,densExtract,densDerivExtract)
             !     if (i.eq.1) then
             !        write(51,*) rSample,densExtract,densDerivExtract
             !     else
             !        write(51,*) rSample,densExtract,densDerivExtract,(densExtract-densExtractPrev)/(drLmps(iSp2,iSp2)/20)
             !     endif
             !     if (mod(i-1,20).eq.0) then
             !        write(52,*) rSample,densExtract,densDerivExtract
             !     endif
             !     densExtractPrev=densExtract
             !  enddo

          else
             ! iSp 'th species in the LAMMPS file can be skipped, as it doesn't
             ! appear in the DFT database

             read(55,*) 
             nLines=floor(dble(NrhoLmps(iSp2))/5d0)
             nLeftOver=mod(NrhoLmps(iSp2),5)
             do iLines=1,nLines
                read(55,*) 
             enddo
             if (nLeftOver.gt.0) then
                read(55,*) 
             endif
             nLines=floor(dble(NrLmps(iSp2,iSp2))/5d0)
             nLeftOver=mod(NrLmps(iSp2,iSp2),5)
             do iLines=1,nLines
                read(55,*) 
             enddo
             if (nLeftOver.gt.0) then
                read(55,*) 
             endif
             
          endif
       enddo
      
       if (notAnySpecies) then
          print *,'ERROR: none of the species in LAMMPS file: ',lammpsPotIn(iLmp),' are in the DFT database, STOPPING.'
          stop
       endif

       !pp part (LAMMPS pairpots ordered as: (1,1), (2,1), (2,2), (3,1), (3,2), (3,3), (4,1), ...
       do iSp=1,nSpeciesLmps
          do iSp2=1,iSp
             ! First check that both species are present in the DFT input files
             print *,'Checking if the LAMMPS pair:',elementLmps(iSp),'-',elementLmps(iSp2), &
                 ' is present in the DFT files'
             notASpecies_iSp=.true.
             notASpecies_iSp2=.true.
             do iSp3=1,maxspecies
                if ( element(speciestoZ(iSp3)).eq.elementLmps(iSp) ) then
                   print *,'The ',iSp3,'th species from the DFT files =',elementLmps(iSp2)
                   notASpecies_iSp=.false.
                   iSpTmp1=iSp3
                endif
                if ( element(speciestoZ(iSp3)).eq.elementLmps(iSp2) ) then
                   print *,'The ',iSp3,'th species from the DFT files =',elementLmps(iSp2)
                   notASpecies_iSp2=.false.
                   iSpTmp2=iSp3
                endif
             enddo
             if ((notASpecies_iSp.eqv..false.).and.(notASpecies_iSp2.eqv..false.)) then
                print *,'   LAMMPS pair:',elementLmps(iSp),'-',elementLmps(iSp2),' represented by species from the DFT files: ',iSpTmp1,' and ',iSpTmp2
                if (useSpline2PartFns(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)).eqv..true.) then
                   print *,'ERROR: attempting to read the pairpotential function for ',elementLmps(iSp),'-',elementLmps(iSp2),' from ',trim(lammpsPotIn(iLmp)),' however this has already been read in'
                   print *,'from a previous LAMMPS file, STOPPING.'
                   stop
                endif
                !----- check all this carefully...
                useSpline2PartFns(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))=.true.
                useSplines=.true.
                print *,'Setting useSpline2PartFns(',min(iSpTmp1,iSpTmp2),',',max(iSpTmp1,iSpTmp2),')=.true.'
                NrLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))=Nr_tmp
                drLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))=drLmps_tmp
                rmaxLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))=rmaxLmps_tmp
                !nLines and nLeftOver still good from previous density read in
                do iLines=1,nLines
                   !print *,'iLine=',iLines
                   read(55,*) pairpotSpline((iLines-1)*5+1:iLines*5,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))
                enddo
                if (nLeftOver.gt.0) then
                   !Sweep up the remaining data points
                   read(55,*) pairpotSpline(nLines*5+1:nLines*5+nLeftOver,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))
                   print *,'pairpotSpline(',nLines*5+1,':',nLines*5+nLeftOver,',',min(iSpTmp1,iSpTmp2),',',max(iSpTmp1,iSpTmp2),')=',pairpotSpline(nLines*5+1:nLines*5+nLeftOver,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))
                endif
                print *,'first two and final two pairpot values:'
                print *,pairpotSpline(1,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),pairpotSpline(2,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),'...',pairpotSpline(NrLmps(iSp2,iSp2)-1,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),pairpotSpline(NrLmps(iSp2,iSp2),min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))

                call fitSpline(drLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),pairpotSpline(1:nRMaxSpline,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),NrLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),nRMaxSpline,pairpotSplineSecDer(1:nRMaxSpline,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)))

                !   ! Test of the spline:
                !   if ((min(iSpTmp1,iSpTmp2).eq.1).and.(max(iSpTmp1,iSpTmp2).eq.2)) then
                !      do i=1,20*NrLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))
                !         rSample=dble(i-1)*drLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))/20
                !         rIndex=1+FLOOR(rSample/drLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)))
                !         call interpolateSplineDeriv(drLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),pairpotSpline(1:nRhoMaxSpline,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),pairpotSplineSecDer(1:nRhoMaxSpline,min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2)),nRMaxSpline,rIndex,rSample,pairpotExtract,pairpotDerivExtract)
                !         if (i.eq.1) then
                !            write(51,*) rSample,pairpotExtract,pairpotDerivExtract
                !         else
                !            write(51,*) rSample,pairpotExtract,pairpotDerivExtract,(pairpotExtract-pairpotExtractPrev)/(drLmps(min(iSpTmp1,iSpTmp2),max(iSpTmp1,iSpTmp2))/20)
                !         endif
                !         if (mod(i-1,20).eq.0) then
                !            write(52,*) rSample,pairpotExtract,pairpotDerivExtract
                !         endif
                !         pairpotExtractPrev=pairpotExtract
                !      enddo
                !      stop
                !   endif
                

                !-----------------------------------

             else
                print *,'   LAMMPS pair:',elementLmps(iSp),'-',elementLmps(iSp2),' not present in the DFT files'
                !Skip past data
             endif
          enddo
       enddo 
       close(55)

    enddo
    if(allocated(words)) deallocate(words)

end subroutine initializeLammpsPot
