subroutine amendFreep

    !---------------------------------------------------------------c
    !
    !     This subroutine checks for non-zero elements of the 
    !     potential file being read in, and if it finds them
    !     then it will set freep to 0 or 1 for the corresponding
    !     parameters (for FIXPOTIN=.true., .false. respectively).
    !     Furthermore, all remaining freep's for that function
    !     (for a given species or species-pair and for the given 
    !     l-value for densities) will also be set to 0. In this way, 
    !     the settings of the NTERMS keywords are over-written
    !     (note, this function will not be called if PARASTOOPT=
    !     .true., since in this case PARASTOOPT should uniquely
    !     determine the freep values).
    !
    !     Furthermore, if FIXPOTIN=.true. and a density or meamtau
    !     is read in from a potential file for a given species and
    !     given l-value, then freep is set to 0 for all other
    !     l-values of that function for that species, except for
    !     l-values where parameters have been specified in the file.
    !     In this way, an EAM potential supplied by a user will
    !     _stay_ EAM (MEAMfit will not see the zeros for l>0
    !     densities and then apply the NTERMS rule to try to
    !     generate and optimize these parameters).
    !
    !     Also adjust randgenparas here to reflect whether any 
    !     parameters are still to be randomly generated.
    !
    !     Returns:       freep, randgenparas
    !
    !     Andrew Duff
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_optimization
    use m_atomproperties
    use m_meamparameters

    implicit none

    logical foundNonZero
    integer ip,isp,jsp,l,spcCnt

    if (holdcutoffs) then
       print *,'ERROR: you are using HOLDCUTOFFS=.true. and also reading in'
       print *,'a potential file. Please first update amendFreep, STOPPING'
       stop
       !Need to set nrandcutoffDens and nrandcutoffPairpot to 0 for those
       !parameters being read in, otherwise with holdcutoffs=.true. The
       !holdcutoffparas subroutine will over-write the cutoffs of the input
       !parameters!
    endif

    !cmin and cmax:
    do ip=1,2*m3
       if (p(ip).ne.0d0) then
          !This potential parameter has been read in from the potential
          !file and is not to be randomly generated
          if (fixPotIn.eqv..true.) then
            freep(ip)=0
          else
            freep(ip)=1
          endif
       endif
    enddo

    !meamtau:
    if (lmax.gt.0) then
       do isp=1,maxspecies
          foundNonZero=.false.
          do ip=2*m3+isp,2*m3+isp+maxspecies*(lmax-1),maxspecies !Note, this loop will still execute once if lmax=0 (do i=1,1 in f90 executes once)
             if (p(ip).ne.0d0) then
                foundNonZero=.true.
             endif
          enddo
          if (foundNonZero) then
             if (fixPotIn) then
                !A single non-zero meamtau(l,isp) for a particular l of a given isp will fix all meamtau(l=0,lmax,isp)
                do ip=2*m3+isp,2*m3+isp+maxspecies*(lmax-1),maxspecies
                   freep(ip)=0
                enddo
             else
                !The non zero elements will be allowed to but all other elements will be held fixed
                do ip=2*m3+isp,2*m3+isp+maxspecies*(lmax-1),maxspecies
                   if (p(ip).ne.0d0) then
                      freep(ip)=1
                   else
                      freep(ip)=0
                   endif
                enddo
             endif
          endif
       enddo
    endif

    !meamrhodecay:
    spcCnt=-1
    do isp=1,maxspecies
       do jsp=1,maxspecies
          spcCnt=spcCnt+1
          if (fixPotIn) then
             do l=0,lmax
                foundNonZero=.false.
                do ip=2*m3+lmax*m1+12*l+spcCnt*lm1*12+1,2*m3+lmax*m1+12*l+spcCnt*lm1*12+12
                   if (p(ip).ne.0d0) then
                      foundNonZero=.true.
                      exit
                   endif
                enddo
             enddo
             if (foundNonZero) then
                !For fixPotIn=.true., a single non zero meamrhodecay (for a given
                !species) will fix all meamrhodecay parameters for all l values for
                !that species
                do l=0,lmax
                   do ip=2*m3+lmax*m1+12*l+spcCnt*lm1*12+1,2*m3+lmax*m1+12*l+spcCnt*lm1*12+12
                      freep(ip)=0
                   enddo
                enddo
             endif
          else
             do l=0,lmax
                foundNonZero=.false.
                do ip=2*m3+lmax*m1+12*l+spcCnt*lm1*12+1,2*m3+lmax*m1+12*l+spcCnt*lm1*12+12
                   if (p(ip).ne.0d0) then
                      foundNonZero=.true.
                      exit
                   endif
                enddo
                if (foundNonZero) then
                   !For fixPotIn=.false., non-zero parameters optimized from their read-in values, with all other meamrhodecay
                   !parameters for that species and l-value held fixed. meamrhodecay parameters for other l values for that species are
                   !unaffected.
                   do ip=2*m3+lmax*m1+12*l+spcCnt*lm1*12+1,2*m3+lmax*m1+12*l+spcCnt*lm1*12+12
                      if (p(ip).ne.0d0) then
                         freep(ip)=1
                      else
                         freep(ip)=0
                      endif
                   enddo
                endif
             enddo
          endif
       enddo
    enddo

    !embedding function parameters:
    do isp=1,maxspecies
       foundNonZero=.false.
       if (p(2*m3+lmax*m1+12*lm1*m2+isp).ne.0d0) then
          foundNonZero=.true.
       endif
       if (p(2*m3+lmax*m1+12*lm1*m2+isp+m1).ne.0d0) then
          foundNonZero=.true.
       endif
       if (p(2*m3+lmax*m1+12*lm1*m2+isp+2*m1).ne.0d0) then
          foundNonZero=.true.
       endif
       if (p(2*m3+lmax*m1+12*lm1*m2+isp+3*m1).ne.0d0) then
          foundNonZero=.true.
       endif
       if (foundNonZero) then
          freep(2*m3+lmax*m1+12*lm1*m2+isp)=0
          freep(2*m3+lmax*m1+12*lm1*m2+isp+m1)=0
          freep(2*m3+lmax*m1+12*lm1*m2+isp+2*m1)=0
          freep(2*m3+lmax*m1+12*lm1*m2+isp+3*m1)=0
          if (fixPotIn.eqv..false.) then
             if (p(2*m3+lmax*m1+12*lm1*m2+isp).ne.0d0) then
                freep(2*m3+lmax*m1+12*lm1*m2+isp)=1
             endif
             if (p(2*m3+lmax*m1+12*lm1*m2+isp+m1).ne.0d0) then
                freep(2*m3+lmax*m1+12*lm1*m2+isp+m1)=1
             endif
             if (p(2*m3+lmax*m1+12*lm1*m2+isp+2*m1).ne.0d0) then
                freep(2*m3+lmax*m1+12*lm1*m2+isp+2*m1)=1
             endif
             if (p(2*m3+lmax*m1+12*lm1*m2+isp+3*m1).ne.0d0) then
                freep(2*m3+lmax*m1+12*lm1*m2+isp+3*m1)=1
             endif
          endif
       endif
    enddo

    !pair-potential
    spcCnt=-1
    do isp=1,maxspecies
       do jsp=1,maxspecies
          spcCnt=spcCnt+1
          foundNonZero=.false.
          do ip=2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt+1,2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt+32
             if (p(ip).ne.0d0) then
                foundNonZero=.true.
                exit
             endif
          enddo
          if (foundNonZero) then
             if (fixPotIn) then
                do ip=2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt+1,2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt+32
                   freep(ip)=0
                enddo
             else
                do ip=2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt+1,2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt+32
                   if (p(ip).ne.0d0) then
                      freep(ip)=1
                   else
                      freep(ip)=0
                   endif
                enddo
             endif
          endif
       enddo
    enddo

    !enconst:
    do isp=1,maxspecies
       if (p(m4+2*m2+isp).ne.0d0) then
          if (fixPotIn) then
             freep(m4+2*m2+isp)=0
          else
             freep(m4+2*m2+isp)=1
          endif
       endif
    enddo

    !Re-check based on changes made here to freep whether any random parameter
    !seeding is still required
    if ( all(freep.lt.2) ) then
       randgenparas=.false.
    endif

end subroutine amendFreep
