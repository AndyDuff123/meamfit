subroutine readmeamparam(filename)

    !---------------------------------------------------------------c
    !
    !     Read in a set of MEAM parameters from a file into the
    !     variables: cmin, etc. The name of the file is stored in
    !     the variable, filename.
    !
    !     Notes: lmaxPot is read in rather than lmax because lmax
    !     will already have been read in from the potential
    !     file (by extractlmax called from MEAMfit), but might have
    !     been changed by the settings file. E.g, we might want to
    !     optimize a MEAM potential but the input potential is EAM,
    !     in which case we want to keep lmax=3, but use a different
    !     variable, lmaxPot, to denote which angular momenta of
    !     data we should read in from the potential file.
    !
    !     Called by:     program MEAMfit,initializemeam,meamenergy
    !     Calls:         -
    !     Arguments:     filename
    !     Returns:       cmin,cmax,meamtau,
    !                 meamrhodecay,meame0,meamrho0,
    !                 meamemb3,meamemb4,pairpotparameter,
    !                 rs,rc
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Ian Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_filenames
    use m_meamparameters
    use m_optimization
    use m_atomproperties
    use m_generalinfo

    implicit none

    integer i,j,l,lmaxPot
    real(8), allocatable:: meamtau_dummy(:,:)

    character*80 filename

    open(unit=1,file=trim(filename),status='old')
    read(1,*) lmaxPot
    if (verbosity.gt.1) then
      print *,'lmaxPot=',lmaxPot
    endif
    read(1,*) cmin !species 3x
    if (verbosity.gt.1) then
      print *,'cmin=',cmin
    endif
    read(1,*) cmax !species 3x
    if (verbosity.gt.1) then
      print *,'cmax=',cmax
    endif
    read(1,*) envdepmeamt !environ dep meamt's?
    if (verbosity.gt.1) then
      print *,'envdepmeamt=',envdepmeamt
    endif
    !meamtau is stored in the potential files with indices reversed (I.e., with
    !species first), for legacy reasons. Therefore first read into dummy array
    !before transfering to meamtau.
   !print *,'maxspecies=',maxspecies,' lmaxPot=',lmaxPot
    allocate(meamtau_dummy(1:maxspecies,1:lmaxPot))
    read(1,*) meamtau_dummy(1:maxspecies,1:lmaxPot)
    if (verbosity.gt.1) then
      print *,'read in meamtau_dummy(1:',maxspecies,'1:',lmaxPot,')=',meamtau_dummy(1:maxspecies,1:lmaxPot)
    endif
    do i=1,maxspecies
       do l=1,lmaxPot
          meamtau(l,i)=meamtau_dummy(i,l)
       enddo
    enddo
    deallocate(meamtau_dummy)
    if (verbosity.gt.1) then
      print *,'meamtau(l=0,1)=',meamtau(0,1)
      print *,'meamtau(l=1,1)=',meamtau(1,1)
      print *,'meamtau(l=2,1)=',meamtau(2,1)
      print *,'meamtau(l=3,1)=',meamtau(3,1)
      print *,'meamtau(l=0,2)=',meamtau(0,2)
      print *,'meamtau(l=1,2)=',meamtau(1,2)
      print *,'meamtau(l=2,2)=',meamtau(2,2)
      print *,'meamtau(l=3,2)=',meamtau(3,2)
    endif
   !stop
    read(1,*) thiaccptindepndt
    if (verbosity.gt.1) then
      print *,'thiaccptindepnt=',thiaccptindepndt
    endif
    if (thiaccptindepndt.eqv..true.) then
        do i=1,maxspecies
            read(1,*) typethi(1,i)
            read(1,*) meamrhodecay(1:12,0:lmaxPot,1,i)
            if (verbosity.gt.1) then
               print *,'meamrhodecay=',meamrhodecay
            endif
        enddo
    else
        do i=1,maxspecies
            do j=1,maxspecies
                read(1,*) typethi(i,j)
                read(1,*) meamrhodecay(1:12,0:lmaxPot,i,j)
            enddo
        enddo
    endif
    read(1,*) embfunctype
    read(1,*) meame0 !species
    if (verbosity.gt.1) then
      print *,'meame0=',meame0
    endif
    read(1,*) meamrho0 !species
    if (verbosity.gt.1) then
      print *,'meamrho0=',meamrho0
    endif
    read(1,*) meamemb3 !species
    read(1,*) meamemb4 !species
    do i=1,maxspecies
        do j=1,maxspecies
            if (j.ge.i) then
                read(1,*) typepairpot(i,j)
                read(1,*) pairpotparameter(1:32,i,j)
                if (verbosity.gt.1) then
                  print *,'pairpotparameter=',pairpotparameter
                endif
            endif
        enddo
    enddo
    read(1,*) rs !species 2x
    if (verbosity.gt.1) then
      print *,'rs=',rs
    endif
    read(1,*) rc !species 2
    if (verbosity.gt.1) then
      print *,'rc=',rc
    endif
    read(1,*) enconst
    if (verbosity.gt.1) then
      print *,'enconst=',enconst
    endif

    close(unit=1)


end subroutine readmeamparam
