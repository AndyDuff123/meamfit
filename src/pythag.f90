function pythag(a,b)

    !     Copyright (c) 2018, STFC

    implicit none
    real(8) a,b,pythag,absa,absb
    absa=abs(a)
    absb=abs(b)
    if(absa.gt.absb)then
        pythag=absa*sqrt(1d0+(absb/absa)**2)
    else
        if(absb.eq.0d0)then
            pythag=0d0
        else
            pythag=absb*sqrt(1d0+(absa/absb)**2)
        endif
    endif
end function pythag
