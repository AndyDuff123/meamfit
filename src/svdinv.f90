subroutine svdinv(a,m,n,mdim,ndim, &
        ainv)
    ! computes the least squares inverse Ainv for a m*n matrix A
    ! method is based on Singular Value Decomposition as described in
    ! NUMERICAL RECIPES (2nd Ed) by W. Press et al. (Cambridge Univ. Press,
    ! New York, 1992), pp.51.
    ! the input is left intact
    ! A is defined as A(mdim,ndim), Ainv(ndim,mdim)
    ! Marcel Sluiter, Nov 28 2001
    !     Copyright (c) 2018, STFC

    implicit none
    real(8), parameter:: eps=1d-10  !cutoff for singularities
    integer mdim,ndim,i,n,m
    real(8) a(mdim,ndim),ainv(ndim,mdim)
    real(8), allocatable:: ac(:,:),y(:),wm(:),vm(:,:),tmp(:)
    intent(in) a,m,n,mdim,ndim
    intent(out) ainv

    allocate(ac(m,n),y(m),wm(n),vm(n,n),tmp(n))
    ac(1:m,1:n)=a(1:m,1:n)
    wm = 0d0
    tmp= 0d0
    vm = 0d0
    call svdcmp(m,n, &
        ac, &
        tmp, &
        wm,vm)
    where(wm(1:n).lt.eps) wm=0d0
    do i=1,m
        y=0d0
        y(i)=1d0
        call svbksb(ac,wm,vm,m,n,y, &
            tmp,ainv(i,1:n))
    enddo
    deallocate(ac,y,wm,vm,tmp)
end subroutine svdinv
