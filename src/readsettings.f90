subroutine readsettings(iseed)
 
    !-------------------------------------------------------------------c
    !
    !     Reads in the settings from the settings file.
    !
    !     Called by:     program MEAMfit
    !     Calls:         -
    !     Arguments:     settingsfile,maxspecies,lmax
    !     Returns:       nsteps,freep,
    !                    ncutoffDens and ncutoffPairpot (these are only
    !        used outside of this subroutine if holdcutoffs=.true., in
    !        which case they record the number of cutoff radii which
    !        are to be randomly generated).
    !     Files read:    settingsfile
    !     Files written: -
    !
    !     Andrew Duff 2006-2016
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_filenames
    use m_datapoints
    use m_atomproperties
    use m_generalinfo
    use m_meamparameters
    use m_geometry
    use m_optimization
    use m_plotfiles
    use m_objectiveFunction
    use m_electrondensity

    implicit none

    logical onebyone,exist,foundvariable,opt_meamtau,startWordFound
    integer i,iLmp,j,k,iseed,nPara,ncutoffs,istart,&
            ipara,paraCnt,l,nembterms,ncutoffDens_overall,ncutoffPairpot_overall,nMultiple,lineNum,nVar
    character*1 string2,string3
    character*5 writecheck
    character*80 searchfor,found
    character*80 foundMultiple(10)
    character*80 string

    !Check there is a settings file. If not, write one and stop.
    open(unit=1,file=trim(settingsfile))
    if (settingsfileexist.eqv..false.) then
       print *
       print *,'WARNING: settings file does not exist.'
       nGuesses_Rnd=10
       nGuesses_GA=10
       maxoptfuncallowed=0.1d0
       optimizestruc=.false.
       nsteps=100000
       lookuptables=.false.
       onebyone=.true.
       write(*,*) 'Writing template settings file.'
       write(1,*) 'TYPE=EAM'
       write(1,*) '# CUTOFF_MAX='
       write(1,*) 'NTERMS=3'
       write(1,*) 'NTERMS_EMB=3'
       close(1)
       createdSettings=.true.
       return
    else

       !Search through settings file tags (VERBOSITY, etc) and place values in the
       !corresponding variables. Keep track of total number of variables using nVar.
       !At the end check that this equals total number of '=' symbols in settings file.
       nVar=0
       if (allocated(typethi)) deallocate(typethi)
       if (allocated(typepairpot)) deallocate(typepairpot)
       allocate( typethi(maxspecies,maxspecies), &
                 typepairpot(maxspecies,maxspecies) )
       readParasOnebyone=.false.
       readParasPairwise=.false. !Keeps track of whether parameters to be optimized have been specified corresponding to pair
                                 !potentials between species (used later in checking and deciding how to action redundant parameters)
       printoptparas=.false.
       randgenparas=.false. !This will be set to true later if at least one
                            !parameter is to be randomly generated

       !Check first that verbosity hasn't been set to 1 already (no/little standard output) through a command line argument
       if (verbosity.eq.2) then
          searchfor="VERBOSITY" !Extra information written to screen
          call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
          if (foundvariable) then
             nVar=nVar+1
             read(found,*) verbosity
!            if (verbosity.eq.0) then
!                  print *,'ERROR: To specify verbosity=0 please use the -verb/-verbosity flag instead'
!                  print *,'of the VERBOSITY flag in the settings file, STOPPING.'
!            endif
             if (verbosity.gt.1) write(*,'(A11,I1,A21)') ' verbosity=',verbosity,' (from settings file)'
          else
             verbosity=2
             if (verbosity.gt.1) write(*,'(A48,I1)') 'No VERBOSITY in settings file, using VERBOSITY=',verbosity
          endif
       endif

       searchfor="READLAMMPSPOT" !Read in a LAMMPS file (XY.eam.alloy). All non-zero 
                        !contributions will then be held fixed in optimization
       call getsettingspara(searchfor,-1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       !print *,'found=',found
       !print *,'foundMultiple(1:',nMultiple,')=',foundMultiple(1:nMultiple)
       !print *,'foundvariables=',foundvariable
       if (foundvariable) then
          nVar=nVar+1
          nLammpsFiles=nMultiple+1
          lammpsPotIn(1)=found
          if (nLammpsFiles.gt.1) then
             do iLmp=2,nLammpsFiles
                lammpsPotIn(iLmp)=foundMultiple(iLmp-1)
             enddo
          endif
          if (verbosity.gt.1) then
             write(*,'(A15,L1,A21)') ' READLAMMPSPOT specified (from settings file):'
             write(*,'(A3,I1,A13)',ADVANCE='NO') '   ',nLammpsFiles,' to read in: '
             do iLmp=1,nLammpsFiles
                write(*,'(A)',ADVANCE='NO') trim(lammpsPotIn(iLmp))
                if (iLmp<nLammpsFiles) then
                   write(*,'(A2)',ADVANCE='NO') ', '
                else
                   write(*,'(A1)') ''
                endif
             enddo
          endif
          do iLmp=1,nLammpsFiles
             inquire(file=trim(lammpsPotIn(iLmp)),exist=exist)
             if (exist.eqv..false.) then
                print *,'ERROR: specified read in LAMMPS potential file ', &
                       trim(lammpsPotIn(iLmp)),' but file does not exist, stopping.'
                stop
             endif
          enddo

          !stop
       else
          nLammpsFiles=0
       endif
       !stop

       searchfor="ANALYZEVIB" !Read in forces for MD runs of vibrating solids using different 
                     !timesteps, currently coded for VASP. wFr in fitdbse must contain the POTIM value for 
                     !each vasprun file. Will print out average period of oscillation of atoms (in terms of
                     !number of MD propogation timesteps) as a function of POTIM and atomic species.
                     !For subsequent use in APD.
                     !NOTE: ensure that all DFT files corresponding to a given
                     !POTIM value are grouped together in the fitdbse file.
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) analyzeVib
          if (verbosity.gt.1) write(*,'(A12,L1,A21)') ' analyzeVib=',analyzeVib,' (from settings file)'
          !If specified check that weights in fitdbse are correct
          do istr=1,nstruct
                if (weightsFr(istr).eq.0d0) then
                   print *,'ERROR: to use the analyzeVib functionality you need to set weights'
                   print *,'on forces to non-zero in fitdbse, STOPPING.'
                   stop
                elseif ((weightsEn(istr).ne.0d0).or.(weightsSt(istr).ne.0d0)) then
                   print *,'ERROR: to use the analyzeVib functionality you need to set weights'
                   print *,'on energies and stresses to zero in fitdbse, STOPPING.'
                endif
           enddo
       else
          analyzeVib=.false.
          if (verbosity.gt.1) write(*,'(A49,L1)') 'No ANALYZEVIB in settings file, using ANALYZEVIB=',analyzeVib
       endif

       searchfor="CUTOFFPEN" !Cut off penalty to stop cutoffs getting too large or small
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) cutoffPen
          if (verbosity.gt.1) write(*,'(A11,L1,A21)') ' cutoffPen=',cutoffPen,' (from settings file)'
       else
          cutoffPen=.true.
          if (verbosity.gt.1) write(*,'(A48,L1)') 'No CUTOFFPEN in settings file, using CUTOFFPEN=',cutoffPen
       endif

       searchfor="CUTOFFPENDIST" ! Penalty = prefact * ( cutoff - (rmax-dist) )^3, for rmax
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) cutoffPenDist
          if (verbosity.gt.1) write(*,'(A15,F15.10,A21)') ' cutoffPenDist=',cutoffPenDist,' (from settings file)'
       else 
          cutoffPenDist=0.1d0
          if (verbosity.gt.1) write(*,'(A56,F15.10)') 'No CUTOFFPENDIST in settings file, using CUTOFFPENDIST=',cutoffPenDist
       endif

       searchfor="CUTOFFPENPREFACT" !Use quasi Newton scheme (as opposed to CG)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) cutoffPenPrefact
          if (verbosity.gt.1) write(*,'(A18,F20.10,A21)') ' cutoffPenPrefact=',cutoffPenPrefact,' (from settings file)'
       else 
          cutoffPenPrefact=1000d0
          if (verbosity.gt.1) write(*,'(A62,F20.10)') 'No CUTOFFPENPREFACT in settings file, using CUTOFFPENPREFACT=',cutoffPenPrefact
       endif

       searchfor="ANALYTICDERIVS" !Use quasi Newton scheme (as opposed to CG)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) analyticDerivs
          if (verbosity.gt.1) write(*,'(A16,L1,A21)') ' analyticDerivs=',analyticDerivs,' (from settings file)'
       else
          analyticDerivs=.true.
       endif

       searchfor="QUASINEWTON" !Use quasi Newton scheme (as opposed to CG)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) quasiNewton
          if (verbosity.gt.1) write(*,'(A13,L1,A21)') ' quasiNewton=',quasiNewton,' (from settings file)'
       else
          quasiNewton=.true.
       endif

       searchfor="LAMMPSDURINGOPT" !Write out lammps potential files (NaCl.eam.alloy_1, etc) during the optimization. This is costly (since we ideally want a lot of radial points) so set to false by default. When false lammps potential files only output on successful convergence or when using --nopt (or equivalent settings tag)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) lammpsDuringOpt
          if (verbosity.gt.1) write(*,'(A17,L1,A21)') ' lammpsDuringOpt=',lammpsDuringOpt,' (from settings file)'
       else
          lammpsDuringOpt=.false.
       endif

       searchfor="EXCLUDECOULOMB" !Calculate Coulomb energy/Coulomb contribution to 
       !forces, stress tensors, for each config and subtract these from the
       !values read in from the DFT file. (so that we only fit to the 'non
       !Coulomb' part.
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) excludeCoulomb
          if (verbosity.gt.1) write(*,'(A16,L1,A21)') ' excludeCoulomb=',excludeCoulomb,' (from settings file)'
       else
          excludeCoulomb=.false.
       endif

       searchfor="EXCLUDEFORCEMAX" !do not try to fit forces with very large magnitude
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) excludeforcemax
          write(*,'(A16,g12.4,A21)') ' excludeforcemax=',excludeforcemax,' (from settings file)'
       else
          excludeforcemax=huge(0d0)
       endif
       searchfor="EXCLUDEFORCEMIN" !do not try to fit forces with very small magnitude
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) excludeforcemin
          write(*,'(A16,g12.4,A21)') ' excludeforcemin=',excludeforcemin,' (from settings file)'
       else
          excludeforcemin=0d0
       endif
       searchfor="FORCEMAGNITUDEPOWER" !assign fitting weight according to force magnitude
       !with the idea to decrease the impact of large forces, move towards a LASSO like fit
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) forcemagnitudepower
          if(forcemagnitudepower.lt.-2d0.or.forcemagnitudepower.gt.0d0)then
             write(*,'(A16,g12.4,A21)') ' ERROR: forcemagnitudepower must be [-2:0], stopping'
             stop
          endif
          write(*,'(A16,g12.4,A21)') ' forcemagnitudepower=',forcemagnitudepower,' (from settings file)'
          if(forcemagnitudepower.ne.0d0.and.excludeforcemin.eq.0d0) then
             excludeforcemin=1d-3  !can be adjusted: currently  1 meV/Angstrom
             write(*,'(a)') ' excludeforcemin has been adjusted to avoid singular force-weights'
             write(*,'(A16,g12.4,a)') ' excludeforcemin=',excludeforcemin,' (imposed through FORCEMAGNITUDEPOWER)'
          endif
       else
          forcemagnitudepower=0d0
       endif

       searchfor="WRITEPOT" !Write out the Camelion and LAMMPS potentials
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) writePot
          if (verbosity.gt.1) write(*,'(A10,L1,A21)') ' WRITEPOT=',writePot,' (from settings file)'
       else
          writePot=.true.
       endif

       searchfor="DLPOLYOUT" !Write out the Camelion and LAMMPS potentials
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) dlpolyOut
          if (verbosity.gt.1) write(*,'(A11,L1,A21)') ' DLPOLYOUT=',dlpolyOut,' (from settings file)'
       else
          dlpolyOut=.false.
       endif

       searchfor="POTFUNCOUT" !Write out emb func, pair pots, elec dens, etc
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) potFuncOut
          if (verbosity.gt.1) write(*,'(A12,L1,A21)') ' POTFUNCOUT=',potFuncOut,' (from settings file)'
       else
          potFuncOut=.false.
       endif

       searchfor="USEREF" !Use reference structure to calculate energy differences
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) useRef
          if (verbosity.gt.1) write(*,'(A8,L1,A21)') ' USEREF=',useRef,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No USEREF in settings file, using USEREF=true'
          useRef=.true.
       endif
       if ((analyticDerivs.eqv..true.).and.(useRef.eqv..false.).and.(noOpt.eqv..false.).and.(energyfit.eqv..true.)) then
          print *,'ERROR: analytic derivatives not yet implemented for useRef=.false., '
          print *,'when energy fitting, STOPPING.'
          stop
       endif
       if ((useRef.eqv..true.).and.(weightsEn(1).eq.0d0).and.(analyzeVib.eqv..false.)) then
          print *,'ERROR: you wish to use reference energies, but the first configuration (always taken as an'
          print *,'energy reference by default) has weightEn=0. Ensure > 0 or set USEREF=.false. if you are only'
          print *,'fitting to forces and/or stress tensors. STOPPING.'
          stop
       endif

       searchfor="SCALEOBYVAR" !Scale Fen by var of energies. For int ens you shouldn't (energies can be very far apart and we
       !are only interested in the differences expressed by the interaction energy equations)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) scaleObyVar
          if (verbosity.gt.1) write(*,'(A13,L1,A21)') ' SCALEOBYVAR=',scaleObyVar,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No SCALEOBYVAR in settings file, using SCALEOBYVAR=true'
          scaleObyVar=.true.
       endif

       searchfor="SCALEOFRCBYVAR" !Scale Ffrc by var of forces. When fitting only to zero forces need to remove this
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) scaleOFrcbyVar
          if (verbosity.gt.1) write(*,'(A16,L1,A21)') ' SCALEOFRCBYVAR=',scaleOFrcbyVar,' (from settings file)'
       else
          if (verbosity.gt.1) then
             print *,'No SCALEOFRCBYVAR in settings file, using SCALEOFRCBYVAR=true'
          endif
          scaleOFrcbyVar=.true.
       endif

       searchfor="FITLARGEFORCES" !Fit forces only if their magnitude is above some critical value (see readtargetdata.f90)
       !which is currently hard-wired. Forces can still be specified to be fit in addition using the selective force tagging
       !method.
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) fitLargeForces
          if (verbosity.gt.1) write(*,'(A16,L1,A21)') ' FITLARGEFORCES=',fitLargeForces,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No FITLARGEFORCES in settings file, using FITLARGEFORCES=false'
          fitLargeForces=.false.
       endif

       searchfor="OUTPUTLAMMPSPOSNS" !Produce LAMMPS positions files based on the input positions
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) outputLAMMPSposns
          if (verbosity.gt.1) write(*,'(A19,L1,A21)') ' outputLAMMPSposns=',outputLAMMPSposns,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No OUTPUTLAMMPSPOSNS in settings file, using OUTPUTLAMMPSPOSNS=false'
          outputLAMMPSposns=.false.
       endif

       !   searchfor="LOWDENSPEN" !Penalty for low electron densities. I tried
       !       !to use this to stop negative electron densities but there are
       !       too many issues. See hand-written notes 'Gamma limit', 'negative
       !       density test', etc.
       !   call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       !   if (foundvariable) then
       !      read(found,*) lowDensPen
       !      if (verbosity.gt.1) write(*,'(A12,L1,A21)') ' LOWDENSPEN=',lowDensPen,' (from settings file)'
       !   else
       !      print *,'No LOWDENSPEN in settings file, using LOWDENSPEN=true'
       !      lowDensPen=.true.
       !   endif

       searchfor="NUMDIFF" !Perform a numerical differentiation check on the
                           !analytic derivatives dO/dpara for single shot calculation
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) numDiff
          if (verbosity.gt.1) write(*,'(A5,L1,A21)') ' NUMDIFF=',numDiff,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No NUMDIFF in settings file, using NUMDIFF=false'
          numDiff=.false.
       endif

       searchfor="DEBUG" !Allow negative electron densities if true
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) debug
          if (verbosity.gt.1) write(*,'(A7,L1,A21)') ' DEBUG=',debug,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No DEBUG in settings file, using DEBUG=false'
          debug=.false.
       endif


       searchfor="NEGELECDENS" !Allow negative electron densities if true
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) negElecDensAllow
          if (verbosity.gt.1) write(*,'(A13,L1,A21)') ' NEGELECDENS=',negElecDensAllow,' (from settings file)'
          if (negElecDensAllow.eqv..false.) then
             negElecDensWarning=.false.
          endif
       else
          if (verbosity.gt.1) print *,'No NEGELECDENS in settings file, using NEGELECDENS=false'
          negElecDensAllow=.false.
          negElecDensWarning=.false.
       endif

       searchfor="ORTHOGELECDENS" !Add extra term to l=3 electron density to get
                       !orthogonal electron densities (Legendre polynomials)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) orthogElecDens
          if (verbosity.gt.1) write(*,'(A16,L1,A21)') ' ORTHOGELECDENS=',orthogElecDens,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No ORTHOGELECDENS in settings file, using ORTHOGELECDENS=true'
          orthogElecDens=.true.
       endif

       searchfor="SEED" !A seed can be specified for debugging purposes
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) iseed
          if (verbosity.gt.1) write(*,'(A6,I10,A21)') ' seed=',iseed,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No SEED in settings file, using time to seed random numbers.'
          iseed=0
       endif

       searchfor="OBJFUNC" !Type of objective function
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) objFuncType
          if (verbosity.gt.1) then
             write(*,'(A13,I10,A21)') ' OBJFUNCTYPE=',objFuncType,' (from settings file)'
             if (objFuncType.eq.1) then
                print *,'    (standard variance weighted least squares- the default)'
             elseif (objFuncType.eq.2) then
                print *,'    (standard deviation objective function- for testing purposes)'
             else
                print *,'    ERROR: OBJFUNC not equal 1 or 2, STOPPING.'
                stop
             endif
          endif
       else
          if (verbosity.gt.1) print *,'No OBJFUNC in settings file, using default value of 1 (standard variance weighted least squares).'
          objFuncType=1
       endif


       searchfor="MUT_PROB" !Mutation probability in the GA
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) probRand
          if (verbosity.gt.1) write(*,'(A10,F15.10,A21)') ' MUT_PROB=',probRand,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No MUT_PROB in settings file, using default (0.3).'
          probRand=0.3d0
         !if (verbosity.gt.1) then
         !   print *,'   (Probability of a mutation, I.e. random parameterization, for a'
         !   print *,'    given potential parameter/group of potential parameters)'
         !endif
       endif

       searchfor="PROBPOT1" !Probability that 'genes' of parent 1 are passed to offspring
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) probPot1
          if (verbosity.gt.1) write(*,'(A10,F15.10,A21)') ' PROBPOT1=',probPot1,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No PROBPOT1 in settings file, using default (0.5).'
          probPot1=0.5d0
       endif

       searchfor="NOPTFUNCSTORE" !No. parents to be stored for GA (also the number of 
                                 !potentials to be saved)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) noptfuncstore
          if (verbosity.gt.1) write(*,'(A15,I10,A21)') ' NOPTFUNCSTORE=',noptfuncstore,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No NOPTFUNCSTORE in settings file, using default (10).'
          noptfuncstore=10
       endif

       searchfor="OUTVSTIME" !Produce every # hrs potparas_best1, objective function, and rms energies, 
                             !forces, stresses (output to 'potentialvsTime.dat'). Set to -1 to switch off.
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) outputVsTime
          if (verbosity.gt.1) write(*,'(A11,I3,A21)') ' OUTVSTIME=',outputVsTime,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No OUTVSTIME in settings file, using default (OUTVSTIME=5).'
          outputVsTime=5
       endif

       searchfor="NGUESSES_RND" !A minimum of this number of random guesses to move from random 
                             !sampling to CG optimization phase
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) nGuesses_Rnd
          if (verbosity.gt.1) write(*,'(A14,F10.5,A21)') ' NGUESSES_RND=',nGuesses_Rnd,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No NGUESSES_RND in settings file, using default (NGUESSES_RND=10).'
          nGuesses_Rnd=10
       endif

       searchfor="GENALGO" !Whether to use genetic algorithm having filled up
                           !optimization function table
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) genAlgo
          if (verbosity.gt.1) write(*,'(A9,F10.5,A21)') ' GENALGO=',genAlgo,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No GENALGO in settings file, using default (GENALGO=TRUE).'
          genAlgo=.true.
       endif

       searchfor="OPTFUNC_ERR" !This quantity is used in the CG routine, and denotes
                             !the error in calculating the optimization
                             !function. Needs reducing for force-fit due to
                             !finite-difference approach
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) optfunc_err
          if (verbosity.gt.1) write(*,'(A13,F10.5,A21)') ' OPTFUNC_ERR=',optfunc_err,' (from settings file)'
       else
          if (forcefit.eqv..false.) then
             if (verbosity.gt.1) print *,'No OPTFUNC_ERR in settings file, using default for energy fit (OPTFUNC_ERR=10^-14).'
             optfunc_err=1.d-14
          else
             if (verbosity.gt.1) print *,'No OPTFUNC_ERR in settings file, using default for fit including forces (OPTFUNC_ERR=10^-4).'
             optfunc_err=1.d-4
          endif
       endif

       searchfor="NGUESSES_GA" !A minimum of this number of GA guesses for a spliced/mutated 
                                !offspring to be CG optimized
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) nGuesses_GA
          if (verbosity.gt.1) write(*,'(A13,F10.5,A21)') ' NGUESSES_GA=',nGuesses_GA,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No NGUESSES_GA in settings file, using default (NGUESSES_GA=10).' 
          nGuesses_GA=10
       endif

       searchfor="OPTDIFF" !The precision in the relative optimization function at which
                           !CG minimization is terminated
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) optdiff
          if (verbosity.gt.1) write(*,'(A9,E10.5,A21)') ' OPTDIFF=',optdiff,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No OPTDIFF in settings file, using default (=10^-10)'
          optdiff=1d-10
       endif

       searchfor="OPTACC" !When the optfuncs of the best and worst stored potentials are within
                          !this amount of one another, optimization will stop.
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) optAcc
          if (verbosity.gt.1) write(*,'(A8,F10.5,A21)') ' OPTACC=',optAcc,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No OPTACC in settings file, using default (=0.01)'
          optAcc=0.01d0 !Usd to use 0.0005 but this seems excessive. By 0.01 my tests on BaZrO3 show that a further local optimization does not yield an overall better solution than letting the genetic algorithm proceeed to 0.0005 and then locally optimizing
       endif

       searchfor="STOPTIME" !Optimization will stop after this length of time (see further
                            !HARDSTOP)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) stopTime
          if (verbosity.gt.1) write(*,'(A10,I5,A21)') ' STOPTIME=',INT(stopTime),' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No STOPTIME in settings file, using default (=168 hours=1 week)'
          stopTime=168d0
       endif

       searchfor="HARDSTOP" !If .true., simulation will definately end before
                            !stopTime (conservative) -useful if running remotely
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) hardStop
          if (verbosity.gt.1) write(*,'(A10,L1,A21)') ' HARDSTOP=',hardStop,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No HARDSTOP in settings file, using default (=.false.)'
          hardStop=.false.
       endif


       searchfor="MAXFUNCEVALS" !Maximum number of function evaluations before CG minimzation
                                !is terminated)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) maxfuncevals
          if (verbosity.gt.1) write(*,'(A14,I7,A21)') ' MAXFUNCEVALS=',maxfuncevals,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No MAXFUNCEVALS in settings file, using default (=500)'
          maxfuncevals=500
       endif

       searchfor="OPTFUNCSTP" !If the optfunc is smaller than this, optimization will stop
                              !(Andy: no practical use - remove this)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) maxoptfuncallowed
          if (verbosity.gt.1) write(*,'(A12,F10.5,A21)') ' OPTFUNCSTP=',maxoptfuncallowed,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No OPTFUNCSTP in settings file, using default (=0d0)'
          maxoptfuncallowed=0d0
       endif

       searchfor="OPTIMIZESTRUC" !If true, relax crystal structures according to potential during
                                 !optimization (Andy: do not use yet, needs further development/testing)
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) optimizestruc
          if (verbosity.gt.1) write(*,'(A13,L1,A21)') ' OPTIMIZESTRUC=',optimizestruc,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No OPTIMIZESTRUC in settings file, using default (=.false.)'
          optimizestruc=.false.
       endif

       searchfor="forcemut" !force one (and only one) mutation per cross-breeding
       call getsettingspara(searchfor,1,found,foundmultiple,nmultiple,foundvariable,linenum)
       if (foundvariable) then
          nvar=nvar+1
          read(found,*) forcemut
          if (verbosity.gt.1) write(*,'(a10,l1,a21)') ' forcemut=',forcemut,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'no forcemut in settings file, using default (=.false.)'
          forcemut=.false.
       endif

       !Check if a potential is to be read in from a file
       if (readpotfile.eqv..true.) then
          if (verbosity.gt.1) print *,'POTFILEIN=',trim(startparameterfile),' (from command-line)'
       else
          searchfor="POTFILEIN"
          call getsettingspara(searchfor,1,startparameterfile,foundMultiple,nMultiple,foundvariable,lineNum)
          if (foundvariable) then
             nVar=nVar+1
             if (verbosity.gt.1) print *,'POTFILEIN=',trim(startparameterfile),' (from settings)'
             readpotfile=.true.
          else
             if (verbosity.gt.1) print *,'No POTFILEIN in settings file, not reading in potential from file'
             readpotfile=.false.
          endif
       endif

       searchfor="WRITEEXPOT" !Produce an example potential file as a guide for user to enter in
                              !their own starting point for an optimization (Andy: in development)
       call getsettingspara(searchfor,1,startparameterfile,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          if (verbosity.gt.1) print *,'WRITEEXPOT=',trim(startparameterfile),' (from settings file)'
          if (readpotfile) then
             print *,'ERROR: you cannot both read in (POTFILEIN) and read out (WRITEEXPOT)'
             print *,'a potential file, stopping.'
             stop
          endif
          writeExPot=.true.
       else
          if (verbosity.gt.1) print *,'No WRITEEXPOT in settings file, using default (=.false.)'
          writeExPot=.false.
       endif

       searchfor="WRITECFG" !Once DFT input read in, write it out in a .cfg format (for use with MLIP
                            !for training MTP potentials) then exit. Useful for CASTEP->.cfg
       call getsettingspara(searchfor,1,found,foundmultiple,nmultiple,foundvariable,linenum)
       if (foundvariable) then
          nvar=nvar+1
          read(found,*) writecfg
          if (verbosity.gt.1) write(*,'(a10,l1,a21)') ' WRITECFG=',writecfg,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'no WRITECFG in settings file, using default (=.false.)'
          writecfg=.false.
       endif

       searchfor="HOLDCUTOFFS" !Check if cut off radii in pairwise terms are to be held fixed initially
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) holdcutoffs
          if (verbosity.gt.1) write(*,'(A13,L1,A21)') ' HOLDCUTOFFS=',holdcutoffs,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No HOLDCUTOFFS in settings file, using default (=.false.)'
          holdcutoffs=.false. !Later, once tested, the default will be changed to true
       endif

       searchfor="FIXPOTIN" !Check if the input potential is to be fixed or allowed to optimize
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          if (readpotfile.eqv..false.) then
             print *,'ERROR: FIXPOTIN specified, yet no input parameter file provided,'
             print *,'stopping.'
             stop
          endif
          read(found,*) fixPotIn
          if (verbosity.gt.1) write(*,'(A10,L1,A21)') ' FIXPOTIN=',fixPotIn,' (from settings file)'
       else
          if (readpotfile) then
             if (verbosity.gt.1) print *,'No FIXPOTIN in settings file: allow input potential to optimize'
          endif
          fixPotIn=.false.
       endif

       !Check if a continuation job is to be performed (uses the potparas_best files and 
       !bestoptfuncs file as starting point)
       if (contjob.eqv..true.) then
          if (verbosity.gt.1) write(*,'(A6,L1,A20)') ' CONT=',contjob,' (from command line)'
       else
          searchfor="CONT"
          call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
          if (foundvariable) then
             nVar=nVar+1
             read(found,*) contjob
             if (verbosity.gt.1) write(*,'(A6,L1,A21)') ' CONT=',contjob,' (from settings file)'
             if (readpotfile.and.contjob) then
                print *,'ERROR: you cannot have POTFILEIN=T and CONT=T simultaneously, stopping.'
                stop
             endif
          else
             if (verbosity.gt.1) print *,'No CONT in settings file, using default (=.false.)'
             contjob=.false.
          endif
       endif

       !Check if an optimization is to be performed (NOPT=TRUE if not)
       if (noOpt.eqv..true.) then
          !Check first if NOOPT has been specified already from the command-line
          nsteps=1
          if (verbosity.gt.1) write(*,'(A31)') ' NOOPT=TRUE (from command-line)'
       else
          !Default values if NOOPT is not found:
          nsteps=2 !infinite no. steps
          searchfor="NOOPT" !aka nsteps, which is either 1 for no opt or >1
                            !otherwise
          call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
          if (foundvariable) then
             nVar=nVar+1
             read(found,*) noOpt
             if (noOpt.eqv..true.) then
                if (verbosity.gt.1) write(*,'(A32)') ' NOOPT=TRUE (from settings file)'
                nsteps=1
             endif
          endif
       endif

       !Read in maximum cut-off radius - necessary if optimizing; writing an example potential
       !file; or producing a separation histogram. Cut-off radii cannot take values greater than
       !this.
       searchfor="CUTOFF_MAX"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) cutoffMax
          if (verbosity.gt.1) write(*,'(A12,F10.5,A21)') ' CUTOFF_MAX=',cutoffMax,' (from settings file)'
       else
          if ((nsteps.gt.1).or.(writeExPot)) then
             print *,'No CUTOFF_MAX in settings file, MUST be specified,'
             print *,'STOPPING.'
             stop
          endif
          if (readpotfile.eqv..false.) then
             if (analyzeVib.eqv..false.) then
                !...user is likely interested in generating a sepnHistogram file
                !from the DFT data
                print *,'No CUTOFF_MAX in settings file. Please use this tag to'
                print *,'supply a maximum seperation to be plotted out to in the'
                print *,'sepnHistogram file, STOPPING.'
                stop
             endif
          else
             !Reading in a potential to output observables with no optimization.
             !Cutoff only needed to compute penalty function - if none
             !specified switch off calculation of penalty function.
             cutoffPen=.false.
          endif
       endif

       !Read in minimum interatomic separation to be used in plotting of the
       !graphs. It is better to include this here rather than in the graph
       !plotting software as it allows the cut-off ticks to be scaled
       !accordingly.
       searchfor="PLOT_SEPN_MIN" 
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) plotSepnMin
          if (verbosity.gt.1) write(*,'(A15,F10.5,A21)') ' PLOT_SEPN_MIN=',plotSepnMin,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No PLOT_SEPN_MIN in settings file, using default (PLOT_SEPN_MIN=0)'
          plotSepnMin=0d0
       endif

       !Read in minimum cut-off radius if provided. Cut-off radii cannot take values smaller 
       !than this
       searchfor="CUTOFF_MIN"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) cutoffMin
          if (cutoffMin.lt.cutoffMinLim) then
             print *,'ERROR: cutoffMin < ',cutoffMinLim,'. Please choose larger value.'
             print *,'(cutoff radii would enter the Ziegler-Biersak region otherwise) Stopping.'
             stop
          endif
          if (verbosity.gt.1) write(*,'(A12,F10.5,A21)') ' CUTOFF_MIN=',cutoffMin,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No CUTOFF_MIN in settings file, using default (CUTOFF_MIN=',cutoffMinLim,')'
          cutoffMin=cutoffMinLim
       endif

       !Read in potentials from r vs function tables. Andy: not yet implemented
       searchfor="LOOKUPTABLES"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) lookuptables
          if (verbosity.gt.1) write(*,'(A12,L1,A21)') ' LOOKUPTABLES=',lookuptables,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No LOOKUPTABLES in settings file, using default (=.false.)'
          lookuptables=.false.
       endif

    endif

       !Read in type of potential (EAM or MEAM)
       searchfor="TYPE"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) string
          if (string(1:3).eq.'EAM') then
             if (verbosity.gt.1) print *,'TYPE=EAM (from settings file)'
             if ((readpotfile.eqv..true.).and.(lmax.gt.0)) then
                print *,'ERROR: potential parameter input file describes a MEAM potential,'
                print *,'conflicting with this setting, STOPPING.'
                stop
             endif
             lmax=0
          elseif (string(1:4).eq.'MEAM') then
             if (verbosity.gt.1) print *,'TYPE=MEAM (from settings file)'
             lmax=3
          else
             print *,'ERROR: TYPE not recognized in settings file'
             stop
          endif
       else
          if (readpotfile.eqv..false.) then
             if (verbosity.gt.1) print *,'No TYPE in settings file, using default (=EAM)'
             lmax=0
          endif
       endif

       !Initialize freep array. This is the array which tells the code which
       !parameters are to be optimized (and will be filled in this subroutine -
       !but possibly amended again later in the code)
       np=2*maxspecies*maxspecies*maxspecies+ &
           maxspecies*maxspecies*34+ &
           maxspecies*(6+lmax)+ &  ! i think this should be maxspecies*(5+lmax)+ e.g. to be consistent with writemeamp (search (5+lmax)*m1 ) and elsewhere. howver i don't think this introduces any errors, it just makes arrays slightly too big and checks on parameters (e.g. using freep or to check sign) are carried out on maxspecies extra elements more than necessary. 
           12*(lmax+1)*(maxspecies*maxspecies)
       if (.not.allocated(freep)) then
           allocate(freep(np))
       endif
       freep=0
       call initParaLimits !Set up variables useful for navigating the freep() and p() arrays

       !Upper and lower bounds for random generation of potentials are usually
       !set in-code - the following option allows explicit specification of
       !these limits from the settings file
       searchfor="RANDPARASMANUAL"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) readparasfromsettings
          if (verbosity.gt.1) write(*,'(A17,L1,A21)') ' RANDPARASMANUAL=',readparasfromsettings,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No RANDPARASMANUAL in settings file, using default (=.FALSE.)'
          readparasfromsettings=.false.
       endif

       !Option for using environment dependent meam_t's
       if (lmax.gt.0) then
          searchfor="ENVDEPMEAMT"
          call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
          if (foundvariable) then
             nVar=nVar+1
             read(found,*) envdepmeamt
             if (verbosity.gt.1) write(*,'(A13,L1,A21)') ' ENVDEPMEAMT=',envdepmeamt,' (from settings file)'
          else
             if (verbosity.gt.1) print *,'No ENVDEPMEAMT in settings file, using default (=.FALSE.)'
             envdepmeamt=.false.
          endif
       endif

       !Option for allowing electron densities to depend not just on the species
       !of the atom from which it is originating but also on the species of atom
       !which it is incident on (set THIACCPTINDEPNDT=.false. if this is required).
       !Andy: please do not use yet - should work but not extensively tested.
       searchfor="THIACCPTINDEPNDT"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) thiaccptindepndt
          if (verbosity.gt.1) write(*,'(A18,L1,A21)') ' THIACCPTINDEPNDT=',thiaccptindepndt,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No THIACCPTINDEPNDT in settings file, using default (=.TRUE.)'
          thiaccptindepndt=.true.
       endif

       !Analytic form of electron density (Andy: curently only one available but
       !future developments will enable multiple types - also for TYPE_EMB, TYPE_PP)
       searchfor="TYPE_DENS"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) typethi
          if (verbosity.gt.1) write(*,'(A11,I1,A21)') ' TYPE_DENS=',typethi,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No TYPE_DENS in settings file, using default (=1)'
          typethi=1
       endif

       !Analytic form for embedding function
       searchfor="TYPE_EMB"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) embfunctype
          if (verbosity.gt.1) write(*,'(A10,I1,A21)') ' TYPE_EMB=',embfunctype,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No TYPE_EMB in settings file, using default (=1)'
          embfunctype=1
       endif

       !Analytic form for pair-potential
       searchfor="TYPE_PP"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) typepairpot
          if (verbosity.gt.1) write(*,'(A9,I1,A21)') ' TYPE_PP=',typepairpot,' (from settings file)'
       else
          if (verbosity.gt.1) print *,'No TYPE_PP in settings file, using default (=2)'
          typepairpot=2
       endif

       !Maximum electron density to be used in the LAMMPS and Camelion output
       !files. Can either be specified or will be determined based on the
       !maximum value encountered during the fitting.
       searchfor="RHOMAX_LAMMPS"
       call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
       if (foundvariable) then
          nVar=nVar+1
          read(found,*) rhomax
          if (verbosity.gt.1) write(*,'(A15,F20.10,A21)') ' RHOMAX_LAMMPS=',rhomax,' (from settings file)'
          autorhomax=.false.
       else
          if (verbosity.gt.1) then
             print *,'No RHOMAX_LAMMPS in settings file, will create'
             print *,'automatically based on maximum rho encountered from'
             print *,'fitting database.'
          endif
          autorhomax=.true.
       endif

      !searchfor="SPLN_NVALS"
      !call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
      !if (foundvariable) then
      !   read(found,*) splnNvals
      !   write(*,'(A12,I10,A21)') ' SPLN_NVALS=',splnNvals,' (from settings file)'
      !else
      !   if (verbosity.gt.1) then
      !      print *,'No SPLN_NVALS in settings file, using default (=100000),'
      !   endif
      !   splnNvals=100000
      !endif

       if (nsteps.gt.1) then
        allocate(ncutoffDens(maxspecies,maxspecies), &
                 ncutoffPairpot(maxspecies,maxspecies))
        !See if the parameters to optimized are to be given explicitly to the
        !code (PARASTOOPT={...}, where ... are a list of 0's 1's and 2's in the
        !same order as the parameters are given in the potparas_best files. An
        !example of such a list can be requested by entering PARASTOOPT=write).
        !Andy: I do not expect people will use this, as the code has been
        !designed to avoid it - but it might be useful under specific
        !circumstances)
        searchfor="PARASTOOPT"
        call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
        if (foundvariable) then
           nVar=nVar+1
           if (fixPotIn.eqv..true.) then
              print *,'ERROR: Use of FIXPOTIN inconsistent with use of PARASTOOPT.'
              print *,'Please remove one or the other and restart. Stopping.'
              stop
           endif
           writecheck=found(1:5)
           if ((writecheck.eq."write").or.(writecheck.eq."WRITE").or.(writecheck.eq."Write")) then
              if (verbosity.gt.1) print *,'Found PARASTOOPT: Write flag detected. Outputing example input for this tag.'
              printoptparas=.true.
           else
              if (verbosity.gt.1) print *,'Found PARASTOOPT (reading optimization parameters in one-by-one)'
              call readinoptparasonebyone
           endif
           readParasOnebyone=.true.
           randgenparas=.true.
        else
           if (verbosity.gt.1) print *,'No PARASTOOPT, searching for NTERM tags'
           readParasOnebyone=.false.
        endif
        !If PARASTOOPT is not specified, instead look for NTERMS flags. NTERMS
        !specifies the number of terms to be used in the pairwise functions
        ncutoffs=0
        searchfor="NTERMS"
        call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
        if (foundvariable) then
           nVar=nVar+1
           if (readParasOnebyone) then
             print *,'ERROR: NTERMS tag conflicts with PARASTOOPT. Please remove'
             print *,'one and restart, stopping.'
             stop
           endif
           read(found,*) ncutoffs
           if (verbosity.gt.1) write(*,'(A8,I2,A21)') ' NTERMS=',ncutoffs,' (from settings file)'
           randgenparas=.true.
        endif

        !NTERMS can instead be specified for the electron densities and
        !pair-potentials separately using NTERMS_DENS and NTERMS_PP
        ncutoffDens_overall=0
        searchfor="NTERMS_DENS"
        call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
        if (foundvariable) then
           nVar=nVar+1
           if (readParasOnebyone) then
             print *,'ERROR: NTERMS_DENS tag conflicts with PARASTOOPT. Please'
             print *,'remove one and restart, stopping.'
             stop
           endif
           read(found,*) ncutoffDens_overall
           if (verbosity.gt.1) write(*,'(A13,I1,A21)') &
             ' NTERMS_DENS=',ncutoffDens_overall,' (from settings file)'
           randgenparas=.true.
        endif
        paraCnt=2*m3+lmax*m1+1
        do i=1,maxspecies
           do j=1,maxspecies
              if ((i.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                 write(string2,'(I1)') i
                 write(string3,'(I1)') j
                 !NTERMS_DENS tags can also be written as NTERMS_DENS(1,1), 
                 !or NTERMS_DENS(1,2) for example, which for a 2-species system
                 !would provide the numbers of terms in the electron densities
                 !for species 1 and 2 separately (the first index would be used
                 !in the case that THIACCPTINDEPNDT=.FALSE.)
                 searchfor="NTERMS_DENS("//string2//"_"//string3//")"
                 call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
                 if (foundvariable) then
                    nVar=nVar+1
                    if (readParasOnebyone) then
                      write(*,'(A8,A16,A31)') ' ERROR: ',trim(searchfor),' tag conflicts with PARASTOOPT.'
                      print *,'Please remove one and restart, stopping.'
                      stop
                    endif
                    read(found,*) ncutoffDens(i,j)
                    if (verbosity.gt.1) write(*,'(A13,I1,A1,I1,A2,I2,A21)') &
          ' NTERMS_DENS(',i,'_',j,')=',ncutoffDens(i,j),' (from settings file)'
                    randgenparas=.true.
                 else
                    if ((readParasOnebyone.eqv..false.).and.(randgenparas.eqv..true.)) then
                       if (ncutoffDens_overall.gt.0) then
                          !If an NTERMS_DENS(i,j) has not been explicitly
                          !specified, use the 'master' NTERMS_DENS value.
                          if (verbosity.gt.1) write(*,'(A13,I1,A1,I1,A16,I2,A1)') &
                ' NTERMS_DENS(',i,'_',j,')=NTERMS_DENS (=',ncutoffDens_overall,')'
                          ncutoffDens(i,j)=ncutoffDens_overall
                       elseif (ncutoffs.gt.0) then
                          !...or if NTERMS_DENS is not specified use the
                          !'master' NTERMS value.
                          if (verbosity.gt.1) write(*,'(A13,I1,A1,I1,A11,I2,A1)') &
                ' NTERMS_DENS(',i,'_',j,')=NTERMS (=',ncutoffs,')'
                          ncutoffDens(i,j)=ncutoffs
      !                 else
      !                    write(*,'(A16,I1,A1,I1,A48)') &
      !       ' No NTERMS_DENS(',i,'_',j,') in settings file, MUST be specified, STOPPING.'
      !                    stop
                       endif
                    endif
                 endif

                 if ((readParasOnebyone.eqv..false.).and.(randgenparas.eqv..true.)) then
                    !Update the freep array so code optimizes the relevant
                    !electron density parameters.
                    do l=0,lmax
                       do ipara=paraCnt,paraCnt+2*ncutoffDens(i,j)-1
                          freep(ipara)=2
                       enddo
                       !If holdcutoffs=.true., some of these will be altered in
                       !the main loop
                       paraCnt=paraCnt+12
                    enddo
                 endif
                        
              else
                 paraCnt=paraCnt+12*lm1
              endif
           enddo
        enddo 
        
        !Read-in the number of terms, per species, to be used in the embedding
        !function
        searchfor="NTERMS_EMB"
        call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
        if (foundvariable) then
           nVar=nVar+1
           read(found,*) nembterms
           if (readParasOnebyone) then
             print *,'ERROR: NTERMS_EMB tag conflicts with PARASTOOPT. Please'
             print *,'remove one and restart, stopping.'
             stop
           endif
           if (verbosity.gt.1) write(*,'(A12,I1,A21)') ' NTERMS_EMB=',nembterms,' (from settings file)'
           if (nembterms.eq.0) then
              !print *,' ERROR: NTERMS_EMB=0, stopping.'
              !stop
           endif
           if (nembterms.gt.4) then
              print *,' ERROR: NTERMS_EMB>4, stopping.'
              stop
           endif
           randgenparas=.true.
       else
          nembterms=0
!          if (readParasOnebyone.eqv..false.) then
!             write(*,'(A60)') &
!            ' No NTERMS_EMB in settings file, MUST be specified, STOPPING.'
!             stop
!          endif
        endif

        if (readParasOnebyone.eqv..false.) then
           !Based on the NTERMS_EMB setting, fill in relevant parts of freep array 
           !to tell code to optimize the relevant embedding function parameters.
           if (nembterms.ge.1) then
              freep(2*m3+lmax*m1+12*lm1*m2+1:2*m3 &
         +lmax*m1+12*lm1*m2+m1)=2
           endif
           if (nembterms.ge.2) then
              freep(2*m3+lmax*m1+12*lm1*m2+1+m1:2*m3 &
         +lmax*m1+12*lm1*m2+2*m1)=2
           endif
           if (nembterms.ge.3) then
              freep(2*m3+lmax*m1+12*lm1*m2+1+2*m1:2*m3 &
         +lmax*m1+12*lm1*m2+3*m1)=2
           endif
           if (nembterms.eq.4) then
              freep(2*m3+lmax*m1+12*lm1*m2+1+3*m1:2*m3 &
         +lmax*m1+12*lm1*m2+4*m1)=2
           endif
        endif

        !As described above, allows number of terms in pair-potentials to be
        !specified independent of number of terms in electron-densities
        ncutoffPairpot_overall=0
        searchfor="NTERMS_PP"
        call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
        if (foundvariable) then
           nVar=nVar+1
           if (readParasOnebyone) then
             print *,'ERROR: NTERMS_PP tag conflicts with PARASTOOPT. Please'
             print *,'remove one and restart, stopping.'
             stop
           endif
           read(found,*) ncutoffPairpot_overall
           if (verbosity.gt.1) write(*,'(A11,I1,A21)') ' NTERMS_PP=',ncutoffPairpot_overall,' (from settings file)'
           randgenparas=.true.
        endif

        paraCnt=2*m3+(4+lmax)*m1+12*lm1*m2+1
        do i=1,maxspecies
           do j=1,maxspecies
              if (j.ge.i) then
                 !Check if NTERMS_PP(1,2) etc are supplied, which as for
                 !electron densities allows for number of terms to be different
                 !for different pairs of species.
                 write(string2,'(I1)') i
                 write(string3,'(I1)') j
                 searchfor="NTERMS_PP("//string2//"_"//string3//")"
                 call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
                 if (foundvariable) then
                    nVar=nVar+1
                    if (readParasOnebyone) then
                       write(*,'(A8,A14,A31)') ' ERROR: ',trim(searchfor),' tag conflicts with PARASTOOPT.'
                       print *,'Please remove one and restart, stopping.'
                       stop
                    endif
                    read(found,*) ncutoffPairpot(i,j)
                    readParasPairwise=.true.
                    if (verbosity.gt.1) write(*,'(A11,I1,A1,I1,A2,I2,A21)') &
          ' NTERMS_PP(',i,'_',j,')=',ncutoffPairpot(i,j),' (from settings file)'
                    randgenparas=.true.
                 else
                    if ((readParasOnebyone.eqv..false.).and.(randgenparas.eqv..true.)) then
                       if (ncutoffPairpot_overall.gt.0) then
                          if (verbosity.gt.1) write(*,'(A19,I1,A1,I1,A16,I2,A1)') &
                ' setting NTERMS_PP(',i,'_',j,')=NTERMS_PP (=',ncutoffPairpot_overall,')'
                          ncutoffPairpot(i,j)=ncutoffPairpot_overall
                       elseif (ncutoffs.gt.0) then
                          if (verbosity.gt.1) write(*,'(A19,I1,A1,I1,A11,I2,A1)') &
                ' setting NTERMS_PP(',i,'_',j,')=NTERMS (=',ncutoffs,')'
                          ncutoffPairpot(i,j)=ncutoffs
                       else
                          ncutoffPairpot(i,j)=0
           !              write(*,'(A14,I1,A1,I1,A48)') &
           ! ' No NTERMS_PP(',i,'_',j,') in settings file, MUST be specified, STOPPING.'
           !              stop
                       endif
                    endif
                 endif

                 !Fill freep array to tell MEAMfit which pair-potential parameters to
                 !optimize
                 if ((readParasOnebyone.eqv..false.).and.(randgenparas.eqv..true.)) then
                    if (ncutoffPairpot(i,j).gt.0) then
                       do ipara=paraCnt,paraCnt+2*ncutoffPairpot(i,j)-1
                          freep(ipara)=2
                       enddo
                       !If holdcutoffs=.true., some of these will be altered in
                       !the main loop
                    endif
                 endif

              endif
              paraCnt=paraCnt+32
           enddo
        enddo

        !Whether to optimize the energy constant (per species) - see paper. This
        !should normally be true, except for example where another potential is
        !used as a starting point for optimization of, eg, a cross-potential.
        searchfor="OPT_ENCONST"
        call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
        if (foundvariable) then
           nVar=nVar+1
           if (readParasOnebyone) then
             print *,'ERROR: OPT_ENCONST tag conflicts with PARASTOOPT. Please'
             print *,'remove one and restart, stopping.'
             stop
           endif
           read(found,*) opt_enconst
           if (verbosity.gt.1) write(*,'(A13,L1,A21)') ' OPT_ENCONST=',opt_enconst,' (from settings file)'
           randgenparas=.true.
           if (opt_enconst.and.useRef) then
              print *,'ERROR: you have specified to optimize the energy constant but since you are '
              print *,'using a reference structure to calculate the energy contributions to the'
              print *,'objective function, opt_enconst will have no effect. Please alter one or the other, STOPPING.'
              stop
           endif
        else
           if (readParasOnebyone.eqv..false.) then
              if (verbosity.gt.1) print *,'No OPT_ENCONST in settings file, using default (=.false.)'
              opt_enconst=.false.
           endif
        endif

        !Whether to optimize the 'meam t' coefficients appearing in the
        !background density (total electron density)
        searchfor="OPT_MEAMTAU"
        call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
        if (foundvariable) then
           nVar=nVar+1
           if (readParasOnebyone) then
             print *,'ERROR: OPT_ENCONST tag conflicts with PARASTOOPT. Please'
             print *,'remove one and restart, stopping.'
             stop
           endif
           read(found,*) opt_meamtau
           if (verbosity.gt.1) write(*,'(A12,L1,A21)') ' OPT_MEAMTAU',opt_meamtau,' (from settings file)'
        else
           if (readParasOnebyone.eqv..false.) then
              if (randgenparas.eqv..true.) then
                 if (lmax.eq.0) then
                    if (verbosity.gt.1) print *,'No OPT_MEAMTAU in settings file, using default for TYPE=EAM (=.false.)'
                    opt_meamtau=.false.
                 elseif (lmax.eq.3) then
                    if (verbosity.gt.1) print *,'No OPT_MEAMTAU in settings file, using default for TYPE=MEAM (=.true.)'
                    opt_meamtau=.true.
                 endif
              else
                 if (verbosity.gt.1) then
                    print *,'No OPT_MEAMTAU in settings file, using default (=.false., since no other optimizable'
                    print *,'parameters specified)'
                 endif
                 opt_meamtau=.false.
              endif
           endif
        endif

        if (readParasOnebyone.eqv..false.) then

           if (opt_meamtau) then
              do ipara=2*m3+1,2*m3+lmax*m1
                 freep(ipara)=2
              enddo
           endif

           if (opt_enconst) then
              !Only one enconst varied by default (assumes same number of
              !species per configuration) Andy: this will need to be changed for
              !the case where there are more than two species.
              freep(m4+2*m2+1)=2
            ! do ipara=m4+2*m2+1,m4+2*m2+m1
            !     freep(ipara)=2
            ! enddo
           endif

        endif

       endif

! code fragment (from earlier); didn't work because I couldn't find a way
! to put the value into the varable with the name 'NamePara(i)'.

     ! character*10, NamePara(2)
     ! character*1, TypePara(2)
     ! character*10, DfltPara(2)
     ! logical ReqPara(2)

     ! data NamePara/'VERBOSE','SEED'/
     ! data TypePara/'L','I'/
     ! data DfltPara/'.TRUE.','0'/
     ! data ReqPara/'F','F'/

     ! nPara=2

     ! verbose=.false.
     ! do i=1,nPara
     !    searchfor=trim(NamePara(i))
     !    call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,typePara(i),foundvariable,lineNum)
     !    !print *,'looked for:',searchfor,' of type ',typePara(i)
     !    !print *,'found:',foundvariable
     !    if (foundvariable.eqv..true.) then
     !       read(found,*) NamePara(i)
     !       print *,'found ',searchfor,'=',NamePara(i),' in settings file.'
     !       print *,'should have been assigned to verbose:',verbose
     !       stop
     !    else
     !       if (ReqPara(i).eqv..true.) then
     !          print *,'ERROR: '
     !       else
     !       endif
     !    endif
     ! enddo
     ! stop

end subroutine readsettings
