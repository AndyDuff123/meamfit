subroutine variables_to_p

    !---------------------------------------------------------------c
    !
    !     Copies the sensibly-named MEAM variables (cmin, etc)
    !     onto the p() array.
    !
    !     Called by:     program MEAMfit,initializemeam,meamenergy
    !     Calls:         -
    !     Arguments:     -
    !     Returns:       cmin,cmax,meamtau,
    !                 meamrhodecay,meame0,meamrho0,
    !                 meamemb3,meamemb4,pairpotparameter,
    !                 rs,rc
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_filenames
    use m_meamparameters
    use m_optimization
    use m_atomproperties
    use m_generalinfo

    implicit none

    integer i,j,k,l,m,isp
    j=1
    k=1
    l=1
    do i=1,m3
        p(i)=cmin(j,k,l)
        if (j.lt.maxspecies) then
            j=j+1
        elseif (k.lt.maxspecies) then
            j=1
            k=k+1
        elseif (l.lt.maxspecies) then
            j=1
            k=1
            l=l+1
        endif
    enddo

    j=1
    k=1
    l=1
    do i=m3+1,2*m3
        p(i)=cmax(j,k,l)
        if (j.lt.maxspecies) then
            j=j+1
        elseif (k.lt.maxspecies) then
            j=1
            k=k+1
        elseif (l.lt.maxspecies) then
            j=1
            k=1
            l=l+1
        endif
    enddo
    !      print *,'cmax=',cmax
    !      cmax(1:maxspecies,1:maxspecies,1:maxspecies)=p(1+m3:2*m3)

    isp=1
    l=1
    do i=2*m3+1,2*m3+lmax*m1
        p(i)=meamtau(l,isp)
        !print *,'p(',i,')=',p(i),', meamtau(',k,',',j,')=',meamtau(k,j)
        if (isp.lt.maxspecies) then
            isp=isp+1
        else
            isp=1
            l=l+1
        endif
    enddo
    !print *,'in variables_to_p:'
    !print *
    !print *,'meamtau=',meamtau
    !print *,'p(2*m3+1,2*m3+lm1*m1)=',p(2*m3+1:2*m3+lm1*m1)
    !      meamtau(1:lmax,1:maxspecies)=p(2*m3+1:2*m3+lm1*m1)

    j=1
    k=0
    l=1
    m=1
    do i=2*m3+lmax*m1+1,2*m3+lmax*m1+12*lm1*m2
        p(i)=meamrhodecay(j,k,l,m)
        !print *,'conv to p(',i,')=',p(i)
        if (j.lt.12) then
            j=j+1
        elseif (k.lt.lmax) then
            j=1
            k=k+1
        elseif (m.lt.maxspecies) then
            j=1
            k=0
            m=m+1
        elseif (l.lt.maxspecies) then
            j=1
            k=0
            l=l+1
            m=1
        endif
    enddo
    p(2*m3+lmax*m1+12*lm1*m2+1: &
        2*m3+m1+lmax*m1+12*lm1*m2)=meame0(1:maxspecies)
    !      print *,'meame0=',meame0
    p(2*m3+m1+lmax*m1+12*lm1*m2+1: &
        2*m3+2*m1+lmax*m1+12*lm1*m2)=meamrho0(1:maxspecies)
    !      print *,'meamrho0=',meamrho0
    p(2*m3+2*m1+lmax*m1+12*lm1*m2+1: &
        2*m3+3*m1+lmax*m1+12*lm1*m2)=meamemb3(1:maxspecies)
    !      print *,'meamemb3=',meamemb3
    p(2*m3+3*m1+lmax*m1+12*lm1*m2+1: &
        2*m3+4*m1+lmax*m1+12*lm1*m2)=meamemb4(1:maxspecies)
    !      print *,'meamemb4=',meamemb4

    j=1
    if (maxspecies.eq.1) then
        do i=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4
            p(i)=pairpotparameter(j,1,1)
            j=j+1
        enddo
    else
        k=1
        l=1
        do i=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4
            p(i)=pairpotparameter(j,k,l)
            if (j.lt.32) then
                j=j+1
            elseif (l.lt.maxspecies) then
                j=1
                l=l+1
            elseif (k.lt.maxspecies) then
                j=1
                l=1
                k=k+1
            endif
        enddo
    endif
    !      print *,'pairpotparameter=',pairpotparameter
    !      pairpotparameter(1:32,1,1)=p(2*m3+(4+lm1)*m1+6*lm1*m2+1:
    !     +             2*m3+(4+lm1)*m1+6*lm1*m2+32)
    !      pairpotparameter(1:32,2,1)=p(2*m3+(4+lm1)*m1+6*lm1*m2+33:
    !     +             2*m3+(4+lm1)*m1+6*lm1*m2+64)
    !      pairpotparameter(1:32,1,2)=p(2*m3+(4+lm1)*m1+6*lm1*m2+65:
    !     +             2*m3+(4+lm1)*m1+6*lm1*m2+96)
    !      pairpotparameter(1:32,2,2)=p(2*m3+(4+lm1)*m1+6*lm1*m2+97:
    !     +             2*m3+(4+lm1)*m1+6*lm1*m2+128)

    j=1
    k=1
    do i=m4+1,m4+m2
        p(i)=rs(j,k)
        if (j.lt.maxspecies) then
            j=j+1
        elseif (k.lt.maxspecies) then
            j=1
            k=k+1
        endif
    enddo

    j=1
    k=1
    do i=m4+m2+1,m4+2*m2
        p(i)=rc(j,k)
        if (j.lt.maxspecies) then
            j=j+1
        elseif (k.lt.maxspecies) then
            j=1
            k=k+1
        endif
    enddo

    j=1
    do i=m4+2*m2+1,m4+2*m2+m1
        p(i)=enconst(j)
        j=j+1
    enddo

end subroutine variables_to_p
