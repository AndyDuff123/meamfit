subroutine setupnntables

    !----------------------------------------------------------------------c
    !
    !     Set-up interatomic seperation arrays, to avoid having to
    !     recalculate the interatomic seperations each time they are used.
    !     Note: array are also initialized for the force calculations. In
    !     these cases, seperations are also recorded for the configurations
    !     where each atom, taken in turn, is displaced in the 3 cartesian
    !     directions.
    !
    !     Called by:     program MEAMfit
    !
    !     Andy Duff, Oct 2013
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_geometry
    use m_datapoints

    implicit none

    integer i,j,jj,nni,idisp,cart
    real(8), parameter:: dist=1d-8 !The distance moved in the x,y and
    !z directions for the force-calculation  1d-8
    !Store all interatomic distances in arrays to speed up code (also squares
    !and cubes of distances, and cartesian components of seperations)
    do istr=1,nstruct
!        print *,'istr=',istr,'/',nstruct
        do i=1,gn_inequivalentsites(istr)
            nni=gn_neighbors(i,istr)
            do jj=1,nni
                j=gneighborlist(jj,i,istr)
                dxstr(jj,i,0,0,istr)=gxyz(1,j,istr)-gxyz(1,i,istr)
                dystr(jj,i,0,0,istr)=gxyz(2,j,istr)-gxyz(2,i,istr)
                dzstr(jj,i,0,0,istr)=gxyz(3,j,istr)-gxyz(3,i,istr)
                dist2str(jj,i,0,0,istr)=dxstr(jj,i,0,0,istr)**2 + &
            dystr(jj,i,0,0,istr)**2 + dzstr(jj,i,0,0,istr)**2
                diststr(jj,i,0,0,istr)=sqrt(dist2str(jj,i,0,0,istr))
                !dist2str(jj,i,0,0,istr)=distance2(i,j)
                dist3str(jj,i,0,0,istr)= &
                    diststr(jj,i,0,0,istr)*dist2str(jj,i,0,0,istr)
            enddo
        enddo

    enddo

end subroutine setupnntables
