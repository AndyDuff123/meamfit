subroutine screening_ij

    !---------------------------------------------------------------c
    !
    !     Fills the module m_screening with the S_ij parameters
    !     (NOTE: the screening parameters Cmin and Cmax are species
    !     dependent)
    !
    !     Called by:     meamenergy
    !     Calls:         distance2
    !     Arguments:     istr,gn_inequivalentsites,gn_neighbors,
    !                    cmin_cmax_zero,gn_neighbors,gspecies,gneighborlist,
    !                    cmax,cmin
    !     Returns:       screening
    !     Files read:    -
    !     Files written: -
    !
    !     Marcel Sluiter, Feb 8 2006
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_screening   !S_ij

    implicit none

    integer i,k,j,jj,kk,nni,iaux(1),isp,jsp,ksp
    real(8) s_ij,alpha_ik,alpha_jk,aux,c_ikj,y_ikj,y,s_ikj

    if(allocated(screening)) deallocate(screening)
    iaux=maxval(gn_neighbors(1:gn_inequivalentsites(istr),istr))
    allocate(screening(iaux(1),gn_inequivalentsites(istr)))
    if (cmin_cmax_zero) then

        screening=1.d0

       ! do i=1,gn_inequivalentsites(istr)

       !     nni=gn_neighbors(i,istr)
       !     do jj=1,nni
       !         screening(jj,i)=1.d0
       !     enddo
       ! enddo

    else

        do i=1,gn_inequivalentsites(istr)
            isp=gspecies(i,istr)
            nni=gn_neighbors(i,istr)
            do jj=1,nni
                j=gneighborlist(jj,i,istr)
                jsp=gspecies(j,istr)
                s_ij=1d0
                kk=1
                do while(s_ij.ne.0d0.and.kk.le.nni)
                    if(kk.ne.jj) then !avoid k=j
                        k=gneighborlist(kk,i,istr)
                        ksp=gspecies(k,istr)
                        if ((cmin(isp,ksp,jsp).ne.0d0).and. &
                            (cmax(isp,ksp,jsp).ne.0d0)) then
                            alpha_ik=distance2(i,k)/distance2(i,j)
                            alpha_jk=distance2(j,k)/distance2(i,j)
                            print *,distance2(i,k),dist2str(kk,i,0,0,istr)
                            aux=(alpha_ik-alpha_jk)**2
                            c_ikj=(2d0*(alpha_ik+alpha_jk)-aux-1d0)/ &
                                (1d0-aux)
                            if((1.d0-aux).lt.0.d0) then
                                s_ikj=1d0
                            else
                                y_ikj=(c_ikj-cmin(isp,ksp,jsp))/ &
                                    (cmax(isp,ksp,jsp)-cmin(isp,ksp,jsp))
                                y=min(1d0,y_ikj) !for y>1 result same as for
                                !y=1
                                s_ikj=0d0
                                if(y.gt.0d0) s_ikj=(1d0-(1d0-y)**4)**2
                            endif
                            s_ij=s_ij*s_ikj
                            !                        if (s_ikj.ne.1d0) then
                            !                           print *,
                            !     +     'istr,i,distance2(i,k),distance2(j,k),',
                            !     +     'distance2(i,j),alpha_ik,alpha_jk,c_ikj,cmin(isp,ksp,jsp),',
                            !     +        'cmax(isp,ksp,jsp),y_ikj,s_ikj,isp,jsp,ksp'
                            !                          print *,istr,i,distance2(i,k),distance2(j,k),
                            !     +        distance2(i,j),alpha_ik,alpha_jk,c_ikj,cmin(isp,ksp,jsp),
                            !     +        cmax(isp,ksp,jsp),y_ikj,s_ikj,isp,jsp,ksp
                            !c                           stop
                            !                        endif
                        endif
                    endif
                    kk=kk+1
                enddo
                screening(jj,i)=s_ij
            enddo

        enddo

    endif

end subroutine screening_ij
