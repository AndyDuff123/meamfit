subroutine backgrounddensity

    !--------------------------------------------------------------c
    !
    !     Calculates the background densities rho_i for each of
    !     the inequivalent sites (these are then used to calculate
    !     the embedding functions). Also calculated drhoi_dpara
    !     and drhoi_dmeamtau.
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Arguments:     istr,gn_inequivalentsites,meam_t,rhol
    !     Returns:       rho_i
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_generalinfo
    use m_geometry
    use m_meamparameters
    use m_electrondensity
    use m_optimization
    use m_atomproperties
    use m_objectiveFunction

    implicit none

    logical errorOnce
    integer i,isp,jsp,l,iiCoeff,iiCutoff,ip
    real(8) gamma
    real(8), allocatable:: dgamma_dpara(:,:,:), dgamma_dmeamtau(:,:)

    !  if(allocated(rho_i)) deallocate(rho_i,drhoi_dpara)
    !  allocate( rho_i(gn_inequivalentsites(istr)), &
    !            drhoi_dpara(12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)) )

    if (lmax.eq.0) then

       !Loop over all atom sites for atomic configuration, istr
       do i=1,gn_inequivalentsites(istr)

           isp=1
           if (thiaccptindepndt.eqv..false.) isp=gspecies(i,istr)

           !Calculate total electron density on site i
           rho_i(i)=rhol(0,i)

           do jsp=1,maxspecies
              !Calculate analytic derivative of rho_i w.r.t radial density
              !parameters
              do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                 ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                 drhoi_dpara(ip,0,jsp,i) = drhol_dpara(ip,0,jsp,i)
              enddo
              do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                 ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                 drhoi_dpara(ip,0,jsp,i) = drhol_dpara(ip,0,jsp,i)
              enddo
           enddo
            
       enddo
 
    else

       if(allocated(dgamma_dpara)) deallocate(dgamma_dpara)
       if(allocated(dgamma_dmeamtau)) deallocate(dgamma_dmeamtau)
       !  if(allocated(drhoi_dmeamtau)) deallocate(drhoi_dmeamtau)
       allocate( dgamma_dpara(12,0:lmax,1:maxspecies), &
                 dgamma_dmeamtau(1:lmax,1:maxspecies) )!, &
       !            drhoi_dmeamtau(1:lmax,1:maxspecies,gn_inequivalentsites(istr)) )

       !Loop over all atom sites for atomic configuration, istr
       do i=1,gn_inequivalentsites(istr)

           isp=1
           if (thiaccptindepndt.eqv..false.) isp=gspecies(i,istr)

           !Construct gamma, to be used in the equation for rho_i.
           !Note, that the final equation: rho_i(i)=rhol(0,i)*2d0/(1d0+exp(-gamma))
           !means that, if rhol(0,i)=0, rho_i and its derivatives will also =0.
           !Therefore to avoid singularities wrongly appearing in rho_i and its
           !derivatives in this case, can set gamma and its derivatives =0.
           gamma=0d0

           if (rhol(0,i).eq.0d0) then

              dgamma_dmeamtau=0d0
              dgamma_dpara=0d0

           else
              do l=1,lmax
                  gamma=gamma+meam_t(l,i)*(rhol(l,i)**2)
                 !if (i.le.2) then
                 ! print *,'meam_t(',l,i,')=',meam_t(l,i),', rhol(',l,i,')=',rhol(l,i)
                 !endif
              enddo
              gamma=gamma/(rhol(0,i)**2)
              !if (i.le.2) then
              !print *,'rhol(',0,i,')=',rhol(0,i)
              !print *,'gamma=',gamma
              !endif

              if ((gamma.lt.gammaMin).or.(gamma.gt.-gammaMin)) then
                 gammaOutOfBounds=.true.
                 if (noOpt.eqv..false.) then
                    return
                 endif
              endif
              do jsp=1,maxspecies
                 !Calculate analytic derivative of gamma w.r.t meamtau
                 !parameters
                 do l=1,lmax
                    dgamma_dmeamtau(l,jsp)=dmeamt_dmeamtau(l,jsp,i)* &
                          (rhol(l,i)**2)/(rhol(0,i)**2)
                 enddo
                 !Calculate analytic derivative of gamma w.r.t the radial density
                 !parameters:
                 !l=0 contribution
                 do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                    ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                    dgamma_dpara(ip,0,jsp)= &
                        - 2d0*gamma*drhol_dpara(ip,0,jsp,i)/rhol(0,i) + &
                        ( dmeamt_dpara(ip,1,jsp,i)*(rhol(1,i)**2) + &
                          dmeamt_dpara(ip,2,jsp,i)*(rhol(2,i)**2) + &
                          dmeamt_dpara(ip,3,jsp,i)*(rhol(3,i)**2) ) / (rhol(0,i)**2)
                 enddo
                 do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                    ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                    dgamma_dpara(ip,0,jsp)= &
                        - 2d0*gamma*drhol_dpara(ip,0,jsp,i)/rhol(0,i) + &
                        ( dmeamt_dpara(ip,1,jsp,i)*(rhol(1,i)**2) + &
                          dmeamt_dpara(ip,2,jsp,i)*(rhol(2,i)**2) + &
                          dmeamt_dpara(ip,3,jsp,i)*(rhol(3,i)**2) ) / (rhol(0,i)**2)
                 enddo
                 !l>1 contribution
                 do l=1,lmax
                    do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                       ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                       dgamma_dpara(ip,l,jsp)= &
                          2d0*meam_t(l,i)*rhol(l,i)* &
                          drhol_dpara(ip,l,jsp,i)/(rhol(0,i)**2)
                    enddo
                    do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                       ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                       dgamma_dpara(ip,l,jsp)= &
                          2d0*meam_t(l,i)*rhol(l,i)* &
                          drhol_dpara(ip,l,jsp,i)/(rhol(0,i)**2)
                    enddo
                 enddo

              enddo

           endif

           !Calculate total electron density on site i
           rho_i(i)=rhol(0,i)*2d0/(1d0+exp(-gamma))
           !print *,'rho_i(',i,')=',rho_i(i),'; gamma=',gamma
           !if ((istr.eq.533).and.(i.eq.63)) then
           !   print *,'rhoi=',rho_i(i)
           !endif


           do jsp=1,maxspecies
              !Calculate analytic derivative of gamma w.r.t meamtau
              !parameters
              do l=1,lmax
                 ! drhoi_dmeamtau(l,jsp,i)= &
                 !    rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * &
                 !    exp(-gamma) * dgamma_dmeamtau(l,jsp)
                 drhoi_dmeamtau(l,jsp,i)= &
                     rho_i(i) * dgamma_dmeamtau(l,jsp) / (1d0 + exp(gamma) )
              enddo
              !Calculate analytic derivative of rho_i w.r.t the radial density:
              !l=0 contribution
              do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                 ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                 !Originally wrote as:
                 ! drhoi_dpara(ip,0,jsp,i) = &
                 !    drhol_dpara(ip,0,jsp,i)*2d0/(1d0+exp(-gamma)) + &
                 !    rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * &
                 !    exp(-gamma) * dgamma_dpara(ip,0,jsp)
                 !However, when gamma is -ve, this can wrongly return a NaN, so:
                 drhoi_dpara(ip,0,jsp,i) = &
                    drhol_dpara(ip,0,jsp,i)*2d0/(1d0+exp(-gamma)) + &
                    rho_i(i) * dgamma_dpara(ip,0,jsp) / (1d0 + exp(gamma) )

              enddo
              do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                 ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                 drhoi_dpara(ip,0,jsp,i) = &
                    drhol_dpara(ip,0,jsp,i)*2d0/(1d0+exp(-gamma)) + &
                    rho_i(i) * dgamma_dpara(ip,0,jsp) / (1d0 + exp(gamma) )

              enddo
              !l>1 contribution
              do l=1,lmax
                 do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                    ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                    ! drhoi_dpara(ip,l,jsp,i) = &
                    !    rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * &
                    !    exp(-gamma) * dgamma_dpara(ip,l,jsp)
                    drhoi_dpara(ip,l,jsp,i) = &
                       rho_i(i) * dgamma_dpara(ip,l,jsp) / (1d0 + exp(gamma) )
                 enddo
              enddo
              do l=1,lmax
                 do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                    ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                    !drhoi_dpara(ip,l,jsp,i) = &
                    !   rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * &
                    !   exp(-gamma) * dgamma_dpara(ip,l,jsp)
                    drhoi_dpara(ip,l,jsp,i) = &
                       rho_i(i) * dgamma_dpara(ip,l,jsp) / (1d0 + exp(gamma) )

                 enddo
              enddo
           enddo
            
       enddo

       deallocate(dgamma_dpara)
       deallocate(dgamma_dmeamtau)

    endif

end subroutine backgrounddensity


subroutine backgrounddensityForce

    !--------------------------------------------------------------c
    !
    !     Calculates the background densities rho_i for each of
    !     the inequivalent sites (these are then used to calculate
    !     the embedding functions). Also calculated drhoi_dpara
    !     and drhoi_dmeamtau.
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Arguments:     istr,gn_inequivalentsites,meam_t,rhol
    !     Returns:       rho_i
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_generalinfo
    use m_geometry
    use m_meamparameters
    use m_electrondensity
    use m_optimization
    use m_atomproperties
    use m_objectiveFunction

    implicit none

    logical errorOnce
    integer i,isp,jsp,l,iiCoeff,iiCutoff,ip,alph,jj
    real(8) gamma,tmp,tmp2,tmp3,tmp4
    real(8) dgamma_dxyz(3,0:maxneighbors),dgamma_dpara(12,0:lmax,1:maxspecies),dgamma_dmeamtau(1:lmax,1:maxspecies), &
       d2gamma_dxyz_dpara(3,0:maxneighbors,12,0:lmax,maxspecies), &
       d2gamma_dxyz_dmeamtau(3,0:maxneighbors,1:lmax,maxspecies)

    !  if(allocated(rho_i)) deallocate(rho_i,drhoi_dpara)
    !  if(allocated(drhoi_dxyz)) deallocate(drhoi_dxyz,d2rhoi_dxyz_dmeamtau,d2rhoi_dxyz_dpara)
    !  allocate( rho_i(gn_inequivalentsites(istr)), &
    !            drhoi_dpara(12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)), &
    !            drhoi_dxyz(3,0:maxneighbors,gn_inequivalentsites(istr)), &
    !            d2rhoi_dxyz_dmeamtau(3,0:maxneighbors,1:lmax,maxspecies,gn_inequivalentsites(istr)), &
    !            d2rhoi_dxyz_dpara(3,0:maxneighbors,12,0:lmax,maxspecies,gn_inequivalentsites(istr)) )

    errorOnce=.false.

    if (lmax.eq.0) then

       !Loop over all atom sites for atomic configuration, istr
       do i=1,gn_inequivalentsites(istr)

           isp=1
           if (thiaccptindepndt.eqv..false.) isp=gspecies(i,istr)

           !Calculate total electron density on site i
           rho_i(i)=rhol(0,i)
          
           do jj=0,gn_neighbors(i,istr)
              do alph=1,3
                 drhoi_dxyz(alph,jj,i)=drhol_dxyz(alph,jj,0,i)

                 do jsp=1,maxspecies
                    do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                       ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                       d2rhoi_dxyz_dpara(alph,jj,ip,0,jsp,i)= &
                           d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,i)
                    enddo
                    do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                       ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                       d2rhoi_dxyz_dpara(alph,jj,ip,0,jsp,i)= &
                           d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,i)
                    enddo
                 enddo

              enddo
           enddo

           do jsp=1,maxspecies
              !Calculate analytic derivative of rho_i w.r.t radial density
              !parameters
              do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                 ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                 drhoi_dpara(ip,0,jsp,i) = drhol_dpara(ip,0,jsp,i)
              enddo
              do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                 ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                 drhoi_dpara(ip,0,jsp,i) = drhol_dpara(ip,0,jsp,i)
              enddo
           enddo
            
       enddo
 
    else

       !  if(allocated(drhoi_dmeamtau)) deallocate(drhoi_dmeamtau)
       !  allocate( drhoi_dmeamtau(1:lmax,1:maxspecies,gn_inequivalentsites(istr)))

       !Loop over all atom sites for atomic configuration, istr
       do i=1,gn_inequivalentsites(istr)

           isp=1
           if (thiaccptindepndt.eqv..false.) isp=gspecies(i,istr)

           !Construct gamma, to be used in the equation for rho_i.
           !Note, that the final equation:
           !rho_i(i)=rhol(0,i)*2d0/(1d0+exp(-gamma))
           !means that, if rhol(0,i)=0, rho_i and its derivatives will also =0.
           !Therefore to avoid singularities wrongly appearing in rho_i and its
           !derivatives in this case, can set gamma and its derivatives =0.

           !The following are incremented over in 'l' loops, so must be initialized to zero:
           gamma=0d0
           dgamma_dxyz=0d0
           d2gamma_dxyz_dpara=0d0

           if (rhol(0,i).eq.0d0) then

              dgamma_dmeamtau=0d0
              dgamma_dpara=0d0
              d2gamma_dxyz_dmeamtau=0d0

           else

              do l=1,lmax
                  gamma=gamma+meam_t(l,i)*(rhol(l,i)**2)
           !       print *,'meam_t(',l,i,')=',meam_t(l,i),', rhol(',l,i,')=',rhol(l,i)
              enddo
              gamma=gamma/(rhol(0,i)**2)
           !   print *,'rhol(',0,i,')=',rhol(0,i)
           !   print *,'gamma_',i,'=',gamma
           !   print *,'gamma=',gamma,' gammamMin=',gammaMin,', exp(gamma)=',exp(gamma),' exp(-gamma)=',exp(-gamma)

              if ((gamma.lt.gammaMin).or.(gamma.gt.-gammaMin)) then
                 gammaOutOfBounds=.true.
                 !print *,'gamma out of bounds, = ',gamma,' (rhol(0,',i,')=',rhol(0,i),')'
                 if (noOpt.eqv..false.) then
                    return
                 endif
              endif

              tmp3=0d0
              tmp4=0d0
              !Derivatives w.r.t coordinates
              do jj=0,gn_neighbors(i,istr)

                 do alph=1,3
                    tmp=0d0
                    tmp2=0d0
                    !d2gamma_dxyz_dmeamtau(alph,jj,1:lmax,1:maxspecies)=0d0
                    do l=1,lmax
                       dgamma_dxyz(alph,jj)=dgamma_dxyz(alph,jj)+ &
                          dmeamt_dxyz(alph,jj,l,i)*(rhol(l,i)**2) + &
                          2d0*meam_t(l,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i)
                       tmp=tmp+dmeamt_dxyz(alph,jj,l,i)*(rhol(l,i)**2)
                       tmp2=tmp2+2d0*meam_t(l,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i)
                       do jsp=1,maxspecies
                          d2gamma_dxyz_dmeamtau(alph,jj,l,jsp)= &
                             d2meamt_dxyz_dmeamtau(alph,jj,l,jsp,i)*(rhol(l,i)**2) + &
                             2d0*dmeamt_dmeamtau(l,jsp,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i)
                          do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                             ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                             !The following are only for l=0, so not to be
                             !included in the proceeding:
                             !d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,i)*(rhol(l,i)**2)
                             !2d0*dmeamt_dpara(ip,l,jsp,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i)
                             d2gamma_dxyz_dpara(alph,jj,ip,l,jsp)= &
                                2d0*dmeamt_dxyz(alph,jj,l,i)*rhol(l,i)*drhol_dpara(ip,l,jsp,i) + &
                                2d0*meam_t(l,i)*drhol_dpara(ip,l,jsp,i)*drhol_dxyz(alph,jj,l,i) + &
                                2d0*meam_t(l,i)*rhol(l,i)*d2rhol_dxyz_dpara(alph,jj,ip,l,jsp,i)
                             d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)=d2gamma_dxyz_dpara(alph,jj,ip,0,jsp) + &
                                d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,i)*(rhol(l,i)**2) + &
                                2d0*dmeamt_dpara(ip,l,jsp,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i) - &
                                2d0*( dmeamt_dxyz(alph,jj,l,i)*(rhol(l,i)**2) + &
                                      2d0*meam_t(l,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i) )*drhol_dpara(ip,0,jsp,i)/rhol(0,i)
                             if ((i.eq.1).and.(jj.eq.0).and.(alph.eq.1).and.(jsp.eq.1).and.(iiCoeff.eq.1)) then
                                tmp3=tmp3+d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,i)*(rhol(l,i)**2) - &
                                   2d0*dmeamt_dxyz(alph,jj,l,i)*(rhol(l,i)**2)*drhol_dpara(ip,0,jsp,i)/rhol(0,i)
                                tmp4=tmp4+2d0*dmeamt_dpara(ip,l,jsp,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i) - &
                                   2d0*2d0*meam_t(l,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i)*drhol_dpara(ip,0,jsp,i)/rhol(0,i)
                             endif
                          enddo
                          do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                             ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                             d2gamma_dxyz_dpara(alph,jj,ip,l,jsp)= &
                                2d0*dmeamt_dxyz(alph,jj,l,i)*rhol(l,i)*drhol_dpara(ip,l,jsp,i) + &
                                2d0*meam_t(l,i)*drhol_dpara(ip,l,jsp,i)*drhol_dxyz(alph,jj,l,i) + &
                                2d0*meam_t(l,i)*rhol(l,i)*d2rhol_dxyz_dpara(alph,jj,ip,l,jsp,i)
                             d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)=d2gamma_dxyz_dpara(alph,jj,ip,0,jsp) + &
                                d2meamt_dxyz_dpara(alph,jj,l,ip,jsp,i)*(rhol(l,i)**2) + &
                                2d0*dmeamt_dpara(ip,l,jsp,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i) - &
                                2d0*( dmeamt_dxyz(alph,jj,l,i)*(rhol(l,i)**2) + &
                                      2d0*meam_t(l,i)*rhol(l,i)*drhol_dxyz(alph,jj,l,i) )*drhol_dpara(ip,0,jsp,i)/rhol(0,i)
                          enddo
                       enddo
                    enddo
                    !if ((i.eq.1).and.(jj.eq.0).and.(alph.eq.1)) then
                    !   print *,'dgamma_dxyz(',alph,',',jj,') for i=',i
                    !   print *,'   first part=',dgamma_dxyz(alph,jj)/(rhol(0,i)**2)
                    !   print *,'      part a=',tmp/(rhol(0,i)**2)
                    !   print *,'      part b=',tmp2/(rhol(0,i)**2)
                    !   print *,'   second part=',-2d0*gamma*drhol_dxyz(alph,jj,0,i)/rhol(0,i)
                    !endif
                    dgamma_dxyz(alph,jj)=dgamma_dxyz(alph,jj)/(rhol(0,i)**2) - &
                        2d0*gamma*drhol_dxyz(alph,jj,0,i)/rhol(0,i)
                    !if ((i.eq.1).and.(jj.eq.0).and.(alph.eq.1)) then
                    !   print *,'   total=',dgamma_dxyz(alph,jj)
                    !   print *,'     (constituents: dmeamt_dxyz(1:3)=',dmeamt_dxyz(alph,jj,1:3,i),','
                    !   print *,'                    meam_t(1:3)=',meam_t(1:3,i),','
                    !   print *,'                    rhol(0,i)=',rhol(0,i),')'
                    !endif

                    ! do jsp=1,maxspecies
                    !    do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                    !       ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                    !       d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)

                    !complete the second part of d2gamma_dxyz_dmeamtau below,
                    !once dgamma_dmeamtau has been computed... (also for
                    !d2gamma_dxyz_para, once dgamma_dpara has been computed)
                 enddo
              enddo

              !Derivatives w.r.t potential parameters
              do jsp=1,maxspecies
                 !Calculate analytic derivative of gamma w.r.t meamtau
                 !parameters
                 do l=1,lmax
                    dgamma_dmeamtau(l,jsp)=dmeamt_dmeamtau(l,jsp,i)* &
                          (rhol(l,i)**2)/(rhol(0,i)**2)
                 enddo
                 !Calculate analytic derivative of gamma w.r.t the radial
                 !density
                 !parameters:
                 !l=0 contribution
                 do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                    ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                    dgamma_dpara(ip,0,jsp)= &
                        - 2d0*gamma*drhol_dpara(ip,0,jsp,i)/rhol(0,i) + &
                        ( dmeamt_dpara(ip,1,jsp,i)*(rhol(1,i)**2) + &
                          dmeamt_dpara(ip,2,jsp,i)*(rhol(2,i)**2) + &
                          dmeamt_dpara(ip,3,jsp,i)*(rhol(3,i)**2) ) / (rhol(0,i)**2)
                 enddo
                 do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                    ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                    dgamma_dpara(ip,0,jsp)= &
                        - 2d0*gamma*drhol_dpara(ip,0,jsp,i)/rhol(0,i) + &
                        ( dmeamt_dpara(ip,1,jsp,i)*(rhol(1,i)**2) + &
                          dmeamt_dpara(ip,2,jsp,i)*(rhol(2,i)**2) + &
                          dmeamt_dpara(ip,3,jsp,i)*(rhol(3,i)**2) ) / (rhol(0,i)**2)
                 enddo
                 !l>1 contribution
                 do l=1,lmax
                    do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                       ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                       dgamma_dpara(ip,l,jsp)= &
                          2d0*meam_t(l,i)*rhol(l,i)* &
                          drhol_dpara(ip,l,jsp,i)/(rhol(0,i)**2)
                    enddo
                    do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                       ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                       dgamma_dpara(ip,l,jsp)= &
                          2d0*meam_t(l,i)*rhol(l,i)* &
                          drhol_dpara(ip,l,jsp,i)/(rhol(0,i)**2)
                    enddo
                 enddo

              enddo

              do jsp=1,maxspecies
                 do jj=0,gn_neighbors(i,istr)
                    do alph=1,3
                       do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                          ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                          !if ((i.eq.1).and.(jj.eq.0).and.(alph.eq.1).and.(jsp.eq.1).and.(iiCoeff.eq.1)) then
                          !   print *,'d2gamma_dxyz_dpara(',alph,',',jj,',',ip,',l=0,',jsp,') for i=',i
                          !   print *,'   first part=',d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)/(rhol(0,i)**2)
                          !   print *,'      part a=',tmp3/(rhol(0,i)**2)
                          !   print *,'      part b=',tmp4/(rhol(0,i)**2)
                          !   print *,'      (part a + part b=',tmp3/(rhol(0,i)**2) + tmp4/(rhol(0,i)**2),' or ',(tmp3+tmp4)/(rhol(0,i)**2)
                          !   print *,'   second part=',-2d0*dgamma_dpara(ip,0,jsp)*drhol_dxyz(alph,jj,0,i)/rhol(0,i) &
                          !   -2d0*gamma*d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,i)/rhol(0,i) &
                          !   +2d0*gamma*drhol_dxyz(alph,jj,0,i)/(rhol(0,i)**2)*drhol_dpara(ip,0,jsp,i)
                          !endif
                          d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)=d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)/(rhol(0,i)**2) &
                             -2d0*dgamma_dpara(ip,0,jsp)*drhol_dxyz(alph,jj,0,i)/rhol(0,i) &
                             -2d0*gamma*d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,i)/rhol(0,i) &
                             +2d0*gamma*drhol_dxyz(alph,jj,0,i)/(rhol(0,i)**2)*drhol_dpara(ip,0,jsp,i)
                          !if ((i.eq.1).and.(jj.eq.0).and.(alph.eq.1).and.(jsp.eq.1).and.(iiCoeff.eq.1)) then
                          !   print *,'   total=',d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)
                          !   print *,'     (constituents: d2meamt_dxyz_dpara(1:3)=',d2meamt_dxyz_dpara(alph,jj,1:3,ip,jsp,i),','
                          !   print *,'                    dmeamt_dpara(1:3)=',dmeamt_dpara(ip,1:3,jsp,i),','
                          !   print *,'                    drhol_dpara(0,i)=',drhol_dpara(ip,0,jsp,i),')'
                          !endif
                       enddo
                       do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                          ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                          d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)=d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)/(rhol(0,i)**2) &
                             -2d0*dgamma_dpara(ip,0,jsp)*drhol_dxyz(alph,jj,0,i)/rhol(0,i) &
                             -2d0*gamma*d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,i)/rhol(0,i) &
                             +2d0*gamma*drhol_dxyz(alph,jj,0,i)/(rhol(0,i)**2)*drhol_dpara(ip,0,jsp,i)
                       enddo
                    enddo
                 enddo
              enddo

              do jsp=1,maxspecies
                 do l=1,lmax
                    do jj=0,gn_neighbors(i,istr)
                       do alph=1,3
                          d2gamma_dxyz_dmeamtau(alph,jj,l,jsp)=d2gamma_dxyz_dmeamtau(alph,jj,l,jsp) &
                           /(rhol(0,i)**2) - 2d0*dgamma_dmeamtau(l,jsp)*drhol_dxyz(alph,jj,0,i)/rhol(0,i)
                          do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                             ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                             d2gamma_dxyz_dpara(alph,jj,ip,l,jsp)=d2gamma_dxyz_dpara(alph,jj,ip,l,jsp) &
                                /(rhol(0,i)**2) - 2d0*dgamma_dpara(ip,l,jsp)*drhol_dxyz(alph,jj,0,i)/rhol(0,i)! &
                                !the following two terms are only for l=0
                                !-2d0*gamma*d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,i)/rhol(0,i) &
                                !+2d0*gamma*drhol_dxyz(alph,jj,0,i)/(rhol(0,i)**2)*drhol_dpara(ip,0,jsp,i)
                            !if ((i.eq.1).and.(l.eq.1).and.(jj.eq.0).and.(alph.eq.1).and.(jsp.eq.1).and.(ip.eq.1)) then
                            !   print *,'dgamma_dxyz(1,0) for i=1 =',dgamma_dxyz(alph,jj)
                            !   print *,'d2gamma_dxyz_dpara(1,0,1,0,1)=',d2gamma_dxyz_dpara(alph,jj,ip,l,jsp)
                            !endif
                          enddo
                          do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                             ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                             d2gamma_dxyz_dpara(alph,jj,ip,l,jsp)=d2gamma_dxyz_dpara(alph,jj,ip,l,jsp) &
                                /(rhol(0,i)**2) - 2d0*dgamma_dpara(ip,l,jsp)*drhol_dxyz(alph,jj,0,i)/rhol(0,i)
                          enddo
                       enddo
                    enddo
                 enddo
              enddo

           endif

           !Calculate total electron density on site i
           rho_i(i)=rhol(0,i)*2d0/(1d0+exp(-gamma))

           !Derivatives w.r.t coordinates
           do jj=0,gn_neighbors(i,istr)
              do alph=1,3
                 drhoi_dxyz(alph,jj,i)= drhol_dxyz(alph,jj,0,i)*2d0/(1d0+exp(-gamma)) - &
                       rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * (-exp(-gamma)) * &
                       dgamma_dxyz(alph,jj)
                 !print *,'drhoi_dxyz(',alph,',',jj,',',i,')=',drhoi_dxyz(alph,jj,i), ' drhol_dxyz=',drhol_dxyz(alph,jj,0,i),' dgamma_dxyz(',alph,',',jj,')=',dgamma_dxyz(alph,jj)
              enddo
           enddo

           !Derivatives w.r.t potential parameters
           do jsp=1,maxspecies
              !Calculate analytic derivative of gamma w.r.t meamtau
              !parameters
              do l=1,lmax
                 ! drhoi_dmeamtau(l,jsp,i)= &
                 !    rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * &
                 !    exp(-gamma) * dgamma_dmeamtau(l,jsp)
                 drhoi_dmeamtau(l,jsp,i)= &
                     rho_i(i) * dgamma_dmeamtau(l,jsp) / (1d0 + exp(gamma) )
              enddo
              !Calculate analytic derivative of rho_i w.r.t the radial density:
              !l=0 contribution
              do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                 ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                 !Originally wrote as:
                 ! drhoi_dpara(ip,0,jsp,i) = &
                 !    drhol_dpara(ip,0,jsp,i)*2d0/(1d0+exp(-gamma)) + &
                 !    rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * &
                 !    exp(-gamma) * dgamma_dpara(ip,0,jsp)
                 !However, when gamma is -ve, this can wrongly return a NaN, so:
                 drhoi_dpara(ip,0,jsp,i) = &
                    drhol_dpara(ip,0,jsp,i)*2d0/(1d0+exp(-gamma)) + &
                    rho_i(i) * dgamma_dpara(ip,0,jsp) / (1d0 + exp(gamma) )

              enddo
              do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                 ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                 drhoi_dpara(ip,0,jsp,i) = &
                    drhol_dpara(ip,0,jsp,i)*2d0/(1d0+exp(-gamma)) + &
                    rho_i(i) * dgamma_dpara(ip,0,jsp) / (1d0 + exp(gamma) )

              enddo
              !l>1 contribution
              do l=1,lmax
                 do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                    ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                    ! drhoi_dpara(ip,l,jsp,i) = &
                    !    rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * &
                    !    exp(-gamma) * dgamma_dpara(ip,l,jsp)
                    drhoi_dpara(ip,l,jsp,i) = &
                       rho_i(i) * dgamma_dpara(ip,l,jsp) / (1d0 + exp(gamma) )
                 enddo
              enddo
              do l=1,lmax
                 do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                    ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                    !drhoi_dpara(ip,l,jsp,i) = &
                    !   rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * &
                    !   exp(-gamma) * dgamma_dpara(ip,l,jsp)
                    drhoi_dpara(ip,l,jsp,i) = &
                       rho_i(i) * dgamma_dpara(ip,l,jsp) / (1d0 + exp(gamma) )

                 enddo
              enddo
           enddo
           
           !Derivatives w.r.t coordinates
           do jj=0,gn_neighbors(i,istr)
              do alph=1,3
                 drhoi_dxyz(alph,jj,i)= drhol_dxyz(alph,jj,0,i)*2d0/(1d0+exp(-gamma)) - &
                       rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 ) * (-exp(-gamma)) * &
                       dgamma_dxyz(alph,jj)
                 do jsp=1,maxspecies
                    !l=0 contribution
                    do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                       ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                       d2rhoi_dxyz_dpara(alph,jj,ip,0,jsp,i)= &
                          d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,i)*2d0/(1d0+exp(-gamma)) &
                          + drhol_dxyz(alph,jj,0,i)*2d0*exp(-gamma)*dgamma_dpara(ip,0,jsp)/((1d0+exp(-gamma))**2) &
                          + drhol_dpara(ip,0,jsp,i)*2d0/( (1d0+exp(-gamma))**2 ) * exp(-gamma) * dgamma_dxyz(alph,jj) &
                          + rhol(0,i)*4d0/( (1d0+exp(-gamma))**3 )*exp(-2d0*gamma)*dgamma_dpara(ip,0,jsp)*dgamma_dxyz(alph,jj) &
                          - rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*dgamma_dpara(ip,0,jsp)*dgamma_dxyz(alph,jj) &
                          + rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)
                    enddo
                    do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                       ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                       d2rhoi_dxyz_dpara(alph,jj,ip,0,jsp,i)= &
                          d2rhol_dxyz_dpara(alph,jj,ip,0,jsp,i)*2d0/(1d0+exp(-gamma)) &
                          + drhol_dxyz(alph,jj,0,i)*2d0*exp(-gamma)*dgamma_dpara(ip,0,jsp)/((1d0+exp(-gamma))**2) &
                          - drhol_dpara(ip,0,jsp,i)*2d0/( (1d0+exp(-gamma))**2 ) * (-exp(-gamma)) * dgamma_dxyz(alph,jj) &
                          + rhol(0,i)*4d0/( (1d0+exp(-gamma))**3 )*exp(-2d0*gamma)*dgamma_dpara(ip,0,jsp)*dgamma_dxyz(alph,jj) &
                          - rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*dgamma_dpara(ip,0,jsp)*dgamma_dxyz(alph,jj) &
                          + rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*d2gamma_dxyz_dpara(alph,jj,ip,0,jsp)
                    enddo
                    do l=1,lmax
                       d2rhoi_dxyz_dmeamtau(alph,jj,l,jsp,i)= &
                          drhol_dxyz(alph,jj,0,i)*2d0*exp(-gamma)*dgamma_dmeamtau(l,jsp)/((1d0+exp(-gamma))**2) &
                          + rhol(0,i)*4d0/( (1d0+exp(-gamma))**3 )*exp(-2d0*gamma)*dgamma_dmeamtau(l,jsp)*dgamma_dxyz(alph,jj) &
                          - rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*dgamma_dmeamtau(l,jsp)*dgamma_dxyz(alph,jj) &
                          + rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*d2gamma_dxyz_dmeamtau(alph,jj,l,jsp)
                       if ((debug.eqv..true.).and.(errorOnce.eqv..false.).and.(d2rhoi_dxyz_dmeamtau(alph,jj,l,jsp,i)/=d2rhoi_dxyz_dmeamtau(alph,jj,l,jsp,i))) then
                          print *,'ERROR: d2rhoi_dxyz_dmeamtau(',alph,',',jj,',',l,',',jsp,',',i,')=',d2rhoi_dxyz_dmeamtau(alph,jj,l,jsp,i)
                          print *,'  ( contributions: exp(-gamma)=',exp(-gamma),','
                          print *,'    exp(-2*gamma)=',exp(-2d0*gamma),','
                          print *,'    1/( (1d0+exp(-gamma))**2 ) = ',1d0/((1d0+exp(-gamma))**2 ),','
                          print *,'    1/( (1d0+exp(-gamma))**3 ) = ',1d0/((1d0+exp(-gamma))**3 ),','
                          print *,'    d2gamma_dxyz_dmeamtau(',alph,jj,l,jsp,')=',d2gamma_dxyz_dmeamtau(alph,jj,l,jsp)
                          print *
                          print *,'    Alternative factor: [=1/(e^2g +3e^g + 3 +e^-g)]'
                          print *,'       =',1d0/(exp(2d0*gamma)+3d0*exp(gamma)+3d0+exp(-gamma))
                          print *,'    Alternative factor: [=1/(e^g +2 + e^-g)]'
                          print *,'       =',1d0/(exp(gamma)+2d0+exp(-gamma))

                          errorOnce=.true.
                       endif
                       do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                          ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                          !d2rhol_dxyz_dpara(alph,jj,ip,l,jsp,i)*2d0/(1d0+exp(-gamma)) <--- this is just for l=0, and so not included below
                          !drhol_dpara(ip,0,jsp,i)*2d0/( (1d0+exp(-gamma))**2 ) * exp(-gamma) * dgamma_dxyz(alph,jj)    <--- same applies
                          d2rhoi_dxyz_dpara(alph,jj,ip,l,jsp,i)= &
                             drhol_dxyz(alph,jj,0,i)*2d0*exp(-gamma)*dgamma_dpara(ip,l,jsp)/((1d0+exp(-gamma))**2) &
                             + rhol(0,i)*4d0/( (1d0+exp(-gamma))**3 )*exp(-2d0*gamma)*dgamma_dpara(ip,l,jsp)*dgamma_dxyz(alph,jj) &
                             - rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*dgamma_dpara(ip,l,jsp)*dgamma_dxyz(alph,jj) &
                             + rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*d2gamma_dxyz_dpara(alph,jj,ip,l,jsp)
                       enddo
                       do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                          ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                          d2rhoi_dxyz_dpara(alph,jj,ip,l,jsp,i)= &
                             drhol_dxyz(alph,jj,0,i)*2d0*exp(-gamma)*dgamma_dpara(ip,l,jsp)/((1d0+exp(-gamma))**2) &
                             + rhol(0,i)*4d0/( (1d0+exp(-gamma))**3 )*exp(-2d0*gamma)*dgamma_dpara(ip,l,jsp)*dgamma_dxyz(alph,jj) &
                             - rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*dgamma_dpara(ip,l,jsp)*dgamma_dxyz(alph,jj) &
                             + rhol(0,i)*2d0/( (1d0+exp(-gamma))**2 )*exp(-gamma)*d2gamma_dxyz_dpara(alph,jj,ip,l,jsp)
                       enddo
                    enddo
                 enddo
              enddo
           enddo

 
       enddo

    endif

end subroutine backgrounddensityForce

