subroutine matinv(a,ndim,n,b)

    !-----------------------------------------------------------c
    !
    !     Matrix inversion by Gaussian algorithm. Matrix 'a'
    !     is supplied, and it's inverse, matrix 'b' is returned.
    !     The dimension of 'a' and 'b' is ndim (=n)
    !
    !     Called by:     readtargetstruc
    !     Calls:         -
    !     Arguments:     a,ndim,n
    !     Returns:       b
    !     Files read:    -
    !     Files written: -
    !
    !     Marcel Sluiter, April 25 2000
    !
    !     Copyright (c) 2018, STFC
    !
    !-----------------------------------------------------------c

    implicit none

    integer ndim,n,i,k,imax
    real(8), parameter:: eps=1d-14
    real(8) a(ndim,ndim),b(ndim,ndim),amax,amult,div
    real(8), allocatable:: acopy(:,:),aux(:)

    allocate(acopy(n,n),aux(n))
    b=0d0
    do i=1,n
        b(i,i)=1d0
        acopy(i,1:n)=a(i,1:n)
    enddo

    do k=1,n
        amax=0d0
        imax=0
        if (k .ne. n) then
            do i=k,n
                if (abs(acopy(i,k)) .gt. amax) then
                    amax=abs(acopy(i,k))
                    imax=i
                endif
            enddo
            if (imax.ne.k) then
                aux=acopy(imax,1:n)
                acopy(imax,1:n)=acopy(k,1:n)
                acopy(k,1:n)=aux
                aux=b(imax,1:n)
                b(imax,1:n)=b(k,1:n)
                b(k,1:n)=aux
            endif
        endif
        div=acopy(k,k)
        if(abs(div).le.eps) div=eps  !dont crash if singular
        acopy(k,1:n)=acopy(k,1:n)/div
        b(k,1:n)=b(k,1:n)/div
        do i=1,n
            if (i.ne.k) then
                amult=acopy(i,k)
                acopy(i,1:n)=acopy(i,1:n)-amult*acopy(k,1:n)
                b(i,1:n)=b(i,1:n)-amult*b(k,1:n)
            endif
        enddo
    enddo
    deallocate(acopy,aux)

end subroutine matinv
