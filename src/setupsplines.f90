subroutine setupsplines

    !     Copyright (c) 2018, STFC

    use m_meamparameters
    use m_optimization
    use m_generalinfo

    implicit none

    integer i
    real(8) pairpotAcklandFeFe

    !Setup spline for V_XX
    do i=1,narrpairpotXX
        pairpotXXarr(i)=pairpotAcklandFeFe(r_pairpotXXarr(i))
    enddo
    call spline(r_pairpotXXarr,pairpotXXarr,narrpairpotXX, &
        0d0,0d0,secderpairpotXX)

    !Setup spline for thi_XX
    do i=1,narrthiXX
        call raddens(r_thiXXarr(i),1,1,thiXXarr(i))
    enddo
    call spline(r_thiXXarr,thiXXarr,narrthiXX, &
        0d0,0d0,secderthiXX)

end subroutine setupsplines
