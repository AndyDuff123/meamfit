
       subroutine mixPotential

       ! Randomly selects two potentials out of those in the high-score
       ! board and mixs them together to create a new trial potential.
       !
       ! If forceMut=.true. then we force at least one mutation to take
       ! place.
       !
       ! Note: meamemb4 never gets randomly seeded at present...
       ! forgotten whether this was by design...
       ! embedding functions mutated in block 'by type' rather than 'by species'
       ! which doesn't make sense..
       !
       ! Andy Duff, 2014
       !
       !     Copyright (c) 2018, STFC


       use m_optimization
       use m_meamparameters
       use m_atomproperties

       implicit none

       logical randGen
       integer pot1,pot2,i,j,k,l,m,ip,isp
       real(8) tmp

       ! Select two potentials at random
       call RANDOM_NUMBER(tmp)
       pot1=tmp*dble(noptfuncstore)+1
       do
          call RANDOM_NUMBER(tmp)
          pot2=tmp*dble(noptfuncstore)+1
     !    ! print *,pot1,pot2
          if (pot1.ne.pot2) exit
       enddo
     !! print *,'Preparing to mix pot ',pot1,' and pot ',pot2
     !print *
     !! print *,'pot1:'
     !! print *,p_saved(1:np,pot1)
     !print *
     !! print *,'pot2:'
     !! print *,p_saved(1:np,pot2)
     !print *
     !! print *,'new pot:'

!        do j=min(n_optfunc-1,noptfuncstore-1),i,-1
!            bestoptfuncs(j+1)=bestoptfuncs(j)
!            do k=1,np
!                p_saved(k,j+1)=p_saved(k,j)
!            enddo
!        enddo

       !If forceMut=.true. then we need to check in the following that a
       !mutation has indeed occured. If not we repeat this loop until
       !one has
       randGen=.false.
       do

          j=1
          k=1
          l=1
          do i=1,m3
              call RANDOM_NUMBER(tmp)
              if (tmp.gt.probRand) then !Otherwise leave cmin, cmax to
                               !their previous, randomly generated, values.
                 call RANDOM_NUMBER(tmp)
                 if (tmp.lt.probPot1) then
                    cmin(j,k,l)=p_saved(i,pot1)
                 else
                    cmin(j,k,l)=p_saved(i,pot2)
                 endif
              endif
              !cmin(j,k,l)=p(i)
              if (j.lt.maxspecies) then
                  j=j+1
              elseif (k.lt.maxspecies) then
                  j=1
                  k=k+1
              elseif (l.lt.maxspecies) then
                  j=1
                  k=1
                  l=l+1
              endif
          enddo
          ! ! print *,'cmin=',cmin

          j=1
          k=1
          l=1
          do i=m3+1,2*m3
              call RANDOM_NUMBER(tmp)
              if (tmp.gt.probRand) then
                 call RANDOM_NUMBER(tmp)
                 if (tmp.lt.probPot1) then
                    cmax(j,k,l)=p_saved(i,pot1)
                 else
                    cmax(j,k,l)=p_saved(i,pot2)
                 endif
              endif
              !cmax(j,k,l)=p(i)
              if (j.lt.maxspecies) then
                  j=j+1
              elseif (k.lt.maxspecies) then
                  j=1
                  k=k+1
              elseif (l.lt.maxspecies) then
                  j=1
                  k=1
                  l=l+1
              endif
          enddo
          ! ! print *,'cmax=',cmax

          isp=1
          l=1
          do ip=2*m3+1,2*m3+lmax*m1
             call RANDOM_NUMBER(tmp)
             if (tmp.gt.probRand) then
                call RANDOM_NUMBER(tmp)
                if (tmp.lt.probPot1) then
                   meamtau(l,isp)=p_saved(ip,pot1)
                else
                   meamtau(l,isp)=p_saved(ip,pot2)
                endif
             else
                randGen=.true.
                ! print *,'rand gen meamtau(l=',l,', isp=',isp,')'
             endif
             !meamtau(k,j)=p(i)
             if (isp.lt.maxspecies) then
                isp=isp+1
             else
                isp=1
                l=l+1
             endif
          enddo
          ! ! print *,'meamtau=',meamtau

          j=1
          k=0
          l=1
          m=1
          do i=2*m3+lmax*m1+1,2*m3+lmax*m1+12*lm1*m2
              if (j.eq.1) then
                 call RANDOM_NUMBER(tmp)
                 if (tmp.gt.probRand) then
                    call RANDOM_NUMBER(tmp)
                 else
                    tmp=0d0 !Use this to signal mutation here
                    ! print *,'rand gen density(1:12,l=',k,', isp=',l, &
                            ! ', jsp=', m,')'
                    randGen=.true.
                 endif
              endif
              if (tmp.ne.0d0) then
                 if (tmp.lt.probPot1) then
                    meamrhodecay(j,k,l,m)=p_saved(i,pot1)
                 else
                    meamrhodecay(j,k,l,m)=p_saved(i,pot2)
                 endif
              endif
              !meamrhodecay(j,k,l,m)=p(i)
              if (j.lt.12) then
                  j=j+1
              elseif (k.lt.lmax) then
                  j=1
                  k=k+1
              elseif (m.lt.maxspecies) then
                  j=1
                  k=0
                  m=m+1
              elseif (l.lt.maxspecies) then
                  j=1
                  k=0
                  l=l+1
                  m=1
              endif
          enddo
          ! ! print *,'meamrhodecay=',meamrhodecay

          call RANDOM_NUMBER(tmp)
          if (tmp.gt.probRand) then
             call RANDOM_NUMBER(tmp)
             if (tmp.lt.probPot1) then
                meame0(1:maxspecies)=p_saved(2*m3+lmax*m1+12*lm1*m2+1: &
                    2*m3+m1+lmax*m1+12*lm1*m2,pot1)
             else
                meame0(1:maxspecies)=p_saved(2*m3+lmax*m1+12*lm1*m2+1: &
                    2*m3+m1+lmax*m1+12*lm1*m2,pot2)
             endif
          else
             ! print *,'rand gen an meame0(1:maxspecies)'
             randGen=.true.
          endif
          ! ! print *,'meame0=',meame0

          call RANDOM_NUMBER(tmp)
          if (tmp.gt.probRand) then
             call RANDOM_NUMBER(tmp)
             if (tmp.lt.probPot1) then
               meamrho0(1:maxspecies)=p_saved(2*m3+m1+lmax*m1+12*lm1*m2+1: &
                2*m3+2*m1+lmax*m1+12*lm1*m2,pot1)
             else
               meamrho0(1:maxspecies)=p_saved(2*m3+m1+lmax*m1+12*lm1*m2+1: &
                2*m3+2*m1+lmax*m1+12*lm1*m2,pot2)
             endif
          else
             randGen=.true.
          endif
          ! ! print *,'meamrho0=',meamrho0

          call RANDOM_NUMBER(tmp)
          if (tmp.gt.probRand) then
            call RANDOM_NUMBER(tmp)
            if (tmp.lt.probPot1) then
             meamemb3(1:maxspecies)=p_saved(2*m3+2*m1+lmax*m1+12*lm1*m2+1: &
                 2*m3+3*m1+lmax*m1+12*lm1*m2,pot1)
            else
             meamemb3(1:maxspecies)=p_saved(2*m3+2*m1+lmax*m1+12*lm1*m2+1: &
                 2*m3+3*m1+lmax*m1+12*lm1*m2,pot2)
            endif
          else
            randGen=.true.
          endif
          ! ! print *,'meamemb3=',meamemb3

          call RANDOM_NUMBER(tmp)
          if (tmp.lt.probPot1) then
             meamemb4(1:maxspecies)=p_saved(2*m3+3*m1+lmax*m1+12*lm1*m2+1: &
                 2*m3+4*m1+lmax*m1+12*lm1*m2,pot1)
          else
             meamemb4(1:maxspecies)=p_saved(2*m3+3*m1+lmax*m1+12*lm1*m2+1: &
                 2*m3+4*m1+lmax*m1+12*lm1*m2,pot2)
          endif
          !meamemb4(1:maxspecies)=p(2*m3+3*m1+lm1*m1+12*lm1*m2+1: &
          !    2*m3+4*m1+lm1*m1+12*lm1*m2)
          ! ! print *,'meamemb4=',meamemb4

          j=1
          if (maxspecies.eq.1) then
              call RANDOM_NUMBER(tmp)
              if (tmp.gt.probRand) then
                 call RANDOM_NUMBER(tmp)
              else
                 tmp=0d0 !Use this to signal mutation here
               !  ! print *,'rand genning a pairpot para'
                 randGen=.true.
              endif
              do i=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4
                  if (tmp.ne.0d0) then
                     if (tmp.lt.probPot1) then
                        pairpotparameter(j,1,1)=p_saved(i,pot1)
                     else
                        pairpotparameter(j,1,1)=p_saved(i,pot2)
                     endif
                  endif
                  !pairpotparameter(j,1,1)=p(i)
                  j=j+1
              enddo
          else
              k=1
              l=1
              do i=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4
                  if (j.eq.1) then
                     call RANDOM_NUMBER(tmp)
                     if (tmp.gt.probRand) then
                        call RANDOM_NUMBER(tmp)
                     else
                        tmp=0d0 !Use this to signal mutation here
                     !   ! print *,'rand genning a pairpot para'
                        randGen=.true.
                     endif
                  endif
                  if (tmp.ne.0d0) then
                     if (tmp.lt.probPot1) then
                        pairpotparameter(j,k,l)=p_saved(i,pot1)
                     else
                        pairpotparameter(j,k,l)=p_saved(i,pot2)
                     endif
                  endif
                  !pairpotparameter(j,k,l)=p(i)
                  if (j.lt.32) then
                      j=j+1
                  elseif (l.lt.maxspecies) then
                      j=1
                      l=l+1
                  elseif (k.lt.maxspecies) then
                      j=1
                      l=1
                      k=k+1
                  endif
              enddo
          endif
          ! ! print *,'pairpotparameter=',pairpotparameter

          j=1
          k=1
          do i=m4+1,m4+m2
              call RANDOM_NUMBER(tmp)
              if (tmp.gt.probRand) then
                 call RANDOM_NUMBER(tmp)
                 if (tmp.lt.probPot1) then
                    rs(j,k)=p_saved(i,pot1)
                 else
                    rs(j,k)=p_saved(i,pot2)
                 endif
              endif
              !rs(j,k)=p(i)
              ! ! print *,'rs(',j,',',k,')=',rs(j,k)
              if (j.lt.maxspecies) then
                  j=j+1
              elseif (k.lt.maxspecies) then
                  j=1
                  k=k+1
              endif
          enddo

          j=1
          k=1
          do i=m4+m2+1,m4+2*m2
              call RANDOM_NUMBER(tmp)
              if (tmp.gt.probRand) then
                 call RANDOM_NUMBER(tmp)
                 if (tmp.lt.probPot1) then
                    rc(j,k)=p_saved(i,pot1)
                 else
                    rc(j,k)=p_saved(i,pot2)
                 endif
              endif
              !rc(j,k)=p(i)
              ! ! print *,'rc(',j,',',k,')=',rc(j,k)
              if (j.lt.maxspecies) then
                  j=j+1
              elseif (k.lt.maxspecies) then
                  j=1
                  k=k+1
              endif
          enddo

          j=1
          do i=m4+2*m2+1,m4+2*m2+m1
              call RANDOM_NUMBER(tmp)
              if (tmp.gt.probRand) then
                 call RANDOM_NUMBER(tmp)
                 if (tmp.lt.probPot1) then
                    enconst(j)=p_saved(i,pot1)
                 else
                    enconst(j)=p_saved(i,pot2)
                 endif
              else
              !   ! print *,'rand gening an enconst (=',enconst,')'
              endif
              !enconst(j)=p(i)
              ! ! print *,'enconst(',j,')=',enconst(j)
              j=j+1
          enddo

          !Check if we can are satisfied with the cross-breeded
          !potential.
          if (forceMut.eqv..false.) then
             !No requirement that at least one parameter block is randomly
             !generated
             exit
          else
             if (randGen.eqv..true.) then
                !At least one parameter block has been randomly seeded
                exit
             else
                !forceMut = true, and randGen = false, so repeat
                !cross-breeding procedure
             endif
          endif

       enddo

       !stop

       end subroutine mixPotential 

