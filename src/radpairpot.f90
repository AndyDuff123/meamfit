subroutine radpairpot(species1,species2,distance,pairpot,dpairpot_dpara)

    !------------------------------------------------------------------c
    !
    !     Calculates the 'pairpot', for two atoms
    !     of species, 'species1' and 'species2' which are a
    !     distance, 'distance', apart from one another.
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Arguments:     species1,species2,distance,pairpotparameter
    !     Returns:       pairpot
    !     Files read:    -
    !     Files written: -
    !
    !     Marcel Sluiter, Feb 9 2006
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_atomproperties
    use m_meamparameters
    use m_filenames
    use m_generalinfo
    use m_optimization

    implicit none

    integer species1,species2,i,index,isp,jsp,speciesSml,speciesLrg,iiCoeff,iiCutoff
    real(8) distance,pairpot,tmp,rsBier,x,eps,B0,B1,B2,B3,k1,k2,k3,k4,depsdr, &
            rdiffcb,rBiersq,rBiercb,cutoffMinsq,cutoffMincb!,diff
    real(8), parameter :: rBier=0.9d0
    real(8), parameter :: e2ov4pieps0=14.3977847299112d0 !e^2 / 4 pi eps_0, in eV/Angstrom
    ! ( = (1.602*10^-19)^2 / (4*3.142*8.854*10^-12) * (6.241 * 10^18) * 10^10
    !                                             1 J = 6.241*10^18 eV   1 m = 10^10 Angstron
    real(8) dpairpot_dpara(32,maxspecies,maxspecies), & !AnalyticDeriv
         dk3_dpara(32),dk4_dpara(32)
    real(8) dB0_dpara,dB1_dpara,dB2_dpara,dB3_dpara
    intent(in) species1,species2!distance
    intent(out) pairpot
    !print *,'species1=',species1,', species2=',species2
    !print *,'Z1=',speciestoZ(species1),' Z2=',speciestoZ(species2)

    if (distance.lt.rBier) then

      rsBier = 0.88534d0 * 0.52917721092d0 / ( ( speciestoZ(species1) )**0.23d0 &
                         + ( speciestoZ(species2) )**0.23d0  )
      x = distance/rsBier
      eps = 0.18175d0*exp(-3.19980d0*x)  + 0.50986d0*exp(-0.94229d0*x) + &
            0.28022d0*exp(-0.40290d0*x)  + 0.02817d0*exp(-0.20162d0*x)
      pairpot = e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * eps / distance
    elseif (distance.lt.cutoffMin) then

      !Here we interpolate between the lower radius Biersack result and the
      !larger radius sum over cubic terms. We must first evaluate the pairpotential 
      !and its derivative at the lower and upper radial values.

      !Evaluate k1: value of pairpotential at R=rBier
      rsBier = 0.88534d0 * 0.52917721092d0 / ( ( speciestoZ(species1))**0.23d0 &
                         + ( speciestoZ(species2) )**0.23d0  )
      x = rBier/rsBier
      eps = 0.18175d0*exp(-3.19980d0*x)  + 0.50986d0*exp(-0.94229d0*x) + &
            0.28022d0*exp(-0.40290d0*x)  + 0.02817d0*exp(-0.20162d0*x)
      k1 = e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * eps / rBier

      !Evaluate k2: value of d(pairpotential)/dr at R=rBier
      depsdr = (1d0/rsBier)*( 0.18175d0*(-3.19980d0)*exp(-3.19980d0*x) + 0.50986d0*(-0.94229d0)*exp(-0.94229d0*x) + &
            0.28022d0*(-0.40290d0)*exp(-0.40290d0*x) + 0.02817d0*(-0.20162d0)*exp(-0.20162d0*x) )

      k2 = - e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * eps / rBier**2 + &
           e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * depsdr / rBier

      !Evaluate k3: value of pairpotential at R=cutoffMin
      k3 = 0d0
      if (lookuptables.eqv..true.) then
         print *,'Need to update code in pairpotential subroutine, stopping.'
         stop
      endif

      speciesSml=min(species1,species2)
      speciesLrg=max(species1,species2)
      if (typepairpot(speciesSml,speciesLrg).eq.2) then
         do i=1,31,2
            if (cutoffMin.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
                k3=k3 + pairpotparameter(i,speciesSml,speciesLrg)  * &
                    ((pairpotparameter(i+1,speciesSml,speciesLrg)   - cutoffMin) **3)
            endif
         enddo
      else
         print *,'typepairpot(',speciesSml,',',speciesLrg,')=',typepairpot(speciesSml,speciesLrg), &
                 'not implemented, stopping.'
         stop
      endif

      !Calculate derivative of k3 w.r.t parameters
      dk3_dpara = 0d0
      !Coefficients:
      do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
         i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
         if (cutoffMin.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
            dk3_dpara(i) = dk3_dpara(i) + &
                    ((pairpotparameter(i+1,speciesSml,speciesLrg)   - cutoffMin) **3)
         endif
      enddo
      !Cutoffs:
      do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
         i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
         if (cutoffMin.le.pairpotparameter(i,speciesSml,speciesLrg)) then
            dk3_dpara(i) = dk3_dpara(i) + &
                   3d0 * pairpotparameter(i-1,speciesSml,speciesLrg) * &
                   ((pairpotparameter(i,speciesSml,speciesLrg)   - cutoffMin) **2)
         endif
      enddo

      !Evaluate k4: value of d(pairpotential)/dr at R=cutoffMin
      k4 = 0d0
      do i=1,31,2
         if (cutoffMin.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
            k4=k4 - 3d0*pairpotparameter(i,speciesSml,speciesLrg) * &
                ((pairpotparameter(i+1,speciesSml,speciesLrg)-cutoffMin)**2)
         endif
      enddo

      !Calculate derivative of k4 w.r.t parameters
      dk4_dpara = 0d0
      !Coefficients:
      do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
         i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
         if (cutoffMin.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
            dk4_dpara(i) = dk4_dpara(i) -3d0* &
                    ((pairpotparameter(i+1,speciesSml,speciesLrg)   - cutoffMin) **2)
         endif
      enddo
      !Cutoffs:
      do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
         i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
         if (cutoffMin.le.pairpotparameter(i,speciesSml,speciesLrg)) then
            dk4_dpara(i) = dk4_dpara(i) - &
                   6d0 * pairpotparameter(i-1,speciesSml,speciesLrg) * &
                   (pairpotparameter(i,speciesSml,speciesLrg)   - cutoffMin)
         endif
      enddo

      !Now calculate B0-B3 from the values k1-k4
      !...but first, preliminary quantities:      
      rdiffcb=(rBier-cutoffMin)**3
      rBiersq=rBier**2
      rBiercb=rBier**3
      cutoffMinsq=cutoffMin**2
      cutoffMincb=cutoffMin**3
     !print *,'rBier=',rBier
     !print *,'rdiffcb=',rdiffcb,', rBiersq=',rBiersq
     !print *,'rBiercb=',rBiercb,', cutoffMin=',cutoffMin
     !print *,'cutoffMinqs=',cutoffMinsq,', cutoffMincb=',cutoffMincb
      B0=(-1d0/rdiffcb) * ( -k3*rBiercb + &
            3d0*k3*rBiersq*cutoffMin + &
            k4*rBiercb*cutoffMin - &
            3d0*k1*rBier*cutoffMinsq + &
            k2*rBiersq*cutoffMinsq - &
            k4*rBiersq*cutoffMinsq + &
            k1*cutoffMincb - k2*rBier*cutoffMincb )
      B1=(-1d0/rdiffcb) * ( -k4*rBiercb + &
            6d0*k1*rBier*cutoffMin - &
            6d0*k3*rBier*cutoffMin - &
            2d0*k2*rBiersq*cutoffMin - &
            k4*rBiersq*cutoffMin + &
            k2*rBier*cutoffMinsq + 2d0*k4*rBier*cutoffMinsq + &
            k2*cutoffMincb )
      B2=(-1d0/rdiffcb) * ( -3d0*k1*rBier + 3d0*k3*rBier + &
            k2*rBiersq + 2d0*k4*rBiersq - &
            3d0*k1*cutoffMin + 3d0*k3*cutoffMin + &
            k2*rBier*cutoffMin - k4*rBier*cutoffMin - &
            2d0*k2*cutoffMinsq - k4*cutoffMinsq )
      B3=-(2d0*k1-2d0*k3-k2*rBier-k4*rBier+k2*cutoffMin+k4*cutoffMin) / &
          rdiffcb
     !print *,'B0=',B0
     !print *,'B1=',B1
     !print *,'B2=',B2
     !print *,'B3=',B3
      pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)

      !Calculate derivative of B0, ..., B3 w.r.t parameters, and from these
      !the derivatives of pairpot
      !Coefficients:
      do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
         i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
         dB0_dpara = (-1d0/rdiffcb) * ( -dk3_dpara(i)*rBiercb + &
            3d0*dk3_dpara(i)*rBiersq*cutoffMin + &
            dk4_dpara(i)*rBiercb*cutoffMin - &
            dk4_dpara(i)*rBiersq*cutoffMinsq )
         dB1_dpara=(-1d0/rdiffcb) * ( -dk4_dpara(i)*rBiercb - &
            6d0*dk3_dpara(i)*rBier*cutoffMin - &
            dk4_dpara(i)*rBiersq*cutoffMin + &
            2d0*dk4_dpara(i)*rBier*cutoffMinsq )
         dB2_dpara=(-1d0/rdiffcb) * ( 3d0*dk3_dpara(i)*rBier + &
            2d0*dk4_dpara(i)*rBiersq + &
            3d0*dk3_dpara(i)*cutoffMin - &
            dk4_dpara(i)*rBier*cutoffMin - &
            dk4_dpara(i)*cutoffMinsq )
         dB3_dpara=-(-2d0*dk3_dpara(i)-dk4_dpara(i)*rBier+ &
            dk4_dpara(i)*cutoffMin) / rdiffcb
         dpairpot_dpara(i,speciesSml,speciesLrg) = dpairpot_dpara(i,speciesSml,speciesLrg) + &
                    dB0_dpara + dB1_dpara*distance + &
                    dB2_dpara*(distance**2) + dB3_dpara*(distance**3)
      enddo
      !Cutoffs:
      do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
         i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
         dB0_dpara = (-1d0/rdiffcb) * ( -dk3_dpara(i)*rBiercb + &
            3d0*dk3_dpara(i)*rBiersq*cutoffMin + &
            dk4_dpara(i)*rBiercb*cutoffMin - &
            dk4_dpara(i)*rBiersq*cutoffMinsq )
         dB1_dpara=(-1d0/rdiffcb) * ( -dk4_dpara(i)*rBiercb - &
            6d0*dk3_dpara(i)*rBier*cutoffMin - &
            dk4_dpara(i)*rBiersq*cutoffMin + &
            2d0*dk4_dpara(i)*rBier*cutoffMinsq )
         dB2_dpara=(-1d0/rdiffcb) * ( 3d0*dk3_dpara(i)*rBier + &
            2d0*dk4_dpara(i)*rBiersq + &
            3d0*dk3_dpara(i)*cutoffMin - &
            dk4_dpara(i)*rBier*cutoffMin - &
            dk4_dpara(i)*cutoffMinsq )
         dB3_dpara=-(-2d0*dk3_dpara(i)-dk4_dpara(i)*rBier+ &
            dk4_dpara(i)*cutoffMin) / rdiffcb
         dpairpot_dpara(i,speciesSml,speciesLrg) = dpairpot_dpara(i,speciesSml,speciesLrg) + &
                    dB0_dpara + dB1_dpara*distance + &
                    dB2_dpara*(distance**2) + dB3_dpara*(distance**3)
      enddo

     !distance=rBier
     !pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)
     !print *,'interpolated value at r=rBier, =',pairpot
     !print *,'... should equal k1=',k1
     !print *,'interpolated derivative at r=rBier=',B1+2d0*B2*distance+3d0*B3*(distance**2)
     !tmp=pairpot
     !distance=rBier+0.0001d0
     !pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)
     !print *,'by finite difference=',(pairpot-tmp)/0.0001d0
     !print *,'... should equail k2=',k2

     !distance=cutoffMin
     !pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)
     !print *,'interpolated value at r=cutoffMin, =',pairpot
     !print *,'... should equal k3=',k3
     !print *,'interpolated derivative at r=rBier=',B1+2d0*B2*distance+3d0*B3*(distance**2)
     !tmp=pairpot
     !distance=cutoffMin+0.0001d0
     !pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)
     !print *,'by finite difference=',(pairpot-tmp)/0.0001d0
     !print *,'... should equail k4=',k4
     !stop

    else

!      if (lookuptables.eqv..true.) then
!         !Add code for reading in data direct from tables here.
!      else

       speciesSml=min(species1,species2)
       speciesLrg=max(species1,species2)
       if (typepairpot(speciesSml,speciesLrg).eq.2) then
          pairpot=0d0
          do i=1,31,2
             if (distance.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
                 pairpot=pairpot + pairpotparameter(i,speciesSml,speciesLrg)  * &
                     ((pairpotparameter(i+1,speciesSml,speciesLrg)   - distance) **3)
             endif
          enddo
       endif

       !Calculate derivative of pair potential w.r.t parameters
       !Coefficients:
       do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
          i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
          if (distance.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
             dpairpot_dpara(i,speciesSml,speciesLrg) = dpairpot_dpara(i,speciesSml,speciesLrg) + &
                         (pairpotparameter(i+1,speciesSml,speciesLrg)   - distance) **3
          endif
       enddo
       !Cutoffs:
       do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
          i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
          if (distance.le.pairpotparameter(i,speciesSml,speciesLrg)) then
             dpairpot_dpara(i,speciesSml,speciesLrg) = dpairpot_dpara(i,speciesSml,speciesLrg) + &
                        3d0 * pairpotparameter(i-1,speciesSml,speciesLrg)  * &
                        ((pairpotparameter(i,speciesSml,speciesLrg)   - distance) **2)
          endif
       enddo

 !  do isp=1,maxspecies
 !      do jsp=isp,maxspecies
 !         do iiCoeff=1,max_noptpairpotCoeff
 !            print *,'ioptpairpotCoeff(',iiCoeff,',',isp,',',jsp,')=',ioptpairpotCoeff(iiCoeff,isp,jsp)
 !         enddo
 !         do iiCoeff=1,max_noptpairpotCoeff
 !            print *,'ioptpairpotCutoff(',iiCoeff,',',isp,',',jsp,')=',ioptpairpotCutoff(iiCoeff,isp,jsp)
 !         enddo
 !      enddo
 !  enddo
 !  stop
 

!      endif

    endif

end subroutine radpairpot


subroutine radpairpotSpline(species1,species2,distance,pairpot,dpairpot_dpara)

    !------------------------------------------------------------------c
    !
    !     Calculates the 'pairpot', for two atoms
    !     of species, 'species1' and 'species2' which are a
    !     distance, 'distance', apart from one another.
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Arguments:     species1,species2,distance,pairpotparameter
    !     Returns:       pairpot
    !     Files read:    -
    !     Files written: -
    !
    !     Marcel Sluiter, Feb 9 2006
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_atomproperties
    use m_meamparameters
    use m_filenames
    use m_generalinfo
    use m_optimization

    implicit none

    integer species1,species2,i,index,isp,jsp,speciesSml,speciesLrg,iiCoeff,iiCutoff,rIndex
    real(8) distance,pairpot,tmp,rsBier,x,eps,B0,B1,B2,B3,k1,k2,k3,k4,depsdr, &
            rdiffcb,rBiersq,rBiercb,cutoffMinsq,cutoffMincb!,diff
    real(8) dpairpot_dpara(32,maxspecies,maxspecies)
    intent(in) species1,species2!distance
    intent(out) pairpot

    speciesSml=min(species1,species2)
    speciesLrg=max(species1,species2)

    rIndex=1+FLOOR(distance/drLmps(speciesSml,speciesLrg))
    call interpolateSpline(drLmps(speciesSml,speciesLrg),pairpotSpline(1:nRhoMaxSpline,speciesSml,speciesLrg),pairpotSplineSecDer(1:nRhoMaxSpline,speciesSml,speciesLrg),nRMaxSpline,rIndex,distance,pairpot)
    pairpot=pairpot/distance

    dpairpot_dpara=0

end subroutine radpairpotSpline



!     Old code relating to Ackland Fe-Fe potential:
!
!
!
!                 !Ackland Fe-Fe pair-potential
!
!                 !              pairpot=pairpotAcklandFeFe(distance)
!                 !              tmp=pairpot
!                 tmp=((distance/p_rmax)**0.5)*dble(narrpairpotXX)
!                 index=tmp
!                 call splint(r_pairpotXXarr,pairpotXXarr, &
!                     secderpairpotXX,narrpairpotXX,narrpairpotXX, &
!                     index,distance,pairpot)
!                 !              print *,pairpot
!                 !              r_pairpotXXarr(i) =
!                 !     +         ( (dble(i)/dble(narrpairpotXX)) )**2 *
!                 !     +          p_rmax
!                 !              tmp=((distance/p_rmax)**0.5)*dble(narrpairpotXX)
!                 !              i=tmp
!                 !              print *,pairpotXXarr(i),pairpotXXarr(i+1)
!                 !              stop
!
!                 !              write(91,*) distance,tmp,pairpot
!
!                 !            if (distance.lt.1d0) then
!                 !              pairpot=( 9734.2365892908d0 / distance ) *
!                 !    +               ( 0.18180d0*exp(-2.8616724320005d1*distance) !E+01
!                 !    +               + 0.50990d0*exp(-8.4267310396064d0*distance)
!                 !    +               + 0.28020d0*exp(-3.6030244464156d0*distance)
!                 !    +               + 0.02817d0*exp(-1.8028536321603d0*distance) )
!                 !            else
!                 !              pairpot=exp( 7.4122709384068d0 -
!                 !    +               0.64180690713367d0*distance -
!                 !    +               2.6043547961722d0*(distance**2) +
!                 !    +               0.6262539393123d0*(distance**3) ) *
!                 !    +               cutoff(distance,1d0,2.05d0)
!                 !    +              - 27.444805994228d0 * ((2.2d0 - distance) **3) *
!                 !    +               cutoff(distance,2.05d0,2.2d0)
!                 !    +              + 15.738054058489d0 * ((2.3d0 - distance) **3) *
!                 !    +               cutoff(distance,2.05d0,2.3d0)
!                 !    +              + 2.2077118733936d0 * ((2.4d0 - distance) **3) *
!                 !    +               cutoff(distance,2.05d0,2.4d0)
!                 !    +              - 2.4989799053251d0 * ((2.5d0 - distance) **3) *
!                 !    +               cutoff(distance,2.05d0,2.5d0)
!                 !    +              + 4.2099676494795d0 * ((2.6d0 - distance) **3) *
!                 !    +               cutoff(distance,2.05d0,2.6d0)
!                 !    +              - 0.77361294129713d0 * ((2.7d0 - distance) **3) *
!                 !    +                cutoff(distance,2.05d0,2.7d0)
!                 !    +              + 0.80656414937789d0  * ((2.8d0 - distance) **3) *
!                 !    +                 cutoff(distance,2.05d0,2.8d0)
!                 !    +              - 2.3194358924605d0  * ((3.d0 - distance) **3) *
!                 !    +                 cutoff(distance,2.05d0,3.d0)
!                 !    +              + 2.6577406128280d0  * ((3.3d0 - distance) **3) *
!                 !    +                 cutoff(distance,2.05d0,3.3d0)
!                 !    +              - 1.0260416933564d0  * ((3.7d0 - distance) **3) *
!                 !    +                 cutoff(distance,2.05d0,3.7d0)
!                 !    +              + 0.35018615891957d0  * ((4.2d0 - distance) **3) *
!                 !    +                 cutoff(distance,2.05d0,4.2d0)
!                 !    +              - 0.058531821042271d0  * ((4.7d0 - distance) **3)*
!                 !    +                 cutoff(distance,2.05d0,4.7d0)
!                 !    +              - 0.0030458824556234d0  *
!                 !    +              ((5.3d0 - distance) **3)*
!                 !    +                 cutoff(distance,2.05d0,5.3d0)
!                 !            endif
!                 !               print *,'pairpot analytic=',pairpot
!                 !               stop





!   Old code used when I tried using a spline rather than explicit calculation
!   of pairpotentials each time (did not give a speed increase so dropped this)
!
!
!
!
!           ! tmp=(splnNvals-1)*(distance-cutoffMin)/(cutoffMax-cutoffMin)
!           ! splnPosn=floor(tmp)+1 !Finds the lower bound array element
!           ! diff=tmp-floor(tmp)
!           ! pairpot=(1d0-diff)*pairpotStr(splnPosn,1,2) + &
!           !                diff*pairpotStr(splnPosn+1,1,2)
!
!              !if (pairpot.ne.0d0) then
!              !   write(81,*) 'distance=',distance
!              !   write(81,*) '   from array, pairpot(1,2)=',pairpot
!              !endif




!    Code for Fe-C hepburn potential:
!
!
!             !Following is Hepburn form, but HARD-WIRED
!             !               if (species1.eq.2) then
!             !                  print *,'distance=',distance
!             !               endif
!
!             !              do i=1,1000
!             !
!             !              distance=0.92d0+(dble(i-1)/999d0)*(4d0-0.92d0)
!
!             if (distance.lt.0.92d0) then
!                 print *,'need to implement full Fe-C pot'
!                 stop
!             elseif (distance.lt.1.7286d0) then
!                 pairpot=   1421.5171091881768d0 &
!                     -      3690.825251313584d0  *  distance &
!                     +      3710.621639067296    * (distance**2) &
!                     -      1679.6860367988738   * (distance**3) &
!                     +       285.39847077772725  * (distance**4)
!             elseif (distance.lt.1.88d0) then
!                 pairpot=   1521.0470658708155 &
!                     -      2389.4324313634993   *  distance &
!                     +      1252.1878589565072   * (distance**2) &
!                     -       218.9361400835967   * (distance**3)
!             elseif (distance.lt.2.25d0) then
!                 pairpot=    241.09303032117546 &
!                     -       346.952587401322    *  distance &
!                     +       165.7624100404569   * (distance**2) &
!                     -        26.307514389261673 * (distance**3)
!             elseif (distance.lt.2.42d0) then
!                 pairpot= -  581.6276060699361 &
!                     +       750.0082611201603   *  distance &
!                     -       321.77574485797965  * (distance**2) &
!                     +        45.9203604105067   * (distance**3)
!             elseif (distance.lt.2.7244d0) then
!                 pairpot=    364.0122288533755 &
!                     -       422.2725259748537   *  distance &
!                     +       162.63780352838967  * (distance**2) &
!                     -        20.803268843814134 * (distance**3)
!             elseif (distance.lt.3.1581d0) then
!                 pairpot= -  271.6958017654534 &
!                     +       277.7436580864025   *  distance &
!                     -        94.30544418165887  * (distance**2) &
!                     +        10.634019820362498 * (distance**3)
!             elseif (distance.lt.3.5d0) then
!                 pairpot=   4005.3336322295286 &
!                     -      4759.736033262177    *  distance &
!                     +      2117.9776780306693   * (distance**2) &
!                     -       418.29875898347865  * (distance**3) &
!                     +        30.94094273871914  * (distance**4)
!             else
!                 pairpot=0d0
!             endif
!
!
!
!    Hepburn for C-C:
!
!             !Following is Hepburn form, but HARD-WIRED
!             if (distance.lt.1d0) then
!                 print *,'tend to the pairpot subroutine...'
!                 stop
!             elseif (distance.lt.1.2857838598417968d0) then
!                 pairpot=   1199.5739471496045d0 &
!                     -      3835.3033425305593d0  *  distance &
!                     +      4786.499640264303d0  * (distance**2) &
!                     -      2705.687420612359d0 * (distance**3) &
!                     +       577.189423411637d0 * (distance**4)
!             elseif (distance.lt.1.8008513964923578d0) then
!                 pairpot=    286.8908260379106d0 &
!                     -       478.88759650017056d0 *  distance &
!                     +       267.62987770250356d0 * (distance**2) &
!                     -        49.910297272136546d0* (distance**3)
!             elseif (distance.lt.2.2863452818753887d0) then
!                 pairpot= -   16.399912881986573d0 &
!                     +        26.357988189333234d0 *  distance &
!                     -        12.929409795401792d0 * (distance**2) &
!                     +         2.020563142807547d0* (distance**3)
!             elseif (distance.lt.3.5d0) then
!                 pairpot=     11.60221676482102d0 &
!                     -        10.554683135163948d0 *  distance &
!                     +         3.3641528432894177d0 * (distance**2) &
!                     -         0.4199752489948345d0* (distance**3) &
!                     +         0.014225677158589412d0* (distance**4)
!             else
!                 pairpot=0d0
!             endif






! Old spline code:
!
!
!
!
!           ! tmp=(splnNvals-1)*(distance-cutoffMin)/(cutoffMax-cutoffMin)
!           ! splnPosn=floor(tmp)+1 !Finds the lower bound array element
!           ! diff=tmp-floor(tmp)
!           ! pairpot=(1d0-diff)*pairpotStr(splnPosn,1,2) + &
!           !                diff*pairpotStr(splnPosn+1,1,2)
!
!              !if (pairpot.ne.0d0) then
!              !   write(81,*) 'distance=',distance
!              !   write(81,*) '   from array, pairpot(1,2)=',pairpot
!              !endif

