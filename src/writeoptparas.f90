
subroutine writeoptparasSub

    !-------------------------------------------------------------------c
    !
    !     Read in the which potential parameters are to be optimized.
    !
    !     Called by:     readsettings
    !     Calls:         -
    !     Arguments:     -
    !     Returns:       -
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff 2014
    !
    !     Copyright 2017, STFC
    !
    !-------------------------------------------------------------------c

    use m_optimization
    use m_atomproperties
    use m_meamparameters

    implicit none

    integer i,isp,jsp,spcCnt,l

    do i=1,np
       if (p(i).ne.0d0) then
          freep(i)=1
       endif
    enddo

    print *
    print *,'-------------------------------------'
    print *

    write(*,'(A12)') 'PARASTOOPT={'
    do i=1,m3
       write(*,'(I1,A1)',advance='no') freep(i),' '
    enddo
    write(*,*)
    do i=1,m3
       write(*,'(I1,A1)',advance='no') freep(i+m3),' '
    enddo
    write(*,*)
    if (lmax.gt.0) then
       do i=2*m3+1,2*m3+lmax*m1
          write(*,'(I1,A1)',advance='no') freep(i),' '
       enddo
    endif
    write(*,*)
    spcCnt=0
    do isp=1,m1
       do jsp=1,m1
          do l=0,lmax
             do i=1,12
                write(*,'(I1,A1)',advance='no') &
          freep(2*m3+lmax*m1+12*(spcCnt*lm1+l)+i),' '
             enddo
             write(*,*)
          enddo
          spcCnt=spcCnt+1
       enddo
    enddo
    do i=2*m3+lmax*m1+12*lm1*m2+1,2*m3+m1 &
        +lmax*m1+12*lm1*m2
       write(*,'(I1,A1)',advance='no') freep(i),' '
    enddo
    write(*,*)
    do i=2*m3+m1+lmax*m1+12*lm1*m2+1, &
        2*m3+2*m1+lmax*m1+12*lm1*m2
       write(*,'(I1,A1)',advance='no') freep(i),' '
    enddo
    write(*,*)
    do i=2*m3+2*m1+lmax*m1+12*lm1*m2+1, &
        2*m3+3*m1+lmax*m1+12*lm1*m2
       write(*,'(I1,A1)',advance='no') freep(i),' '
    enddo
    write(*,*)
    do i=2*m3+3*m1+lmax*m1+12*lm1*m2+1, &
        2*m3+4*m1+lmax*m1+12*lm1*m2
       write(*,'(I1,A1)',advance='no') freep(i),' '
    enddo
    write(*,*)
    spccnt=0
    do isp=1,m1
       do jsp=1,m1
          do i=1,32
             write(*,'(I1,A1)',advance='no') freep(2*m3+(4+lmax)*m1+ &
                   12*lm1*m2+32*spccnt+i),' '
          enddo
          write(*,*)
          spccnt=spccnt+1
       enddo
    enddo
    !pairpotparameter(16,maxspecies,maxspecies)
    do i=m4+1,m4+m2
       write(*,'(I1,A1)',advance='no') freep(i),' '
    enddo
    write(*,*)
    do i=m4+m2+1,m4+2*m2
       write(*,'(I1,A1)',advance='no') freep(i),' '
    enddo
    write(*,*)
    do i=m4+2*m2+1,m4+2*m2+m1
       write(*,'(I1,A1)',advance='no') freep(i),' '
    enddo
    write(*,*)
    write(*,'(A1)') '}'
    write(*,*)
    close(1)


 !  write(*,*) freep(1:m3)  !cmin(maxspecies,maxspecies,maxspecies)
 !  write(*,*) freep(1+m3:2*m3) !cmax(maxspecies,maxspecies,maxspecies)
 !  write(*,*) freep(2*m3+1:2*m3+lm1*m1) !meamtau(1:lmax,maxspecies)
 !  write(*,*) freep(2*m3+lm1*m1+1:2*m3+lm1*m1+12*lm1*m2)
 !  !meamrhodecay(6,0:lmax,maxspecies,maxspecies)
 !  !meamrhodecay(6,0:lmax,maxspecies,maxspecies)
 !  write(*,*) freep(2*m3+lm1*m1+12*lm1*m2+1:2*m3+m1 &
 !      +lm1*m1+12*lm1*m2)   !meame0(maxspecies)
 !  write(*,*) freep(2*m3+m1+lm1*m1+12*lm1*m2+1:2*m3+ &
 !      2*m1+lm1*m1+12*lm1*m2) !meamrho0(maxspecies)
 !  write(*,*) freep(2*m3+2*m1+lm1*m1+12*lm1*m2+1:2*m3+ &
 !      3*m1+lm1*m1+12*lm1*m2) !meamemb3
 !  write(*,*) freep(2*m3+3*m1+lm1*m1+12*lm1*m2+1:2*m3+ &
 !      4*m1+lm1*m1+12*lm1*m2) !meamemb4
 !  write(*,*) freep(2*m3+(4+lm1)*m1+12*lm1*m2+1:m4)
 !  !pairpotparameter(16,maxspecies,maxspecies)
 !  write(*,*) freep(m4+1:m4+m2) !rs(maxspecies,maxspecies)
 !  write(*,*) freep(m4+m2+1:m4+2*m2) !rc(maxspecies,maxspecies)
 !  write(*,*) freep(m4+2*m2+1:m4+2*m2+m1) !enconst(maxspecies)

    print *
    print *,'Please copy and paste the above into the settings file.'
    print *,'0=no opt; 1=opt but do not randomly seed;'
    print *,'2=opt and randomly seed; 3=no opt but randomly seed'
    print *,'Ordering corresponds to that in the potparas_best files'

end subroutine writeoptparasSub
