!
!    Mve: The entire comment section below is about the MPI modification:
!    Because the iterations of the main optimization loop are divided over multiple process when running it
!    in parallel, the processes need to communicate some data with each other after the main optimization loop
!    has ended. In order to do this communication, send and receive (recv) arrays are initialized.
!    Different send/recv arrays are initialized: for single variables (reals), for 1d arrays, for 2d arrays
!    and for 3d arrays. Below is an overview of all the reals/arrays that need to be communicated after
!    the main optimization loop.
!
!    Overview of arrays to be sent, sorted by dimension and MPI call
!    +-----------------+----------------+----------------+----------------+
!    |    1D array     |    2D array    |    3D array    |    3D array    |
!    +-----------------+----------------+----------------+----------------+
!    | dFen_dmeame0    | dFen_dmeamtau  | dFen_draddens  | dFen_dpairpot  |
!    | dFen_dmeamrho0  |                |                |                |
!    | dFen_dmeamemb3  |                |                |                |
!    | dFen_dmeamemb4  |                |                |                |
!    | dFen_denconst   |                |                |                |
!    +-----------------+----------------+----------------+----------------+
!    | dFen2_dmeame0   | dFen2_dmeamtau | dFen2_draddens | dFen2_dpairpot |
!    | dFen2_dmeamrho0 |                |                |                |
!    | dFen2_dmeamemb3 |                |                |                |
!    | dFen2_dmeamemb4 |                |                |                |
!    +-----------------+----------------+----------------+----------------+
!    | dFfrc_dmeame0   | dFfrc_dmeamtau | dFfrc_draddens | dFfrc_dpairpot |
!    | dFfrc_dmeamrho0 |                |                |                |
!    | dFfrc_dmeamemb3 |                |                |                |
!    | dFfrc_dmeamemb4 |                |                |                |
!    +-----------------+----------------+----------------+----------------+
!    | dFstr_dmeame0   | dFstr_dmeamtau | dFstr_draddens | funcforcdenom  |
!    | dFstr_dmeamrho0 |                |                |                |
!    | dFstr_dmeamemb3 |                |                |                |
!    | dFstr_dmeamemb4 |                |                |                |
!    | dFstr_dpairpot  |                |                |                |
!    +-----------------+----------------+----------------+----------------+
!
!    List of the single variables (reals) that need to be communicated after the main optimization loop:
!    - funcforc
!    - funcen
!    - funcstr
!    - funcforcdenom
!    - funcendenom
!    - funcstrdenom
!    - Fen
!    - Ffrc
!    - avg_deltaE   !TODO: this variable still needs some work
!    - Fstr


subroutine p_setup

  ! This subroutine initialized MPI and the variables
  ! that are constant throughout the application

  use m_mpi_info

  implicit none

  ! Header for MPI
  include 'mpif.h'

  call MPI_init(ierr)
  call MPI_comm_size(MPI_COMM_WORLD, nprocs, ierr) ! sets nprocs to the amount
                                                   ! of processes running
  call MPI_comm_rank(MPI_COMM_WORLD, procid, ierr) ! sets procid to the current
                                                   ! process's ID. It procid=0
                                                   ! this is the main thread

  if (procid.eq.0) then
    print *, '[MPI Info] Running MPI with ',nprocs,' nodes'
  endif

end subroutine p_setup



subroutine p_allocateArrays

  ! This subroutine allocates all arrays used to send around data between nodes

  use m_optimization
  use m_atomproperties
  use m_meamparameters
  use m_mpi_info
  use m_datapoints

  implicit none

  ! allocating send and receiving buffer arrays for single variables.
  ! There are 10 reals to send, so the length of the array is 10
  allocate(send_arr(10))
  allocate(recv_arr(10))

  ! allocating send and receive arrays for the communication of 1d arrays. The length is dependent on
  ! whether objFuncType=2:    if objFuncType =    2 --> all 1d dFen2_... should be allocated
  !                           if objFuncType !=   2 --> all 1d dFen2_... should NOT be allocated
  if (objFuncType.eq.2) then
      allocate(recv_all_1darr(17*maxspecies))
      allocate(send_All_1darr(17*maxspecies))
  else
      allocate(recv_all_1darr(13*maxspecies))
      allocate(send_All_1darr(13*maxspecies))
  endif

  ! Maybe add "if((noopt.eqv..false.)" later on?
  ! allocating send and receive arrays for the communication of 2d arrays. The length is dependent on
  ! -whether objFuncType=2:   if objFuncType =    2 --> all 2d dFen2_... should be allocated
  !                           if objFuncType !=   2 --> all 2d dFen2_... should NOT be allocated
  ! -whether lmax > 0:        if lmax !>          0 -->
  !                           if lmax >           0 -->
   if ((objFuncType.eq.2) .AND. (lmax .gt.0)) then
       allocate(send_all_2darr(maxspecies*4*lmax))
       allocate(recv_all_2darr(maxspecies*4*lmax))
   elseif (objFuncType.eq.2) then
       allocate(send_all_2darr(maxspecies*2*lmax))
       allocate(recv_all_2darr(maxspecies*2*lmax))
   elseif (lmax.gt.0) then
       allocate(send_all_2darr(maxspecies*3*lmax))
       allocate(recv_all_2darr(maxspecies*3*lmax))
   else
       allocate(send_all_2darr(maxspecies*lmax))
       allocate(recv_all_2darr(maxspecies*lmax))
   endif


  ! allocating send and receive arrays for the communication of 3d arrays. The length is dependent on
  ! whether objFuncType=2:    if objFuncType =    2 --> all 3d dFen2_... should be allocated
  !                           if objFuncType !=   2 --> all 3d dFen2_... should NOT be allocated
  if (objFuncType.eq.2) then
      allocate(recv_draddens_3darr(12*(lmax+1)*maxspecies*4))
      allocate(send_draddens_3darr(12*(lmax+1)*maxspecies*4))
      allocate(recv_dpairpot_3darr(32*maxspecies*maxspecies*4))
      allocate(send_dpairpot_3darr(32*maxspecies*maxspecies*4))
  else
      allocate(recv_draddens_3darr(12*(lmax+1)*maxspecies*3))
      allocate(send_draddens_3darr(12*(lmax+1)*maxspecies*3))
      allocate(recv_dpairpot_3darr(32*maxspecies*maxspecies*3))
      allocate(send_dpairpot_3darr(32*maxspecies*maxspecies*3))
  endif

  ! Allocate variables needed for MPI communication
  allocate(q(np))
  
  allocate(recv_fitdata(ndatapoints))

end subroutine p_allocateArrays



subroutine p_workerThread
  ! This routine contains the main loop of the worker threads

  ! Initialize the workers once
  ! This includes allocating all required arrays
  call p_workerSetup

  ! The worker threads keep on executing
  ! the objectiveFunction indefinitely
  ! Inside the objective function they will
  ! wait to receive data from the main thread
  do
      call objectiveFunction(.false.)
  end do
  ! Once the main thread terminates,
  ! the worker threads will be killed automatically
  ! This is not a problem, since the workers have
  ! already shared any important data the might contain by then

end subroutine p_workerThread


subroutine p_workerSetup
  ! MvE
  ! The workerInit subroutine is used to allocate
  ! required arrays for the worker processes,
  ! needed in the objectivfunction routine

  use m_datapoints
  use m_geometry
  use m_optimization
  use m_generalinfo
  use m_meamparameters
  use m_filenames
  use m_poscar
  use m_electrondensity
  use m_atomproperties
  use m_plotfiles
  use m_objectiveFunction
  use m_observables
  use m_mpi_info

  call allocateArrays

  ! copied from optimizeParameters
  allocate(dF_dmeamtau(1:lmax,maxspecies))
  allocate(dF_draddens(12,0:lmax,maxspecies))
  allocate(dF_dmeame0(maxspecies),dF_dmeamrho0(maxspecies), &
      dF_dmeamemb3(maxspecies),dF_dmeamemb4(maxspecies))
  allocate(dF_dpairpot(32,maxspecies,maxspecies))
  allocate(dF_denconst(maxspecies))
  allocate(dF_dpara(np))
  !Also for objective function components
  allocate(dFen_dmeamtau(1:lmax,maxspecies))
  allocate(dFen_draddens(12,0:lmax,maxspecies))
  allocate(dFen_dmeame0(maxspecies),dFen_dmeamrho0(maxspecies), &
      dFen_dmeamemb3(maxspecies),dFen_dmeamemb4(maxspecies))
  allocate(dFen_dpairpot(32,maxspecies,maxspecies))
  allocate(dFen_denconst(maxspecies))
  if (objFuncType.eq.2) then
     !The following are for Tom's objective function
     allocate(dFen2_dmeamtau(1:lmax,maxspecies))
     allocate(dFen2_draddens(12,0:lmax,maxspecies))
     allocate(dFen2_dmeame0(maxspecies),dFen2_dmeamrho0(maxspecies), &
         dFen2_dmeamemb3(maxspecies),dFen2_dmeamemb4(maxspecies))
     allocate(dFen2_dpairpot(32,maxspecies,maxspecies))
  endif

  if (lmax.gt.0) then
     allocate(dFfrc_dmeamtau(1:lmax,maxspecies))
  endif
  allocate(dFfrc_draddens(12,0:lmax,maxspecies))
  allocate(dFfrc_dmeame0(maxspecies),dFfrc_dmeamrho0(maxspecies), &
      dFfrc_dmeamemb3(maxspecies),dFfrc_dmeamemb4(maxspecies))
  allocate(dFfrc_dpairpot(32,maxspecies,maxspecies))
  if (lmax.gt.0) then
     allocate(dFstr_dmeamtau(1:lmax,maxspecies))
  endif
  allocate(dFstr_draddens(12,0:lmax,maxspecies))
  allocate(dFstr_dmeame0(maxspecies),dFstr_dmeamrho0(maxspecies), &
      dFstr_dmeamemb3(maxspecies),dFstr_dmeamemb4(maxspecies))
  allocate(dFstr_dpairpot(32,maxspecies,maxspecies))
  allocate(dFcutoffPen_draddens(12,0:lmax,maxspecies), &
      dFcutoffPen_dpairpot(32,maxspecies,maxspecies)) !, &
      !dFlowDensPen_draddens(12,0:lmax,maxspecies))

  allocate(positivep(np),p_orig(np))
  if (contjob.eqv..false.) then
     allocate(p_saved(np,noptfuncstore))
  endif

  ! copied from some other function
  allocate( meamtau_minorder(1:lmax,maxspecies), &
      meamtau_maxorder(1:lmax,maxspecies), &
      meamrhodecay_minradius(0:lmax,maxspecies,maxspecies), &
      meamrhodecay_maxradius(0:lmax,maxspecies,maxspecies), &
      meamrhodecay_minorder(1:6,0:lmax,maxspecies,maxspecies), &
      meamrhodecay_maxorder(1:6,0:lmax,maxspecies,maxspecies), &
      meame0_minorder(maxspecies), meame0_maxorder(maxspecies), &
      meamrho0_minorder(maxspecies), meamrho0_maxorder(maxspecies), &
      meamemb3_minorder(maxspecies), meamemb3_maxorder(maxspecies), &
      meamemb4_minorder(maxspecies), meamemb4_maxorder(maxspecies), &
      pairpotparameter_minradius(maxspecies,maxspecies), &
      pairpotparameter_maxradius(maxspecies,maxspecies), &
      pairpotparameter_minorder(1:8,maxspecies,maxspecies), &
      pairpotparameter_maxorder(1:8,maxspecies,maxspecies), &
      minordervaluepairpot(1:16), &
      maxordervaluepairpot(1:16), &
      nfreeppairpot(maxspecies,maxspecies), &
      enconst_minorder(maxspecies), enconst_maxorder(maxspecies) )

  !Set the maxradius variables to zero (then only those 'in use' will be used
  !in the 'checkParas' subroutine).
  meamrhodecay_maxradius=0d0
  pairpotparameter_maxradius=0d0

  ! copied from meamfit.f90
  if (contjob.eqv..false.) then
     n_optfunc=1
     allocate(bestoptfuncs(noptfuncstore),timeoptfuncHr(noptfuncstore), &
         timeoptfuncMin(noptfuncstore))
  else
     n_optfunc=noptfuncstore+1
  endif

  call initialize_noptp

end subroutine p_workerSetup



subroutine p_beforeLoop(finalOutput)

  use m_datapoints
  use m_geometry
  use m_optimization
  use m_generalinfo
  use m_meamparameters
  use m_filenames
  use m_poscar
  use m_electrondensity
  use m_atomproperties
  use m_plotfiles
  use m_objectiveFunction
  use m_objectiveFunctionShared
  use m_observables
  use m_mpi_info

  implicit none

  ! Header for MPI
  include 'mpif.h'
  
  logical finalOutput
  integer iat
  
  !start_i and stop_i are the starting and stopping points for the processes to make sure each process does a part of the loop
  start_i = int(real(nstruct*procid)/real(nprocs)) + 1
  stop_i = int(real(nstruct*(procid+1))/real(nprocs))
  !start_i = start_i_stored(procid)
  !stop_i = stop_i_stored(procid)

  ! Make sure all structs are taken into account, giving the last process the remaining iterations
  if ((procid+1).eq.nprocs) then
      stop_i = nstruct
  endif

  ! print *,'in p_beforeLoop, start_i=',start_i,' stop_i=',stop_i
  
  ! Normally this is set to false
  ! When the main thread terminates however, it broadcasts true and all workers will terminate
  p_killWorkers = .false.
  call MPI_bcast(p_killWorkers, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, ierr)
  
  ! If we get the kill command from the main thread, stop here
  if (p_killWorkers) then
      call p_finalize
      stop
  endif

  ! Here, variables_to_p and p_to_variables are used to put the variables that need to be communicated to the workers,
  ! are communicated before the loop. Putting them into the vector p makes communication easier. Afterwards, p_to_variables
  ! is called to put the values back in the variables for the workers.
  if (procid.eq.0) then ! In the main thread we backup p and put the variables to be sent into p
      q(:) = p(:)
      call variables_to_p
  endif

  call MPI_bcast(p, np, MPI_DOUBLE, 0, MPI_COMM_WORLD, ierr)

  if (procid.eq.0) then ! In the main thread we restore p
      p(:) = q(:)
  else
      call p_to_variables
  endif
  
  ! For the workers finalOutput is always false.
  ! After this bcast however, p_finalOutput is true if (and only if) finalOutput was true in the main thread
  p_finalOutput = finalOutput
  call MPI_bcast(p_finalOutput, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, ierr)

  ! The workers do not start at the beginning of the loop over nstruct, so they're missing important data
  ! in the loop below this important data is calculated for all workers. 
  ! Note: it might be that some important variables are still missing. They can then be added in a similar fashion
  idatapoint=1
  do istr=1,start_i-1
        
    if (weightsEn(istr).ne.0d0) then
    
        if (refenergy(istr).ge.1) then
           !We only want the energy here, but we still need to call meamforce if
           !structures have been set up with force fitting in mind (see
           !'meamforce' subroutine for more info)
           if (computeForces(istr)) then
              call meamforce
           else
              call meamenergy
           endif
           !call meamenergy
           fitdata(idatapoint)=energy
           !print *,' setting up ref energies for procid=',procid,' fitdata(',idatapoint,')=',fitdata(idatapoint)
           ref_datapoint(refenergy(istr))=idatapoint
           ref_strucnum(refenergy(istr))=istr
           nRef=MAX(nRef,refenergy(istr))

           if (noOpt.eqv..false.) then
              dsummeamf_dmeamtau_ref(refenergy(istr),:,:)=dsummeamf_dmeamtau(:,:)
              dsummeamf_dpara_ref(refenergy(istr),:,:,:,:)=dsummeamf_dpara(:,:,:,:)
              dsummeamf_dmeame0_ref(refenergy(istr),:)=dsummeamf_dmeame0(:)
              dsummeamf_dmeamrho0_ref(refenergy(istr),:)=dsummeamf_dmeamrho0(:)
              dsummeamf_dmeamemb3_ref(refenergy(istr),:)=dsummeamf_dmeamemb3(:)
              dsummeamf_dmeamemb4_ref(refenergy(istr),:)=dsummeamf_dmeamemb4(:)
              dsummeam_paire_dpara_ref(refenergy(istr),:,:,:)=dsummeam_paire_dpara(:,:,:)
           endif
      endif
      
      idatapoint = idatapoint + 1
    endif
        
    if (weightsFr(istr).gt.0d0) then
       idatapoint = idatapoint + 3*gn_forces(istr)
    endif
    
    if (weightsSt(istr).gt.0d0) then
       idatapoint = idatapoint + 6
    endif
    
  enddo    

end subroutine p_beforeLoop




subroutine p_synchronizeResults

  use m_datapoints
  use m_geometry
  use m_optimization
  use m_generalinfo
  use m_meamparameters
  use m_filenames
  use m_poscar
  use m_electrondensity
  use m_atomproperties
  use m_plotfiles
  use m_objectiveFunction
  use m_objectiveFunctionShared
  use m_observables
  use m_mpi_info

  implicit none

  ! Header for MPI
  include 'mpif.h'
  
  ! these variables are used when combining fitdata for the final output
  integer idatapoint_start, idatapoint_stop, fitdata_start_i, fitdata_stop_i, iat


  !filling the send array with the variables that need to be communicated
      send_arr(1) = funcforc
      send_arr(2) = funcen
      send_arr(3) = funcstr
      send_arr(4) = funcforcdenom
      send_arr(5) = funcendenom
      send_arr(6) = funcstrdenom
      send_arr(7) = Fen
      send_arr(8) = Ffrc
      send_arr(9) = avg_deltaE
      send_arr(10)= Fstr

      if (noOpt.eqv..false.) then
         !filling a 1d array with all the 1d arrays that need to be communicated
         if (objFuncType.eq.2) then
            send_all_1darr=[dFen_dmeame0,dFen_dmeamrho0,dFen_dmeamemb3,dFen_dmeamemb4,dFen_denconst,dFfrc_dmeame0,dFfrc_dmeamrho0,dFfrc_dmeamemb3,dFfrc_dmeamemb4,dFstr_dmeame0,dFstr_dmeamrho0,dFstr_dmeamemb3,dFstr_dmeamemb4,dFen2_dmeame0,dFen2_dmeamrho0,dFen2_dmeamemb3,dFen2_dmeamemb4]
         else
            send_all_1darr=[dFen_dmeame0,dFen_dmeamrho0,dFen_dmeamemb3,dFen_dmeamemb4,dFen_denconst,dFfrc_dmeame0,dFfrc_dmeamrho0,dFfrc_dmeamemb3,dFfrc_dmeamemb4,dFstr_dmeame0,dFstr_dmeamrho0,dFstr_dmeamemb3,dFstr_dmeamemb4]
         endif

         !Filling a 1d array with all the 2d arrays that need to be communicated
         if ((objFuncType.eq.2) .AND. (lmax .gt.0)) then
            Send_all_2darr = [dFen_dmeamtau, dFfrc_dmeamtau, dFstr_dmeamtau, dFen2_dmeamtau]
         elseif (objFuncType.eq.2) then
            send_all_2darr = [dFen_dmeamtau, dFen2_dmeamtau]
         elseif (lmax.gt.0) then
            send_all_2darr = [dFen_dmeamtau, dFfrc_dmeamtau, dFstr_dmeamtau]
         else
            send_all_2darr = [dFen_dmeamtau]
         endif

         !Filling a 1d array with all the 3d arrays that need to be communicated
         if (objFuncType.eq.2) then
            send_draddens_3darr=[dFen_draddens,dFfrc_draddens,dFstr_draddens,dFen2_draddens]
            send_dpairpot_3darr=[dFen_dpairpot,dFfrc_dpairpot,dFstr_dpairpot,dFen2_dpairpot]
         else
            send_draddens_3darr=[dFen_draddens,dFfrc_draddens,dFstr_draddens]
            send_dpairpot_3darr=[dFen_dpairpot,dFfrc_dpairpot,dFstr_dpairpot]
         endif

      endif

      !communicating the results. So the arrays send_arr, send_all_1darr, send_all_2d_arr, send_draddens_3darr, send_dpairpot_3darr are sent and received
      !for loop over all process id's
      do i_node = 1, nprocs-1
         !the process that currently sends his result enters this if statement
         if (i_node.eq.procid) then

            !Sending the array with the variables to other processes
            call MPI_Send(send_arr, 10, MPI_DOUBLE, 0,1, MPI_COMM_WORLD,ierr)

            if (noOpt.eqv..false.) then
               !Sending the array with the 1d arrays to other processes, its size is dependent on whether objfunctype=2 or not.
               if (objFuncType.eq.2) then
                   call MPI_Send(send_all_1darr,17*maxspecies,MPI_DOUBLE, 0, 1 ,MPI_COMM_WORLD, ierr)
               else
                   call MPI_Send(send_all_1darr,13*maxspecies,MPI_DOUBLE, 0,1,MPI_COMM_WORLD,ierr)
               endif

               !Sending the array with the 2d arrays to other processes
               if ((objFuncType.eq.2) .AND. (lmax .gt.0)) then
                   call MPI_Send(send_all_2darr,4*maxspecies*lmax,MPI_DOUBLE,0,1,MPI_COMM_WORLD,ierr)
               elseif (objFuncType.eq.2) then
                   call MPI_Send(send_all_2darr,2*maxspecies*lmax,MPI_DOUBLE,0,1,MPI_COMM_WORLD,ierr)
               elseif (lmax.gt.0) then
                   call MPI_Send(send_all_2darr,3*maxspecies*lmax,MPI_DOUBLE,0,1,MPI_COMM_WORLD,ierr)
               else
                   call MPI_Send(send_all_2darr,1*maxspecies*lmax,MPI_DOUBLE,0,1,MPI_COMM_WORLD,ierr)
               endif

               !Sending the two arrays with the 3d arrays to other processes
               if (objFuncType.eq.2) then
                   call MPI_Send(send_draddens_3darr,12*(lmax+1)*maxspecies*4,MPI_DOUBLE, 0,1,MPI_COMM_WORLD, ierr)
                   call MPI_Send(send_dpairpot_3darr,32*maxspecies*maxspecies*4,MPI_DOUBLE,0,1,MPI_COMM_WORLD, ierr)
               else
                   call MPI_Send(send_draddens_3darr,12*(lmax+1)*maxspecies*3,MPI_DOUBLE, 0,1,MPI_COMM_WORLD,ierr)
                   call MPI_Send(send_dpairpot_3darr,32*maxspecies*maxspecies*3,MPI_DOUBLE, 0,1,MPI_COMM_WORLD, ierr)
               endif
            endif
            
            !Synchronize fitdata points, because these are written to file
            if (p_finalOutput) then
              
                call MPI_send(fitdata, ndatapoints, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, ierr)
              
            endif

         !all processes that receive in the current iteration enter the else statement
         elseif (procid.eq.0) then
            !Receiving the array with the variables from other processes
            call MPI_Recv(recv_arr, 10, MPI_DOUBLE, i_node, 1 ,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)

            if (noOpt.eqv..false.) then
               !Receiving the array with the 1d arrays to other processes, its size is dependent on whether objfunctype=2 or not.
               if (objFuncType.eq.2) then
                   call MPI_Recv(recv_all_1darr,17*maxspecies,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
               else
                   call MPI_Recv(recv_all_1darr,13*maxspecies,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
               endif

               !Receiving the array with the 2d arrays from other processes
               if ((objFuncType.eq.2) .AND. (lmax .gt.0)) then
                   call MPI_Recv(recv_all_2darr,4*maxspecies*lmax,MPI_DOUBLE,i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
               elseif (objFuncType.eq.2) then
                   call MPI_Recv(recv_all_2darr,2*maxspecies*lmax,MPI_DOUBLE,i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
               elseif (lmax.gt.0) then
                   call MPI_Recv(recv_all_2darr,3*maxspecies*lmax,MPI_DOUBLE,i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
               else
                   call MPI_Recv(recv_all_2darr,1*maxspecies*lmax,MPI_DOUBLE,i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
               endif

               !Receiving the two arrays with the 3d arrays from other processes
               if (objFuncType.eq.2) then
                  call MPI_Recv(recv_draddens_3darr,12*(lmax+1)*maxspecies*4,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
                  call MPI_Recv(recv_dpairpot_3darr,32*maxspecies*maxspecies*4,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
               else
                  call MPI_Recv(recv_draddens_3darr,12*(lmax+1)*maxspecies*3,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                  call MPI_Recv(recv_dpairpot_3darr,32*maxspecies*maxspecies*3,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
               endif
            endif
            
            if (p_finalOutput) then
              
              fitdata_start_i = int(real(nstruct*i_node)/real(nprocs)) + 1
              fitdata_stop_i = int(real(nstruct*(i_node+1))/real(nprocs))
              !fitdata_start_i = start_i_stored(i_node)
              !fitdata_stop_i = stop_i_stored(i_node)

              ! print *,'fitdata_stop_i=',fitdata_stop_i
              ! print *,'nstruct=',nstruct,' nprocs=',nprocs,' i_node=',i_node

              if ((i_node+1).eq.nprocs) then
                  fitdata_stop_i = nstruct
              endif
              
              ! print *,'in p_synchronizeResult, fitdata_start_i=',fitdata_start_i,' fitdata_stop_i=',fitdata_stop_i
              ! if ((i_node+1).eq.nprocs) then
              !     print *,'stopping...'
              !     stop
              ! endif
 
              ! use these to figure out where to stop/start in the array when combining results from workers
              idatapoint = 0
              
              do istr=1,fitdata_start_i-1
                  if (weightsEn(istr).ne.0d0) then
                      idatapoint = idatapoint + 1
                  endif
                  if (weightsFr(istr).gt.0d0) then
                      do iat=1,gn_forces(istr)
                         idatapoint = idatapoint + 3
                      enddo
                  endif
                  if (weightsSt(istr).gt.0d0) then
                      idatapoint = idatapoint + 6
                  endif
              end do
              
              idatapoint_start = idatapoint + 1
              
              do istr=fitdata_start_i,fitdata_stop_i
                  if (weightsEn(istr).ne.0d0) then
                      idatapoint = idatapoint + 1
                  endif
                  if (weightsFr(istr).gt.0d0) then
                      do iat=1,gn_forces(istr)
                         idatapoint = idatapoint + 3
                      enddo
                  endif
                  if (weightsSt(istr).gt.0d0) then
                      idatapoint = idatapoint + 6
                  endif
              end do
              
              idatapoint_stop = idatapoint
              
              ! print *,'receiving from ', i_node, ' start=',idatapoint_start,', stop=',idatapoint_stop
              
              call MPI_recv(recv_fitdata, ndatapoints, MPI_DOUBLE, i_node, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr)
              
              fitdata(idatapoint_start:idatapoint_stop) = recv_fitdata(idatapoint_start:idatapoint_stop)

            endif

            !All the received results are added to the result of this process
            !first the results of the individual variables
            funcforc = funcforc + recv_arr(1)
            funcen = funcen + recv_arr(2)
            funcstr = funcstr + recv_arr(3)
            funcforcdenom = funcforcdenom + recv_arr(4)
            funcendenom = funcendenom + recv_arr(5)
            funcstrdenom = funcstrdenom + recv_arr(6)
            Fen = Fen + recv_arr(7)
            Ffrc = Ffrc + recv_arr(8)
            avg_deltaE = avg_deltaE + recv_arr(9)
            Fstr = Fstr + recv_arr(10)

            if (noOpt.eqv..false.) then
               !Second the results of the 1d arrays
               dFen_dmeame0(:) = dFen_dmeame0(:) + recv_all_1darr(1:maxspecies)
               dFen_dmeamrho0(:) = dFen_dmeamrho0(:) + recv_all_1darr(maxspecies+1:2*maxspecies)
               dFen_dmeamemb3(:) = dFen_dmeamemb3(:) + recv_all_1darr(2*maxspecies+1:3*maxspecies)
               dFen_dmeamemb4(:) = dFen_dmeamemb4(:) + recv_all_1darr(3*maxspecies+1:4*maxspecies)
               dFen_denconst(:) = dFen_denconst(:) + recv_all_1darr(4*maxspecies+1:5*maxspecies)
               dFfrc_dmeame0(:) = dFfrc_dmeame0(:) + recv_all_1darr(5*maxspecies+1:6*maxspecies)
               dFfrc_dmeamrho0(:) = dFfrc_dmeamrho0(:) + recv_all_1darr(6*maxspecies+1:7*maxspecies)
               dFfrc_dmeamemb3(:) = dFfrc_dmeamemb3(:) + recv_all_1darr(7*maxspecies+1:8*maxspecies)
               dFfrc_dmeamemb4(:) = dFfrc_dmeamemb4(:) + recv_all_1darr(8*maxspecies+1:9*maxspecies)
               dFstr_dmeame0(:) = dFstr_dmeame0(:) + recv_all_1darr(9*maxspecies+1:10*maxspecies)
               dFstr_dmeamrho0(:) = dFstr_dmeamrho0(:) + recv_all_1darr(10*maxspecies+1:11*maxspecies)
               dFstr_dmeamemb3(:) = dFstr_dmeamemb3(:) + recv_all_1darr(11*maxspecies+1:12*maxspecies)
               dFstr_dmeamemb4(:) = dFstr_dmeamemb4(:) + recv_all_1darr(12*maxspecies+1:13*maxspecies)
               if (objFuncType.eq.2) then
                   dFen2_dmeame0(:) = dFen2_dmeame0(:) + recv_all_1darr(13*maxspecies+1:14*maxspecies)
                   dFen2_dmeamrho0(:) = dFen2_dmeamrho0(:) + recv_all_1darr(14*maxspecies+1:15*maxspecies)
                   dFen2_dmeamemb3(:) = dFen2_dmeamemb3(:) + recv_all_1darr(15*maxspecies+1:16*maxspecies)
                   dFen2_dmeamemb4(:) = dFen2_dmeamemb4(:) + recv_all_1darr(16*maxspecies+1:17*maxspecies)
               endif

               !Third the results of the 2d arrays
               dFen_dmeamtau(:,:) = dFen_dmeamtau(:,:) + reshape((recv_all_2darr(1:maxspecies*lmax)),(/lmax,maxspecies/))
               if (lmax.gt.0) then
                   dFfrc_dmeamtau(:,:) = dFfrc_dmeamtau(:,:) + reshape((recv_all_2darr(maxspecies*lmax+1:2*maxspecies*lmax)),(/lmax,maxspecies/))
                   dFstr_dmeamtau(:,:) = dFstr_dmeamtau(:,:) + reshape((recv_all_2darr(2*maxspecies*lmax+1:3*maxspecies*lmax)),(/lmax,maxspecies/))
                   if (objFuncType.eq.2) then
                       dFen2_dmeamtau(:,:) = dFen2_dmeamtau(:,:) + reshape((recv_all_2darr(3*maxspecies*lmax+1:4*maxspecies*lmax)),(/lmax,maxspecies/))
                   endif
               else
                   if (objFuncType.eq.2) then
                       dFen2_dmeamtau(:,:) = dFen2_dmeamtau(:,:) + reshape ((recv_all_2darr(maxspecies*lmax+1:2*maxspecies*lmax)),(/lmax,maxspecies/))
                   endif
               endif

               !Fourth the results of the 3d arrays
               dFen_draddens(:,:,:) = dFen_draddens(:,:,:) + reshape((recv_draddens_3darr(1:12*(lmax+1)*maxspecies)), (/12,lmax+1,maxspecies/))
               dFfrc_draddens(:,:,:) = dFfrc_draddens(:,:,:) + reshape((recv_draddens_3darr(12*(lmax+1)*maxspecies+1:12*(lmax+1)*maxspecies*2)), (/12,lmax+1,maxspecies/))
               dFstr_draddens(:,:,:) = dFstr_draddens(:,:,:) + reshape((recv_draddens_3darr(12*(lmax+1)*maxspecies*2+1:12*(lmax+1)*maxspecies*3)), (/12,lmax+1,maxspecies/))
               if (objFuncType.eq.2) then
                   dFen2_draddens(:,:,:) = dFen2_draddens(:,:,:) + reshape((recv_draddens_3darr(12*(lmax+1)*maxspecies*3+1:12*(lmax+1)*maxspecies*4)), (/12,lmax+1,maxspecies/))
               endif
               dFen_dpairpot(:,:,:) = dFen_dpairpot(:,:,:) + reshape((recv_dpairpot_3darr(1:32*maxspecies*maxspecies)), (/32,maxspecies,maxspecies/))
               dFfrc_dpairpot(:,:,:) = dFfrc_dpairpot(:,:,:) + reshape((recv_dpairpot_3darr(32*maxspecies*maxspecies+1:32*maxspecies*maxspecies*2)), (/32,maxspecies,maxspecies/))
               dFstr_dpairpot(:,:,:) = dFstr_dpairpot(:,:,:) + reshape((recv_dpairpot_3darr(32*maxspecies*maxspecies*2+1:32*maxspecies*maxspecies*3)), (/32,maxspecies,maxspecies/))
               if (objFuncType.eq.2) then
                   dFen2_dpairpot(:,:,:) = dFen2_dpairpot(:,:,:) + reshape((recv_dpairpot_3darr(32*maxspecies*maxspecies*3+1:32*maxspecies*maxspecies*4)), (/32,maxspecies,maxspecies/))
               endif
            endif

         endif
      enddo
      !End of the synchronization of the results
  
end subroutine p_synchronizeResults


subroutine p_finalize

  use m_mpi_info
  
  implicit none
  
  include 'mpif.h'

  !after the subroutine is finished, the buffer arrays used to synchronize the results can be deallocated
  deallocate(send_arr)
  deallocate(recv_arr)
  deallocate(send_all_1darr)
  deallocate(recv_all_1darr)
  deallocate(send_all_2darr)
  deallocate(recv_all_2darr)
  deallocate(send_draddens_3darr)
  deallocate(recv_draddens_3darr)
  deallocate(send_dpairpot_3darr)
  deallocate(recv_dpairpot_3darr)
  deallocate(recv_fitdata)

  
  ! Kill all workers
  if (procid.eq.0) then
      p_killWorkers = .true.
      call MPI_bcast(p_killWorkers, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, ierr)
  endif
  
  !MPI is finalized. The worker threads will be terminated before the main thread ends the program.
  call mpi_finalize(ierr)

  stop
end subroutine p_finalize
  
