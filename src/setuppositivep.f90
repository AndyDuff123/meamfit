subroutine setuppositivep

    !----------------------------------------------------------------------c
    !
    !     Set up positivep array, which determines which parameters in the
    !     p() array are to remain positive.
    !
    !     Called by: optimizeparameters
    !     Calls: -
    !     Returns: positivep
    !     Files read: -
    !     Files written: -
    !
    !     Andrew Duff, 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c


    use m_generalinfo
    use m_meamparameters
    use m_optimization
    use m_atomproperties

    implicit none

    integer isp,jsp,ip,k,ll,paracounter,spcCnt

    positivep=.false.

    !Atomic densities: cut-off radii should be positive
    paracounter=2*m3+lmax*m1
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                   if (meamrhodecay_negvals(ll,isp,jsp).eq.1) then
                      !Coefficients and cutoffs positive
                      do k=paracounter+1,paracounter+12
                         positivep(k)=.true.
                      enddo
                      ! No need for the following, since
                      ! rho_i(i)=rhol(0,i)*2d0/(1d0+exp(-gamma)) 
                      ! gamma=gamma+meam_t(ll,i)*(rhol(ll,i)**2) (1<ll<4)
                      ! !ALSO make meamtau's positive
                      ! do k=2*m3+1,2*m3+lm1*m1
                      !     positivep(k)=.true.
                      ! enddo
                   elseif (meamrhodecay_negvals(ll,isp,jsp).eq.2) then
                      !Only cutoffs positive
                      do k=paracounter+2,paracounter+12,2
                         positivep(k)=.true.
                      enddo
                   endif
                   paracounter=paracounter+12
                enddo
            else
               paracounter=paracounter+12*lm1
            endif

           !    if (typethi(i,j).eq.1) then
           !       !Only cutoffs positive
           !       do k=paracounter+2,paracounter+12*lm1,2
           !          positivep(k)=.true.
           !       enddo
           !    elseif (typethi(i,j).eq.2) then
           !       !Coefficients and cutoffs positive
           !       do k=paracounter+1,paracounter+12*lm1
           !          positivep(k)=.true.
           !       enddo
           !       !ALSO make meamtau's positive
           !       do k=2*m3+1,2*m3+lm1*m1
           !           positivep(k)=.true.
           !       enddo
           !    else
           !       print *,'typethi(',i,',',j,') (=',typethi(i,j),') not supported, STOPPING.'
           !       stop
           !    endif
           !endif
           !paracounter=paracounter+12*lm1
        enddo
    enddo

    !Embedding function
    !------------------
    if (embfunctype.eq.1) then
       !The rho^1/2 coefficient should always be positive (and finally
       !multiplied by a minus sign)
       positivep(2*m3+lmax*m1+12*lm1*m2+1: &
                 2*m3+lmax*m1+12*lm1*m2+m1)=.true.
    else
       print *,'embfunctype=2 not supported, stopping.'
       stop
    endif

    !Pairpotential
    !-------------
    !Cut-offs should be positive
    spcCnt=0
    do isp=1,m1
        do jsp=1,m1
            if (jsp.ge.isp) then
               if (typepairpot(isp,jsp).eq.2) then
                  do ip=2*m3+(4+lmax)*m1+12*lm1*m2+2,2*m3+(4+lmax)*m1+12*lm1*m2+32,2
                      positivep(ip+32*spcCnt)=.true.
                  enddo
               endif
            endif
            spcCnt=spcCnt+1
        enddo
    enddo

end subroutine setuppositivep
