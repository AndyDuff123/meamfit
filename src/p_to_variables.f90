subroutine p_to_variables

    !---------------------------------------------------------------c
    !
    !     Copies the p() array onto the sensibly-named MEAM 
    !     variables (cmin, etc).
    !
    !     Note: improved readibility by replacing indices i, j, etc
    !     with descriptive indices isp, l, etc. Original code
    !     kept commented out below in case of bugs.
    !
    !     Called by:     program MEAMfit,initializemeam,meamenergy
    !     Calls:         -
    !     Arguments:     -
    !     Returns:       cmin,cmax,meamtau,
    !                 meamrhodecay,meame0,meamrho0,
    !                 meamemb3,meamemb4,pairpotparameter,
    !                 rs,rc
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew I. Duff
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_filenames
    use m_meamparameters
    use m_optimization
    use m_atomproperties
    use m_generalinfo

    implicit none

    integer ip,ip2,isp,jsp,ksp,l

    isp=1
    jsp=1
    ksp=1
    do ip=1,m3
        cmin(isp,jsp,ksp)=p(ip)
        if (isp.lt.maxspecies) then
            isp=isp+1
        elseif (jsp.lt.maxspecies) then
            isp=1
            jsp=jsp+1
        elseif (ksp.lt.maxspecies) then
            isp=1
            jsp=1
            ksp=ksp+1
        endif
    enddo

    isp=1
    jsp=1
    ksp=1
    do ip=m3+1,2*m3
        cmax(isp,jsp,ksp)=p(ip)
        if (isp.lt.maxspecies) then
            isp=isp+1
        elseif (jsp.lt.maxspecies) then
            isp=1
            jsp=jsp+1
        elseif (ksp.lt.maxspecies) then
            isp=1
            jsp=1
            ksp=ksp+1
        endif
    enddo
    !      print *,'cmax=',cmax
    !      cmax(1:maxspecies,1:maxspecies,1:maxspecies)=p(1+m3:2*m3)

    isp=1
    l=1
    do ip=2*m3+1,2*m3+lmax*m1
       meamtau(l,isp)=p(ip)
       !print *,'Converting p to meamtau: meamtau(',k,',',j,')=',p(i),' (=p(',i,')'
       if (isp.lt.maxspecies) then
          isp=isp+1
       else
          isp=1
          l=l+1
       endif
    enddo
          !print *,'meamtau=',meamtau
    !      meamtau(1:lmax,1:maxspecies)=p(2*m3+1:2*m3+lm1*m1)

    ip2=1
    l=0
    isp=1
    jsp=1
    do ip=2*m3+lmax*m1+1,2*m3+lmax*m1+12*lm1*m2
        meamrhodecay(ip2,l,isp,jsp)=p(ip)
        if (ip2.lt.12) then
            ip2=ip2+1
        elseif (l.lt.lmax) then
            ip2=1
            l=l+1
        elseif (jsp.lt.maxspecies) then
            ip2=1
            l=0
            jsp=jsp+1
        elseif (isp.lt.maxspecies) then
            ip2=1
            l=0
            isp=isp+1
            jsp=1
        endif
    enddo
    !print *,'meamrhodecay=',meamrhodecay

    meame0(1:maxspecies)=p(2*m3+lmax*m1+12*lm1*m2+1: &
        2*m3+m1+lmax*m1+12*lm1*m2)
          !print *,'meame0=',meame0
    meamrho0(1:maxspecies)=p(2*m3+m1+lmax*m1+12*lm1*m2+1: &
        2*m3+2*m1+lmax*m1+12*lm1*m2)
          !print *,'meamrho0=',meamrho0
    meamemb3(1:maxspecies)=p(2*m3+2*m1+lmax*m1+12*lm1*m2+1: &
        2*m3+3*m1+lmax*m1+12*lm1*m2)
          !print *,'meamemb3=',meamemb3
    meamemb4(1:maxspecies)=p(2*m3+3*m1+lmax*m1+12*lm1*m2+1: &
        2*m3+4*m1+lmax*m1+12*lm1*m2)
          !print *,'meamemb4=',meamemb4

    ip2=1
    if (maxspecies.eq.1) then
        do ip=2*m3+(4+lmax)*m1+12*lm1*m2+1,2*m3+(4+lmax)*m1+12*lm1*m2+32
            pairpotparameter(ip2,1,1)=p(ip)
            ip2=ip2+1
        enddo
    else
        isp=1
        jsp=1
        do ip=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4
            pairpotparameter(ip2,isp,jsp)=p(ip)
            if (ip2.lt.32) then
                ip2=ip2+1
            elseif (jsp.lt.maxspecies) then
                ip2=1
                jsp=jsp+1
            elseif (isp.lt.maxspecies) then
                ip2=1
                jsp=1
                isp=isp+1
            endif
        enddo
    endif
         !print *,'pairpotparameter(1:1)=',pairpotparameter(1:32,1,1)
         !print *,'pairpotparameter(1:2)=',pairpotparameter(1:32,1,2)
    !     print *,'pairpotparameter(1:3)=',pairpotparameter(1:32,1,3)
         !print *,'pairpotparameter(2:2)=',pairpotparameter(1:32,2,2)
    !     print *,'pairpotparameter(2:3)=',pairpotparameter(1:32,2,3)
    !     print *,'pairpotparameter(3:3)=',pairpotparameter(1:32,3,3)

    !      pairpotparameter(1:32,1,1)=p(2*m3+(4+lm1)*m1+6*lm1*m2+1:
    !     +             2*m3+(4+lm1)*m1+6*lm1*m2+32)
    !      pairpotparameter(1:32,2,1)=p(2*m3+(4+lm1)*m1+6*lm1*m2+33:
    !     +             2*m3+(4+lm1)*m1+6*lm1*m2+64)
    !      pairpotparameter(1:32,1,2)=p(2*m3+(4+lm1)*m1+6*lm1*m2+65:
    !     +             2*m3+(4+lm1)*m1+6*lm1*m2+96)
    !      pairpotparameter(1:32,2,2)=p(2*m3+(4+lm1)*m1+6*lm1*m2+97:
    !     +             2*m3+(4+lm1)*m1+6*lm1*m2+128)

    isp=1
    jsp=1
    do ip=m4+1,m4+m2
        rs(isp,jsp)=p(ip)
        if (isp.lt.maxspecies) then
            isp=isp+1
        elseif (jsp.lt.maxspecies) then
            isp=1
            jsp=jsp+1
        endif
    enddo

    isp=1
    jsp=1
    do ip=m4+m2+1,m4+2*m2
        rc(isp,jsp)=p(ip)
        if (isp.lt.maxspecies) then
            isp=isp+1
        elseif (jsp.lt.maxspecies) then
            isp=1
            jsp=jsp+1
        endif
    enddo

    isp=1
    do ip=m4+2*m2+1,m4+2*m2+m1
        enconst(isp)=p(ip)
        isp=isp+1
    enddo
    !print *,'enconst=',enconst

end subroutine p_to_variables

