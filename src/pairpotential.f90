

subroutine pairpotential

    !------------------------------------------------------------------c
    !
    !     Calculates the pair-potential contribution to the total
    !     energy and its derivatives w.r.t potential parmaeters, by 
    !     repeated calls to 'radpairpot'.
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Arguments:     
    !     Returns:       
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_meamparameters
    use m_geometry
    use m_observables

    implicit none
    integer iat,isp,jj,j,jsp
    real(8) pairpot

    if (allocated(meam_paire)) deallocate(meam_paire)
    allocate( meam_paire(gn_inequivalentsites(istr)) )

    meam_paire=0d0
    summeam_paire=0d0
    dsummeam_paire_dpara=0d0 !This is to be summed over i and jj
    !In the following, dsummeam_paire_dpara is augmented directly within
    !'radpairpot', whereas meam_paire is augmented via dummy variable pairpot
    !This is because the ith, jth contribution is needed elsewhere in a separate 
    !call to radpairpot to calculate numerically the stress tensor
    !debug
    do iat=1,gn_inequivalentsites(istr)
       isp=gspecies(iat,istr)
       do jj=1,gn_neighbors(iat,istr)
           j=gneighborlist(jj,iat,istr)
           jsp=gspecies(j,istr)
           rij=diststr(jj,iat,0,0,istr)
           call radpairpot(isp,jsp,rij,pairpot,dsummeam_paire_dpara)
           meam_paire(iat)=meam_paire(iat)+pairpot
       enddo
       meam_paire(iat)=0.5d0*meam_paire(iat)
       summeam_paire=summeam_paire+meam_paire(iat)
    enddo
    dsummeam_paire_dpara=0.5d0*dsummeam_paire_dpara

    summeam_paire=summeam_paire/gn_inequivalentsites(istr)
    dsummeam_paire_dpara=dsummeam_paire_dpara/gn_inequivalentsites(istr)

    !print *,'gn_inequivalentsites(',istr,')=',gn_inequivalentsites(istr)
    !stop

end subroutine pairpotential





subroutine pairpotentialSpline

    !------------------------------------------------------------------c
    !
    !     Calculates the pair-potential contribution to the total
    !     energy and its derivatives w.r.t potential parmaeters, by 
    !     repeated calls to 'radpairpot'.
    !     This is identical to 'pairpotential', except that it performs
    !     a check for each species pair as to whether a spline has
    !     been provided for that interaction, and if so calls
    !     radpairpotSpline instead of radpairpot.
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Arguments:     
    !     Returns:       
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_meamparameters
    use m_geometry
    use m_observables

    implicit none
    integer iat,isp,jj,j,jsp
    real(8) pairpot

    if (allocated(meam_paire)) deallocate(meam_paire)
    allocate( meam_paire(gn_inequivalentsites(istr)) )

    meam_paire=0d0
    summeam_paire=0d0
    dsummeam_paire_dpara=0d0 !This is to be summed over i and jj
    !In the following, dsummeam_paire_dpara is augmented directly within
    !'radpairpot', whereas meam_paire is augmented via dummy variable pairpot
    !This is because the ith, jth contribution is needed elsewhere in a separate 
    !call to radpairpot to calculate numerically the stress tensor
    do iat=1,gn_inequivalentsites(istr)

       isp=gspecies(iat,istr)
       do jj=1,gn_neighbors(iat,istr)
           j=gneighborlist(jj,iat,istr)
           jsp=gspecies(j,istr)
           rij=diststr(jj,iat,0,0,istr)
           if (useSpline2PartFns(min(isp,jsp),max(isp,jsp)).eqv..false.) then
              call radpairpot(isp,jsp,rij,pairpot,dsummeam_paire_dpara)
           else
              rij=1.0d0 !3.52352352352352d0
              print *,'For rij=',rij
              print *,'Calling radpairpotSpline for interaction: ',min(isp,jsp),'-',max(isp,jsp)
              call radpairpot(isp,jsp,rij,pairpot,dsummeam_paire_dpara)
              print *,'analytic, pairpot=',pairpot
              call radpairpotSpline(isp,jsp,rij,pairpot,dsummeam_paire_dpara)
              print *,'numeric, pairpot=',pairpot
              stop 
           endif
           meam_paire(iat)=meam_paire(iat)+pairpot
       enddo
       meam_paire(iat)=0.5d0*meam_paire(iat)
       summeam_paire=summeam_paire+meam_paire(iat)
    enddo
    dsummeam_paire_dpara=0.5d0*dsummeam_paire_dpara

end subroutine pairpotentialSpline

