subroutine svdcmp(m,n, &
        a, &
        tmp, &
        w,v)
    ! based on numerical recipes
    ! Marcel Sluiter, May 10 2002
    !      use m_inout
    !     Copyright (c) 2018, STFC

    implicit none
    integer m,n,i,its,j,jj,k,kk,nm
    real(8) a(m,n),v(n,n),w(n),anorm,c,f,g,h,s,scale,x,y,z,tmp(n), &
        pythag
    intent(in) m,n
    intent(out) w,v,tmp
    !i      include 'incmpi.h'

    g=0d0
    scale=0d0
    anorm=0d0
    do i=1,n
        kk=i+1
        tmp(i)=scale*g
        g=0d0
        s=0d0
        scale=0d0
        if(i.le.m)then
            do k=i,m
                scale=scale+abs(a(k,i))
            enddo
            if(scale.ne.0d0)then
                do k=i,m
                    a(k,i)=a(k,i)/scale
                    s=s+a(k,i)*a(k,i)
                enddo
                f=a(i,i)
                g=-sign(sqrt(s),f)
                h=f*g-s
                a(i,i)=f-g
                do j=kk,n
                    s=0d0
                    do k=i,m
                        s=s+a(k,i)*a(k,j)
                    enddo
                    f=s/h
                    do k=i,m
                        a(k,j)=a(k,j)+f*a(k,i)
                    enddo
                enddo
                do k=i,m
                    a(k,i)=scale*a(k,i)
                enddo
            endif
        endif
        w(i)=scale *g
        g=0d0
        s=0d0
        scale=0d0
        if((i.le.m).and.(i.ne.n))then
            do k=kk,n
                scale=scale+abs(a(i,k))
            enddo
            if(scale.ne.0d0)then
                do k=kk,n
                    a(i,k)=a(i,k)/scale
                    s=s+a(i,k)*a(i,k)
                enddo
                f=a(i,kk)
                g=-sign(sqrt(s),f)
                h=f*g-s
                a(i,kk)=f-g
                do k=kk,n
                    tmp(k)=a(i,k)/h
                enddo
                do j=kk,m
                    s=0d0
                    do k=kk,n
                        s=s+a(j,k)*a(i,k)
                    enddo
                    do k=kk,n
                        a(j,k)=a(j,k)+s*tmp(k)
                    enddo
                enddo
                do k=kk,n
                    a(i,k)=scale*a(i,k)
                enddo
            endif
        endif
        anorm=max(anorm,(abs(w(i))+abs(tmp(i))))
    enddo
    do i=n,1,-1
        if(i.lt.n)then
            if(g.ne.0d0)then
                do j=kk,n
                    v(j,i)=(a(i,j)/a(i,kk))/g
                enddo
                do j=kk,n
                    s=0d0
                    do k=kk,n
                        s=s+a(i,k)*v(k,j)
                    enddo
                    do k=kk,n
                        v(k,j)=v(k,j)+s*v(k,i)
                    enddo
                enddo
            endif
            do j=kk,n
                v(i,j)=0d0
                v(j,i)=0d0
            enddo
        endif
        v(i,i)=1d0
        g=tmp(i)
        kk=i
    enddo
    do i=min(m,n),1,-1
        kk=i+1
        g=w(i)
        do j=kk,n
            a(i,j)=0d0
        enddo
        if(g.ne.0d0)then
            g=1d0/g
            do j=kk,n
                s=0d0
                do k=kk,m
                    s=s+a(k,i)*a(k,j)
                enddo
                f=(s/a(i,i))*g
                do k=i,m
                    a(k,j)=a(k,j)+f*a(k,i)
                enddo
            enddo
            do j=i,m
                a(j,i)=a(j,i)*g
            enddo
        else
            do j= i,m
                a(j,i)=0d0
            enddo
        endif
        a(i,i)=a(i,i)+1d0
    enddo
    do k=n,1,-1
        do its=1,30
            do kk=k,2,-1   !correction based on Kanno's
                nm=kk-1
                if((abs(tmp(kk))+anorm).eq.anorm)  goto 2
                if((abs(w(nm))+anorm).eq.anorm)  goto 1
            enddo
            kk=1   !correction based on Kanno's
            if((abs(tmp(kk))+anorm).eq.anorm)  goto 2   !correction
            1         c=0d0
            s=1d0
            do i=kk,k
                f=s*tmp(i)
                tmp(i)=c*tmp(i)
                if((abs(f)+anorm).eq.anorm) goto 2
                g=w(i)
                h=pythag(f,g)
                w(i)=h
                h=1d0/h
                c= (g*h)
                s=-(f*h)
                do j=1,m
                    y=a(j,nm)
                    z=a(j,i)
                    a(j,nm)=(y*c)+(z*s)
                    a(j,i)=-(y*s)+(z*c)
                enddo
            enddo
            2         z=w(k)
            if(kk.eq.k)then
                if(z.lt.0d0)then
                    w(k)=-z
                    v(:,k)=-v(:,k)
                endif
                goto 3
            endif
            !i          if(mype.eq.0.and.its.eq.30) write(error,'(a)')
            !i     &      '*WARNING* BROYDEN: no convergence in SVDCMP'
            x=w(kk)
            nm=k-1
            y=w(nm)
            g=tmp(nm)
            h=tmp(k)
            f=((y-z)*(y+z)+(g-h)*(g+h))/(2d0*h*y)
            g=pythag(f,1d0)
            f=((x-z)*(x+z)+h*((y/(f+sign(g,f)))-h))/x
            c=1d0
            s=1d0
            do j=kk,nm
                i=j+1
                g=tmp(i)
                y=w(i)
                h=s*g
                g=c*g
                z=pythag(f,h)
                tmp(j)=z
                c=f/z
                s=h/z
                f= (x*c)+(g*s)
                g=-(x*s)+(g*c)
                h=y*s
                y=y*c
                do jj=1,n
                    x=v(jj,j)
                    z=v(jj,i)
                    v(jj,j)= (x*c)+(z*s)
                    v(jj,i)=-(x*s)+(z*c)
                enddo
                z=pythag(f,h)
                w(j)=z
                if(z.ne.0d0)then
                    z=1d0/z
                    c=f*z
                    s=h*z
                endif
                f= (c*g)+(s*y)
                x=-(s*g)+(c*y)
                do jj=1,m
                    y=a(jj,j)
                    z=a(jj,i)
                    a(jj,j)= (y*c)+(z*s)
                    a(jj,i)=-(y*s)+(z*c)
                enddo
            enddo
            tmp(kk)=0d0
            tmp(k)=f
            w(k)=x
        enddo
        3       continue
    enddo
end subroutine svdcmp
