
subroutine savePotential(nfreep,popt,hrs,mins,secs)

    !----------------------------------------------------------------------c
    !
    !      Add potential paramaters to our high-score table, reordering as
    !      necessary, and save to 'potparas_best#' files (re-ordering the latter
    !      as well, as necessary)
    !
    !      Andrew Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_optimization
    use m_geometry
    use m_filenames
    use m_atomproperties
    use m_meamparameters
    use m_objectiveFunction
    use m_plotfiles
    use m_generalinfo

    implicit none

    logical largest
    logical signflipped(np)
    character*80 filename
    character*20 string1,string2,string3,string4
    integer i,ipara,j,k,ip,nfreep,hrs,mins,secs,nf
    real(8) popt(nfreep)

    !Re-order 'noptfuncstore' best optfuncs
    if (n_optfunc.gt.1) then
        largest=.true. !Keep track of whether current optfunc is larger than
                       !the stored values
        do i=1,n_optfunc-1
            if ((F.lt.bestoptfuncs(i)).and.(largest.eqv..true.)) then
                if (i.eq.1) then
                    print *,'New best optfunc !!!'
                endif
                !Move all the optfunc values greater than the new
                !value
                !up by one element, and do the same for the stored
                !MEAM
                !parameters
                do j=min(n_optfunc-1,noptfuncstore-1),i,-1
                    bestoptfuncs(j+1)=bestoptfuncs(j)
                    timeoptfuncHr(j+1)=timeoptfuncHr(j)
                    timeoptfuncMin(j+1)=timeoptfuncMin(j)
                    do k=1,np
                        p_saved(k,j+1)=p_saved(k,j)
                    enddo
                enddo
                bestoptfuncs(i)=F
                timeoptfuncHr(i)=hrs
                timeoptfuncMin(i)=mins
                !Record the potential parametets
                do k=1,np
                    p_saved(k,i)=p(k)
                enddo
                largest=.false.
            endif
        enddo
        if (largest.eqv..true.) then
            !Add the potential to the back of the stored potentials
            if (n_optfunc.le.noptfuncstore) then
                bestoptfuncs(n_optfunc)=F
                timeoptfuncHr(n_optfunc)=hrs
                timeoptfuncMin(n_optfunc)=mins
                !Record the potential parameters
                do k=1,np
                    p_saved(k,n_optfunc)=p(k)
                enddo
            endif
        endif
    else
        if (n_optfunc.le.noptfuncstore) then
            !Save optfunc as the best optfunc
            bestoptfuncs(1)=F
            timeoptfuncHr(1)=hrs
            timeoptfuncMin(1)=mins
            !Save MEAM parameters as the best MEAM parameters
            do i=1,np
                p_saved(i,1)=p(i)
            enddo
            largest=.false.
        endif
    endif
    if (n_optfunc.le.noptfuncstore) then
        n_optfunc=n_optfunc+1
    endif

    !Save best optfuncs to file 'bestoptfuncs' and write to standard
    !output
    print *
    if (n_optfunc.gt.2) then
        open(50,file='bestoptfuncs')
        if (n_optfunc.gt.noptfuncstore) then
            print *,'Top ',noptfuncstore,' optfuncs:'
            write(50,*) 'Top ',noptfuncstore,' optfuncs:'
        else
            print *,'Best optfuncs so far:'
        endif
        do i=1,n_optfunc-1
           write(*,'(A3,I3,A3,D17.10,A9,I3,A8,I2,A6)') &
               '   ',i,':  ',bestoptfuncs(i), &
               '  (time: ',timeoptfuncHr(i),' hrs, ',timeoptfuncMin(i),' mins)'
           write(50,'(A3,I3,A3,D17.10,A9,I3,A8,I2,A6)') &
               '   ',i,':  ',bestoptfuncs(i), &
               '  (time: ',timeoptfuncHr(i),' hrs, ',timeoptfuncMin(i),' mins)'
        enddo
        print *
        write(50,*)
        call displaytime(50,hrs,mins,secs)
        close(50)
    endif

    !Save best MEAM paras to files
    do i=1,n_optfunc-1
        do j=1,np
            p(j)=p_saved(j,i)
        enddo
        !Generate a filename of the form 'potparas_best1', etc
        filename="potparas_best"
        if (i.lt.10) then
            write(string1,'(I1)') i
        elseif (i.lt.100) then
            write(string1,'(I2)') i
        else
            print *,'ERROR: more than 100 files set to save; code needs'
            print *,'changing, STOPPING.'
            stop
        endif
        filename=trim(filename)//trim(string1)
        !Write potential parameters to file
        open(2,file=trim(adjustl(filename)))
        call writemeamp
        close(2) 
    enddo

    !Also save the best potential periodically to a timestamped file
    if (hrs.ge.nextHourRec) then
        print *,' Saving potential to timestamped file (',hrs,'>',nextHourRec,')'
        nextHourRec=nextHourRec+outputVsTime
        do j=1,np
            p(j)=p_saved(j,1)
        enddo
        filename="potparas_best_time"
        if (hrs.lt.10) then
            write(string1,'(I1)') hrs
        elseif (hrs.lt.100) then
            write(string1,'(I2)') hrs
        elseif (hrs.lt.1000) then
            write(string1,'(I3)') hrs
        elseif (hrs.lt.10000) then
            write(string1,'(I4)') hrs
        else
            print *,'ERROR: timestamp>9999 hours; code needs changing, STOPPING.'
            stop
        endif
        filename=trim(filename)//trim(string1)
        !Write potential parameters to file
        open(2,file=trim(adjustl(filename)))
        call writemeamp
        close(2) 

        !Also record the bestoptfunc to the bestoptfuncsVsTime file
        write(64,*) hrs,bestoptfuncs(1)
    endif

    if (lammpsDuringOpt.eqv..true.) then

        do i=1,n_optfunc-1

            !Save fit data and true data for best MEAM paras to files. This has been commented out because: i) it is slow; ii) it does not work in parallel
            !because workers nodes currently only send back objective function contributions, not energies, forces & stresses. (note we have not commented out the parts
            !necessary for the potential saving)

            !Copy saved configurations for ith optfunc to popt
            ip=0
            do j=1,np
                if ((freep(j).eq.1).or.(freep(j).eq.2)) then
                    ip=ip+1
                    if (p_orig(j).ne.0d0) then
                        popt(ip)=p_saved(j,i)*(poptStrt/p_orig(j))
                    else
                        popt(ip)=p(j)
                    endif
                endif
            enddo

            if (np.ge.1) then
               ip=0
               do ipara=1,np
                   if ( (freep(ipara).eq.1).or.(freep(ipara).eq.2) ) then
                       ip=ip+1
                       if (p_orig(ipara).ne.0d0) then
                           p(ipara)=popt(ip)*(p_orig(ipara)/poptStrt)
                       else
                           p(ipara)=popt(ip)
                       endif
                   endif
               enddo
            endif

            call parachange1(signflipped,nf) !Ensure meam parameters have
                                                    !the correct signs and are in the
                                                    !correct range
            call p_to_variables !Convert variables in p() to 'sensibly-named' variables

            !  !Generate a filename of the form 'potparas_best1', etc
            !  filename="datapnts_best"
            !  if (i.lt.10) then
            !      write(string1,'(I1)') i
            !  elseif (i.lt.100) then
            !      write(string1,'(I2)') i
            !  else
            !      print *,'ERROR: more than 100 files set to save; code needs'
            !      print *,'changing, STOPPING.'
            !      stop
            !  endif
            !  filename=trim(filename)//trim(string1)
            !  open(2,file=trim(adjustl(filename)))

            !  !Calculate and then write the fitted datapoints to the above
            !  !file
            !  call objectiveFunction(.true.)    <-- I'm fairly certain this isn't needed to be called for the potential plotting bit below...
            !  call writemeamdatapoints(2,.false.)
            !  close(2)

           !Plot potential functions

           if (lmax.eq.0) then
              !Setup name of LAMMPS output file. These potentials will work with
              !standard LAMMPS, but only for EAM. See below for LAMMPS MEAM
              !potentials.
              filename=""
              do j=1,maxspecies
                  write(string1,'(A2)') element(speciestoZ(j))
                  filename=trim(filename)//trim(string1)
              enddo
              string2='.eam.alloy_'
              if (i.lt.10) then
                  write(string3,'(I1)') i
              elseif (i.lt.100) then
                  write(string3,'(I2)') i
              else
                  print *,'ERROR: more than 100 files set to save; code needs'
                  print *,'changing, STOPPING.'
                  stop
              endif
              filename=trim(filename)//trim(string2)//trim(string3)
              open(58,file=trim(adjustl(filename)))
              if (dlpolyOut) then
                 open(70,file='TABEAM')
                 open(71,file='FIELD')
              endif
           endif

           !Plot the potential and write LAMMPS output
           call plotfunctions(.false.)

           if (lmax.eq.0) then
              close(58)
              if (dlpolyOut) then
                 close(70)
                 close(71)
              endif
           endif

           if (lmax.eq.3) then
              !LAMMPS MEAM potentials for use with Prashanth's altered LAMMPS
              filename=""
              do j=1,maxspecies
                  write(string1,'(A2)') element(speciestoZ(j))
                  filename=trim(filename)//trim(string1)
              enddo
              string2='library.rfmeam.'
              string4='_'
              if (i.lt.10) then
                  write(string3,'(I1)') i
              elseif (i.lt.100) then
                  write(string3,'(I2)') i
              endif
              filename=trim(string2)//trim(filename)//trim(string4)//trim(string3)
              open(2,file=trim(adjustl(filename)))
              call writemeamplammps
              close(2)

           endif

        enddo

    endif


    !Check if optimization termination conditions have been met
    if (n_optfunc.gt.2) then
        if ( (n_optfunc.gt.noptfuncstore).and.(bestoptfuncs(noptfuncstore)- &
              bestoptfuncs(1)).lt.optAcc  ) then
           print *,'Best and worst optimization functions within ',optAcc
           optcomplete=.true.
           return
        endif
     endif
     if ( hrs.ge.stopTime ) then
        print *,'Number of hours elapsed: ',hrs,' greater than STOPTIME=',stopTime
        optcomplete=.true.
        return
     endif

end subroutine savePotential
