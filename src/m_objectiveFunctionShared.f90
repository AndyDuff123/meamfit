
module m_objectiveFunctionShared

  ! MPI: Moved here from objectiveFunction itself so it can be accessed
  ! in the parallel routines as well
  integer nRef, idatapoint, ref_datapoint(9),ref_strucnum(9)
  real(8) funcforcdenom,funcendenom,funcstrdenom
  real(8) avg_deltaE ! T.M

end module m_objectiveFunctionShared
