
subroutine getsettingspara(VarName,nArg,VarValue,VarValueMultiple,nMultiple,foundvariable,lineNum)

    !-------------------------------------------------------------------c
    !
    !     Read in the settings parameter/s specified by 'VarName'. If
    !     found, return foundvariable=.true.
    !
    !     Called by:     readsettings
    !     Calls:         optionalarg
    !     Returns:       VarValue, foundvariable, lineNum
    !     Files read:    -
    !     Files written: -
    !
    !     inputs: nArg = 1 if one parameter is expected for this variable
    !                    -1 if multiple parameters can be returned. In this
    !               case VarValueMultiple contains the additional parameters
    !               (with the first parameter held in VarValue). nMultiple
    !               returns as the number of additional parameters (I.e.,
    !               the total number of parameters minus 1)
    !
    !     dev notes: lineNum was meant to record the line number where
    !     the variable is defined, for purposes of checking for
    !     badly defined settings input. Not used yet.
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_filenames

    implicit none

    logical typelog,foundvariable,equals
    integer i,typeint,IOstatus,nwords,VarNamePlusEqLength,StringLength,wordLength,nArg,iWord,nMultiple,lineNum
    real(8) typedble

    character*80 string
    character*80 VarName,word,VarValue,VarNamePlusEq,VarValueMultiple(10)
    character*80, allocatable:: words(:)
    character*1 tmpstring

    open(unit=1,file=trim(settingsfile))

    foundvariable=.false.
    VarNamePlusEq=trim(VarName)//"="
    do
       read(1,'(a80)',IOSTAT=IOstatus) string

       if (IOstatus.lt.0) then
          exit
       endif

       call optionalarg(string,nwords)
       if (nwords.gt.0) then

        allocate(words(1:nwords))
        read(string,*) words

        !First check that line is not commented:
        if (words(1).ne.'#') then

          if (nwords.ge.1) then
             !Check if first word has an equals sign in it (eg,
             !VERBOSE=TRUE)
             word=words(1)
             wordLength=len_trim(word)
             equals=.false.
             do i=1,wordLength
               if (word(i:i).eq."=") then
                  equals=.true.
                  !print *,'equals in first word.'
                  exit
               endif
             enddo
             if (equals) then
               !print *,'checking if ',trim(word(1:i-1)), &
               !'equals',trim(VarName)
               if (trim(word(1:i-1)).eq.trim(VarName)) then
                 !print *,'i=',i,' wordLength=',wordLength
                 if (i.lt.wordLength) then
                   !Value of variable found in first word. Extract it:
                   !print *,'..found it within first word..'
                   VarValue=word(i+1:wordLength)
                   if (nArg.eq.-1) then
                      if (nwords.gt.10+1) then
                         print *,'ERROR: when reading tag ',trim(VarName),' from settings file, more than 10 arguments, STOPPING.'
                         stop
                      endif
                      do iWord=2,nwords
                         VarValueMultiple(iWord-1)=words(iWord)
                      enddo
                      nMultiple=nwords-1
                   else
!                  Removed the following: otherwise we can't add comments!
!                     if (nwords>1) then
!                        print *,'ERROR: when reading tag ',trim(VarName),' expecting only one argument, STOPPING.'
!                        stop
!                     endif
                   endif
                   foundvariable=.true.
                   exit
                 else
                   if (nwords.ge.2) then
                     !Value of variable found in second word. Extract it:
                     !print *,'..found it in second word..'
                     VarValue=words(2)
                     if (nArg.eq.-1) then
                        if (nwords.gt.10+2) then
                           print *,'ERROR: when reading tag ',trim(VarName),' from settings file, more than 10 arguments, STOPPING.'
                           stop
                        endif
                        do iWord=3,nwords
                           VarValueMultiple(iWord-2)=words(iWord)
                        enddo
                        nMultiple=nwords-2
                     else
!                       if (nwords>2) then
!                          print *,'ERROR: when reading tag ',trim(VarName),' expecting only one argument, STOPPING.'
!                          stop
!                       endif
                     endif
                     foundvariable=.true.
                     exit
                   endif
                 endif
               endif
             else
               !print *,'checking if ',trim(words(1)), &
               !'equals',trim(VarName)
               if (trim(words(1)).eq.trim(VarName)) then
                 if (nwords.ge.2) then
                   if (trim(words(2)).eq."=") then
                     if (nwords.ge.3) then
                       !Value of variable found in third word. Extract it:
                       !print *,'..found it in third word..'
                       VarValue=words(3)
                       if (nArg.eq.-1) then
                         if (nwords.gt.10+3) then
                            print *,'ERROR: when reading tag ',trim(VarName),' from settings file, more than 10 arguments, STOPPING.'
                            stop
                         endif
                         do iWord=4,nwords
                            VarValueMultiple(iWord-3)=words(iWord)
                         enddo
                         nMultiple=nwords-3
                       else
!                        if (nwords>2) then
!                           print *,'ERROR: when reading tag ',trim(VarName),' expecting only one argument, STOPPING.'
!                           stop
!                        endif
                       endif
                       foundvariable=.true.
                       exit
                     endif
                   else
                     word=words(2)
                     wordLength=len_trim(word)
                     do i=1,wordLength
                       if (word(i:i).eq."=") then
                         equals=.true.
                         !print *,'equals in second word.'
                         exit
                       endif
                     enddo
                     if (equals) then
                       !Value of variable found in second word. Extract it:
                       !print *,'..found it in second word..'
                       VarValue=word(i+1:wordLength)
                       if (nArg.eq.-1) then
                         if (nwords.gt.10+2) then
                            print *,'ERROR: when reading tag ',trim(VarName),' from settings file, more than 10 arguments, STOPPING.'
                            stop
                         endif
                         do iWord=3,nwords
                            VarValueMultiple(iWord-2)=words(iWord)
                         enddo
                         nMultiple=nwords-2
                       else
!                        if (nwords>2) then
!                           print *,'ERROR: when reading tag ',trim(VarName),' expecting only one argument, STOPPING.'
!                           stop
!                        endif
                       endif
                       foundvariable=.true.
                       exit
                     endif
                   endif
                 endif
               endif
             endif
          endif

        endif
        deallocate(words)

       endif
    enddo
    !print *,'VarValue=',VarValue
    close(1)

end subroutine getsettingspara
