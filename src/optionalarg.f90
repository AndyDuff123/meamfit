subroutine optionalarg(line,nargs)

    !--------------------------------------------------------------c
    !
    !     Parses the string 'line', searches for number of strings
    !     separated by spaces or commas.
    !
    !     Andrew Ian Duff (Mar 25 2013): words within '' are only
    !     counted as one. This was important, because when the words
    !     of line are read into individual words with the
    !     read(line,*) words(1:nargs) command in the
    !     readtargetenergy subroutine, words within
    !     apostrophes are counted as a single word, irrespective
    !     of any spaces contained within. This resulted in the above
    !     command trying to read in a non-existent word from
    !     line into the word array and thus crashing
    !
    !     Called by:     readtargetstruc,readtargetenergy,
    !                 readtargetforces
    !     Calls:         -
    !     Arguments:     line
    !     Returns:       nargs
    !     Files read:    -
    !     Files written: -
    !
    !     Marcel Sluiter, Dec 28 2005,
    !     Edited by Andrew Ian Duff, Mar 25 2013.
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    implicit none

    integer nargs,length,i
    character line*(*)
    logical newarg,donotcount

    intent(in) line
    intent(out) nargs

    !Analyze LINE
    length=len_trim(line)
    !Count number of sections in LINE that are separated by commas or
    !spaces
    nargs=0
    newarg=.true.
    donotcount=.false. !this flag gets switched on when ' is found.
    !No 'sections' are counted until another ' is
    !found.
    do i=1,length
        if (line(i:i).eq.CHAR(39)) then
            if (donotcount.eqv..false.) then
                donotcount=.true.
            else
                donotcount=.false.
            endif
        endif
        if (donotcount.eqv..false.) then
            if ((line(i:i).eq.',').or.(line(i:i).eq.' ').or. &
                (line(i:i).eq.CHAR(9))) then
                newarg=.true.
            else
                if(newarg) nargs=nargs+1
                newarg=.false.
            endif
        endif
    enddo

end subroutine optionalarg
