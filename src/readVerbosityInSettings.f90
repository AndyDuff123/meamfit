subroutine readVerbosityInSettings
 
    !-------------------------------------------------------------------c
    !
    !     Check the verbosity tag in the settings file.
    !
    !     Andrew Duff 2021
    !
    !-------------------------------------------------------------------c

    use m_filenames
    use m_datapoints
    use m_atomproperties
    use m_generalinfo
    use m_meamparameters
    use m_geometry
    use m_optimization
    use m_plotfiles
    use m_objectiveFunction
    use m_electrondensity

    implicit none

    logical onebyone,exist,foundvariable,opt_meamtau,startWordFound
    integer i,iLmp,j,k,nPara,ncutoffs,istart,&
            ipara,paraCnt,l,nembterms,ncutoffDens_overall,ncutoffPairpot_overall,nMultiple,lineNum,nVar
    character*1 string2,string3
    character*5 writecheck
    character*80 searchfor,found
    character*80 foundMultiple(10)
    character*80 string

    if (settingsfileexist.eqv..true.) then
       open(unit=1,file=trim(settingsfile))
       !Check that verbosity hasn't been set to 1 already (no/little standard output) through a command line argument
       if (verbosity.eq.2) then
          searchfor="VERBOSITY" !Extra information written to screen
          call getsettingspara(searchfor,1,found,foundMultiple,nMultiple,foundvariable,lineNum)
          if (foundvariable) then
             nVar=nVar+1
             read(found,*) verbosity
             if (verbosity.gt.1) write(*,'(A11,I1,A21)') ' verbosity=',verbosity,' (from settings file)'
          else
             verbosity=2
             if (verbosity.gt.1) write(*,'(A48,I1)') 'No VERBOSITY in settings file, using VERBOSITY=',verbosity
          endif
       endif

        close(1)
    endif

end subroutine readVerbosityInSettings
