


module m_meamparameters

    !     Copyright (c) 2018, STFC

    !MEAM parameters
    logical cmin_cmax_zero,lookuptables,pairpotonly,envdepmeamt
    integer lmax!,nCutoffOverride
    real(8), allocatable:: cmin(:,:,:), & !species 3x  [,]
        cmax(:,:,:),         & !species 3x  [,]
        meamtau(:,:),        & !species,L [,]
        meamrhodecay(:,:,:,:),& !12,L,species,species
        meame0(:),           & !species  [,]
        meamrho0(:),         & !species  [,]
        meamemb3(:),           & !species  [,]
        meamemb4(:),         & !species  [,]
        rs(:,:),             & !species 2x
        rc(:,:),             & !species 2x
        !temporary values &
        meam_f(:),           & !site
        meam_t(:,:),         & !L,site
        meam_paire(:),       & !site
        enconst(:)           !species

    !Analytic derivatives of embedding function (meamf) w.r.t potential
    !parameters
    real(8), allocatable:: dmeamt_dmeamtau(:,:,:), & !L,isp,iat
        dmeamt_dpara(:,:,:,:), &   !ip,L,isp,iat
        dmeamt_dxyz(:,:,:,:), &    !3, nn, l, site i
        d2meamt_dxyz_dmeamtau(:,:,:,:,:), & !3, nn, l, isp, site
        d2meamt_dxyz_dpara(:,:,:,:,:,:), & !3, nn, l, 12, isp, site
        d2meamf_dxyz_dmeame0(:,:,:), & !3, nn, site
        d2meamf_dxyz_dmeamrho0(:,:,:), & !3, nn, site
        d2meamf_dxyz_dmeamemb3(:,:,:), & !3, nn, site
        d2meamf_dxyz_dmeamemb4(:,:,:), & !3, nn, site
        dmeamf_dxyz(:,:,:), &      !3,site,site
        dmeamf_dpara(:,:,:,:), &   !ip,L,jsp,iat
        dmeamf_dmeamtau(:,:,:), &  !L,jsp,iat
        dmeamf_dmeame0(:),dmeamf_dmeamrho0(:),dmeamf_dmeamemb3(:), &
        dmeamf_dmeamemb4(:), &     !iat
        d2meamf_dxyz_dmeamtau(:,:,:,:,:), d2meamf_dxyz_dpara(:,:,:,:,:,:), &
        dmeam_paire_dxyz(:,:,:), & !3, iat, iat
        d2meam_paire_dxyz_dpara(:,:,:,:,:,:) !3, ip, isp, jsp, jj, iat
    !Pair potential parameters
    integer, parameter:: pot_samespecies_arrsize=1000 !For internal
    !Fe-Fe Ackland potential 1000
    integer emb_Fe_arrsz,emb_C_arrsz !For external look-up tables
    real(8), allocatable:: pairpotparameter(:,:,:) !ispecies,jspecies
    real(8), allocatable:: pot_samespecies(:),sec_der(:) !For internal
    ! Fe-Fe Ackland potential
    real(8) r_array_samespeciesC(5), &
        pot_samespeciesC(5),sec_der_samespeciesC(5), &
        r_array_thiFeC(4),thiFeC(4),sec_der_thiFeC(4)

    real(8), allocatable:: &
        secder_smspc1(:), &
        secder_smspc2(:),r_potdifspc(:),secder_difspc(:), &
        rho_embFe(:),embFe(:),secder_embFe(:), &
        rho_embC(:),embC(:),secder_embC(:) !For external look-up tables

    logical thiaccptindepndt
    integer embfunctype
    integer, allocatable:: typethi(:,:),typepairpot(:,:)
    !Variables for splines:
    integer narrpairpotXX,narrthiXX
    real(8), allocatable:: r_pairpotXXarr(:),pairpotXXarr(:), &
        secderpairpotXX(:),r_thiXXarr(:),thiXXarr(:), &
        secderthiXX(:)
    integer, parameter:: nRhoMaxSpline=20000, nRMaxSpline=20000
    integer, allocatable:: NrhoLmps(:), NrLmps(:,:) !isp
    real(8), allocatable:: embfuncSpline(:,:), embfuncSplineSecDer(:,:), & !narr, isp
        densSpline(:,:), densSplineSecDer(:,:), & !narr, isp
        pairpotSpline(:,:,:), pairpotSplineSecDer(:,:,:), & !narr, isp, jsp
        drhoLmps(:), & !isp
        drLmps(:,:),rmaxLmps(:,:) ! isp, jsp
    logical, allocatable :: useSpline1PartFns(:), & !isp
        useSpline2PartFns(:,:) !isp, jsp
    logical useSplines
end module m_meamparameters
