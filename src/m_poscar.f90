

module m_poscar

    !     Copyright (c) 2018, STFC

    !Data from the poscar file
    integer nspecies,natoms,norigatoms,natoms_old
    integer, allocatable:: z(:),zz(:)!,nat(:)
    real(8) vol,lattPara,volumeInitial ! Latter are just used by PWscf (QE). It is read in in first iteration, and may be necessary to use it in subsequent iterations to rescale the positions.
    real(8) tr(3,3),trinv(3,3)
    real(8), allocatable:: coordinates(:,:),coords_old(:,:)
end module m_poscar
