
subroutine radpairpotForce(species1,species2,distance, &
                distComp,pairpot,dpairpot_dpara, &
                dpairpot_dxyz,d2pairpot_dxyz_dpara)

    !------------------------------------------------------------------c
    !
    !     Calculates pair-potential between two atoms, species1 and
    !     species2, at a separation 'distance', with x, y and z
    !     components stored in 'distComp'. Also calculates the
    !     derivative w.r.t coordinates of atom 2 (distance is sqrt(
    !     (x(2)-x(1))^2 + ... )
    !
    !     Called by:     meamforce
    !     Calls:         -
    !     Arguments:     species1,species2,distance,pairpotparameter
    !     Returns:       pairpot
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_atomproperties
    use m_meamparameters
    use m_filenames
    use m_generalinfo
    use m_optimization

    implicit none

    integer species1,species2,i,index,isp,jsp,speciesSml,speciesLrg,iiCoeff,iiCutoff,alph
    real(8) distance,pairpot,tmp,rsBier,x,eps,B0,B1,B2,B3,k1,k2,k3,k4,depsdr, &
            rdiffcb,rBiersq,rBiercb,cutoffMinsq,cutoffMincb,dpairpotFact, &
            distComp(1:3),distCompOver_rij(1:3), &
            dk3_dpara(32),dk4_dpara(32)

    real(8), parameter :: rBier=0.9d0
    real(8), parameter :: twothrds=0.666666666666667d0
    real(8), parameter :: e2ov4pieps0=14.3977847299112d0 !e^2 / 4 pi eps_0, in eV/Angstrom
    ! ( = (1.602*10^-19)^2 / (4*3.142*8.854*10^-12) * (6.241 * 10^18) * 10^10
    !                                             1 J = 6.241*10^18 eV   1 m = 10^10 Angstron
    real(8) dpairpot_dpara(32,maxspecies,maxspecies), dpairpot_dxyz(3), d2pairpot_dxyz_dpara(3,32,maxspecies,maxspecies)
    real(8) dB0_dpara,dB1_dpara,dB2_dpara,dB3_dpara,deps_dx,distOverRs,deps_ddistOverRs
    intent(in) species1,species2!distance
    !print *,'species1=',species1,', species2=',species2
    !print *,'Z1=',speciestoZ(species1),' Z2=',speciestoZ(species2)

    dpairpot_dxyz=0d0
    d2pairpot_dxyz_dpara=0d0
 
    if (distance.lt.rBier) then

      rsBier = 0.88534d0 * 0.52917721092d0 / ( ( speciestoZ(species1) )**0.23d0 &
                         + ( speciestoZ(species2) )**0.23d0  )
      distOverRs = distance/rsBier
      eps = 0.18175d0*exp(-3.19980d0*distOverRs)  + 0.50986d0*exp(-0.94229d0*distOverRs) + &
            0.28022d0*exp(-0.40290d0*distOverRs)  + 0.02817d0*exp(-0.20162d0*distOverRs)
      pairpot = e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * eps / distance

      !print *,'distance < rBier   -- need to update derivs in radpairpotForce'
      !stop

      !--- Added 07/01/2022 ----
      deps_ddistOverRs = -0.58156365d0*exp(-3.19980d0*distOverRs) - 0.4804359794d0*exp(-0.94229d0*distOverRs) - &
            0.112900638*exp(-0.40290d0*distOverRs) -0.0056796354*exp(-0.20162d0*distOverRs)
      do alph=1,3
         distCompOver_rij(alph)=distComp(alph)/distance ! this is d rij/d{x,y,z}
         !Note: dpairpot_dxyz w.r.t j atom coords:  
         dpairpot_dxyz(alph) = - e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * eps * distCompOver_rij(alph) / (distance**2) &
                  + e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) / distance * deps_ddistOverRs * distCompOver_rij(alph) / rsBier
         ! need to double check this...
      enddo

      !Calculate derivative of pair potential w.r.t parameters
      !Coefficients:
      speciesSml=min(species1,species2)
      speciesLrg=max(species1,species2)
      do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
         i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
         dpairpot_dpara(i,speciesSml,speciesLrg) = 0d0
      enddo
      !Cutoffs:
      do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
         i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
         dpairpot_dpara(i,speciesSml,speciesLrg) = 0d0
      enddo
      
      !Calculate derivative of dpairpot_dxyz w.r.t parameters
      !Coefficients:
      do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
         i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
         do alph=1,3
            d2pairpot_dxyz_dpara(alph,i,speciesSml,speciesLrg) = 0d0
         enddo
      enddo
      !Cutoffs:
      do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
         i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
         do alph=1,3
            d2pairpot_dxyz_dpara(alph,i,speciesSml,speciesLrg) = 0d0 
         enddo
      enddo

    elseif (distance.lt.cutoffMin) then

      !Here we interpolate between the lower radius Biersack result and the
      !larger radius sum over cubic terms. We must first evaluate the pairpotential 
      !and its derivative at the lower and upper radial values.

      !Evaluate k1: value of pairpotential at R=rBier
      rsBier = 0.88534d0 * 0.52917721092d0 / ( ( speciestoZ(species1))**0.23d0 &
                         + ( speciestoZ(species2) )**0.23d0  )
      x = rBier/rsBier
      eps = 0.18175d0*exp(-3.19980d0*x)  + 0.50986d0*exp(-0.94229d0*x) + &
            0.28022d0*exp(-0.40290d0*x)  + 0.02817d0*exp(-0.20162d0*x)
      k1 = e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * eps / rBier

      !Evaluate k2: value of d(pairpotential)/dr at R=rBier
      depsdr = (1d0/rsBier)*( 0.18175d0*(-3.19980d0)*exp(-3.19980d0*x) + 0.50986d0*(-0.94229d0)*exp(-0.94229d0*x) + &
            0.28022d0*(-0.40290d0)*exp(-0.40290d0*x) + 0.02817d0*(-0.20162d0)*exp(-0.20162d0*x) )

      k2 = - e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * eps / rBier**2 + &
           e2ov4pieps0 * speciestoZ(species1) * speciestoZ(species2) * depsdr / rBier

      !Evaluate k3: value of pairpotential at R=cutoffMin
      k3 = 0d0
      if (lookuptables.eqv..true.) then
         print *,'Need to update code in pairpotential subroutine, stopping.'
         stop
      endif

      speciesSml=min(species1,species2)
      speciesLrg=max(species1,species2)
      if (typepairpot(speciesSml,speciesLrg).eq.2) then
         do i=1,31,2
            if (cutoffMin.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
                k3=k3 + pairpotparameter(i,speciesSml,speciesLrg)  * &
                    ((pairpotparameter(i+1,speciesSml,speciesLrg)   - cutoffMin) **3)
            endif
         enddo
      else
         print *,'typepairpot(',speciesSml,',',speciesLrg,')=',typepairpot(speciesSml,speciesLrg), &
                 'not implemented, stopping.'
         stop
      endif

      !Calculate derivative of k3 w.r.t parameters
      dk3_dpara = 0d0
      !Coefficients:
      do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
         i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
         if (cutoffMin.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
            dk3_dpara(i) = dk3_dpara(i) + &
                    ((pairpotparameter(i+1,speciesSml,speciesLrg)   - cutoffMin) **3)
         endif
      enddo
      !Cutoffs:
      do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
         i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
         if (cutoffMin.le.pairpotparameter(i,speciesSml,speciesLrg)) then
            dk3_dpara(i) = dk3_dpara(i) + &
                   3d0 * pairpotparameter(i-1,speciesSml,speciesLrg) * &
                   ((pairpotparameter(i,speciesSml,speciesLrg)   - cutoffMin) **2)
         endif
      enddo

      !Evaluate k4: value of d(pairpotential)/dr at R=cutoffMin
      k4 = 0d0
      do i=1,31,2
         if (cutoffMin.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
            k4=k4 - 3d0*pairpotparameter(i,speciesSml,speciesLrg) * &
                ((pairpotparameter(i+1,speciesSml,speciesLrg)-cutoffMin)**2)
         endif
      enddo

      !Calculate derivative of k4 w.r.t parameters
      dk4_dpara = 0d0
      !Coefficients:
      do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
         i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
         if (cutoffMin.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
            dk4_dpara(i) = dk4_dpara(i) -3d0* &
                    ((pairpotparameter(i+1,speciesSml,speciesLrg)   - cutoffMin) **2)
         endif
      enddo
      !Cutoffs:
      do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
         i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
         if (cutoffMin.le.pairpotparameter(i,speciesSml,speciesLrg)) then
            dk4_dpara(i) = dk4_dpara(i) - &
                   6d0 * pairpotparameter(i-1,speciesSml,speciesLrg) * &
                   (pairpotparameter(i,speciesSml,speciesLrg)   - cutoffMin)
         endif
      enddo

      !Now calculate B0-B3 from the values k1-k4
      !...but first, preliminary quantities:      
      rdiffcb=(rBier-cutoffMin)**3
      rBiersq=rBier**2
      rBiercb=rBier**3
      cutoffMinsq=cutoffMin**2
      cutoffMincb=cutoffMin**3
     !print *,'rBier=',rBier
     !print *,'rdiffcb=',rdiffcb,', rBiersq=',rBiersq
     !print *,'rBiercb=',rBiercb,', cutoffMin=',cutoffMin
     !print *,'cutoffMinqs=',cutoffMinsq,', cutoffMincb=',cutoffMincb
      B0=(-1d0/rdiffcb) * ( -k3*rBiercb + &
            3d0*k3*rBiersq*cutoffMin + &
            k4*rBiercb*cutoffMin - &
            3d0*k1*rBier*cutoffMinsq + &
            k2*rBiersq*cutoffMinsq - &
            k4*rBiersq*cutoffMinsq + &
            k1*cutoffMincb - k2*rBier*cutoffMincb )
      B1=(-1d0/rdiffcb) * ( -k4*rBiercb + &
            6d0*k1*rBier*cutoffMin - &
            6d0*k3*rBier*cutoffMin - &
            2d0*k2*rBiersq*cutoffMin - &
            k4*rBiersq*cutoffMin + &
            k2*rBier*cutoffMinsq + 2d0*k4*rBier*cutoffMinsq + &
            k2*cutoffMincb )
      B2=(-1d0/rdiffcb) * ( -3d0*k1*rBier + 3d0*k3*rBier + &
            k2*rBiersq + 2d0*k4*rBiersq - &
            3d0*k1*cutoffMin + 3d0*k3*cutoffMin + &
            k2*rBier*cutoffMin - k4*rBier*cutoffMin - &
            2d0*k2*cutoffMinsq - k4*cutoffMinsq )
      B3=-(2d0*k1-2d0*k3-k2*rBier-k4*rBier+k2*cutoffMin+k4*cutoffMin) / &
          rdiffcb
      pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)

      !Calculate deriviates of pairpot w.r.t cartesian coordinates
      do alph=1,3
         distCompOver_rij(alph)=distComp(alph)/distance
         !Note: dpairpot_dxyz w.r.t j atom coords:
         dpairpot_dxyz(alph)=dpairpot_dxyz(alph) + &
              B1*distCompOver_rij(alph) + 2d0*B2*distance*distCompOver_rij(alph) + &
              3d0*B3*(distance**2)*distCompOver_rij(alph)
      enddo

      !Calculate derivative of B0, ..., B3 w.r.t parameters, and from these
      !the derivatives of dpairpot_dxyz
      !Coefficients:
      do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
         i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
         dB0_dpara = (-1d0/rdiffcb) * ( -dk3_dpara(i)*rBiercb + &
            3d0*dk3_dpara(i)*rBiersq*cutoffMin + &
            dk4_dpara(i)*rBiercb*cutoffMin - &
            dk4_dpara(i)*rBiersq*cutoffMinsq )
         dB1_dpara=(-1d0/rdiffcb) * ( -dk4_dpara(i)*rBiercb - &
            6d0*dk3_dpara(i)*rBier*cutoffMin - &
            dk4_dpara(i)*rBiersq*cutoffMin + &
            2d0*dk4_dpara(i)*rBier*cutoffMinsq )
         dB2_dpara=(-1d0/rdiffcb) * ( 3d0*dk3_dpara(i)*rBier + &
            2d0*dk4_dpara(i)*rBiersq + &
            3d0*dk3_dpara(i)*cutoffMin - &
            dk4_dpara(i)*rBier*cutoffMin - &
            dk4_dpara(i)*cutoffMinsq )
         dB3_dpara=-(-2d0*dk3_dpara(i)-dk4_dpara(i)*rBier+ &
            dk4_dpara(i)*cutoffMin) / rdiffcb
         dpairpot_dpara(i,speciesSml,speciesLrg) = dpairpot_dpara(i,speciesSml,speciesLrg) + &
                    dB0_dpara + dB1_dpara*distance + &
                    dB2_dpara*(distance**2) + dB3_dpara*(distance**3)
         do alph=1,3
            d2pairpot_dxyz_dpara(alph,i,speciesSml,speciesLrg) = &
                  dB1_dpara*distCompOver_rij(alph) + 2d0*dB2_dpara*distance*distCompOver_rij(alph) + &
                  3d0*dB3_dpara*(distance**2)*distCompOver_rij(alph)
         enddo
      enddo
      !Cutoffs:
      do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
         i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
         dB0_dpara = (-1d0/rdiffcb) * ( -dk3_dpara(i)*rBiercb + &
            3d0*dk3_dpara(i)*rBiersq*cutoffMin + &
            dk4_dpara(i)*rBiercb*cutoffMin - &
            dk4_dpara(i)*rBiersq*cutoffMinsq )
         dB1_dpara=(-1d0/rdiffcb) * ( -dk4_dpara(i)*rBiercb - &
            6d0*dk3_dpara(i)*rBier*cutoffMin - &
            dk4_dpara(i)*rBiersq*cutoffMin + &
            2d0*dk4_dpara(i)*rBier*cutoffMinsq )
         dB2_dpara=(-1d0/rdiffcb) * ( 3d0*dk3_dpara(i)*rBier + &
            2d0*dk4_dpara(i)*rBiersq + &
            3d0*dk3_dpara(i)*cutoffMin - &
            dk4_dpara(i)*rBier*cutoffMin - &
            dk4_dpara(i)*cutoffMinsq )
         dB3_dpara=-(-2d0*dk3_dpara(i)-dk4_dpara(i)*rBier+ &
            dk4_dpara(i)*cutoffMin) / rdiffcb
         dpairpot_dpara(i,speciesSml,speciesLrg) = dpairpot_dpara(i,speciesSml,speciesLrg) + &
                    dB0_dpara + dB1_dpara*distance + &
                    dB2_dpara*(distance**2) + dB3_dpara*(distance**3)
         do alph=1,3
            d2pairpot_dxyz_dpara(alph,i,speciesSml,speciesLrg) = &
                  dB1_dpara*distCompOver_rij(alph) + 2d0*dB2_dpara*distance*distCompOver_rij(alph) + &
                  3d0*dB3_dpara*(distance**2)*distCompOver_rij(alph)
         enddo
      enddo

     !distance=rBier
     !pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)
     !print *,'interpolated value at r=rBier, =',pairpot
     !print *,'... should equal k1=',k1
     !print *,'interpolated derivative at r=rBier=',B1+2d0*B2*distance+3d0*B3*(distance**2)
     !tmp=pairpot
     !distance=rBier+0.0001d0
     !pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)
     !print *,'by finite difference=',(pairpot-tmp)/0.0001d0
     !print *,'... should equail k2=',k2

     !distance=cutoffMin
     !pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)
     !print *,'interpolated value at r=cutoffMin, =',pairpot
     !print *,'... should equal k3=',k3
     !print *,'interpolated derivative at r=rBier=',B1+2d0*B2*distance+3d0*B3*(distance**2)
     !tmp=pairpot
     !distance=cutoffMin+0.0001d0
     !pairpot = B0 + B1*distance + B2*(distance**2) + B3*(distance**3)
     !print *,'by finite difference=',(pairpot-tmp)/0.0001d0
     !print *,'... should equail k4=',k4
     !stop

    else

       speciesSml=min(species1,species2)
       speciesLrg=max(species1,species2)
       if (typepairpot(speciesSml,speciesLrg).eq.2) then
          pairpot=0d0
          do alph=1,3
             distCompOver_rij(alph)=distComp(alph)/distance
          enddo
          do i=1,31,2
             if (distance.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
                 pairpot=pairpot + pairpotparameter(i,speciesSml,speciesLrg) * &
                     ((pairpotparameter(i+1,speciesSml,speciesLrg) - distance) **3)
                 !dpairpot is w.r.t atom j coords, so in the following there is a -ve 
                 !contribution due to d/dr_ij (cuttoff-r_ij)^3
                 dpairpotFact=3d0*pairpotparameter(i,speciesSml,speciesLrg) * &
                     ((pairpotparameter(i+1,speciesSml,speciesLrg) - distance)**2)
                 do alph=1,3
                    dpairpot_dxyz(alph)=dpairpot_dxyz(alph) - &
                         dpairpotFact*distCompOver_rij(alph)
                 enddo
             endif
          enddo
       endif

       !Calculate derivative of pair potential w.r.t parameters
       !Coefficients:
       do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
          i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
          if (distance.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
             dpairpot_dpara(i,speciesSml,speciesLrg) = dpairpot_dpara(i,speciesSml,speciesLrg) + &
                         (pairpotparameter(i+1,speciesSml,speciesLrg)   - distance) **3
          endif
       enddo
       !Cutoffs:
       do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
          i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
          if (distance.le.pairpotparameter(i,speciesSml,speciesLrg)) then
             dpairpot_dpara(i,speciesSml,speciesLrg) = dpairpot_dpara(i,speciesSml,speciesLrg) + &
                        3d0 * pairpotparameter(i-1,speciesSml,speciesLrg)  * &
                        ((pairpotparameter(i,speciesSml,speciesLrg)   - distance) **2)
          endif
       enddo

       !Calculate derivative of dpairpot_dxyz w.r.t parameters
       !Coefficients:
       do iiCoeff=1,noptpairpotCoeff(speciesSml,speciesLrg)
          i = ioptpairpotCoeff(iiCoeff,speciesSml,SpeciesLrg)
          if (distance.le.pairpotparameter(i+1,speciesSml,speciesLrg)) then
             dpairpotFact=3d0*((pairpotparameter(i+1,speciesSml,speciesLrg) - distance)**2)
             do alph=1,3
                d2pairpot_dxyz_dpara(alph,i,speciesSml,speciesLrg) = &
                    -dpairpotFact*distCompOver_rij(alph)
             enddo
          endif
       enddo
       !Cutoffs:
       do iiCutoff=1,noptpairpotCutoff(speciesSml,speciesLrg)
          i = ioptpairpotCutoff(iiCutoff,speciesSml,SpeciesLrg)
          if (distance.le.pairpotparameter(i,speciesSml,speciesLrg)) then
             dpairpotFact=6d0*pairpotparameter(i-1,speciesSml,speciesLrg) * &
                        (pairpotparameter(i,speciesSml,speciesLrg) - distance)
             do alph=1,3
                d2pairpot_dxyz_dpara(alph,i,speciesSml,speciesLrg) = &
                    -dpairpotFact*distCompOver_rij(alph)
             enddo
          endif
       enddo

    endif

end subroutine radpairpotForce

subroutine radpairpotForceSpline(species1,species2,distance, &
                distComp,pairpot,dpairpot_dpara, &
                dpairpot_dxyz,d2pairpot_dxyz_dpara)

    !------------------------------------------------------------------c
    !
    !     Calculates the 'pairpot', for two atoms
    !     of species, 'species1' and 'species2' which are a
    !     distance, 'distance', apart from one another.
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Arguments:     species1,species2,distance,pairpotparameter
    !     Returns:       pairpot
    !     Files read:    -
    !     Files written: -
    !
    !     Marcel Sluiter, Feb 9 2006
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_atomproperties
    use m_meamparameters
    use m_filenames
    use m_generalinfo
    use m_optimization

    implicit none

    integer species1,species2,i,index,isp,jsp,speciesSml,speciesLrg,iiCoeff,iiCutoff,rIndex,alph
    real(8) distance,pairpot,tmp,rsBier,x,eps,B0,B1,B2,B3,k1,k2,k3,k4,depsdr, &
            rdiffcb,rBiersq,rBiercb,cutoffMinsq,cutoffMincb,dpairpot_dr
    real(8) dpairpot_dpara(32,maxspecies,maxspecies), dpairpot_dxyz(3), &
         d2pairpot_dxyz_dpara(3,32,maxspecies,maxspecies),distComp(1:3),distCompOver_rij(1:3)

    intent(in) species1,species2!distance
    intent(out) pairpot

    speciesSml=min(species1,species2)
    speciesLrg=max(species1,species2)

    do alph=1,3
       distCompOver_rij(alph)=distComp(alph)/distance
    enddo   

    rIndex=1+FLOOR(distance/drLmps(speciesSml,speciesLrg))
    call interpolateSplineDeriv(drLmps(speciesSml,speciesLrg),pairpotSpline(1:nRhoMaxSpline,speciesSml,speciesLrg),pairpotSplineSecDer(1:nRhoMaxSpline,speciesSml,speciesLrg),nRMaxSpline,rIndex,distance,pairpot,dpairpot_dr)
    pairpot=pairpot/distance
    dpairpot_dr=(dpairpot_dr-pairpot)/distance

    do alph=1,3
       dpairpot_dxyz(alph)= dpairpot_dr*distCompOver_rij(alph)
    enddo

    dpairpot_dpara=0d0
    d2pairpot_dxyz_dpara=0d0

end subroutine radpairpotForceSpline
