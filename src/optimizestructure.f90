subroutine optimizestructure(allstrucs)

    !--------------------------------------------------------------c
    !
    !     Relax the atomic coordinates of the first POSCAR file
    !     in 'fitdbse'. allstrucs determines if only the files
    !     which have been specified to relax and which have weights
    !     >0 are relaxed (.false.) or whether all files which have
    !     been specified to relax are relaxed regardless of the
    !     weight.
    !
    !     Called by:     program MEAMfit
    !     Calls:         initializestruc, initializemeam,
    !                 setuppositivep,parachange1,meamforce,
    !                 broyden
    !     Arguments:     allstrucs
    !     Returns:       -- nothing --
    !     Files read:    targetfiles
    !     Files written: crystalstrucfile
    !
    !     Marcel Sluiter and Andy Duff, March 14 2011
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c


    use m_geometry
    use m_datapoints
    use m_optimization
    use m_poscar
    use m_generalinfo
    use m_filenames
    use m_meamparameters

    implicit none
    logical allstrucs,firstrlx
    character(len=1) :: back
    integer, parameter:: maxiter=50!1000
    real(8), parameter:: forcetolerance=1d-3 !eV/angstrom
    real(8), parameter:: maxmovement=0.5, & !angstrom. Try bigger step
        !for Fe: 1.0, recently 1
    assumedspringconstant=1d0
    logical, allocatable:: signflipped(:)
    integer i,k,j,iter,nstor,nf
    real(8) force(3)
    real(8) mixparam,latticeconst,poscarcell(9),magforce,maxmagforce, &
        aux,dx(3),dx2,scaledispl
    real(8), allocatable:: x(:),work(:),f(:),cutoff(:)
    character*80 poscarheader,poscarspecies

    print *,'Optimizing structure'

    mixparam=0.001d0
    do istr=1,nstruct
        computeForces_backup(istr)=computeForces(istr)
    enddo

    computeForces=.false.
    do istr=1,nstruct
        if (rlxstruc(istr).eq.1) then
            if ((allstrucs.eqv..true.).or.(weightsEn(istr).gt.0d0)) then
                computeForces(istr)=.true.
            endif
        endif
    enddo

    call setupnntables
    firstrlx=.true.
    do istr=1,nstruct
        if (rlxstruc(istr).eq.1) then

            if ( (weightsEn(istr).gt.0d0).or. (allstrucs.eqv..true.) ) then

                if (firstrlx) then
                    firstrlx=.false.
                    write(*,*) ' Relaxing all structures.'
                endif

                write (*,'(A10,A)') 'Relaxing ',strucnames(istr)

                !Update neighborhood
                call initializestruc(istr)

                if (gn_forces(istr).gt.1000) then
                    print *,'ERROR: number of atomic forces in first ', &
                        'POSCAR'
                    print *,'file is larger than 1000 (=',gn_forces(istr), &
                        ' Need to increase ', &
                        'size of force_store array in ', &
                        'optimizestructure subroutine. STOPPING.'
                    stop
                endif
                nstor = min(5,3*gn_forces(istr),maxiter)
                !               print *,'allocating x of size:',3*gn_forces(istr)
                if (allocated(x)) deallocate(x)
                if (allocated(f)) deallocate(f)
                if (allocated(work)) deallocate(work)

                allocate(x(3*gn_forces(istr)),f(3*gn_forces(istr)), &
                    work(3*gn_forces(istr)+ &
                    2*nstor*(1+nstor+3*gn_forces(istr))) )

                !Set up meam parameters
                if (allocated(p)) deallocate(p)
                if (allocated(positivep)) deallocate(positivep)
                if (allocated(cutoff)) deallocate(cutoff)
                if (allocated(signflipped)) deallocate(signflipped)
                allocate(p(np), &
                    positivep(np),cutoff(np),signflipped(np))
                call setuppositivep
                call parachange1(signflipped,cutoff,nf) !Ensure meam
                !parameters have the correct signs and are in
                !the correct range
                ! call setupsplines !Prepare splines for pair-potentials

                iter=1  !Loop over structural optimization steps
                do

                    !Calculate forces
                    maxmagforce=0d0 !Largest magnitude of force on any
                    !atom
                    do j=1,gn_forces(istr)
                        !Variables in following call are out of date:
                        !call meamforce(j,force,0,0d0) !Calculate the force acting on atom j in structure i.
                        magforce= &
                            sqrt( force(1)**2 + force(2)**2 + force(3)**2 )
                        !                        print *,'magforce=',magforce
                        maxmagforce=max(magforce,maxmagforce)
                        x(3*(j-1)+1)=gxyz(1,j,istr)
                        x(3*(j-1)+2)=gxyz(2,j,istr)
                        x(3*(j-1)+3)=gxyz(3,j,istr)
                        f(3*(j-1)+1)=force(1)
                        f(3*(j-1)+2)=force(2)
                        f(3*(j-1)+3)=force(3)
                    enddo
                    !                  print *,'done force calc'
                    back=char(8)
                    if (iter.eq.1) then
                        write(*,'(A30,F15.10,A10,F15.10,A7)') &
                            '  Maximum magnitude of force:', &
                            maxmagforce,' eV/Ang > ',forcetolerance, &
                            ' eV/Ang'
                    else
                        write(*,'(256A1)') (back, k=1, 89)
                        write(*,'(A30,F15.10,A10,F15.10,A7)') &
                            '  Maximum magnitude of force:', &
                            maxmagforce,' eV/Ang > ',forcetolerance, &
                            ' eV/Ang'
                    endif

                    !Determine good mixparam for 1st structural
                    !optimization only
                    if (iter.eq.1) then
                        !                    aux=min(maxmovement,maxmagforce/
                        !     +assumedspringconstant)
                        !                    mixparam=aux/maxmagforce
                        !                    print *,mixparam
                        !                    stop
                        mixparam=0.05d0!0.04 gives the lowest second value
                    endif

                    call broyden(iter,3*gn_forces(istr), &
                        nstor,mixparam,work,x,f)

                    !Check that position update does not move atoms more
                    !than maxmovement
                    scaledispl = 1d0
                    aux = 0d0
                    do i=1,gn_forces(istr)
                        dx(1) = gxyz(1,i,istr)-x(3*(i-1)+1)!f(3*(i-1)+1)
                        dx(2) = gxyz(2,i,istr)-x(3*(i-1)+2)!f(3*(i-1)+2)
                        dx(3) = gxyz(3,i,istr)-x(3*(i-1)+3)!f(3*(i-1)+3)
                        dx2   = sqrt( dot_product(dx,dx) )
                        aux   = max( aux, dx2 )
                        if (dx2.gt.maxmovement) then
                            !                         write(*,'(a,a,i3,a,f9.5)')
                            !     +                  '*WARNING* optimizestructure: movement too',
                            !     +                  ' great, atom:',i,' movement [Ang]=',dx2
                            scaledispl=min(scaledispl,maxmovement/dx2)
                        endif
                    enddo
                    if(abs(scaledispl-1d0).gt.1d-6) then
                        scaledispl=scaledispl*0.9d0 !arbitrary, but safe
                        !                    write(*,'(a,a,f9.5)')
                        !     +               'movement of atoms has been scaled ',
                        !     +               'by factor:',scaledispl
                    endif
                    do i=1,gn_forces(istr)
                        dx(1) =  gxyz(1,i,istr)-x(3*(i-1)+1)
                        dx(2) =  gxyz(2,i,istr)-x(3*(i-1)+2)
                        dx(3) =  gxyz(3,i,istr)-x(3*(i-1)+3)
                        dx=dx*scaledispl
                        gxyz(1,i,istr) = gxyz(1,i,istr) - dx(1) !update
                        gxyz(2,i,istr) = gxyz(2,i,istr) - dx(2) !positions
                        gxyz(3,i,istr) = gxyz(3,i,istr) - dx(3)
                    enddo

                    !Update the POSCAR file with the new atomic positions.
                    !This is necessary at the moment, because the
                    !subroutine 'initializestruc' reads in from the POSCAR
                    !files, and this subroutine has to be called to reset
                    !the atomic environments (neighbors) each time the
                    !atoms are moved. Note: update so that this is not
                    !necessary.
                    open(unit=1,file=trim(targetfiles(istr)))
                    read(1,'(a)') poscarheader
                    read(1,*) latticeconst
                    read(1,*) poscarcell
                    read(1,'(a)') poscarspecies
                    rewind(unit=1)
                    write(1,'(a)') trim(poscarheader)
                    write(1,*) latticeconst
                    write(1,'(3f12.6)') poscarcell
                    write(1,'(a)') trim(poscarspecies)
                    write(1,'(a)') 'Cartesian'
                    do j=1,gn_forces(istr)
                        write(1,*) gxyz(1:3,j,istr)/latticeconst
                        !                     print *,gxyz(1:3,j,1)/latticeconst
                    enddo
                    close(1)
                    !                  stop

                    iter=iter+1
                    if ((iter.gt.maxiter).or. &
                        (maxmagforce.lt.forcetolerance)) then
                        exit
                    endif

                    !Update neighborhood
                    call initializestruc(istr)

                enddo !end loop over structural optimization
                if (maxmagforce.ge.forcetolerance) then
                    write(*,'(256A1)') (back, k=1, 89)
                    write(*,'(A30,F15.10,A10,F15.10,A7)') &
                        '  Maximum magnitude of force:', &
                        maxmagforce,' eV/Ang > ',forcetolerance, &
                        ' eV/Ang'
                    print *
                    write(*,'(A20,A14,I4,A1,I4,A1)') '  Structural optim.', &
                        'failed (iter:',maxiter,'/',maxiter,')'
                else
                    write(*,'(256A1)') (back, k=1, 89)
                    write(*,'(A30,F15.10,A10,F15.10,A7)') &
                        '  Maximum magnitude of force:', &
                        maxmagforce,' eV/Ang < ',forcetolerance, &
                        ' eV/Ang'
                    print *
                    write(*,'(A20,A12,I4,A1,I4,A1)') '  Structural optim.', &
                        'done (iter:',iter,'/',maxiter,')'
                endif

            endif

        endif

    enddo

    !De-allocate relevant arrays (ANDY should you not put
    !this
    !stuff outside the loop???)
    !      if (allocated(zz)) deallocate(zz)
    !      if (allocated(gn_inequivalentsites))
    !     +     deallocate(gn_inequivalentsites)
    !      if (allocated(gspecies)) deallocate(gspecies)
    !      IF (allocated(gn_neighbors)) deallocate(gn_neighbors)
    !      if (allocated(gneighborlist)) deallocate(gneighborlist)
    !      if (allocated(gxyz)) deallocate(gxyz)
    !      if (allocated(gn_forces)) deallocate(gn_forces)
    !      if (allocated(optforce)) deallocate(optforce)

    do istr=1,nstruct
        computeForces(istr)=computeForces_backup(istr)
    enddo

end subroutine optimizestructure
