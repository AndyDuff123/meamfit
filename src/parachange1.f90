subroutine parachange1(signflipped,nf)

    !     Copyright (c) 2018, STFC

    use m_optimization
    use m_generalinfo
    use m_meamparameters
    use m_atomproperties
    use m_objectiveFunction

    implicit none

    logical signflipped(np)
    integer i,ip,isp,jsp,spc_cnt,ll,nf

    !print *,'positivep=',positivep
    !stop

    !CutoffPenalty=0d0
    !CutoffPenCoeff=10000d0 !10 too small (order 10^-7 in optfunc in trial)

    !Check to see if any parameters ought to be positive, then
    !flip these parameters to be positive, but recording that the
    !sign has been flipped so that we can return the original negative
    !value to the conjugate-gradient minimizer.
    signflipped(1:np)=.false.
    do ip=1,np
        !         if (ip.eq.36) then
        !             print *,'for 36, positivep=',positivep(ip)
        !             print *,' and p(36)=',p(36)
        !c             stop
        !         endif
        if ((positivep(ip)).and.(p(ip).lt.0d0)) then
            signflipped(ip)=.true.
            p(ip)=abs(p(ip))
        endif
    enddo
    !Check to see if any of the radial cutoff parameters has exceeded
    !cutoffMax, or fallen below cutoffMin. If so return 'cutoffOutOfBounds'
    !so that we can inform the optimizer to take a smaller step
    cutoffOutOfBounds=.false.
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    do i=2,12,2
                       ip=2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i
                       if (p(ip).gt.cutoffMax) then
                           !CutoffPenalty=CutoffPenalty+CutoffPenCoeff*((p(ip)-cutoffMax)**2)
                           cutoffOutOfBounds=.true.
                           print *,'elec dens cutoff exceeded for ip|ll|isp|jsp|='
                           print *,i,ll,isp,jsp
                           print *,'   =',p(ip),' > ',cutoffMax
                       endif
                       if ((p(ip).lt.cutoffMin).and.(p(ip).ne.0d0)) then
                           !CutoffPenalty=CutoffPenalty+CutoffPenCoeff*((cutoffMin-p(ip))**2)
                           cutoffOutOfBounds=.true.
                           print *,'elec dens cutoff too small for ip|ll|isp|jsp|='
                           print *,i,ll,isp,jsp
                           print *,'   =',p(ip),' < ',cutoffMin
                       endif
                    enddo
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                do i=2,32,2
                   ip=2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt
                   if (p(ip).gt.cutoffMax) then
                       !CutoffPenalty=CutoffPenalty+CutoffPenCoeff*((p(ip)-cutoffMax)**2)
                       cutoffOutOfBounds=.true.
                       print *,'pairpot cutoff exceeded for ip|isp|jsp|='
                       print *,i,isp,jsp
                       print *,'   =',p(ip),' > ',cutoffMax
                       !stop
                   endif
                   !if (p(ip).ne.0d0) then  !<---- to visualize change in paras
                   !   print *,'pairpot cutoff for ip|ll|isp|jsp|='
                   !   print *,i,ll,isp,jsp
                   !   print *,'   =',p(ip)
                   !endif
                   if ((p(ip).lt.cutoffMin).and.(p(ip).ne.0d0)) then
                       !CutoffPenalty=CutoffPenalty+CutoffPenCoeff*((cutoffMin-p(ip))**2)
                       cutoffOutOfBounds=.true.
                       print *,'pairpot cutoff too small for ip|ll|isp|jsp|='
                       print *,i,ll,isp,jsp
                       print *,'   =',p(ip),' < ',cutoffMin
                       !stop
                   endif
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

end subroutine parachange1
