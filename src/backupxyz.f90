subroutine backupxyz

    !Backup xyz files. This will be used once we have structural relaxation
    !included: After each optimization the positions can then be reset to their
    !original positions
    !
    !Andrew Duff, 2013
    !
    !     Copyright (c) 2018, STFC

    use m_geometry

    integer i,j

    !Store initial atom positions in gxyz_backup
    do i=1,nstruct
        do j=1,gn_inequivalentsites(i)
            gxyz_backup(1:3,j,i)=gxyz(1:3,j,i)
        enddo
    enddo

end subroutine backupxyz
