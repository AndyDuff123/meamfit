subroutine plotfunctions(all)

    !     Currently for just one species. I have tested for a single
    !     species, namely for Fe, and got -4.013 eV for the bulk cell using
    !     the Fe.eam.alloy file as input to LAMMPS. Now I am generalising
    !     to FeC, and testing agreement between my code and LAMMPS.
    !     Currently doing for non FS-like radial densities (I.e., assume
    !     thi_YX=thi_XX and thi_YY=thi_XY). The energies will then be
    !     'wrong' using this code, but can at least test agreement with the
    !     LAMMPS values.
    !     NOTE: For the pair-potential, since the LAMMPS format demands the
    !     quantity V(r)*r @ r=0, and since V(r) must be evaluated first
    !     by 'pairpot' before it is multiplied by r, we would get an
    !     infinity for this value. We could determine this value
    !     analytically, but then the code would not be general. Instead I
    !     interpolate the r=0 value from the first two lowest r values.
    !     Presumambly this would only introduce an error when atoms become
    !     infinatesimally close; I.e., never.
    !
    !     If all=.false., only produce the 58, file.
    !
    !     Copyright (c) 2018, STFC

    use m_geometry
    use m_electrondensity
    use m_meamparameters
    use m_generalinfo
    use m_poscar
    use m_atomproperties
    use m_optimization
    use m_plotfiles
    use m_filenames

    implicit none

    logical all
    character*80 filename,string1,string2,string3,alloyString
    integer i,j,jj,isp,jsp,Nr,Nrho,Npot,Nebf,gspeciesprev,l,ncomb, &
        npotfns,Nr_dlpoly,gn_inequivalentsites_backup
    integer k,m,cnt,stringLen
    real(8) distmin,distmax,pairpot,sepn,drho,dr, &
        hartreeovereV,bohroverangstrom,deltar2,deltarho, &
        delta, LrgVal, &
        rho_out(5),meam_out(5),pairpot_out(5),sepn_out(5), &
        density_out(5)
    real(8) density(0:lmax),density_deriv(12,0:lmax)
    real(8) tmpArray(32,maxspecies,maxspecies) !AnalyticDeriv (not explicitly 
                                  !needed here, but needed for call to plotfunctions

    hartreeovereV=27.2116d0
    bohroverangstrom=0.529177249d0

    if (autorhomax.eqv..true.) then
       !Automatically choose rhomax based on the largest value of rho
       !encountered in the fit
        do i=-5,15
           if (maxlargestrho.lt.10.d0**dble(i)) then
              exit
           endif
        enddo
        rhomax=10.d0**dble(i)
        if (2d0*maxlargestrho.gt.rhomax) then
           rhomax=5d0*(10.d0**dble(i))
        endif
        if (verbosity.gt.1) then
           print *,'Automatically choosing rhomax for lammps output file:'
           print *,'Largest rho value from fitting database=',maxlargestrho
        endif
    endif
    if (verbosity.gt.1) then
       print *,'rhomax=',rhomax
    endif

    !Variables for potential plotting and LAMMPS potential file
    Nr=100000 !No. points used to define radial functions 1000 100000
    Nrho=10000000 !No. points used to define embedding functions 10000 1000000
    if ( (mod(Nr,5).ne.0).or.(mod(Nrho,5).ne.0) ) then
        print *,'ERROR:'
        print *,'   Please choose Nr and Nrho to be multiples of 5'
        print *,'to ensure correct writing of data to .eam file,'
        print *,'stopping.'
        stop
    endif
    dr=(p_rmax/dble(Nr-1)) !For LAMMPS potential file, units Angstrom
    drho=(rhomax/dble(Nrho-1)) !For LAMMPS potential file, units
    !Angstrom^-3

    !Variables for Camelion output
    !r-grid:
    !Camelion requires r-grid parameters entered into goion file in
    !format:
    !NPOT, RCUT, DELTAS, 0, 0, 0
    !Meaning: no. points on grid; radial cut-off; delta r^2
    !(first r^2 point is DELTA (rather than zero) )
    !A separate line is included for each isp, jsp, although here they
    !are defined once for all isp, jsp.
    !Note: since Camelion uses in general more radial points (it
    !doesn't use splines), we define Npot separately to Nr.
    Npot=10000 !7000
    deltar2=(p_rmax**2)/dble(Npot)
    !embfunc-grid:
    Nebf=14000!10000
    deltarho=rhomax/dble(Nebf-1) !This time first rho is zero
    delta=0.001d0 !This is use to calculate the virials

    if (lmax.eq.0) then
       !Write pre-amble to LAMMPS input file
       string2=""
       do i=1,maxspecies
          write(string1,'(A2)') element(speciestoZ(i))
          string2=trim(string2)//trim(string1)
       enddo
       string3=trim(string2)//" potential in LAMMPS format"
       write(58,*) trim(string3)
       write(58,*)
       write(58,*)
       if (dlpolyOut) then
           string3=trim(string2)//" potential in DLPOLY format"
           write(       70,*) trim(string3)
           write(71,*) trim(string3)
           write(71,'(A9)') 'units  eV'
       endif
       if (maxspecies.lt.10) then
           write(string1,'(I1)') maxspecies
       else
           print *,'ERROR: more than 10 species in plotfunctions?!'
           print *,'STOPPING.'
           stop
       endif
       string2=trim(string1)
       do i=1,maxspecies
          write(string1,'(A2)') element(speciestoZ(i))
          string2=trim(string2)//" "//trim(string1)
       enddo
       write(58,*) trim(string2)
       write(58,'(I13,A2,F25.16,A2,I13,A2,E25.16,A2,F25.16)') Nrho,'  ',drho,'  ',Nr,'  ',dr,'  ',p_rmax
       if (dlpolyOut) then
          write(71,'(A17)') 'molecular types 1'
          if (maxspecies.eq.1) then
             write(string1,'(A2)') element(speciestoZ(1))
          else
             string1=""
             do i=1,maxspecies
                write(string2,'(A2)') element(speciestoZ(i))
                if (i.eq.maxspecies) then
                   string1=trim(string1)//trim(string2)//" Alloy"
                else
                   string1=trim(string1)//trim(string2)//"/"
                endif
             enddo
          endif
          write(71,*) trim(string1)
          write(71,'(A22)') "nummols <PLEASE ENTER>"
          write(71,'(A6,I1)') "atoms ",maxspecies
          do i=1,maxspecies
             write(71,'(A2,A6,F10.5,A20)') element(speciestoZ(i)),'      ', & 
                    mass(speciestoZ(i)),'       0.0         1'
          enddo
          write(71,'(A6)') "finish"
          ncomb=maxspecies*(maxspecies+1)/2
          npotfns=maxspecies*(maxspecies+5)/2
          write(70,'(I2)') npotfns
          write(71,'(A6,I2)') 'metal ',ncomb
          do i=1,maxspecies
            do j=1,maxspecies
               if (j.ge.i) then
                  write(string1,'(A2)') element(speciestoZ(i))
                  write(string2,'(A2)') element(speciestoZ(j))
                  string1=trim(string1)//"      "//trim(string2)//"      eam"
                  write(71,'(A19)') string1
               endif
            enddo
          enddo
          write(71,'(A5)') "close"
       endif
    endif

    !If user supplies a min sepn value, use this here. If not, set it to the
    !smallest separation in the following look.
    if (plotSepnMin.ne.0d0) then
       distmin=plotSepnMin
    else
       distmin=0d0
    endif
    distmax=0d0
    do istr=1,nstruct
        call radialdensityfunction
        do i=1,gn_inequivalentsites(istr)
            do jj=1,gn_neighbors(i,istr)
                j=gneighborlist(jj,i,istr)
                rij=distance(i,j)
                if (plotsepnMin.eq.0d0) then
                   if (distmin.eq.0d0) then
                       distmin=rij
                   else
                       if (rij.lt.distmin) then
                           distmin=rij
                       endif
                   endif
                endif
                if (distmax.eq.0d0) then
                    distmax=rij
                else
                    if (rij.gt.distmax) then
                        distmax=rij
                    endif
                endif
                isp=gspecies(i,istr)
                jsp=gspecies(j,istr)
                if ((isp.eq.1).and.(jsp.eq.1)) then
                    call radpairpot(isp,jsp,rij, &
                        pairpot,tmpArray)
                endif
            enddo
        enddo
    enddo

    gspeciesprev=gspecies(1,1)
    istr=1
    !rho_i will be accessed later in 'embeddingfunction'. Only the first element will be 
    !used to extract the functional form, so temporarily set gn_inequiv...(1)=1. Usually
    !'backgrounddensity' will allocate rho_i accordingly, however since it is
    !not called here, allocated rho_i as below:
    gn_inequivalentsites_backup=gn_inequivalentsites(1)
    gn_inequivalentsites(1)=1
    !if(allocated(rho_i)) deallocate(rho_i,drhoi_dpara)
    !allocate( rho_i(gn_inequivalentsites(istr)), &
    !          drhoi_dpara(12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)) )

    LrgVal=0d0

    do isp=1,maxspecies

       if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
          filename="embfunc"
          write(string1,'(I1)') isp
          filename=trim(filename)//trim(string1)
          open(55,file=trim(adjustl(filename)))

          filename="elecdens"
          write(string1,'(I1)') isp
          filename=trim(filename)//trim(string1)
          open(57,file=trim(adjustl(filename)))
          write(57,'(A2,A2)') '# ',element(speciestoz(isp))
          filename=trim(filename)//"_full"
          open(56,file=trim(adjustl(filename)))
          write(56,'(A2,A2)') '# ',element(speciestoz(isp))
       endif

       if (lmax.eq.0) then
          !The final two elements in the following write command are not used in
          !LAMMPS, so are placed here as place-holders.
          write(58,'(I3,A2,F15.10,A22)') speciestoZ(isp),'  ',mass(speciestoZ(isp)),' 2.85531246000000 bcc'
          !units of grams/mol (correct for 'metal' units)
       endif

       !Plot embedding function to seperate file as well as to LAMMPS file
       gspecies(1,1)=isp
       if ( ((all.eqv..true.).and.(potFuncOut.eqv..true.)) .or. (lmax.eq.0) ) then
          do i=1,Nrho-4,5
              !Write out five values per line
              do j=1,5
                  rho_i(1)=dble(i+j-2)*dble(rhomax)/dble(Nrho-1)
                  call embeddingfunction
                  meam_out(j)=meam_f(1)
                  rho_out(j)=rho_i(1)
                  if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
                   write(55,*) rho_out(j),meam_out(j)
                  endif
              enddo
              if (lmax.eq.0) then
                 write(58,'(e24.16,e24.16,e24.16,e24.16,e24.16)') &
                     meam_out(1),meam_out(2),meam_out(3), &
                     meam_out(4),meam_out(5)
              endif
          enddo
       endif
       if (dlpolyOut.and.(lmax.eq.0)) then
          write(70,'(A7,A2,A2,I5,A2,e24.16,A2,e24.16)') 'EMBED  ', &
             element(speciestoZ(isp)),'  ',Nrho,'  ',0d0,'  ',rhomax
          !dlpoly format requires four values per line
          do i=0,Nrho-4,4
              !Write out four values per line
              do j=1,4
                  rho_i(1)=dble(i+j-1)*dble(rhomax)/dble(Nrho-1)
                  call embeddingfunction
                  meam_out(j)=meam_f(1)
              enddo
              write(70,'(e24.16,e24.16,e24.16,e24.16)') &
                  meam_out(1),meam_out(2),meam_out(3), &
                  meam_out(4)
          enddo
          !add remaining, non multiple of four, meam_out values to end
          print *,'remaining values:',Nrho-i
          if (Nrho-i.gt.0) then
             do j=1,Nrho-i
                 rho_i(1)=dble(i+j-2)*dble(rhomax)/dble(Nrho-1)
                 call embeddingfunction
                 meam_out(j)=meam_f(1)
             enddo
          endif
          if ((Nrho-i).eq.1) then
             write(70,'(e24.16)') meam_out(1)
          elseif ((Nrho-i).eq.2) then
             write(70,'(e24.16,e24.16)') meam_out(1),meam_out(2)
          elseif ((Nrho-i).eq.3) then
             write(70,'(e24.16,e24.16,e24.16)') meam_out(1),meam_out(2),meam_out(3)
          endif
       endif

       !Plot electron density to seperate file as well as to LAMMPS file
       if ( ((all.eqv..true.).and.(potFuncOut.eqv..true.)) .or. (lmax.eq.0) ) then
          do i=1,Nr-4,5
              !Write out five values per line
              do j=1,5
                  rij=dble(i+j-2)*dble(p_rmax)/dble(Nr-1)
                  call raddens(rij,1,isp,density,density_deriv)
                  density_out(j)=density(0)
                  sepn_out(j)=rij
                  if (density(0).ne.0d0) then
                      if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
                       write(56,*) rij,density(0)
                      endif
                      if ((rij.gt.(distmin)).and. &
                          (rij.lt.(distmax))) then
                          if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
                           write(57,*) rij,density(0)
                          endif
                          LrgVal=max(LrgVal,sqrt(density(0)**2))
                      endif
                  endif
              enddo
              if (lmax.eq.0) then
                 write(58,'(e24.16,e24.16,e24.16,e24.16,e24.16)') &
                     density_out(1),density_out(2),density_out(3), &
                     density_out(4),density_out(5)
              endif
          enddo
       endif

       !write out to dlpoly file (4 entries per line)
       if (dlpolyOut.and.(lmax.eq.0)) then
          !Do not want to print zeros for dlpoly input
          Nr_dlpoly=Nr
          do i=1,Nr
             rij=dble(i-1)*dble(p_rmax)/dble(Nr-1)
             call raddens(rij,1,isp,density,density_deriv)
             if (density(0).eq.0d0) then
                Nr_dlpoly=i-1
                exit
             endif
          enddo
          print *,'Nr_dlpoly=',Nr_dlpoly
          rij=dble(Nr_dlpoly-1)*dble(p_rmax)/dble(Nr-1)
          write(70,'(A6,A2,A2,I5,A2,e24.16,A2,e24.16)') 'DENS  ', &
             element(speciestoZ(isp)),'  ',Nr_dlpoly,'  ',0d0,'  ',rij
          print *,'loop, i=',0,' to ',Nr_dlpoly-4,' steps of 4'
          do i=0,Nr_dlpoly-4,4
             do j=1,4
                 rij=dble(i+j-1)*dble(p_rmax)/dble(Nr-1)
                 call raddens(rij,1,isp,density,density_deriv)
                 density_out(j)=density(0)
             enddo
             write(70,'(e24.16,e24.16,e24.16,e24.16)') &
                 density_out(1),density_out(2),density_out(3), &
                 density_out(4)
          enddo
          !add remaining, non multiple of four, meam_out values to end
          print *,'Adding remaining nr_dlpoly-i=',Nr_dlpoly-i
          if (Nr_dlpoly-i.gt.0) then
             do j=1,Nr_dlpoly-i
                 rij=dble(i+j-1)*dble(p_rmax)/dble(Nr-1)
                 call raddens(rij,1,isp,density,density_deriv)
                 density_out(j)=density(0)
                 print *,'j=',j,' density_out(j)=',density_out(j)
             enddo
          endif
          if ((Nr_dlpoly-i).eq.1) then
             write(70,'(e24.16)') density_out(1)
          elseif ((Nr_dlpoly-i).eq.2) then
             write(70,'(e24.16,e24.16)') density_out(1),density_out(2)
          elseif ((Nr_dlpoly-i).eq.3) then
             write(70,'(e24.16,e24.16,e24.16)') density_out(1),density_out(2),density_out(3)
             print *,'should have printed:'
             write(*,'(e24.16,e24.16,e24.16)') density_out(1),density_out(2),density_out(3)
             print *,'...to file'
          endif
       endif

       if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
          close(55)
          close(56)
          close(57)
       endif

    enddo
    !Append to the end electron density files the positions of cutoffs

    if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
       do isp=1,maxspecies
          filename="elecdens"
          write(string1,'(I1)') isp
          filename=trim(filename)//trim(string1)
          open(57,file=trim(adjustl(filename)),status="old",position="append",action='write')
          do i=2*m3+lmax*m1+12*lm1*(isp-1)+2,2*m3+lmax*m1+12*lm1*(isp-1)+12,2
             if (p(i).ne.0d0) then
                write(57,*)
                write(57,*) p(i),0d0
                write(57,*) p(i),LrgVal/5d0
             endif
          enddo
          close(57)
       enddo
    endif

    !Write embedding functions and electron densities to Camelion file
    if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then

        filename=""
        do i=1,maxspecies
            write(string1,'(A2)') element(speciestoZ(i))
            filename=trim(filename)//trim(string1)
        enddo
        string2='_MEAM.eb'
        filename=trim(filename)//trim(string2)
        open(64,file=trim(adjustl(filename)))

        do l=0,lmax
           filename=""
           do i=1,maxspecies
               write(string1,'(A2)') element(speciestoZ(i))
               filename=trim(filename)//trim(string1)
           enddo
           string2='_MEAM'
           filename=trim(filename)//trim(string2)
           write(string1,'(I1)') l
           filename=trim(filename)//trim(string1)
           filename=trim(filename)//".edv"
           open(59+l,file=trim(adjustl(filename)))
        enddo

        do isp=1,maxspecies
            gspecies(1,1)=isp
            istr=1
            !if (allocated(meam_f)) deallocate(meam_f)
            !allocate(meam_f(gn_inequivalentsites(1)))
            do i=1,Nebf
                rho_i(1)=dble(i-1)*deltarho
                call embeddingfunction
                meam_out(1)=meam_f(1)
                rho_i(1)=rho_i(1)+delta
                call embeddingfunction
                rho_i(1)=rho_i(1)-delta
                meam_out(2)=(meam_f(1)-meam_out(1))/delta
                write(64,*) meam_out(1)/0.0872d0, &
                    meam_out(2)*0.0517d0/0.0872d0
            enddo
        enddo
        gspecies(1,1)=gspeciesprev
        do l=0,lmax
            do isp=1,maxspecies
                do i=1,Npot
                    rij=sqrt(deltar2*dble(i))
                    call raddens(rij,1,isp,density,density_deriv)
                    density_out(1)=density(l)
                    rij=rij+delta
                    call raddens(rij,1,isp,density,density_deriv)
                    rij=rij-delta
                    density_out(2)=rij*(density(l)-density_out(1))/delta
                    !calc dens and r*d dens/dr
                    write(59+l,*) density_out(1)/0.0517d0, & !Convert to
                        density_out(2)/0.0517d0    !camelion units
                enddo

            enddo
        enddo

        close(59)
        close(60)
        close(61)
        close(62)
        close(64)

    endif

    !Plot pair-potentials seperately and to LAMMPS file
    LrgVal=0d0
    do isp=1,maxspecies
      do jsp=1,isp
         !if (jsp.ge.isp) then
            if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
               filename="pairpot"
               write(string1,'(I1)') isp
               filename=trim(filename)//trim(string1)
               write(string1,'(I1)') jsp
               filename=trim(filename)//trim(string1)
               open(54,file=trim(adjustl(filename)))
               write(54,'(A2,A2,A1,A2)') '# ',element(speciestoz(isp)),' ',element(speciestoz(jsp))
               filename=trim(filename)//"_full"
               open(53,file=trim(adjustl(filename)))
               write(53,'(A2,A2,A1,A2)') '# ',element(speciestoz(isp)),' ',element(speciestoz(jsp))
            endif

            do i=1,Nr-4,5
                !Write out five values per line
                do j=1,5
                    sepn=dble(i+j-2)*dble(p_rmax)/dble(Nr-1)
                   ! print *,i,'/',Nr-4,' j=',j,' sepn=',sepn,'/p',p_rmax
                    if ((sepn.eq.0d0).or.(sepn.eq.p_rmax)) then
                        pairpot=0d0 !Don't call radpairpot in this case,
                        !as the SPLINT call will fail.
                    else
                        call radpairpot(isp,jsp,sepn,pairpot,tmpArray)
                    endif
                    !print *,'A',pairpot
                    pairpot_out(j)=sepn*pairpot
                    sepn_out(j)=sepn
                    !print *, 'sepn=',sepn,' pairpot=',pairpot
                    if (pairpot.ne.0d0) then
                        if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
                         write(53,*) sepn,pairpot
                        endif
                        !print *,'pairpot=0'
                        if ((sepn.gt.(distmin)).and. &
                            (sepn.lt.(distmax))) then
                            if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
                             write(54,*) sepn,pairpot
                            endif
                            LrgVal=max(LrgVal,sqrt(pairpot**2))
                        endif
                    endif
                enddo
                if (i.eq.1) then
                    pairpot_out(1) = 2d0*pairpot_out(2) - pairpot_out(3)
                endif
                if (lmax.eq.0) then
                   write(58,'(e24.16,e24.16,e24.16,e24.16,e24.16)') &
                       pairpot_out(1),pairpot_out(2),pairpot_out(3), &
                       pairpot_out(4),pairpot_out(5)
                endif
            enddo

            if (dlpolyOut.and.(lmax.eq.0)) then
               !Do not want to print zeros for dlpoly input
               Nr_dlpoly=Nr
               do i=2,Nr
                  sepn=dble(i-1)*dble(p_rmax)/dble(Nr-1)
                  if (sepn.eq.p_rmax) then
                      pairpot=0d0
                  else
                      call radpairpot(isp,jsp,sepn,pairpot,tmpArray)
                  endif
                  if (pairpot.eq.0d0) then
                     Nr_dlpoly=i-1
                     exit
                  endif
               enddo
               print *,'Nr_dlpoly=',Nr_dlpoly
               sepn=dble(Nr_dlpoly-1)*dble(p_rmax)/dble(Nr-1)

               write(70,'(A6,A2,A2,A2,A2,I5,A2,e24.16,A2,e24.16)') 'PAIR  ', &
                  element(speciestoZ(isp)),'  ', &
                  element(speciestoZ(jsp)),'  ', Nr_dlpoly,'  ',0d0,'  ',sepn
               do i=0,Nr_dlpoly-4,4
                   !Write out four values per line
                   do j=1,4
                       sepn=dble(i+j-1)*dble(p_rmax)/dble(Nr-1)
                       if ((sepn.eq.0d0).or.(sepn.eq.p_rmax)) then
                           pairpot=0d0 !Don't call radpairpot in this case,
                           !as the SPLINT call will fail.
                       else
                           call radpairpot(isp,jsp,sepn,pairpot,tmpArray)
                       endif
                       pairpot_out(j)=pairpot
                   enddo
                   if (i.eq.0) then
                       pairpot_out(1) = 2d0*pairpot_out(2) - pairpot_out(3)
                   endif
                   write(70,'(e24.16,e24.16,e24.16,e24.16)') &
                       pairpot_out(1),pairpot_out(2),pairpot_out(3), &
                       pairpot_out(4)
               enddo

               !add remaining, non multiple of four, meam_out values to end
               if (Nr_dlpoly-i.gt.0) then
                  do j=1,Nr_dlpoly-i
                       sepn=dble(i+j-1)*dble(p_rmax)/dble(Nr-1)
                       if ((sepn.eq.0d0).or.(sepn.eq.p_rmax)) then
                           pairpot=0d0 !Don't call radpairpot in this case,
                           !as the SPLINT call will fail.
                       else
                           call radpairpot(isp,jsp,sepn,pairpot,tmpArray)
                       endif
                       pairpot_out(j)=pairpot
                  enddo
               endif
               if ((Nr_dlpoly-i).eq.1) then
                  write(70,'(e24.16)') pairpot_out(1)
               elseif ((Nr_dlpoly-i).eq.2) then
                  write(70,'(e24.16,e24.16)') pairpot_out(1),pairpot_out(2)
               elseif ((Nr_dlpoly-i).eq.3) then
                  write(70,'(e24.16,e24.16,e24.16)') pairpot_out(1),pairpot_out(2),pairpot_out(3)
               endif
            endif

            if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
               close(53)
               close(54)
            endif
       enddo
    enddo

    !Append to the end of the file the positions of cutoffs
    if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then
       cnt=1
       do isp=1,maxspecies
          do jsp=1,maxspecies
             if (jsp.ge.isp) then
                filename="pairpot"
                write(string1,'(I1)') jsp
                filename=trim(filename)//trim(string1)
                write(string1,'(I1)') isp
                filename=trim(filename)//trim(string1)
                open(54,file=trim(adjustl(filename)),status="old",position="append",action='write')
                !Append to the end of the file the positions of cutoffs
                do i=2*m3+(4+lmax)*m1+12*lm1*m2+(cnt-1)*32+2,2*m3+(4+lmax)*m1+12*lm1*m2+cnt*32,2
                   if (p(i).ne.0d0) then
                      if (all) then
                       write(54,*)
                       write(54,*) p(i),0d0
                       write(54,*) p(i),LrgVal/5d0
                      endif
                   endif
                enddo
                close(54)
             endif
             cnt=cnt+1
          enddo
       enddo
    endif

    !Plot pair-potentials for Camelion
    if ((all.eqv..true.).and.(potFuncOut.eqv..true.)) then

       filename=""
       do i=1,maxspecies
          write(string1,'(A2)') element(speciestoZ(i))
          filename=trim(filename)//trim(string1)
       enddo
       string2='_MEAM.pv'
       filename=trim(filename)//trim(string2)
       open(63,file=trim(adjustl(filename)))

       do isp=1,maxspecies
          do jsp=1,maxspecies
             if (jsp.ge.isp) then

                do i=1,Npot
                   sepn=sqrt(deltar2*dble(i))
                   call radpairpot(isp,jsp,sepn,pairpot,tmpArray)
                   pairpot_out(1)=pairpot
                   sepn=sepn+delta
                   call radpairpot(isp,jsp,sepn,pairpot,tmpArray)
                   sepn=sepn-delta
                   pairpot_out(2)=sepn*(pairpot-pairpot_out(1))/delta
                   !calc pairpot and r*d pairpot/dr
                   if (all) then
                      write(63,*) pairpot_out(1)/0.0872d0, &
                          pairpot_out(2)/0.0872d0
                   endif
                enddo

             endif
          enddo
       enddo

       close(63)

    endif

    gspecies(1,1)=gspeciesprev
    gn_inequivalentsites(1)=gn_inequivalentsites_backup

end subroutine plotfunctions
