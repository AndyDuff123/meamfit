subroutine writederivedquantities(outch)

    !---------------------------------------------------------------------c
    !
    !     Prints a set of user defined quantities derived from the
    !     energies (or potentially also the force components). This
    !     subroutine should be adjusted manually by the user to print out
    !     the desired quantities.
    !
    !     Called by:     program MEAMfit
    !     Calls:         -
    !     Arguments:     outch
    !     Returns:       -
    !     Files read:    -
    !     Files written: Depends on value of outch (6=standard output)
    !
    !     Andy Duff, Feb 2011.
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------------c

    use m_datapoints
    use m_optimization
    use m_geometry
    use m_filenames

    implicit none

    integer outch,i,idatapoint
    real(8) ECoct_fitted,ECtet_fitted,E2C100b_fitted,EFe_fitted, &
        ECoct_true,ECtet_true,E2C100b_true,EFe_true, &
        E2C111_fitted,E2C111_true,E2C1h0_fitted,E2C1h0_true, &
        E2C11h_fitted,E2C11h_true, &
        E2C110a_fitted,E2C110a_true,E2C110b_fitted,E2C110b_true, &
        EFeV_fitted,EFeV_true,E2CVcis_fitted,E2CVcis_true, &
        E2CVtrans_fitted,E2CVtrans_true, &
        E3CVfac_fitted,E3CVfac_true, &
        E3CVmer_fitted,E3CVmer_true, &
        ECV11h_fitted,ECV11h_true, &
        ECV1h0_fitted,ECV1h0_true, &
        ECV1hh_fitted,ECV1hh_true, &
        ECVh00_fitted,ECVh00_true, &
        ECVj00_fitted,ECVj00_true, &
        ECVhh0_fitted,ECVhh0_true, &
        ECVjh0_fitted,ECVjh0_true, &
        ECVjj0_fitted,ECVjj0_true, &
        ECVj1h_fitted,ECVj1h_true, &
        ECVsubs_fitted,ECVsubs_true, &
        ESI_fitted,ESI_true, &
        ECSIh00a_fitted,ECSIh00a_true

    ECoct_true=0d0
    ECtet_true=0d0
    E2C100b_true=0d0
    E2C111_true=0d0
    E2C1h0_true=0d0
    E2C11h_true=0d0
    E2C110a_true=0d0
    E2C110b_true=0d0
    ECVh00_true=0d0
    ECVj00_true=0d0
    ECVhh0_true=0d0
    ECV1h0_true=0d0
    ECV1hh_true=0d0
    ECV11h_true=0d0
    ECVjh0_true=0d0
    ECVjj0_true=0d0
    ECVj1h_true=0d0
    ECVsubs_true=0d0
    E2CVtrans_true=0d0
    E2CVcis_true=0d0
    E3CVfac_true=0d0
    E3CVmer_true=0d0
    EFe_true=0d0
    EFeV_true=0d0
    ESI_true=0d0
    ECSIh00a_true=0d0

    idatapoint=1
    do i=1,nstruct
        if (computeForces(i)) then
            idatapoint=idatapoint+3*gn_forces(i)
        elseif (computeForces(i).eqv..false.) then
            if (strucnames(i).eq.'Coct') then
                ECoct_fitted=fitdata(idatapoint)
                ECoct_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'Ctet') then
                ECtet_fitted=fitdata(idatapoint)
                ECtet_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'2C100b') then
                E2C100b_fitted=fitdata(idatapoint)
                E2C100b_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'2C1h0') then
                E2C1h0_fitted=fitdata(idatapoint)
                E2C1h0_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'2C110a') then
                E2C110a_fitted=fitdata(idatapoint)
                E2C110a_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'2C110b') then
                E2C110b_fitted=fitdata(idatapoint)
                E2C110b_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'2C11h') then
                E2C11h_fitted=fitdata(idatapoint)
                E2C11h_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'2C111') then
                E2C111_fitted=fitdata(idatapoint)
                E2C111_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'Fe') then
                EFe_fitted=fitdata(idatapoint)
                EFe_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'FeV') then
                EFeV_fitted=fitdata(idatapoint)
                EFeV_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'2CVcis') then
                E2CVcis_fitted=fitdata(idatapoint)
                E2CVcis_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'2CVtrans') then
                E2CVtrans_fitted=fitdata(idatapoint)
                E2CVtrans_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'3CVfac') then
                E3CVfac_fitted=fitdata(idatapoint)
                E3CVfac_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'3CVmer') then
                E3CVmer_fitted=fitdata(idatapoint)
                E3CVmer_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CVh00') then
                ECVh00_fitted=fitdata(idatapoint)
                ECVh00_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CVj00') then
                ECVj00_fitted=fitdata(idatapoint)
                ECVj00_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CVhh0') then
                ECVhh0_fitted=fitdata(idatapoint)
                ECVhh0_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CVjj0') then
                ECVjj0_fitted=fitdata(idatapoint)
                ECVjj0_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CV11h') then
                ECV11h_fitted=fitdata(idatapoint)
                ECV11h_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CV1h0') then
                ECV1h0_fitted=fitdata(idatapoint)
                ECV1h0_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CV1hh') then
                ECV1hh_fitted=fitdata(idatapoint)
                ECV1hh_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CVjh0') then
                ECVjh0_fitted=fitdata(idatapoint)
                ECVjh0_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CVj1h') then
                ECVj1h_fitted=fitdata(idatapoint)
                ECVj1h_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CVsubs') then
                ECVsubs_fitted=fitdata(idatapoint)
                ECVsubs_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'SI') then
                ESI_fitted=fitdata(idatapoint)
                ESI_true=truedata(idatapoint)
            elseif (strucnames(i).eq.'CSIh00a') then
                ECSIh00a_fitted=fitdata(idatapoint)
                ECSIh00a_true=truedata(idatapoint)
            endif
            idatapoint=idatapoint+1
        endif
    enddo

    write(outch,*)
    write(outch,*) 'Derived quantities:'
    write(outch,*)
    write(outch,*) 'Structure          fitdata', &
        '       truedata'
    write(outch,*) '----------------------------------------', &
        '----'
    if ((ECoct_true.ne.0d0).and.(ECtet_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_migration       ', &
            ECtet_fitted-ECoct_fitted,'  ',ECtet_true-ECoct_true
    endif
    if ((ECoct_true.ne.0d0).and.(E2C100b_true.ne.0d0).and. &
        (EFe_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(2C100b)       ', &
            E2C100b_fitted+EFe_fitted-2d0*ECoct_fitted,'  ', &
            E2C100b_true+EFe_true-2d0*ECoct_true
    endif
    if ((ECoct_true.ne.0d0).and.(E2C1h0_true.ne.0d0).and. &
        (EFe_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(2C1h0)        ', &
            E2C1h0_fitted+EFe_fitted-2d0*ECoct_fitted,'  ', &
            E2C1h0_true+EFe_true-2d0*ECoct_true
    endif
    if ((ECoct_true.ne.0d0).and.(E2C110a_true.ne.0d0).and. &
        (EFe_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(2C110a)       ', &
            E2C110a_fitted+EFe_fitted-2d0*ECoct_fitted,'  ', &
            E2C110a_true+EFe_true-2d0*ECoct_true
    endif
    if ((ECoct_true.ne.0d0).and.(E2C110b_true.ne.0d0).and. &
        (EFe_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(2C110b)       ', &
            E2C110b_fitted+EFe_fitted-2d0*ECoct_fitted,'  ', &
            E2C110b_true+EFe_true-2d0*ECoct_true
    endif
    if ((ECoct_true.ne.0d0).and.(E2C11h_true.ne.0d0).and. &
        (EFe_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(2C11h)        ', &
            E2C11h_fitted+EFe_fitted-2d0*ECoct_fitted,'  ', &
            E2C11h_true+EFe_true-2d0*ECoct_true
    endif
    if ((ECoct_true.ne.0d0).and.(E2C111_true.ne.0d0).and. &
        (EFe_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(2C111)        ', &
            E2C111_fitted+EFe_fitted-2d0*ECoct_fitted,'  ', &
            E2C111_true+EFe_true-2d0*ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECVh00_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CVh00)        ', &
            ECVh00_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECVh00_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECVj00_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CVj00)        ', &
            ECVj00_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECVj00_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECVhh0_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CVhh0)        ', &
            ECVhh0_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECVhh0_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECVjj0_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CVjj0)        ', &
            ECVjj0_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECVjj0_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECV1h0_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CV1h0)        ', &
            ECV1h0_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECV1h0_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECV1hh_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CV1hh)        ', &
            ECV1hh_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECV1hh_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECV11h_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CV11h)        ', &
            ECV11h_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECV11h_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECVjh0_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CVjh0)        ', &
            ECVjh0_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECVjh0_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECVj1h_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CVj1h)        ', &
            ECVj1h_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECVj1h_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(EFe_true.ne.0d0) &
        .and.(ECVsubs_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CVsubs)       ', &
            ECVsubs_fitted+EFe_fitted-EFeV_fitted-ECoct_fitted,'  ', &
            ECVsubs_true+EFe_true-EFeV_true-ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(E2CVcis_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(2CVcis)       ', &
            E2CVcis_fitted+2d0*EFe_fitted-EFeV_fitted-2d0*ECoct_fitted,'  ', &
            E2CVcis_true+2d0*EFe_true-EFeV_true-2d0*ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(E2CVtrans_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(2CVtrans)     ', &
            E2CVtrans_fitted+2d0*EFe_fitted-EFeV_fitted-2d0*ECoct_fitted,'  ' &
            ,E2CVtrans_true+2d0*EFe_true-EFeV_true-2d0*ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(E3CVmer_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(3CVmer)       ', &
            E3CVmer_fitted+3d0*EFe_fitted-EFeV_fitted-3d0*ECoct_fitted,'  ' &
            ,E3CVmer_true+3d0*EFe_true-EFeV_true-3d0*ECoct_true
    endif
    if ((EFeV_true.ne.0d0).and.(E3CVfac_true.ne.0d0).and. &
        (ECoct_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(3CVfac)       ', &
            E3CVfac_fitted+3d0*EFe_fitted-EFeV_fitted-3d0*ECoct_fitted,'  ' &
            ,E3CVfac_true+3d0*EFe_true-EFeV_true-3d0*ECoct_true
    endif
    if ((ESI_true.ne.0d0).and.(ECSIh00a_true.ne.0d0).and. &
        (ECoct_true.ne.0d0).and.(EFe_true.ne.0d0)) then
        write(outch,'(A19,F12.5,A2,F12.5)') 'E_b(CSIh00a)      ', &
            ECSIh00a_fitted+EFe_fitted-ESI_fitted-ECoct_fitted,'  ' &
            ,ECSIh00a_true+EFe_true-ESI_true-ECoct_true
    endif

end subroutine writederivedquantities
