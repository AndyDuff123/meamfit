

!--------------------  M O D U L E     I N F O  ----------------------c
!
!
!     Copyright (c) 2018, STFC

module m_generalinfo
    !General stuff that is carried from the main header routines
    logical readpotfile,settingsfileexist,writeExPot,writecfg,contjob,writePot,analyzeVib,lammpsDuringOpt
    real(8) p_rmax !longest interatomic distance to be considered 6
    real(8), parameter:: smllnum=1.120d-16 !Smallest number for which 1 + smllnum /ne 1
    real(8), parameter:: gammaMin=-55 !see 'Gamma limit (ii) & (iii)' hand-written notes -55
    integer,parameter:: nsigdig=15 !=floor(-log10(smllnum))
    integer tStart,verbosity
end module m_generalinfo
