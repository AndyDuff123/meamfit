subroutine radialdensityfunction_onlyiatom(iatom)

    !     Copyright (c) 2018, STFC

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity   !f_j_L_ij

    implicit none

    integer i,j,jj,nni,iaux(1),isp,jsp,iatom

    iaux=maxval(gn_neighbors(1:gn_inequivalentsites(istr),istr))

    isp=gspecies(iatom,istr)
    nni=gn_neighbors(iatom,istr)


    do jj=1,nni
        j=gneighborlist(jj,iatom,istr)
        jsp=gspecies(j,istr)
        rij=diststr(jj,iatom,0,0,istr)
    
        if (thiaccptindepndt.eqv..true.) then
            call raddens(rij,1,jsp,fjlij(0:lmax,jj,iatom),dfjlij_dpara(1:12,0:lmax,jj,iatom))
        else
            call raddens(rij,isp,jsp,fjlij(0:lmax,jj,iatom),dfjlij_dpara(1:12,0:lmax,jj,iatom))
        endif
    enddo
    
    do i=1,gn_inequivalentsites(istr)
    
        if (i.ne.iatom) then
            isp=gspecies(i,istr)
            nni=gn_neighbors(i,istr)
    
            do jj=1,nni
                j=gneighborlist(jj,i,istr)
                if (j.eq.iatom) then
                    jsp=gspecies(j,istr)
                    rij=diststr(jj,i,0,0,istr)
    
                    if (thiaccptindepndt.eqv..true.) then
                        call raddens(rij,1,jsp,fjlij(0:lmax,jj,i),dfjlij_dpara(1:12,0:lmax,jj,i))
                    else
                        call raddens(rij,isp,jsp,fjlij(0:lmax,jj,i),dfjlij_dpara(1:12,0:lmax,jj,i))
                    endif
                endif
            enddo
    
        endif
    
    enddo

end subroutine radialdensityfunction_onlyiatom


