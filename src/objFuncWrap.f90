
subroutine objFuncWrap(nfreep, popt, nf, Fval, uip, urp, ufp )

    !-------------------------------------------------------------------c
    !
    !     Wrapper subroutine to call objectiveFunction subroutine,
    !     called by toms611 quasi Newton optimization subroutines.
    !
    !     Andy Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_datapoints
    use m_geometry
    use m_optimization
    use m_generalinfo
    use m_meamparameters
    use m_filenames
    use m_poscar
    use m_electrondensity
    use m_atomproperties
    use m_plotfiles
    use m_objectiveFunction
    use m_observables

    implicit none

    logical signflipped(np),minimumfailed
    integer ipara,ispc,iatom,ip,idatapoint,nfreep,foundM,errDenom
    !integer tottimeBefore,tottimeAfter
    integer nf,uip(*)
    real(8) Fval,dF_dpopt_norm,dFen_err,dFfrc_err,dFstr_err,dF_err
    real(8) urp(nfreep,3)
    real(8) popt(nfreep)

    !The following variables are for testing the analytic derivatives of the objective function
    !Note that I have not been consistent with whether I define _dmeamtau arrays
    !as from 0:lmax or 1:lmax. Need to tidy this up. The code works, as is, but
    !looks messy.
    integer isp,jsp,l,iiCoeff,iiCutoff
    real(8) F_prev,Fen_prev,Ffrc_prev,Fstr_prev,dFen_dmeamtau_prev(1:lmax,maxspecies),dFen_draddens_prev(12,0:lmax,maxspecies), &
         dFen_dmeame0_prev(maxspecies),dFen_dmeamrho0_prev(maxspecies),dFen_dmeamemb3_prev(maxspecies),dFen_dmeamemb4_prev(maxspecies), &
         dFen_dpairpot_prev(32,maxspecies,maxspecies), &
         dFfrc_dmeamtau_prev(1:lmax,maxspecies),dFfrc_draddens_prev(12,0:lmax,maxspecies), &
         dFfrc_dmeame0_prev(maxspecies),dFfrc_dmeamrho0_prev(maxspecies),dFfrc_dmeamemb3_prev(maxspecies),dFfrc_dmeamemb4_prev(maxspecies), &
         dFfrc_dpairpot_prev(32,maxspecies,maxspecies), &
         dFstr_dmeamtau_prev(1:lmax,maxspecies),dFstr_draddens_prev(12,0:lmax,maxspecies), &
         dFstr_dmeame0_prev(maxspecies),dFstr_dmeamrho0_prev(maxspecies),dFstr_dmeamemb3_prev(maxspecies),dFstr_dmeamemb4_prev(maxspecies), &
         dFstr_dpairpot_prev(32,maxspecies,maxspecies), &
         dF_dmeamtau_prev(1:lmax,maxspecies),dF_draddens_prev(12,0:lmax,maxspecies), &
         dF_dmeame0_prev(maxspecies),dF_dmeamrho0_prev(maxspecies),dF_dmeamemb3_prev(maxspecies),dF_dmeamemb4_prev(maxspecies), &
         dF_dpairpot_prev(32,maxspecies,maxspecies)
    real(8), parameter:: dist=1d-8 !Distance of displacement used in computing finite difference derivatives w.r.t atomic positions
    real(8), parameter:: deltaPara=1d-8 !Change in parameter used in computing finite difference derivatives w.r.t potential parameters

    external ufp

    !Convert parameters back to their correct values
    !(the popt array is used for the conjugate gradient optimizer, which prefers
    !numbers of a similar order)
    if (np.ge.1) then
       ip=0
       do ipara=1,np
           if ( (freep(ipara).eq.1).or.(freep(ipara).eq.2) ) then
               ip=ip+1
               if (p_orig(ipara).ne.0d0) then
                   p(ipara)=popt(ip)*(p_orig(ipara)/poptStrt)
               else
                   p(ipara)=popt(ip)
               endif
           endif
       enddo
    endif
    if ((popt(1).gt.0d0).or.(popt(1).le.0d0)) then
       !
    else
       !print *,'popt=',popt
    endif

    call parachange1(signflipped,nf) !Ensure meam parameters have
                                            !the correct signs and are in the correct range
    if (cutoffOutOfBounds) then
       print *,'radial function cutoffs out of bounds [greater than (less than) maximum (minimum) cutoff]'
       nf=0 !sumsl will try a smaller step length
       return
    endif

    call p_to_variables !Convert variables in p() to 'sensibly-named' variables
    !call gettime(tottimeBefore) !debug line
    call objectiveFunction(.false.)
    !call gettime(tottimeAfter) !debug line
    !print *,'time of objectivefunction eval=',tottimeAfter-tottimeBefore !debug line
    !print *,'F=',F
    !print *,'dF_denconst(1)=',dF_denconst(1)
    !print *,'enconst=',p(m4+2*m2+1)
    !print * 
    !print*,'Fen, Ffrc, Fstr, F, nEn, nFrcComp, nStrComp, varEn, varFrc, varStr=', &
    !                 Fen, Ffrc, Fstr, F, nEn, nFrcComp, nStrComp, varEn, varFrc, varStr

    if (((negElecDens.eqv..true.).and.(negElecDensAllow.eqv..false.)).or.(negBackDens.eqv..true.).or.(gammaOutOfBounds.eqv..true.).or.(finiteF.eqv..false.)) then
       !Return to sumsl with my 'kill command' (nf=-1)
       if (debug) then
          print *,'Objective function evaluation encountered problem:'
          if ((negElecDens.eqv..true.).and.(negElecDensAllow.eqv..false.)) then
             print *,'   Negative electron density occured'
          endif
          if (negBackDens) then
             print *,'   Negative background density occured'
          endif
          if (gammaOutOfBounds) then
             print *,'   Gamma exceeded bounds'
          endif
          if (finiteF.eqv..false.) then
             print *,'   F returned NaN'
          endif
       endif
       nf=-1
       return
    endif

    ! Calculate numerical derivatives of objective function w.r.t popt
    !   print *,'numerical derivatives:'
    !   F_prev=F
    !   ip=0
    !   do ipara=1,np
    !       if ( (freep(ipara).eq.1).or.(freep(ipara).eq.2) ) then
    !           ip=ip+1
    !           popt(ip)=popt(ip)+deltaPara
    !           p(ipara)=popt(ip)*(p_orig(ipara)/poptStrt)
    !           call parachange1(signflipped,nf) !Ensure meam parameters have
    !                                                   !the correct signs and are in the
    !                                                   !correct range
    !           call p_to_variables !Convert variables in p() to 'sensibly-named' variables
    !           call objectiveFunction(.false.)
    !           print *,'dF_dpopt(',ip,')=',(F-F_prev)/deltaPara
    !           popt(ip)=popt(ip)-deltaPara
    !           p(ipara)=popt(ip)*(p_orig(ipara)/poptStrt)
    !       endif
    !   enddo
    !   !stop

    if (numDiff) then
       !---- Test analytic derivatives of objective function w.r.t 'sensible-named' variables ----
       dFen_err=0d0
       dFfrc_err=0d0
       dFstr_err=0d0
       dF_err=0d0
       errDenom=0
       F_prev=F
       Fen_prev=Fen
       dFen_dmeamtau_prev=dFen_dmeamtau
       dFen_draddens_prev=dFen_draddens
       dFen_dmeame0_prev=dFen_dmeame0
       dFen_dmeamrho0_prev=dFen_dmeamrho0
       dFen_dmeamemb3_prev=dFen_dmeamemb3
       dFen_dmeamemb4_prev=dFen_dmeamemb4
       dFen_dpairpot_prev=dFen_dpairpot
       if ( ANY( computeForces.eqv..true. ) ) then
          Ffrc_prev=Ffrc
          Fstr_prev=Fstr
          dFfrc_dmeamtau_prev=dFfrc_dmeamtau
          dFfrc_draddens_prev=dFfrc_draddens
          dFfrc_dmeame0_prev=dFfrc_dmeame0
          dFfrc_dmeamrho0_prev=dFfrc_dmeamrho0
          dFfrc_dmeamemb3_prev=dFfrc_dmeamemb3
          dFfrc_dmeamemb4_prev=dFfrc_dmeamemb4
          dFfrc_dpairpot_prev=dFfrc_dpairpot
          dFstr_dmeamtau_prev=dFstr_dmeamtau
          dFstr_draddens_prev=dFstr_draddens
          dFstr_dmeame0_prev=dFstr_dmeame0
          dFstr_dmeamrho0_prev=dFstr_dmeamrho0
          dFstr_dmeamemb3_prev=dFstr_dmeamemb3
          dFstr_dmeamemb4_prev=dFstr_dmeamemb4
          dFstr_dpairpot_prev=dFstr_dpairpot
       endif
       dF_dmeamtau_prev=dF_dmeamtau
       dF_draddens_prev=dF_draddens
       dF_dmeame0_prev=dF_dmeame0
       dF_dmeamrho0_prev=dF_dmeamrho0
       dF_dmeamemb3_prev=dF_dmeamemb3
       dF_dmeamemb4_prev=dF_dmeamemb4
       dF_dpairpot_prev=dF_dpairpot
    
       print *,'numerical:'
       print *
       print *,'meamtau:'
       do isp=1,maxspecies
          do l=1,lmax
             meamtau(l,isp)=meamtau(l,isp)+deltaPara
             call objectiveFunction(.false.)
             meamtau(l,isp)=meamtau(l,isp)-deltaPara 
             print *,'dFen_dmeamtau(',l,isp,')=',dFen_dmeamtau_prev(l,isp)/(dble(nEn)*varEn)
             print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
             dFen_err=dFen_err+(dFen_dmeamtau_prev(l,isp)/(dble(nEn)*varEn) - (Fen-Fen_prev)/deltaPara)**2
             if ( ANY( computeForces.eqv..true. ) ) then
                print *,'dFfrc_dmeamtau(',l,isp,')=',dFfrc_dmeamtau_prev(l,isp)/(dble(nFrcComp)*varFrc)
                print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
                dFfrc_err=dFfrc_err+(dFfrc_dmeamtau_prev(l,isp)/(dble(nFrcComp)*varFrc) - (Ffrc-Ffrc_prev)/deltaPara)**2
                print *,'dFstr_dmeamtau(',l,isp,')=',dFstr_dmeamtau_prev(l,isp)/(dble(nStrComp)*varStr)
                print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
                dFstr_err=dFstr_err+(dFstr_dmeamtau_prev(l,isp)/(dble(nStrComp)*varStr) - (Fstr-Fstr_prev)/deltaPara)**2
             endif
             print *,'dF_dmeamtau(',l,isp,')=',dF_dmeamtau_prev(l,isp)
             print *,'...numerical = ',(F-F_prev)/deltaPara
             dF_err=dF_err+(dF_dmeamtau_prev(l,isp) - (F-F_prev)/deltaPara)**2
             errDenom=errDenom+1
          enddo
       enddo
       !stop
       print *,'meamrhodecay:'
       do jsp=1,maxspecies
          do l=0,lmax
             do iiCoeff=1,noptdensityCoeff(l,1,jsp)
                ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
                meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
                call objectiveFunction(.false.)
                meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
                print *,'dFen_draddens(',ip,l,jsp,')=',dFen_draddens_prev(ip,l,jsp)/(dble(nEn)*varEn)
                print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
                dFen_err=dFen_err+(dFen_draddens_prev(ip,l,jsp)/(dble(nEn)*varEn) - (Fen-Fen_prev)/deltaPara )**2
                if ( ANY( computeForces.eqv..true. ) ) then
                   print *,'dFfrc_draddens(',ip,l,jsp,')=',dFfrc_draddens_prev(ip,l,jsp)/(dble(nFrcComp)*varFrc)
                   print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
                   dFfrc_err=dFfrc_err+(dFfrc_draddens_prev(ip,l,jsp)/(dble(nFrcComp)*varFrc) - (Ffrc-Ffrc_prev)/deltaPara)**2
                   print *,'dFstr_draddens(',ip,l,jsp,')=',dFstr_draddens_prev(ip,l,jsp)/(dble(nStrComp)*varStr)
                   print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
                   dFstr_err=dFstr_err+(dFstr_draddens_prev(ip,l,jsp)/(dble(nStrComp)*varStr) - (Fstr-Fstr_prev)/deltaPara)**2
                endif
                print *,'dF_draddens(',ip,l,jsp,')=',dF_draddens_prev(ip,l,jsp)
                print *,'...numerical = ',(F-F_prev)/deltaPara
                dF_err=dF_err+(dF_draddens_prev(ip,l,jsp) - (F-F_prev)/deltaPara)**2
                errDenom=errDenom+1
                !stop
             enddo
             do iiCutoff=1,noptdensityCutoff(l,1,jsp)
                ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
                meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
                call objectiveFunction(.false.)
                meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
                print *,'dFen_draddens(',ip,l,jsp,')=',dFen_draddens_prev(ip,l,jsp)/(dble(nEn)*varEn)
                print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
                dFen_err=dFen_err+(dFen_draddens_prev(ip,l,jsp)/(dble(nEn)*varEn) - (Fen-Fen_prev)/deltaPara )**2
                if ( ANY( computeForces.eqv..true. ) ) then
                   print *,'dFfrc_draddens(',ip,l,jsp,')=',dFfrc_draddens_prev(ip,l,jsp)/(dble(nFrcComp)*varFrc)
                   print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
                   dFfrc_err=dFfrc_err+(dFfrc_draddens_prev(ip,l,jsp)/(dble(nFrcComp)*varFrc) - (Ffrc-Ffrc_prev)/deltaPara)**2
                   print *,'dFstr_draddens(',ip,l,jsp,')=',dFstr_draddens_prev(ip,l,jsp)/(dble(nStrComp)*varStr)
                   print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
                   dFstr_err=dFstr_err+(dFstr_draddens_prev(ip,l,jsp)/(dble(nStrComp)*varStr) - (Fstr-Fstr_prev)/deltaPara)**2
                endif
                print *,'dF_draddens(',ip,l,jsp,')=',dF_draddens_prev(ip,l,jsp)
                print *,'...numerical = ',(F-F_prev)/deltaPara
                dF_err=dF_err+(dF_draddens_prev(ip,l,jsp) - (F-F_prev)/deltaPara)**2
                errDenom=errDenom+1
             enddo
          enddo
       enddo
       !stop
       print *,'embedding functions:'
       do isp=1,maxspecies
          meame0(isp)=meame0(isp)+deltaPara
          call objectiveFunction(.false.)
          meame0(isp)=meame0(isp)-deltaPara
          print *,'dFen_dmeame0(',isp,')=',dFen_dmeame0_prev(isp)/(dble(nEn)*varEn)
          print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
          if ( ANY( computeForces.eqv..true. ) ) then
             print *,'dFfrc_dmeame0(',isp,')=',dFfrc_dmeame0_prev(isp)/(dble(nFrcComp)*varFrc)
             print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
             print *,'dFstr_dmeame0(',isp,')=',dFstr_dmeame0_prev(isp)/(dble(nStrComp)*varStr)
             print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
          endif
          print *,'dF_dmeame0(',isp,')=',dF_dmeame0_prev(isp)
          print *,'...numerical = ',(F-F_prev)/deltaPara
       enddo
       do isp=1,maxspecies
          meamrho0(isp)=meamrho0(isp)+deltaPara
          call objectiveFunction(.false.)
          meamrho0(isp)=meamrho0(isp)-deltaPara
          print *,'dFen_dmeamrho0(',isp,')=',dFen_dmeamrho0_prev(isp)/(dble(nEn)*varEn)
          print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
          if ( ANY( computeForces.eqv..true. ) ) then
             print *,'dFfrc_dmeamrho0(',isp,')=',dFfrc_dmeamrho0_prev(isp)/(dble(nFrcComp)*varFrc)
             print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
             print *,'dFstr_dmeamrho0(',isp,')=',dFstr_dmeamrho0_prev(isp)/(dble(nStrComp)*varStr)
             print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
          endif
          print *,'dF_dmeamrho0(',isp,')=',dF_dmeamrho0_prev(isp)
          print *,'...numerical = ',(F-F_prev)/deltaPara
       enddo
       do isp=1,maxspecies
          meamemb3(isp)=meamemb3(isp)+deltaPara
          call objectiveFunction(.false.)
          meamemb3(isp)=meamemb3(isp)-deltaPara
          print *,'dFen_dmeamemb3(',isp,')=',dFen_dmeamemb3_prev(isp)/(dble(nEn)*varEn)
          print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
          if ( ANY( computeForces.eqv..true. ) ) then
             print *,'dFfrc_dmeamemb3(',isp,')=',dFfrc_dmeamemb3_prev(isp)/(dble(nFrcComp)*varFrc)
             print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
             print *,'dFstr_dmeamemb3(',isp,')=',dFstr_dmeamemb3_prev(isp)/(dble(nStrComp)*varStr)
             print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
          endif
          print *,'dF_dmeamemb3(',isp,')=',dF_dmeamemb3_prev(isp)
          print *,'...numerical = ',(F-F_prev)/deltaPara
       enddo
       do isp=1,maxspecies
          meamemb4(isp)=meamemb4(isp)+deltaPara
          call objectiveFunction(.false.)
          meamemb4(isp)=meamemb4(isp)-deltaPara
          print *,'dFen_dmeamemb4(',isp,')=',dFen_dmeamemb4_prev(isp)/(dble(nEn)*varEn)
          print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
          if ( ANY( computeForces.eqv..true. ) ) then
             print *,'dFfrc_dmeamemb4(',isp,')=',dFfrc_dmeamemb4_prev(isp)/(dble(nFrcComp)*varFrc)
             print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
             print *,'dFstr_dmeamemb4(',isp,')=',dFstr_dmeamemb4_prev(isp)/(dble(nStrComp)*varStr)
             print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
          endif
          print *,'dF_dmeamemb4(',isp,')=',dF_dmeamemb4_prev(isp)
          print *,'...numerical = ',(F-F_prev)/deltaPara
       enddo
       !  stop
       print *,'pairpotentials:'
       do isp=1,maxspecies
          do jsp=isp,maxspecies
             do iiCoeff=1,noptpairpotCoeff(isp,jsp)
                ip = ioptpairpotCoeff(iiCoeff,isp,jsp)
                pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)+deltaPara
                call objectiveFunction(.false.)
                pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)-deltaPara
                print *,'dFen_dpairpot(',ip,isp,jsp,')=',dFen_dpairpot_prev(ip,isp,jsp)/(dble(nEn)*varEn)
                print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
                dFen_err=dFen_err+(dFen_dpairpot_prev(ip,isp,jsp)/(dble(nEn)*varEn) - (Fen-Fen_prev)/deltaPara)**2
                if ( ANY( computeForces.eqv..true. ) ) then
                   print *,'dFfrc_dpairpot(',ip,isp,jsp,')=',dFfrc_dpairpot_prev(ip,isp,jsp)/(dble(nFrcComp)*varFrc)
                   print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
                   dFfrc_err=dFfrc_err+(dFfrc_dpairpot_prev(ip,isp,jsp)/(dble(nFrcComp)*varFrc) - (Ffrc-Ffrc_prev)/deltaPara)**2
                   print *,'dFstr_dpairpot(',ip,isp,jsp,')=',dFstr_dpairpot_prev(ip,isp,jsp)/(dble(nStrComp)*varStr)
                   print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
                   dFstr_err=dFstr_err+(dFstr_dpairpot_prev(ip,isp,jsp)/(dble(nStrComp)*varStr) - (Fstr-Fstr_prev)/deltaPara)**2
                endif
                print *,'dF_dpairpot(',ip,isp,jsp,')=',dF_dpairpot_prev(ip,isp,jsp)
                print *,'...numerical = ',(F-F_prev)/deltaPara
                dF_err=dF_err+(dF_dpairpot_prev(ip,isp,jsp) - (F-F_prev)/deltaPara)**2
                errDenom=errDenom+1
             enddo
             do iiCutoff=1,noptpairpotCutoff(isp,jsp)
                ip = ioptpairpotCutoff(iiCutoff,isp,jsp)
                pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)+deltaPara
                call objectiveFunction(.false.)
                pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)-deltaPara
                print *,'dFen_dpairpot(',ip,isp,jsp,')=',dFen_dpairpot_prev(ip,isp,jsp)/(dble(nEn)*varEn)
                print *,'...numerical = ',(Fen-Fen_prev)/deltaPara
                dFen_err=dFen_err+(dFen_dpairpot_prev(ip,isp,jsp)/(dble(nEn)*varEn) - (Fen-Fen_prev)/deltaPara)**2
                if ( ANY( computeForces.eqv..true. ) ) then
                   print *,'dFfrc_dpairpot(',ip,isp,jsp,')=',dFfrc_dpairpot_prev(ip,isp,jsp)/(dble(nFrcComp)*varFrc)
                   print *,'...numerical = ',(Ffrc-Ffrc_prev)/deltaPara
                   dFfrc_err=dFfrc_err+(dFfrc_dpairpot_prev(ip,isp,jsp)/(dble(nFrcComp)*varFrc) - (Ffrc-Ffrc_prev)/deltaPara)**2
                   print *,'dFstr_dpairpot(',ip,isp,jsp,')=',dFstr_dpairpot_prev(ip,isp,jsp)/(dble(nStrComp)*varStr)
                   print *,'...numerical = ',(Fstr-Fstr_prev)/deltaPara
                   dFstr_err=dFstr_err+(dFstr_dpairpot_prev(ip,isp,jsp)/(dble(nStrComp)*varStr) - (Fstr-Fstr_prev)/deltaPara)**2
                endif
                print *,'dF_dpairpot(',ip,isp,jsp,')=',dF_dpairpot_prev(ip,isp,jsp)
                print *,'...numerical = ',(F-F_prev)/deltaPara
                dF_err=dF_err+(dF_dpairpot_prev(ip,isp,jsp) - (F-F_prev)/deltaPara)**2
                errDenom=errDenom+1
             enddo
    
          enddo
       enddo
       dFen_err=sqrt(dFen_err/dble(errDenom))
       dFfrc_err=sqrt(dFfrc_err/dble(errDenom))
       dFstr_err=sqrt(dFstr_err/dble(errDenom))
       dF_err=sqrt(dF_err/dble(errDenom))
       print *,'rms diff between analytic and numerical obj func derivs:'
       print *,'   Fen:',dFen_err
       print *,'   Ffrc:',dFfrc_err
       print *,'   Fstr:',dFstr_err
       stop
    endif
    !---------------------------------------------------------


    !---- Test for convergence (toms611 does not support this criteria so it has to be tested outside the local optimization subroutine) ----
    if ((Fprev.ne.0d0).and.(Fprev2.ne.0d0)) then
       !Test to see if objective function decrease less than deltaOlimit (starting value = 1) over two subsquent iterations
!       if ((abs(Fprev-F)<deltaOlimit).and.abs(Fprev2-Fprev)<deltaOlimit) then
       if (((F-Fprev)>-deltaOlimit).and.(F.lt.Fprev).and.((Fprev-Fprev2)>-deltaOlimit).and.(Fprev.lt.Fprev2)) then
          print *,'Objective function decrease over two subsequent iterations:',F-Fprev,', ',Fprev-Fprev2,' > -deltaOlimit =',-deltaOlimit,' (and both are _decreases_)'
          nf=-1
       endif
    endif
    Fprev2=Fprev
    Fprev=F
    !----------------------------------------------------------------------------------------------------------------------------------------


    Fval=F !toms611 does not read my modules, so pass F back as Fval

    !print *
    !print *,'Fval=',Fval

    !Convert derivatives of F to dF_dpara
    call dFdmeam_to_dFdpara
    !Convert dF_dpara to dF_dpopt (I.e., just w.r.t paras being
    !optimized)
    !and compute norm of dF_dpopt

    !print *
    !print *,'analytic derivatives:'
    if (np.ge.1) then
       ip=0
       dF_dpopt_norm=0d0
       do ipara=1,np
           if ( freep(ipara).ge.1 ) then
               ip=ip+1
               !print *,'dF_dpara(',ip,')=',dF_dpara(ipara)
               dF_dpopt(ip)=dF_dpara(ipara)*(p_orig(ipara)/poptStrt)
    !           print *,'dF_dpopt(',ip,')=',dF_dpopt(ip)
               dF_dpopt_norm=dF_dpopt_norm+dF_dpopt(ip)**2
           endif
       enddo
    endif
    !stop
    dF_dpopt_norm=sqrt(dF_dpopt_norm/dble(ip))

    !print *,'dF_dpopt=',dF_dpopt
    !print *
    !stop

    !Save gradient for this function evaluation
    !print *,'Saving gradient for this value of nf:',dF_dpopt
    if (nf.gt.100000) then
       print *,'ERROR: nf>100000, please change code'
       stop
    endif
    if (nf.ne.-1) then !nf=-1 is used to signal end of optimizer. In this case if we set the above array we would be out of bounds (and the dF_dpoptStore quantity won't be needed because we 'return' from toms before calling calcg, aka objFuncDerivWrap
       do ip=1,nfreep
          dF_dpoptStore(ip,nf)=dF_dpopt(ip)
       enddo
    endif

end subroutine objFuncWrap


subroutine objFuncDerivWrap(nfreep, popt, nf, FderivVal, uip, urp, ufp )

    !-------------------------------------------------------------------c
    !
    !     Wrapper subroutine to return the analytic derivatives of the
    !     objective function with respect to the potential parameters,
    !     called by toms611 quasi Newton optimization subroutines.
    !     Note there is no need to re-compute these as they will already
    !     have been calculated by 'objFuncWrap'
    !
    !     Andy Duff 2017
    !
    !-------------------------------------------------------------------c

    use m_datapoints
    use m_geometry
    use m_optimization
    use m_generalinfo
    use m_meamparameters
    use m_filenames
    use m_poscar
    use m_electrondensity
    use m_atomproperties
    use m_plotfiles
    use m_objectiveFunction

    implicit none

    logical signflipped(np)
    integer ipara,ispc,iatom,ip,idatapoint,nfreep,foundM
    integer nf,uip(*)
    real(8) FderivVal(nfreep)
    real(8) urp(nfreep,3)
    real(8) popt(nfreep)

    external ufp

    !Extract the required derivative (for function evaluation 'nf') from the
    !array dF_dpoptStore, which has already been filled by objFuncWrap
    do ip=1,nfreep
       FderivVal(ip)=dF_dpoptStore(ip,nf)
    enddo

    !print *,'FderivVal for ',nf,'th function evaluation =',FderivVal
    !stop

end subroutine objFuncDerivWrap
