

module m_screening

    !     Copyright (c) 2018, STFC

    !S_ij parameters, computed in subroutine screening_ij
    real(8), allocatable:: screening(:,:)   !S_ij
end module m_screening
