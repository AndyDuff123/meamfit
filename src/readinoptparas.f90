
subroutine readinoptparasonebyone

    !-------------------------------------------------------------------c
    !
    !     Read in the which potential parameters are to be optimized.
    !
    !     Called by:     readsettings
    !     Calls:         -
    !     Arguments:     -
    !     Returns:       -
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_filenames
    use m_optimization
    use m_atomproperties
    use m_meamparameters !lmax

    implicit none

    logical typelog,foundvariable,equals,found0or1,error
    integer i,j,k,l,typeint,IOstatus,nwords,VarNamePlusEqLength,StringLength,wordLength

    real(8) typedble

    character*80 string
    character*80 VarName,word,VarValue,VarNamePlusEq
    character*80, allocatable:: words(:)
    character*1 tmpstring

    open(unit=1,file=trim(settingsfile))

    VarName="PARASTOOPT"
    foundvariable=.false.
    VarNamePlusEq=trim(VarName)//"="
    do
       read(1,'(a80)',IOSTAT=IOstatus) string
       !Currently not working:
       if (IOstatus.lt.0) then
          exit
       endif
       !if (string(1:9).eq."ENDOFFILE") then
       !   exit
       !endif

       call optionalarg(string,nwords)
       if (nwords.gt.0) then
        allocate(words(1:nwords))
        read(string,*) words

        if (words(1).ne.'#') then

          if (nwords.ge.1) then
             !First check if first word has an equals sign in it (eg,
             !NOOPT=TRUE)
             word=words(1)
             wordLength=len_trim(word)
             equals=.false.
             do i=1,wordLength
               if (word(i:i).eq."=") then
                  equals=.true.
                  !print *,'equals in first word.'
                  exit
               endif
             enddo
             if (equals) then
               if (trim(word(1:i-1)).eq.trim(VarName)) then
                 !print *,trim(word(1:i-1)),'=',trim(VarName)
                 !print *,'i=',i,' wordLength=',wordLength
                 if (i.lt.wordLength) then
                   !print *,'..found it within first word..'
                   foundvariable=.true.
                   exit
                 else
                   if (nwords.ge.2) then
                     !print *,'..found it in second word..'
                     foundvariable=.true.
                     exit
                   endif
                 endif
               endif
             else
               if (trim(words(1)).eq.trim(VarName)) then
                 !print *,trim(words(1)),'=',trim(VarName)
                 if (nwords.ge.2) then
                   if (trim(words(2)).eq."=") then
                     if (nwords.ge.3) then
                       !print *,'..found it in third word..'
                       foundvariable=.true.
                       exit
                     endif
                   else
                     word=words(2)
                     wordLength=len_trim(word)
                     do i=1,wordLength
                       if (word(i:i).eq."=") then
                         equals=.true.
                         !print *,'equals in second word.'
                         exit
                       endif
                     enddo
                     if (equals) then
                       !print *,'..found it in second word..'
                       foundvariable=.true.
                       exit
                     endif
                   endif
                 endif
               endif
             endif
          endif

        endif
        deallocate(words)
       endif
    enddo
    read(1,*) freep(1:m3)  !cmin(maxspecies,maxspecies,maxspecies)
    print *,'freep(',1,':',m3,') : ',freep(1:m3)
    read(1,*) freep(1+m3:2*m3) !cmax(maxspecies,maxspecies,maxspecies)
    print *,'freep(',1+m3,':',2*m3,') :',freep(1+m3:2*m3) 
    if (lmax.gt.0) then
       read(1,*) freep(2*m3+1:2*m3+lmax*m1) !meamtau(1:lmax,maxspecies)
       print *,'freep(',2*m3+1,':',2*m3+lmax*m1,') :',freep(2*m3+1:2*m3+lmax*m1) 
    endif
    read(1,*) freep(2*m3+lmax*m1+1:2*m3+lmax*m1+12*lm1*m2)
    print *,'freep(',2*m3+lmax*m1+1,':',2*m3+lmax*m1+12*lm1*m2,') :',freep(2*m3+lmax*m1+1:2*m3+lmax*m1+12*lm1*m2) 
    !meamrhodecay(6,0:lmax,maxspecies,maxspecies)
    !meamrhodecay(6,0:lmax,maxspecies,maxspecies)
    read(1,*) freep(2*m3+lmax*m1+12*lm1*m2+1:2*m3+m1 &
        +lmax*m1+12*lm1*m2)   !meame0(maxspecies)
    print *,'freep(2*m3+lmax*m1+12*lm1*m2+1:2*m3+m1+lmax*m1+12*lm1*m2) :',freep(2*m3+lmax*m1+12*lm1*m2+1:2*m3+m1+lmax*m1+12*lm1*m2)
    read(1,*) freep(2*m3+m1+lmax*m1+12*lm1*m2+1:2*m3+ &
        2*m1+lmax*m1+12*lm1*m2) !meamrho0(maxspecies)
    print *,'freep(2*m3+m1+lmax*m1+12*lm1*m2+1:2*m3+2*m1+lmax*m1+12*lm1*m2) :',freep(2*m3+m1+lmax*m1+12*lm1*m2+1:2*m3+ 2*m1+lmax*m1+12*lm1*m2) 
    read(1,*) freep(2*m3+2*m1+lmax*m1+12*lm1*m2+1:2*m3+ &
        3*m1+lmax*m1+12*lm1*m2) !meamemb3
    print *,'freep(2*m3+2*m1+lmax*m1+12*lm1*m2+1:2*m3+3*m1+lmax*m1+12*lm1*m2):',freep(2*m3+2*m1+lmax*m1+12*lm1*m2+1:2*m3+3*m1+lmax*m1+12*lm1*m2) 
    read(1,*) freep(2*m3+3*m1+lmax*m1+12*lm1*m2+1:2*m3+ &
        4*m1+lmax*m1+12*lm1*m2) !meamemb4
    print *,'freep(2*m3+3*m1+lmax*m1+12*lm1*m2+1:2*m3+4*m1+lmax*m1+12*lm1*m2) :',freep(2*m3+3*m1+lmax*m1+12*lm1*m2+1:2*m3+4*m1+lmax*m1+12*lm1*m2) 
    read(1,*) freep(2*m3+(4+lmax)*m1+12*lm1*m2+1:m4)
    !pairpotparameter(16,maxspecies,maxspecies)
    print *,'freep(2*m3+(4+lmax)*m1+12*lm1*m2+1:m4) :',freep(2*m3+(4+lmax)*m1+12*lm1*m2+1:m4) 
    read(1,*) freep(m4+1:m4+m2) !rs(maxspecies,maxspecies)
    print *,'freep(m4+1:m4+m2) :',freep(m4+1:m4+m2) 
    read(1,*) freep(m4+m2+1:m4+2*m2) !rc(maxspecies,maxspecies)
    print *,'freep(m4+m2+1:m4+2*m2) :',freep(m4+m2+1:m4+2*m2) 
    read(1,*) freep(m4+2*m2+1:m4+2*m2+m1) !enconst(maxspecies)
    print *,'freep(m4+2*m2+1:m4+2*m2+m1) :',freep(m4+2*m2+1:m4+2*m2+m1) 
    close(1)

    if (holdcutoffs) then

       !In order to use PARASTOOPT=.true. with HOLDCUTOFFS=.true, the input
       !freep values must conform to the following format: The '2' values in the
       !freep matrix corresponding to the cutoff parameters must occur at the
       !start of each parameter block and be continuous. Eg, the section corresponding to
       !meamrhodecay(12,0,1,1) must look like:
       !    2 2 2 2 2 2 0 0 0 0 0 0
       !for example, and not:
       !    0 0 2 2 2 2 0 0 2 2 0 0
       !Furthermore, at present, for a given parameter and for a given pair of
       !species, the number of cutoffs to randomly generate must be the same for
       !each value of l, the angular parameter.
       !Finally, there cannot be mixtures of 1's and 2's in a given parameter
       !block when using HOLDCUTOFFS=.true.

       !Assign values to ncutoffDens(:,:) and nutcoffPairpot(:,:)

       !Haven't coded in yet for ncutoffDens!! nuctoffPairpot follows...
       !(get the relevant code structure from variables_to_p.f90, and build up a
       !structure analagous to that below)

       j=1
       found0or1=.false.
       if (maxspecies.eq.1) then
           ncutoffPairpot(1,1)=0
           do i=2*m3+(4+lmax)*m1+12*lm1*m2+2,2*m3+(4+lmax)*m1+12*lm1*m2+32,2
               if (freep(i).eq.2) then
                  if (found0or1) then
                     error=.true.
                     exit
                  endif
                  ncutoffPairpot(1,1)=ncutoffPairpot(1,1)+1
               else
                  found0or1=.true.
               endif
           enddo
       else
           k=1
           l=1
           ncutoffPairpot(1,1)=0
           do i=2*m3+(4+lmax)*m1+12*lm1*m2+2,m4,2
               if (freep(i).eq.2) then
                  if (found0or1) then
                     error=.true.
                     exit
                  endif
                  ncutoffPairpot(k,l)=ncutoffPairpot(k,l)+1
               else
                  found0or1=.true.
               endif
               if (j.lt.32) then
                  j=j+1
               elseif (l.lt.maxspecies) then
                  j=1
                  l=l+1
                  ncutoffPairpot(k,l)=0
               elseif (k.lt.maxspecies) then
                  j=1
                  l=1
                  k=k+1
                  ncutoffPairpot(k,l)=0
               endif
           enddo
       endif

       if (error) then
          print *,'ERROR: PARASTOOPT format inconsistent with use of HOLDCUTOFFS=.true.'
          print *,'STOPPING. Hint: please read the comments in the file readinoptparas.f90'
          stop
       endif

       print *,'Use of PARASTOOPT with HOLDCUTOFFS programmed but not tested.'
       print *,'Please check the following are sensible, STOPPING.'

       print *,'ncutoffDens=',ncutoffDens(maxspecies,maxspecies)
       print *,'ncutoffPairpot=',ncutoffPairpot(maxspecies,maxspecies)

       stop

    endif

end subroutine readinoptparasonebyone
