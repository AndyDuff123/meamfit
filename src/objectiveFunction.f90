subroutine objectiveFunction(finalOutput)

    !-------------------------------------------------------------------c
    !
    !     Calculate objective function which can contain: energies,
    !     atomic forces, stress tensors or a combination thereof.
    !
    !     Input:
    !        finalOutput: If true, compare the rho_i to compute
    !           'maxlargestrho' (the largest encountered rho_i) and
    !           stored the pair and embedding energy contributions
    !           separately for each configuration. This is for final
    !           calculation of observables, objective function and
    !           potentials.
    !
    !     Andrew Duff 2017
    !
    !     Tom Mellan 2017:
    !
    !        Added code (search for 'T.M') for the purpose of altering
    !     the standard energy contribution to the objective function from:
    !
    !        F_en = \sum_i w_i [   (fitdata(i) - fitdata(i_ref))
    !                            - (truedata(i) - truedata(i_ref)) ]^2 / (varEn * nEn)
    !
    !     (where i sums over structures, w_i is the energy weight for
    !     that structure, varEn is the variance of the energies and nEn
    !     is the number of energies to fit)
    !
    !     to:
    !
    !        F_en = \sum_i w_i [   (fitdata(i) - fitdata(i_ref))
    !                            - (truedata(i) - truedata(i_ref)) ]^2 / nEn
    !           - ( \sum_i w_i [   (fitdata(i) - fitdata(i_ref))
    !                            - (truedata(i) - truedata(i_ref)) ] / nEn )^2
    !
    !     Note, the \sum (fitdata(i) - fitdata(i_ref)) must be computed
    !     first before it can be squared and added onto F_en. This is
    !     summed using the variable avg_deltaE, and added onto F_en
    !     _after_ the main loop over the structures (within which, up
    !     until now, the entirety of F_en was computed).
    !     Similarly, contributions to the derivatives are summed
    !     over the structure loop, and added to dFen_d... afterwards.
    !
    !     Activate this objective function usng OBJFUNC=2 in settings.
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_datapoints
    use m_geometry
    use m_optimization
    use m_generalinfo
    use m_meamparameters
    use m_filenames
    use m_poscar
    use m_electrondensity
    use m_atomproperties
    use m_plotfiles
    use m_objectiveFunction
    use m_objectiveFunctionShared
    use m_observables
    use m_mpi_info

    implicit none

    include 'mpif.h'

    ! MPI: a couple of variables that were previously declared below
    ! have been moved to m_objectiveFunctionShared because they are required in
    ! the parallel routines as well:
    ! - idatapoint, nRef
    ! - ref_datapoint(9), ref_strucnum(9)
    ! - funcforcdenom
    ! - funcendenom
    ! - funcstrdenom
    ! - avg_deltaE

    logical finalOutput,FenNan,FfrcNaN,FstrNaN,errorOnce
    integer i,ipara,isp,jsp,iat,ip,l,iRef,averageNatoms_denom
    real(8) sumSqrs,Forig,disp, timeStart,Forig2,Forig_noPen, &
        Forig_Pen,dFpartial,dFen_partial, intEn_fit, intEn_true, &
        startTime,endTime,averageNatoms,startTime2,endTime2
    real(8) dFfrc_partial(3),dFstr_partial(6)
    logical returnflag, receivedreturnflag, sharedfiniteF

    returnflag=.false.
    receivedreturnflag=.false.
    sharedfiniteF=.true.

    call getTime(timeStart)

    !Objective function: energy; force; stress-tensor parts
    F=0d0
    FnoScl=0d0
    Fen=0d0
    Ffrc=0d0
    Fstr=0d0
    FcutoffPen=0d0
    funcen=0d0
    funcforc=0d0
    funcstr=0d0
    funcendenom=0d0
    funcforcdenom=0d0
    funcstrdenom=0d0
    negElecDens=.false.
    negBackDens=.false.
    gammaOutOfBounds=.false.
    fitdata=0d0 ! array

    if (noopt.eqv..false.) then
        ! all arrays
       dF_dmeamtau=0d0
       dF_draddens=0d0
       dF_dmeame0=0d0
       dF_dmeamrho0=0d0
       dF_dmeamemb3=0d0
       dF_dmeamemb4=0d0
       dF_dpairpot=0d0
       dF_denconst=0d0
       dFen_dmeamtau=0d0
       dFen_draddens=0d0
       dFen_dmeame0=0d0
       dFen_dmeamrho0=0d0
       dFen_dmeamemb3=0d0
       dFen_dmeamemb4=0d0
       dFen_dpairpot=0d0
       dFen_denconst=0d0
       if (objFuncType.eq.2) then
          !T.M: avg_deltaE keeps track of < fit(i)-true(i) > as we iterate over
          !the structures in the loop used to calculate Fen, Ffrc and Fstr. It must
          !then be squared and added onto Fen afterwards. dFen2_d... record
          !dsummeamf_dmeamtau - dsummeamf_dmeamtau_ref, etc, summed over the
          !structures. These can then be multiplied by an appropriate prefactor to
          !obtain the second contribution to dFen_d... afterwards.
          avg_deltaE=0d0 ! not an array
          dFen2_dmeamtau=0d0
          dFen2_draddens=0d0
          dFen2_dmeame0=0d0
          dFen2_dmeamrho0=0d0
          dFen2_dmeamemb3=0d0
          dFen2_dmeamemb4=0d0
          dFen2_dpairpot=0d0
       endif
       if (lmax.gt.0) then
          dFfrc_dmeamtau=0d0
       endif
       dFfrc_draddens=0d0
       dFfrc_dmeame0=0d0
       dFfrc_dmeamrho0=0d0
       dFfrc_dmeamemb3=0d0
       dFfrc_dmeamemb4=0d0
       dFfrc_dpairpot=0d0
       if (lmax.gt.0) then
          dFstr_dmeamtau=0d0
       endif
       dFstr_draddens=0d0
       dFstr_dmeame0=0d0
       dFstr_dmeamrho0=0d0
       dFstr_dmeamemb3=0d0
       dFstr_dmeamemb4=0d0
       dFstr_dpairpot=0d0
       ! all arrays up until here

       !Objective function: penalties
       !FlowDensPen=0d0
       dFcutoffPen_draddens=0d0
       dFcutoffPen_dpairpot=0d0
    endif

    ! MPI: the workers will now know it is a finalOuput here, so only the main thread does this
    if (finalOutput) then
       !Record the largest background density encountered (this
       !will inform the maxiumum density used when outputting the
       !lammps input file).
       open(60,file='largestrho.dat')
       write(60,*) '# structure-id | largest rho'
       maxlargestrho=0d0
    endif

    !The arrays 'ref_datapoint' and 'ref_strucnum' stored the datapoint index and structure number index for the references.
    !It is necessary to store both because the datapoint is used to index the fitdata and truedata arrays while the structure
    !number is used to index the weightsEn array.
    ref_datapoint(1)=1 !This is the datapoint id for the reference energy. Unless
               !additional reference energies have been specified (;rf) in fitdbse this
               !will stay as 1.
    ref_strucnum(1)=1
    ref_datapoint(2:9)=0 !Additional reference energies are used in the calculaton of interaction energies.
                 !When refenergy(istr)>1 (this will be true for structures with ;rf# (#>1) in fitdbse)
                 !ref_datapoint(#)=idatapoint, and from that structure onwards the INTERACTION ENERGY version of the
                 !objective function contribution will be used instead of the REFERENCE ENERGY form
    ref_strucnum(2:9)=0
    nRef=1     !Number of reference energies
    idatapoint=1

    !'istr' here iterates over the structures (atomic configurations). 'istr' and 'istruc' are used interchangeably in the code.
    !However here, and in some other places IT IS ESSENTIAL to keep 'istr' as 'istr'. This is because istr is stored in a module
    !('m_geometry') and is accessed by subroutines, such as meamenergy and meamforce, called by this subroutine.

    ! MPI: The call below initializes the workers
    ! The routine itself contains a more in depth explanation
    call p_beforeLoop(finalOutput)

    !startTime=MPI_Wtime()
    !averageNatoms=0d0
    !averageNatoms_denom=0
    !print *,start_i,'-',stop_i,': natoms:'
    do istr=start_i,stop_i
       !if (istr.eq.5) then
       !   print *,'setting dsummeamf_dpara(:,:,:,:)=',dsummeamf_dpara(:,:,:,:)
       !endif
       if ( (weightsEn(istr).ne.0d0).or.(weightsFr(istr).gt.0d0).or. &
            (weightsSt(istr).gt.0d0) ) then

          !startTime2=MPI_Wtime()
          !Calculate energy and/or force and/or stress tensor
          !averageNatoms_denom=averageNatoms_denom+1 ! compute average of number of atoms considered by this process for timing purposes
          if (computeForces(istr)) then
             !averagenatoms=averagenatoms+gn_forces(istr)
             !print *,'   istr=',istr,':  natoms=',gn_forces(istr)
             call meamforce
          else
             !averagenatoms=averagenatoms+gn_inequivalentsites(istr)
             !print *,'   istr=',istr,':  natoms=',gn_inequivalentsites(istr)
             call meamenergy
             !print *,'energy =',energy,'  summeam_paire=',summeam_paire,' summeamf=',summeamf,' enconst(1)=',enconst(1)
             !stop
          endif
          if (noOpt.eqv..false.) then
             if ((negBackDens.eqv..true.).or.(gammaOutOfBounds.eqv..true.)) then
                !Negative background density, or gamma that would result in rhoi=NaN
                finiteF=.false.
                returnflag=.true.
                exit
             else
                finiteF=.true.
             endif

             !---- The above check should catch all infinite values, however for some reason it does not. Add an extra ----
             !     fail-safe here.
             if (weightsEn(istr).ne.0d0) then
                if (energy/=energy) then
                   if (debug.eqv..true.) then
                      print *,'ERROR, energy=',energy
                   endif
                   finiteF=.false.
                   returnflag=.true.
                   exit
                endif
             endif

             if (weightsFr(istr).gt.0d0) then
                do iat=1,gn_forces(istr)
                   do i=1,3
                      if (forces(i,iat)/=forces(i,iat)) then
                         if (debug.eqv..true.) then
                            print *,'ERROR, forces(',i,iat,')=',forces(i,iat)
                         endif
                         finiteF=.false.
                         returnflag=.true.
                         exit
                      endif
                   enddo
                enddo
             endif
             !-------------------------------------------------------------------------------------------------------------
             if ((negElecDensAllow.eqv..false.).and.(negElecDens.eqv..true.)) then
                if (negElecDensWarning.eqv..false.) then
  	           print *,'Refusing to calculate F due to negative density (you may need to set NEGELECDENS=.true. in settings)'
                   negElecDensWarning=.true. !We've made the warning once, no need to make it again.
                endif

                ! MPI: the exit statement below used to be return.
                ! This was changed because one process returning and not doing
                ! the synchronization part below causes deadlocks.
                ! Instead the return flag is set and shared between threads
                ! and handled appropriately after the loop
                returnflag=.true.
                exit
             endif
          endif

          if (finalOutput) then
            call getlargestbkgrnddens
            maxlargestrho=MAX(maxlargestrho,largestrho)
            write(60,*) istr,largestrho
          endif

          !'refenergy(istr)' is set to 1-9 depending on the number specified for that configuration in fitdbse, through: ;rf#
          !
          if (refenergy(istr).ge.1) then
             ref_datapoint(refenergy(istr))=idatapoint
             ref_strucnum(refenergy(istr))=istr
             !print *,'refenergy(',istr,')=',refenergy(istr),' current nRef=',nRef,', increasing to...:'
             nRef=MAX(nRef,refenergy(istr))
             !print *,'nRef=',nRef
          endif

          !Fill fitdata and add contributions to objective function, its derivatives, and standard
          !deviations of observables.

          if (weightsEn(istr).ne.0d0) then

             !---- Energy ----
             fitdata(idatapoint)=energy
             !print *,'fitdata(,',idatapoint,')=',fitdata(idatapoint)
             !stop

             ! if (procid.eq.1) then
             !     print *,'procid:',procid,', fitdata:',fitdata(idatapoint)
             ! endif

             !print *,'fitdata(',idatapoint,')=',fitdata(idatapoint)
             if (finalOutput) then
                summeam_paireStr(idatapoint)=summeam_paire
                summeamfStr(idatapoint)=summeamf
             endif

             if ((noOpt.eqv..false.).and.(refenergy(istr).gt.0)) then
                !Record derivatives, as these will be subtracted from
                !subsequent contributions to dFen_dpara
                !dsummeamf_dmeamtau_ref(refenergy(istr),1:lmax,1:maxspecies)=dsummeamf_dmeamtau(1:lmax,1:maxspecies)
                !dsummeamf_dpara_ref(refenergy(istr),1:12,0:lmax,1:maxspecies,1:maxspecies)=dsummeamf_dpara(1:12,0:lmax,1:maxspecies,1:maxspecies)
                !dsummeamf_dmeame0_ref(refenergy(istr),1:maxspecies)=dsummeamf_dmeame0(1:maxspecies)
                !dsummeamf_dmeamrho0_ref(refenergy(istr),1:maxspecies)=dsummeamf_dmeamrho0(1:maxspecies)
                !dsummeamf_dmeamemb3_ref(refenergy(istr),1:maxspecies)=dsummeamf_dmeamemb3(1:maxspecies)
                !dsummeamf_dmeamemb4_ref(refenergy(istr),1:maxspecies)=dsummeamf_dmeamemb4(1:maxspecies)
                !dsummeam_paire_dpara_ref(refenergy(istr),1:32,1:maxspecies,1:maxspecies)=dsummeam_paire_dpara(1:32,1:maxspecies,1:maxspecies)

                dsummeamf_dmeamtau_ref(refenergy(istr),:,:)=dsummeamf_dmeamtau(:,:)
                dsummeamf_dpara_ref(refenergy(istr),:,:,:,:)=dsummeamf_dpara(:,:,:,:)
                !if (istr.eq.5) then
                !   print *,'   For istr=',istr,', recording dsummeamf_dpara_ref(',refenergy(istr),',:,:,:,:)=',dsummeamf_dpara_ref(refenergy(istr),:,:,:,:)
                !endif
                dsummeamf_dmeame0_ref(refenergy(istr),:)=dsummeamf_dmeame0(:)
                dsummeamf_dmeamrho0_ref(refenergy(istr),:)=dsummeamf_dmeamrho0(:)
                dsummeamf_dmeamemb3_ref(refenergy(istr),:)=dsummeamf_dmeamemb3(:)
                dsummeamf_dmeamemb4_ref(refenergy(istr),:)=dsummeamf_dmeamemb4(:)
                dsummeam_paire_dpara_ref(refenergy(istr),:,:,:)=dsummeam_paire_dpara(:,:,:)
             endif

             !Energy contribution to objective function, and derivatives:
             if (useRef) then

                !Only add contribution to objective functions and derivatives w.r.t parameters IF we are not currently
                !evaluating a reference structure [ otherwise the contributions will be zero in the case of nRef=1, and
                !none zero but NOT WANTED for nRef>1 (the interaction energy is evaluated once all references set up) ]
                if (refenergy(istr).eq.0) then

                   !Use energy reference
                   if (nRef.eq.1) then

                      !...singular energy reference (not the 'interaction energy' form)
                      ! print *,'procid:',procid,', ref_datapoint(1):',ref_datapoint(1),', fitdata(ref_datapoint(1)):', fitdata(ref_datapoint(1))
                      Fen=Fen+weightsEn(istr)* &
                          ( (fitdata(idatapoint)-fitdata(ref_datapoint(1))) - &
                            (truedata(idatapoint)-truedata(ref_datapoint(1))) )**2
                      if (objFuncType.eq.2) then
                         ! T.M: the following is for s.d.-type objective function
                         ! Once summed over all structures, this is divided by nEn,
                         ! squared, and then subtracted from Fen.
                         avg_deltaE=avg_deltaE+ weightsEn(istr)* &
                               (fitdata(idatapoint)-fitdata(ref_datapoint(1))) - &
                               (truedata(idatapoint)-truedata(ref_datapoint(1)))
                      endif
                      if (noOpt.eqv..false.) then
                         dFen_partial=2d0*weightsEn(istr)* &
                             ( (fitdata(idatapoint)-fitdata(ref_datapoint(1))) - &
                               (truedata(idatapoint)-truedata(ref_datapoint(1))) )
                         dFen_dmeamtau(:,:)=dFen_dmeamtau(:,:) + dFen_partial * &
                               (dsummeamf_dmeamtau(:,:) - dsummeamf_dmeamtau_ref(1,:,:))
                         !Add check here to see which becomes a NaN
                         if (debug.eqv..true.) then
                            if (fitdata(idatapoint)/=fitdata(idatapoint)) then
                               print *,'ERROR, fitdata(',idatapoint,')=',fitdata(idatapoint)
                            endif
                            if (fitdata(ref_datapoint(1))/=fitdata(ref_datapoint(1))) then
                               print *,'ERROR, fitdata(',ref_datapoint(1),')=',fitdata(ref_datapoint(1))
                            endif
                            do isp=1,maxspecies
                              do l=1,lmax
                                 if (dsummeamf_dmeamtau(l,isp)/=dsummeamf_dmeamtau(l,isp)) then
                                    print *,'ERROR, dsummeamf_dmeamtau=',dsummeamf_dmeamtau
                                 endif
                                 if (dsummeamf_dmeamtau_ref(1,l,isp)/=dsummeamf_dmeamtau_ref(1,l,isp)) then
                                    print *,'ERROR, dsummeamf_dmeamtau_ref(1,:,:)=',dsummeamf_dmeamtau_ref(1,:,:)
                                 endif
                              enddo
                            enddo
                         endif
                         dFen_draddens(:,:,:)=dFen_draddens(:,:,:) + dFen_partial * &
                               (dsummeamf_dpara(:,:,1,:) - dsummeamf_dpara_ref(1,:,:,1,:))
                         dFen_dmeame0(:)=dFen_dmeame0(:) + dFen_partial * &
                               (dsummeamf_dmeame0(:) - dsummeamf_dmeame0_ref(1,:))
                         dFen_dmeamrho0(:)=dFen_dmeamrho0(:) + dFen_partial * &
                               (dsummeamf_dmeamrho0(:) - dsummeamf_dmeamrho0_ref(1,:))
                         dFen_dmeamemb3(:)=dFen_dmeamemb3(:) + dFen_partial * &
                               (dsummeamf_dmeamemb3(:) - dsummeamf_dmeamemb3_ref(1,:))
                         dFen_dmeamemb4(:)=dFen_dmeamemb4(:) + dFen_partial * &
                               (dsummeamf_dmeamemb4(:) - dsummeamf_dmeamemb4_ref(1,:))
                         dFen_dpairpot(:,:,:)=dFen_dpairpot(:,:,:) + dFen_partial * &
                               (dsummeam_paire_dpara(:,:,:) - dsummeam_paire_dpara_ref(1,:,:,:))
                         if (objFuncType.eq.2) then
                            ! T.M: add the following for s.d.-type objective function
                            ! Once summed over all structures, these are multiplied by
                            ! -2*avg_deltaE/nEn and added to the relevant dFen_d... tally
                            dFen2_dmeamtau(:,:)=dFen2_dmeamtau(:,:) + weightsEn(istr)* &
                                  (dsummeamf_dmeamtau(:,:) - dsummeamf_dmeamtau_ref(1,:,:))
                            dFen2_draddens(:,:,:)=dFen2_draddens(:,:,:) + weightsEn(istr)* &
                                  (dsummeamf_dpara(:,:,1,:) - dsummeamf_dpara_ref(1,:,:,1,:))
                            dFen2_dmeame0(:)=dFen2_dmeame0(:) + weightsEn(istr)* &
                                  (dsummeamf_dmeame0(:) - dsummeamf_dmeame0_ref(1,:))
                            dFen2_dmeamrho0(:)=dFen2_dmeamrho0(:) + weightsEn(istr)* &
                                  (dsummeamf_dmeamrho0(:) - dsummeamf_dmeamrho0_ref(1,:))
                            dFen2_dmeamemb3(:)=dFen2_dmeamemb3(:) + weightsEn(istr)* &
                                  (dsummeamf_dmeamemb3(:) - dsummeamf_dmeamemb3_ref(1,:))
                            dFen2_dmeamemb4(:)=dFen2_dmeamemb4(:) + weightsEn(istr)* &
                                  (dsummeamf_dmeamemb4(:) - dsummeamf_dmeamemb4_ref(1,:))
                            dFen2_dpairpot(:,:,:)=dFen2_dpairpot(:,:,:) + weightsEn(istr)* &
                                  (dsummeam_paire_dpara(:,:,:) - dsummeam_paire_dpara_ref(1,:,:,:))
                         endif
                      endif
                      !Standard deviations of energies:
                      ! print *,'it=',istr,', fitdata(idatapoint)=',fitdata(idatapoint),', fitdata(ref_datapoint(1))',fitdata(ref_datapoint(1)),',val=',( (fitdata(idatapoint)-fitdata(ref_datapoint(1))) - &
                        ! (truedata(idatapoint)-truedata(ref_datapoint(1))) )**2
                      funcen=funcen+ &
                          ( (fitdata(idatapoint)-fitdata(ref_datapoint(1))) - &
                            (truedata(idatapoint)-truedata(ref_datapoint(1))) )**2
                   else

                      !Multiple references: Interaction energy form
                      intEn_fit=fitdata(idatapoint)
                      intEn_true=truedata(idatapoint)
                      !print *,'intEn_fit=',intEn_fit,'+...'
                      !print *,'intEn_true=',intEn_true,'+...'
                      do iRef=1,nRef
                         intEn_fit=intEn_fit+weightsEn(ref_strucnum(iRef))*fitdata(ref_datapoint(iRef))
                         intEn_true=intEn_true+weightsEn(ref_strucnum(iRef))*truedata(ref_datapoint(iRef))
                         !print *,'...for intEn_fit: ',weightsEn(ref_strucnum(iRef)),'*',fitdata(ref_datapoint(iRef))
                         !print *,'          (first quantity is weightsEn(ref_strucnum(',iRef,')=weightsEn(',ref_strucnum(iRef),'=',weightsEn(ref_strucnum(iRef))
                         !print *,'...for intEn_true: ',weightsEn(ref_strucnum(iRef)),'*',truedata(ref_datapoint(iRef))
                      enddo
                      Fen=Fen+weightsEn(istr)* &
                          ( ( intEn_fit - intEn_true )**2 )
                      !print *,'istr=',istr,' Fen=',weightsEn(istr),'*',( ( intEn_fit - intEn_true )**2 )
                      !Derivatives:
                      if (noOpt.eqv..false.) then
                         dFen_partial=2d0*weightsEn(istr)* &
                             ( intEn_fit - intEn_true )
                         dintEn_fit_dmeamtau(:,:)=dsummeamf_dmeamtau(:,:)
                         dintEn_fit_draddens(:,:,:)=dsummeamf_dpara(:,:,1,:)
                         dintEn_fit_dmeame0(:)=dsummeamf_dmeame0(:)
                         dintEn_fit_dmeamrho0(:)=dsummeamf_dmeamrho0(:)
                         dintEn_fit_dmeamemb3(:)=dsummeamf_dmeamemb3(:)
                         dintEn_fit_dmeamemb4(:)=dsummeamf_dmeamemb4(:)
                         dintEn_fit_dpairpot(:,:,:)=dsummeam_paire_dpara(:,:,:)
                         do iRef=1,nRef
                            dintEn_fit_dmeamtau(:,:)=dintEn_fit_dmeamtau(:,:)+weightsEn(ref_strucnum(iRef))*dsummeamf_dmeamtau_ref(iRef,:,:)
                            dintEn_fit_draddens(:,:,:)=dintEn_fit_draddens(:,:,:)+weightsEn(ref_strucnum(iRef))*dsummeamf_dpara_ref(iRef,:,:,1,:)
                            !if ((istr.eq.6).and.(iRef.eq.3)) then
                            !   print *,' [ istr=',istr,', ref_datapoint(',iRef,')=',ref_datapoint(iRef),' ]'
                            !   print *,' [ dsummeamf_dpara_ref(',iRef,',:,:,1,:)=',dsummeamf_dpara_ref(iRef,:,:,1,:),' ]'
                            !endif
                            dintEn_fit_dmeame0(:)=dintEn_fit_dmeame0(:)+weightsEn(ref_strucnum(iRef))*dsummeamf_dmeame0_ref(iRef,:)
                            dintEn_fit_dmeamrho0(:)=dintEn_fit_dmeamrho0(:)+weightsEn(ref_strucnum(iRef))*dsummeamf_dmeamrho0_ref(iRef,:)
                            dintEn_fit_dmeamemb3(:)=dintEn_fit_dmeamemb3(:)+weightsEn(ref_strucnum(iRef))*dsummeamf_dmeamemb3_ref(iRef,:)
                            dintEn_fit_dmeamemb4(:)=dintEn_fit_dmeamemb4(:)+weightsEn(ref_strucnum(iRef))*dsummeamf_dmeamemb4_ref(iRef,:)
                            dintEn_fit_dpairpot(:,:,:)=dintEn_fit_dpairpot(:,:,:)+weightsEn(ref_strucnum(iRef))*dsummeam_paire_dpara_ref(iRef,:,:,:)
                         enddo
                         dFen_dmeamtau(:,:)=dFen_dmeamtau(:,:) + dFen_partial * dintEn_fit_dmeamtau(:,:)
                         dFen_draddens(:,:,:)=dFen_draddens(:,:,:) + dFen_partial * dintEn_fit_draddens(:,:,:)
                         dFen_dmeame0(:)=dFen_dmeame0(:) + dFen_partial * dintEn_fit_dmeame0(:)
                         dFen_dmeamrho0(:)=dFen_dmeamrho0(:) + dFen_partial * dintEn_fit_dmeamrho0(:)
                         dFen_dmeamemb3(:)=dFen_dmeamemb3(:) + dFen_partial * dintEn_fit_dmeamemb3(:)
                         dFen_dmeamemb4(:)=dFen_dmeamemb4(:) + dFen_partial * dintEn_fit_dmeamemb4(:)
                         dFen_dpairpot(:,:,:)=dFen_dpairpot(:,:,:) + dFen_partial * dintEn_fit_dpairpot(:,:,:)
                         !print *,'dFen_dmeamtau(:,:)=',dFen_dmeamtau(:,:),', dFen_draddens(:,:,:)=',dFen_draddens(:,:,:)
                         !print *,'dFen_dmeame0(:)=',dFen_dmeame0(:),', dFen_dmeamrho0(:)=',dFen_dmeamrho0(:)
                         !print *,'dFen_dmeamemb3(:)=',dFen_dmeamemb3(:),', dFen_dmeamemb4(:)=',dFen_dmeamemb4(:)
                         !print *,'dFen_dpairpot(:,:,:)=',dFen_dpairpot(:,:,:)

                      endif
                      !Standard deviations of energies:
                      funcen=funcen+ &
                          ( intEn_fit - intEn_true )**2
                   endif

                   funcendenom=funcendenom+1

                endif

             else

                ! No reference:
		!print *,'istr=',istr,' weightsEn(istr)=',weightsEn(istr),' fitdata()=',fitdata(idatapoint),' truedata=',truedata(idatapoint)
                Fen=Fen+weightsEn(istr)* &
                    ( fitdata(idatapoint) - truedata(idatapoint) )**2

                funcen=funcen+ &
                    ( fitdata(idatapoint) - truedata(idatapoint) )**2
                if (objFuncType.eq.2) then
                   ! T.M:
                   avg_deltaE=avg_deltaE+ &
                         fitdata(idatapoint) - truedata(idatapoint)
                endif
                if (noOpt.eqv..false.) then
                   if ((fixPotIn.eqv..false.).and.(analyticDerivs.eqv..true.)) then
                      print *,'Need to code in Fen derivatives in objectiveFunction'
                      print *,'for potential optimization using useRef=.false.'
                      stop
                   else
                      !Currently enconst is just a single constant per atom, independent of species. However, the arrays
                      !have been defined to allow enconst for each species, so can be generalized later.
                      dFen_partial=2d0*weightsEn(istr)* &
                           ( fitdata(idatapoint) - truedata(idatapoint) )
                      dFen_denconst(1)=dFen_denconst(1) + dFen_partial * gn_inequivalentsites(istr)
                      !print *,'dFen_denconst(1) contribution=',dFen_partial * gn_inequivalentsites(istr)
                      !print *,'   [gn_inequivalentsites(istr)=',gn_inequivalentsites(istr),']'
                      !stop
                   endif
                endif
                funcendenom=funcendenom+1

             endif
             idatapoint=idatapoint+1
             !----------------

          endif

          if (weightsFr(istr).gt.0d0) then

             !---- Forces ----
             !print *,'Ffrc calc:'
             errorOnce=.false.
             do iat=1,gn_forces(istr)
                !print *,'istr=',istr,', iat=',iat
                if (optforce(iat,istr).eqv..true.) then
                   fitdata(idatapoint:idatapoint+2)=forces(1:3,iat)
                   !print *,'forces=',forces(1:3,iat)
                   !Add contributions to the objective function due to forces,
                   !and derivatives. Here add to Ffrc, which will later be added to F.
                   sumSqrs=(fitdata(idatapoint)-truedata(idatapoint))**2 &
                         +(fitdata(idatapoint+1)-truedata(idatapoint+1))**2 &
                         +(fitdata(idatapoint+2)-truedata(idatapoint+2))**2
                   !Compute 'funcforc', the s.d. of the force components
                   funcforc=funcforc+sumSqrs
                   funcforcdenom=funcforcdenom+3
                   Ffrc=Ffrc+weightsFr(istr)*sumSqrs
                   !print *,'Ffrc from istr=',istr,' iat=',iat,' =',Ffrc
                   !print *,'truedata(',idatapoint,'-',idatapoint+2,')=',truedata(idatapoint:idatapoint+2)

                   if (noOpt.eqv..false.) then
                      dFfrc_partial(1)=2d0*weightsFr(istr)*(fitdata(idatapoint)-truedata(idatapoint))
                      dFfrc_partial(2)=2d0*weightsFr(istr)*(fitdata(idatapoint+1)-truedata(idatapoint+1))
                      dFfrc_partial(3)=2d0*weightsFr(istr)*(fitdata(idatapoint+2)-truedata(idatapoint+2))
                      !print *,'dFfrc_partial(1:3)=',dFfrc_partial(1:3)
                      if (lmax.gt.0) then
                         dFfrc_dmeamtau(:,:)=dFfrc_dmeamtau(:,:) + & !dFfrc_dmeamtau and dforce_dmeamtau have indices l,jsp
                            dFfrc_partial(1)*dforce_dmeamtau(1,:,:,iat) + &
                            dFfrc_partial(2)*dforce_dmeamtau(2,:,:,iat) + &
                            dFfrc_partial(3)*dforce_dmeamtau(3,:,:,iat)

                         !Add check here to see which becomes a NaN
                         if (debug.eqv..true.) then
                            do i=0,2
                               if (fitdata(idatapoint+i)/=fitdata(idatapoint+i)) then
                                  print *,'ERROR, fitdata(',idatapoint+i,')=',fitdata(idatapoint+i)
                               endif
                               do isp=1,maxspecies
                                 do l=1,lmax
                                    if (dforce_dmeamtau(i+1,l,isp,iat)/=dforce_dmeamtau(i+1,l,isp,iat)) then
                                       if (errorOnce.eqv..false.) then
                                          print *,'ERROR, dforce_dmeamtau(',i+1,l,isp,iat,')=',dforce_dmeamtau(i+1,l,isp,iat)
                                          errorOnce=.true.
                                       endif
                                    endif
                                 enddo
                               enddo
                            enddo
                         endif

                         !print *,'dforce_dmeamtau(1,:,:,iat)=',dforce_dmeamtau(1,:,:,iat)
                         !print *,'dforce_dmeamtau(2,:,:,iat)=',dforce_dmeamtau(2,:,:,iat)
                         !print *,'dforce_dmeamtau(3,:,:,iat)=',dforce_dmeamtau(3,:,:,iat)

                         ! Alternatively:
                         ! dFfrc_dmeamtau=0d0
                         ! do isp=1,maxspecies
                         !    do l=1,lmax
                         !       dFfrc_dmeamtau(l,isp)=dFfrc_dmeamtau(l,isp)+ &
                         !          dFfrc_partial(1)*dforce_dmeamtau(1,l,isp,iat) + &
                         !          dFfrc_partial(2)*dforce_dmeamtau(2,l,isp,iat) + &
                         !          dFfrc_partial(3)*dforce_dmeamtau(3,l,isp,iat)
                         !    enddo
                         ! enddo
                      endif
                      dFfrc_draddens(:,:,:)=dFfrc_draddens(:,:,:) + & !12,0:lmax,maxspecies
                         dFfrc_partial(1)*dforce_draddens(1,:,:,:,iat) + &
                         dFfrc_partial(2)*dforce_draddens(2,:,:,:,iat) + &
                         dFfrc_partial(3)*dforce_draddens(3,:,:,:,iat)
                      ! A longer way of coding this (equivalent - I tested it):
                      ! dFfrc_draddens=0d0
                      ! do ip=1,12
                      !    do l=0,lmax
                      !       do isp=1,maxspecies
                      !          dFfrc_draddens(ip,l,isp)=dFfrc_draddens(ip,l,isp) + &
                      !             dFfrc_partial(1)*dforce_draddens(1,ip,l,isp,iat) + &
                      !             dFfrc_partial(2)*dforce_draddens(2,ip,l,isp,iat) + &
                      !             dFfrc_partial(3)*dforce_draddens(3,ip,l,isp,iat)
                      !       enddo
                      !    enddo
                      ! enddo

                      dFfrc_dmeame0(:)=dFfrc_dmeame0(:) + & !alph,iisp,iat
                         dFfrc_partial(1)*dforce_dmeame0(1,:,iat) + &
                         dFfrc_partial(2)*dforce_dmeame0(2,:,iat) + &
                         dFfrc_partial(3)*dforce_dmeame0(3,:,iat)
                      dFfrc_dmeamrho0(:)=dFfrc_dmeamrho0(:) + & !alph,iisp,iat
                         dFfrc_partial(1)*dforce_dmeamrho0(1,:,iat) + &
                         dFfrc_partial(2)*dforce_dmeamrho0(2,:,iat) + &
                         dFfrc_partial(3)*dforce_dmeamrho0(3,:,iat)
                      dFfrc_dmeamemb3(:)=dFfrc_dmeamemb3(:) + & !alph,iisp,iat
                         dFfrc_partial(1)*dforce_dmeamemb3(1,:,iat) + &
                         dFfrc_partial(2)*dforce_dmeamemb3(2,:,iat) + &
                         dFfrc_partial(3)*dforce_dmeamemb3(3,:,iat)
                      dFfrc_dmeamemb4(:)=dFfrc_dmeamemb4(:) + & !alph,iisp,iat
                         dFfrc_partial(1)*dforce_dmeamemb4(1,:,iat) + &
                         dFfrc_partial(2)*dforce_dmeamemb4(2,:,iat) + &
                         dFfrc_partial(3)*dforce_dmeamemb4(3,:,iat)
                      dFfrc_dpairpot(:,:,:)=dFfrc_dpairpot(:,:,:) + & !alph,ip,isp,jsp,iat
                         dFfrc_partial(1)*dforce_dpairpot(1,:,:,:,iat) + &
                         dFfrc_partial(2)*dforce_dpairpot(2,:,:,:,iat) + &
                         dFfrc_partial(3)*dforce_dpairpot(3,:,:,:,iat)
                   endif
                endif
                idatapoint=idatapoint+3
             enddo
             !----------------

          endif
          !print *,'--------------'

          if (weightsSt(istr).gt.0d0) then

             !---- Stress-tensor ----
             fitdata(idatapoint)=stressTensor(1)
             fitdata(idatapoint+1)=stressTensor(5)
             fitdata(idatapoint+2)=stressTensor(9)
             fitdata(idatapoint+3)=stressTensor(4)
             fitdata(idatapoint+4)=stressTensor(7)
             fitdata(idatapoint+5)=stressTensor(8)
             !print *,'final stress tensor=',fitdata(idatapoint:idatapoint+5)
             !print *,'idatapoint=',idatapoint,' ndatapoint=',ndatapoints
             !print *,'size(fitdata)=',size(fitdata)
             !print *,'size(truedata)=',size(truedata)

             sumSqrs=(fitdata(idatapoint)-truedata(idatapoint))**2 &
                    +(fitdata(idatapoint+1)-truedata(idatapoint+1))**2 &
                    +(fitdata(idatapoint+2)-truedata(idatapoint+2))**2 &
                    +(fitdata(idatapoint+3)-truedata(idatapoint+3))**2 &
                    +(fitdata(idatapoint+4)-truedata(idatapoint+4))**2 &
                    +(fitdata(idatapoint+5)-truedata(idatapoint+5))**2
             funcstr=funcstr+sumSqrs
             funcstrdenom=funcstrdenom+6
             Fstr=Fstr+weightsSt(istr)*sumSqrs

             if (noOpt.eqv..false.) then
                dFstr_partial(1)=2d0*weightsSt(istr)*(fitdata(idatapoint)-truedata(idatapoint))
                dFstr_partial(2)=2d0*weightsSt(istr)*(fitdata(idatapoint+1)-truedata(idatapoint+1))
                dFstr_partial(3)=2d0*weightsSt(istr)*(fitdata(idatapoint+2)-truedata(idatapoint+2))
                dFstr_partial(4)=2d0*weightsSt(istr)*(fitdata(idatapoint+3)-truedata(idatapoint+3))
                dFstr_partial(5)=2d0*weightsSt(istr)*(fitdata(idatapoint+4)-truedata(idatapoint+4))
                dFstr_partial(6)=2d0*weightsSt(istr)*(fitdata(idatapoint+5)-truedata(idatapoint+5))

                if (lmax.gt.0) then
                   dFstr_dmeamtau(:,:)=dFstr_dmeamtau(:,:) + &
                      dFstr_partial(1)*dstresstensor_dmeamtau(1,:,:) + &
                      dFstr_partial(2)*dstresstensor_dmeamtau(5,:,:) + &
                      dFstr_partial(3)*dstresstensor_dmeamtau(9,:,:) + &
                      dFstr_partial(4)*dstresstensor_dmeamtau(4,:,:) + &
                      dFstr_partial(5)*dstresstensor_dmeamtau(7,:,:) + &
                      dFstr_partial(6)*dstresstensor_dmeamtau(8,:,:)
                endif
                dFstr_draddens(:,:,:)=dFstr_draddens(:,:,:) + & !12,0:lmax,maxspecies
                    dFstr_partial(1)*dstresstensor_draddens(1,:,:,:) + &
                    dFstr_partial(2)*dstresstensor_draddens(5,:,:,:) + &
                    dFstr_partial(3)*dstresstensor_draddens(9,:,:,:) + &
                    dFstr_partial(4)*dstresstensor_draddens(4,:,:,:) + &
                    dFstr_partial(5)*dstresstensor_draddens(7,:,:,:) + &
                    dFstr_partial(6)*dstresstensor_draddens(8,:,:,:)

                dFstr_dmeame0(:)=dFstr_dmeame0(:) + & !alph,iisp
                   dFstr_partial(1)*dstresstensor_dmeame0(1,:) + &
                   dFstr_partial(2)*dstresstensor_dmeame0(5,:) + &
                   dFstr_partial(3)*dstresstensor_dmeame0(9,:) + &
                   dFstr_partial(4)*dstresstensor_dmeame0(4,:) + &
                   dFstr_partial(5)*dstresstensor_dmeame0(7,:) + &
                   dFstr_partial(6)*dstresstensor_dmeame0(8,:)
                dFstr_dmeamrho0(:)=dFstr_dmeamrho0(:) + & !alph,iisp,iat
                   dFstr_partial(1)*dstresstensor_dmeamrho0(1,:) + &
                   dFstr_partial(2)*dstresstensor_dmeamrho0(5,:) + &
                   dFstr_partial(3)*dstresstensor_dmeamrho0(9,:) + &
                   dFstr_partial(4)*dstresstensor_dmeamrho0(4,:) + &
                   dFstr_partial(5)*dstresstensor_dmeamrho0(7,:) + &
                   dFstr_partial(6)*dstresstensor_dmeamrho0(8,:)
                dFstr_dmeamemb3(:)=dFstr_dmeamemb3(:) + & !alph,iisp,iat
                   dFstr_partial(1)*dstresstensor_dmeamemb3(1,:) + &
                   dFstr_partial(2)*dstresstensor_dmeamemb3(5,:) + &
                   dFstr_partial(3)*dstresstensor_dmeamemb3(9,:) + &
                   dFstr_partial(4)*dstresstensor_dmeamemb3(4,:) + &
                   dFstr_partial(5)*dstresstensor_dmeamemb3(7,:) + &
                   dFstr_partial(6)*dstresstensor_dmeamemb3(8,:)
                dFstr_dmeamemb4(:)=dFstr_dmeamemb4(:) + & !alph,iisp,iat
                   dFstr_partial(1)*dstresstensor_dmeamemb4(1,:) + &
                   dFstr_partial(2)*dstresstensor_dmeamemb4(5,:) + &
                   dFstr_partial(3)*dstresstensor_dmeamemb4(9,:) + &
                   dFstr_partial(4)*dstresstensor_dmeamemb4(4,:) + &
                   dFstr_partial(5)*dstresstensor_dmeamemb4(7,:) + &
                   dFstr_partial(6)*dstresstensor_dmeamemb4(8,:)
                dFstr_dpairpot(:,:,:)=dFstr_dpairpot(:,:,:) + & !alph,ip,isp,jsp,iat
                   dFstr_partial(1)*dstresstensor_dpairpot(1,:,:,:) + &
                   dFstr_partial(2)*dstresstensor_dpairpot(5,:,:,:) + &
                   dFstr_partial(3)*dstresstensor_dpairpot(9,:,:,:) + &
                   dFstr_partial(4)*dstresstensor_dpairpot(4,:,:,:) + &
                   dFstr_partial(5)*dstresstensor_dpairpot(7,:,:,:) + &
                   dFstr_partial(6)*dstresstensor_dpairpot(8,:,:,:)
             endif
             idatapoint=idatapoint+6
             !-----------------------

          endif
          !endTime2=MPI_Wtime()
          !print *,'      time: ',endTime2-startTime2,' s'
       endif
    enddo
    istr=istr-1
    !endTime=MPI_Wtime()
    !print *,start_i,'-',stop_i,' took:',endTime-startTime,' seconds'!, average natoms = ',averagenatoms/real(averagenatoms_denom)

    ! MPI: the code below is responsible for handling premature returns out of the objective function
    call MPI_allreduce(returnflag,receivedreturnflag,1,MPI_LOGICAL,MPI_LOR,MPI_COMM_WORLD,ierr)
    call MPI_allreduce(finiteF,sharedfiniteF,1,MPI_LOGICAL,MPI_LAND,MPI_COMM_WORLD,ierr)

    finiteF = sharedfiniteF

    if (receivedreturnflag.eqv..true.) then
        return
    endif

    ! MPI: the function call below synchronized the results from the workers to the main process
    ! The function itself contains a more thorough explanation
    call p_synchronizeResults

    if (procid.gt.0) then
        return
        ! MPI: because workers should not do the part below.
    endif

    if (finalOutput) then
       close(60)
    endif

    if ((Fen.ge.0d0).or.(Fen.lt.0d0)) then
       FenNaN=.false.
    else
       FenNaN=.true.
    endif
    if ((Ffrc.ge.0d0).or.(Ffrc.lt.0d0)) then
       FfrcNaN=.false.
    else
       FfrcNaN=.true.
    endif
    if ((Fstr.ge.0d0).or.(Fstr.lt.0d0)) then
       FstrNaN=.false.
    else
       FstrNaN=.true.
    endif

    !Contribution from energies (note, not just 'if Fen>0'. We also want to
    !correctly pass on a NaN to F if Fen=NaN
    if ((Fen.gt.0d0).or.FenNaN) then
       if (objFuncType.eq.1) then
          !Only divide through by variance of energies if it makes sense (I.e., not
          !for where we are fitting interaction energies)
          if ((useRef.eqv..false.).or.((useRef.eqv..true.).and.(nRef.eq.1))) then
             F=Fen/(dble(nEn)*varEn)
          else
             F=Fen
          endif
       else
          ! T.M:
          F=Fen/dble(nEn) - (avg_deltaE/dble(nEn))**2
       endif
       FnoScl=Fen! /dble(nEn)
       if ((useRef.eqv..false.).or.((useRef.eqv..true.).and.(nRef.eq.1))) then
          Fen=Fen/(dble(nEn)*varEn)
       endif
       !print *,'Fen denom=',dble(nEn)*varEn

       if (noOpt.eqv..false.) then
          if (objFuncType.eq.1) then
             dF_dmeamtau=dFen_dmeamtau/(dble(nEn)*varEn)
             dF_draddens=dFen_draddens/(dble(nEn)*varEn)
             dF_dmeame0=dFen_dmeame0/(dble(nEn)*varEn)
             dF_dmeamrho0=dFen_dmeamrho0/(dble(nEn)*varEn)
             dF_dmeamemb3=dFen_dmeamemb3/(dble(nEn)*varEn)
             dF_dmeamemb4=dFen_dmeamemb4/(dble(nEn)*varEn)
             dF_dpairpot=dFen_dpairpot/(dble(nEn)*varEn)
             dF_denconst=dFen_denconst/(dble(nEn)*varEn)
          elseif (objFuncType.eq.2) then
             ! T.M:
             dF_dmeamtau =  dFen_dmeamtau/dble(nEn)  - (2d0*avg_deltaE/dble(nEn)) * dFen2_dmeamtau
             dF_draddens =  dFen_draddens/dble(nEn)  - (2d0*avg_deltaE/dble(nEn)) * dFen2_draddens
             dF_dmeame0 =   dFen_dmeame0/dble(nEn)   - (2d0*avg_deltaE/dble(nEn)) * dFen2_dmeame0
             dF_dmeamrho0 = dFen_dmeamrho0/dble(nEn) - (2d0*avg_deltaE/dble(nEn)) * dFen2_dmeamrho0
             dF_dmeamemb3 = dFen_dmeamemb3/dble(nEn) - (2d0*avg_deltaE/dble(nEn)) * dFen2_dmeamemb3
             dF_dmeamemb4 = dFen_dmeamemb4/dble(nEn) - (2d0*avg_deltaE/dble(nEn)) * dFen2_dmeamemb4
             dF_dpairpot =  dFen_dpairpot/dble(nEn)  - (2d0*avg_deltaE/dble(nEn)) * dFen2_dpairpot
          endif
       endif
    endif

    !Contribution from forces
    if ( (ANY(computeForces.eqv..true.)).and.( (Ffrc.gt.0d0).or.FfrcNan) ) then
       F=F+Ffrc/(dble(nFrcComp)*varFrc)
       !print *,'F after force=',F,'Ffrc=',Ffrc,'nFrcComp=',nFrcComp,'varFrc=',varFrc
       FnoScl=FnoScl+Ffrc !/dble(nFrcComp)
       Ffrc=Ffrc/(dble(nFrcComp)*varFrc)
       !print *,'Ffrc denom=',dble(nFrcComp)*varFrc
       if (noOpt.eqv..false.) then
          dF_dmeamtau=dF_dmeamtau+dFfrc_dmeamtau/(dble(nFrcComp)*varFrc)
          !print *,'adding force part to dF_dmeamtau: ',dFfrc_dmeamtau/(dble(nFrcComp)*varFrc),' (dFfrc_dmeamtau=',dFfrc_dmeamtau,', nFrcComp=',nFrcComp,', varFrc=',varFrc,')'

          dF_draddens=dF_draddens+dFfrc_draddens/(dble(nFrcComp)*varFrc)
          dF_dmeame0=dF_dmeame0+dFfrc_dmeame0/(dble(nFrcComp)*varFrc)
          dF_dmeamrho0=dF_dmeamrho0+dFfrc_dmeamrho0/(dble(nFrcComp)*varFrc)
          dF_dmeamemb3=dF_dmeamemb3+dFfrc_dmeamemb3/(dble(nFrcComp)*varFrc)
          dF_dmeamemb4=dF_dmeamemb4+dFfrc_dmeamemb4/(dble(nFrcComp)*varFrc)
          dF_dpairpot=dF_dpairpot+dFfrc_dpairpot/(dble(nFrcComp)*varFrc)
       endif
    endif

    !Contribution from stress tensor
    if ( (ANY(computeForces.eqv..true.)).and.( (Fstr.gt.0d0).or.FstrNan) ) then
       F=F+Fstr/(dble(nStrComp)*varStr)
       FnoScl=FnoScl+Fstr !/dble(nStrComp)
       Fstr=Fstr/(dble(nStrComp)*varStr)
       if (noOpt.eqv..false.) then
          dF_dmeamtau=dF_dmeamtau+dFstr_dmeamtau/(dble(nStrComp)*varStr)
          !print *,'adding stress part to dF_dmeamtau: ',dFstr_dmeamtau/(dble(nStrComp)*varStr),' (dFstr_dmeamtau=',dFstr_dmeamtau,', nStrComp=',nStrComp,', varStr=',varStr,')'
          !print *,'Fstr=',Fstr,' FstrNaN=',FstrNaN,' so, adding dFstr_dmeamtau (=',&
          !   dFstr_dmeamtau,') to dF_dmeamtau'
          dF_draddens=dF_draddens+dFstr_draddens/(dble(nStrComp)*varStr)
          dF_dmeame0=dF_dmeame0+dFstr_dmeame0/(dble(nStrComp)*varStr)
          dF_dmeamrho0=dF_dmeamrho0+dFstr_dmeamrho0/(dble(nStrComp)*varStr)
          dF_dmeamemb3=dF_dmeamemb3+dFstr_dmeamemb3/(dble(nStrComp)*varStr)
          dF_dmeamemb4=dF_dmeamemb4+dFstr_dmeamemb4/(dble(nStrComp)*varStr)
          dF_dpairpot=dF_dpairpot+dFstr_dpairpot/(dble(nStrComp)*varStr)
       endif
    endif

    F=sqrt(F)

    if (noOpt.eqv..false.) then
       !Derivatives of objective function
       dFpartial=0.5d0/F
       dF_dmeamtau=dFpartial*dF_dmeamtau
       dF_draddens=dFpartial*dF_draddens
       dF_dmeame0=dFpartial*dF_dmeame0
       dF_dmeamrho0=dFpartial*dF_dmeamrho0
       dF_dmeamemb3=dFpartial*dF_dmeamemb3
       dF_dmeamemb4=dFpartial*dF_dmeamemb4
       dF_dpairpot=dFpartial*dF_dpairpot
       dF_denconst=dFpartial*dF_denconst
       !print *,'dF_dmeamtau=',dF_dmeamtau
       !print *,'dF_draddens=',dF_draddens
       !print *,'dF_dmeame0=',dF_dmeame0
       !print *,'dF_dmeamrho0=',dF_dmeamrho0
       !print *,'dF_dmeamemb3=',dF_dmeamemb3
       !print *,'dF_dmeamemb4=',dF_dmeamemb4
       !print *,'dF_dpairpot=',dF_dpairpot
       !print *,'dFen_dmeamtau=',dFen_dmeamtau

       !print *,'dFfrc_dmeamtau=', dFfrc_dmeamtau
       !print *,'dFfrc_draddens=', dFfrc_draddens
       !print *,'dFfrc_dmeame0=',  dFfrc_dmeame0
       !print *,'dFfrc_dmeamrho0=',dFfrc_dmeamrho0
       !print *,'dFfrc_dmeamemb3=',dFfrc_dmeamemb3
       !print *,'dFfrc_dmeamemb4=',dFfrc_dmeamemb4
       !print *,'dFfrc_dpairpot=', dFfrc_dpairpot
       !print *,'dFstr_dmeamtau=', dFstr_dmeamtau
       !print *,'dFstr_draddens=', dFstr_draddens
       !print *,'dFstr_dmeame0=',  dFstr_dmeame0
       !print *,'dFstr_dmeamrho0=',dFstr_dmeamrho0
       !print *,'dFstr_dmeamemb3=',dFstr_dmeamemb3
       !print *,'dFstr_dmeamemb4=',dFstr_dmeamemb4
       !print *,'dFstr_dpairpot=', dFstr_dpairpot
       !print *,'dble(nFrcComp),varFrc=',dble(nFrcComp),varFrc
       !print *,'dFpartial=',dFpartial
    endif

    !Objective function penalties (for cutoffs and l=0 densities close to their
    !limits)
    FnoPen=F
    if (cutoffPen) then
       !Penalty due to cutoffs
       call calcCutoffPen
       F=F+FcutoffPen
       if (noOpt.eqv..false.) then
          dF_draddens=dF_draddens+dFcutoffPen_draddens
          dF_dpairpot=dF_dpairpot+dFcutoffPen_dpairpot
       endif
       !print *,'F=',FnoPen,' (without penalty)'
       !print *,'F=',F,' (including penalty)'
    endif

    !   if (lowDensPen) then
    !      !Add penalty due to minima in the l=0 electron densities that are close
    !      !to crossing over to negative. See 'Negative Density Test' written notes.
    !      !print *,'computing neg dens pen'
    !      print *,'Evaluating calcLowDensPen'
    !      call calcLowDensPen
    !      F=F+FlowDensPen
    !      dF_draddens=dF_draddens+dFlowDensPen_draddens !The latter has zero elements for l>0
    !      print *,'...completed'
    !   endif
    if (noOpt.eqv..false.) then
       !Check none of the derivatives of the objective function are un-defined (NaN)
       do isp=1,maxspecies
          do l=1,lmax
             if (dF_dmeamtau(l,isp)/=dF_dmeamtau(l,isp)) then
                print *,'ERROR: dF_dmeamtau(l=',l,',isp=',isp,') = ',dF_dmeamtau(l,isp)! ,', STOPPING'
                print *,'...yet F=',F,'...?'
                !stop
             endif
          enddo
       enddo
       do l=0,lmax
           do ipara=1,4
              if (dF_draddens(ipara,l,1).ne.dF_draddens(ipara,l,1)) then
                print *,'ERROR: dF_draddens(ipara=',ipara,',l=',l,',isp=1,jsp=1) = ',dF_draddens(ipara,l,1)!, STOPPING'
                print *,'...and yet F=',F,'...?'
                !stop
             endif
          enddo
       enddo
       do isp=1,maxspecies
          if (dF_dmeame0(isp).ne.dF_dmeame0(isp)) then
             print *,'ERROR: dF_dmeame0(isp=',isp,')= ',dF_dmeame0(isp)!, STOPPING'
             print *,'...and yet F=',F,'...?'
             !stop
          endif
       enddo
       do isp=1,maxspecies
          if (dF_dmeamrho0(isp).ne.dF_dmeamrho0(isp)) then
             print *,'ERROR: dF_dmeamrho0(isp=',isp,')= ',dF_dmeamrho0(isp)!, STOPPING'
             print *,'...and yet F=',F,'...?'
             !stop
          endif
       enddo
       do isp=1,maxspecies
          if (dF_dmeamemb3(isp).ne.dF_dmeamemb3(isp)) then
             print *,'ERROR: dF_dmeamemb3(isp=',isp,')= ',dF_dmeamemb3(isp)!NaN, STOPPING'
             print *,'...and yet F=',F,'...?'
             !stop
          endif
       enddo
       do isp=1,maxspecies
          if (dF_dmeamemb4(isp).ne.dF_dmeamemb4(isp)) then
             print *,'ERROR: dF_dmeamemb4(isp=',isp,')= ',dF_dmeamemb4(isp)!, STOPPING'
             print *,'...and yet F=',F,'...?'
             !stop
          endif
       enddo
       do ipara=1,4
          do isp=1,maxspecies
             do jsp=isp,maxspecies
                 if (dF_dpairpot(ipara,isp,jsp).ne.dF_dpairpot(ipara,isp,jsp)) then
                    print *,'ERROR: dF_dpairpot(ipara=',ipara,',isp=',isp,',jsp=',jsp,') = ',dF_dpairpot(ipara,isp,jsp)!, STOPPING'
                    print *,'...and yet F=',F,'...?'
                    !stop
                 endif
             enddo
          enddo
       enddo
    endif

    !Divide through funcforc and funcen to get s.d.s
    if (funcforcdenom.ne.0d0) then
        funcforc=sqrt(funcforc/funcforcdenom)
    endif
    if (funcendenom.ne.0d0) then
        funcen=sqrt(funcen/funcendenom)
    endif
    if (funcstrdenom.ne.0d0) then
        funcstr=sqrt(funcstr/funcstrdenom)
    endif

end subroutine objectiveFunction



subroutine calcCutoffPen

    use m_generalinfo
    use m_meamparameters
    use m_objectiveFunction
    use m_optimization
    use m_atomproperties

    implicit none

    integer isp,jsp,o,ip,iiCutoff

    !Calculate FcutoffPen and derivatives w.r.t parameters
    FcutoffPen=0d0
    if (noopt.eqv..false.) then
       dFcutoffPen_draddens=0d0
    endif
    do isp=1,maxspecies
       do jsp=1,maxspecies
          if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
             do o=0,lmax
                if (noptdensityCutoff(o,isp,jsp).gt.0) then
                   do iiCutoff=1,noptdensityCutoff(o,isp,jsp)
                      ip = ioptdensityCutoff(iiCutoff,o,isp,jsp)
                      if (meamrhodecay(ip,o,isp,jsp).gt.(cutoffMax-cutoffPenDist)) then
                         FcutoffPen=FcutoffPen+cutoffPenPrefact* &
                   (meamrhodecay(ip,o,isp,jsp)-(cutoffMax-cutoffPenDist))**3
                         if (noopt.eqv..false.) then
                            dFcutoffPen_draddens(ip,o,jsp)= 3d0*cutoffPenPrefact* &
                      (meamrhodecay(ip,o,isp,jsp)-(cutoffMax-cutoffPenDist))**2
                         endif
                         if ( ((cutoffPenPrefact* &
                  (meamrhodecay(ip,o,isp,jsp)-(cutoffMax-cutoffPenDist))**3).gt.10d-15).and.(debug.eqv..true.) ) then
                   !       print *,'meamrhodecay(',ip,',',o,',',isp,',',jsp, &
                   !                  ')=',meamrhodecay(ip,o,isp,jsp)
                   !       print *,'   contribution:',cutoffPenPrefact* &
                   ! (meamrhodecay(ip,o,isp,jsp)-(cutoffMax-cutoffPenDist))**3
                         endif
                         if (meamrhodecay(ip,o,isp,jsp).gt.cutoffMax+1d-15) then
                            print *,'ERROR: meamrhodecay(',ip,',',o,',',isp,',',jsp, &
                                ')=',meamrhodecay(ip,o,isp,jsp),' > ',cutoffMax
                            stop
                         endif
                      elseif (meamrhodecay(ip,o,isp,jsp).lt.(cutoffMin+cutoffPenDist)) then
                         FcutoffPen=FcutoffPen+cutoffPenPrefact* &
                   ((cutoffMin+cutoffPenDist)-meamrhodecay(ip,o,isp,jsp))**3
                         if (noopt.eqv..false.) then
                            dFcutoffPen_draddens(ip,o,jsp)= -3d0*cutoffPenPrefact* &
                      ((cutoffMin+cutoffPenDist)-meamrhodecay(ip,o,isp,jsp))**2
                         endif
                         if ( ((cutoffPenPrefact* &
                  ((cutoffMin+cutoffPenDist)-meamrhodecay(ip,o,isp,jsp))**3).gt.10d-15).and.(debug.eqv..true.) ) then
                   !        print *,'meamrhodecay(',ip,',',o,',',isp,',',jsp, &
                   !                   ')=',meamrhodecay(ip,o,isp,jsp)
                   !        print *,'   contribution:',cutoffPenPrefact* &
                   !  ((cutoffMin+cutoffPenDist)-meamrhodecay(ip,o,isp,jsp))**3
                         endif
                      endif
                   enddo
                endif
             enddo
          endif
       enddo
    enddo
    if (noopt.eqv..false.) then
       dFcutoffPen_dpairpot=0d0
    endif
    do isp=1,maxspecies
       do jsp=isp,maxspecies
          if (noptpairpotCutoff(isp,jsp).gt.0) then
             do iiCutoff=1,noptpairpotCutoff(isp,jsp)
                ip = ioptpairpotCutoff(iiCutoff,isp,jsp)
                if (pairpotparameter(ip,isp,jsp).gt.(cutoffMax-cutoffPenDist)) then
                   FcutoffPen=FcutoffPen+cutoffPenPrefact* &
            (pairpotparameter(ip,isp,jsp)-(cutoffMax-cutoffPenDist))**3
                   if (noopt.eqv..false.) then
                      dFcutoffPen_dpairpot(ip,isp,jsp)=3d0*cutoffPenPrefact* &
               (pairpotparameter(ip,isp,jsp)-(cutoffMax-cutoffPenDist))**2
                   endif
                   if ( ((cutoffPenPrefact* &
            (pairpotparameter(ip,isp,jsp)-(cutoffMax-cutoffPenDist))**3).gt.10d-15).and.(debug.eqv..true.) ) then
            !         print *,'pairpotentialparameter(',ip,',',isp,',',jsp, &
            !                   ')=',pairpotparameter(ip,isp,jsp)
            !         print *,'   contribution:',cutoffPenPrefact* &
            !  (pairpotparameter(ip,isp,jsp)-(cutoffMax-cutoffPenDist))**3
                   endif
                   if (pairpotparameter(ip,isp,jsp).gt.cutoffMax+1d-15) then
                      print *,'ERROR: pairpotparameter(',ip,',',isp,',',jsp, &
                          ')=',pairpotparameter(ip,isp,jsp),' > ',cutoffMax
                      stop
                   endif
                elseif (pairpotparameter(ip,isp,jsp).lt.(cutoffMin+cutoffPenDist)) then
                   FcutoffPen=FcutoffPen+cutoffPenPrefact* &
            ((cutoffMin+cutoffPenDist)-pairpotparameter(ip,isp,jsp))**3
                   if (noopt.eqv..false.) then
                      dFcutoffPen_dpairpot(ip,isp,jsp)=-3d0*cutoffPenPrefact* &
               ((cutoffMin+cutoffPenDist)-pairpotparameter(ip,isp,jsp))**2
                   endif
                   if ( (( cutoffPenPrefact* &
            ((cutoffMin+cutoffPenDist)-pairpotparameter(ip,isp,jsp))**3).gt.10d-15).and.(debug.eqv..true.) ) then
             !        print *,'pairpotentialparameter(',ip,',',isp,',',jsp, &
             !                  ')=',pairpotparameter(ip,isp,jsp)
             !        print *,'   contribution:',cutoffPenPrefact* &
             ! ((cutoffMin+cutoffPenDist)-pairpotparameter(ip,isp,jsp))**3
                   endif
                endif
             enddo
          endif
       enddo
    enddo
    !print *,'total cutoffPen=',FcutoffPen

end subroutine calcCutoffPen


!   subroutine calcLowDensPen
!
!      !Scan electron densities for values approaching zero and add appropriate
!      !penalty to FlowDensPen to discourage the value getting smaller (and
!      !ultimately negative). Distinguish between 'unwanted' small and the natural
!      !r -> p_rmax decay by checking the density only at the following r-points:
!      !r=0; r=minima. For further details see 'NEGATIVE DENSITY TEST' in my notes.
!      !
!      !Andrew Ian Duff, STFC, Copyright 2017
!
!      use m_generalinfo
!      use m_objectiveFunction
!      use m_atomproperties
!      use m_meamparameters
!
!      implicit none
!
!      integer i,isp,jsp
!      real(8) r,densityPrev,densityGrad,densityGradPrev,rSepn,tmpDeriv,lowDensPen_cutoff
!      real(8) rij,density(0:lmax),ddensity_dpara(12,0:lmax)
!      real(8), parameter:: rNum=100 !Number of radial points to check (excluding =0)
!
!      rSepn=p_rmax/(dble(rNum))
!      FlowDensPen=0d0
!      dFlowDensPen_draddens=0d0
!
!      if (thiaccptindepndt.eqv..false.) then
!         print *,'update code'
!         stop
!      else
!         isp=1
!         do jsp=1,maxspecies
!            call raddens(0d0,isp,jsp,density,ddensity_dpara)
!            !   if (density(0).lt.lowDensPen_cutoffFrac) then
!            !      !Found a too-small density(r=0)
!            !      FlowDensPen=FlowDensPen+lowDensPen_coeff*(lowDensPen_cutoffFrac-density(0))**2
!            !      print *,'adding penalty for jsp=',jsp,' at r=0, amount=', &
!            !       lowDensPen_coeff*(lowDensPen_cutoffFrac-density(0))**2
!            !      print *,'   (density=',density(0),')'
!            !      !Derivatives:
!            !      tmpDeriv=2d0*lowDensPen_coeff*(lowDensPen_cutoffFrac-density(0))
!            !      dFlowDensPen_draddens(1:12,0,jsp)=dFlowDensPen_draddens(1:12,0,jsp) + &
!            !         tmpDeriv*ddensity_dpara(1:12,0)
!            !   endif
!            lowDensPen_cutoff=lowDensPen_cutoffFrac*density(0)
!            densityPrev=density(0)
!            do i=1,rNum
!               r=dble(i)*rSepn
!               call raddens(r,isp,jsp,density,ddensity_dpara)
!               densityGrad=(density(0)-densityPrev)/rSepn
!               !print *,'r=',r,'/',rNum,', densityGrad=',densityGrad,', densityGradPrev=',densityGradPrev,', density(0)=',density(0),', lowDensPen_cutoff=',lowDensPen_cutoff
!               if ((i.gt.1).and.(densityGrad*densityGradPrev.lt.0d0).and.(density(0).gt.densityPrev)) then
!                  !Found a minimum (last condition above checks it is not maximum), now check if it is too small
!                  print *,'found minimum. density(0)=',density(0),', lowDensPen_cutoff=',lowDensPen_cutoff
!                  if (density(0).lt.lowDensPen_cutoff) then
!                     FlowDensPen=FlowDensPen+lowDensPen_coeff*(lowDensPen_cutoff-density(0))**2
!                     print *,'adding penalty for jsp=',jsp,' at r=',r,', amount=', &
!                      lowDensPen_coeff*(lowDensPen_cutoff-density(0))**2
!                     print *,'   (density=',density(0),')'
!                     !Derivatives:
!                     tmpDeriv=2d0*lowDensPen_coeff*(lowDensPen_cutoff-density(0))
!                     dFlowDensPen_draddens(1:12,0,jsp)=dFlowDensPen_draddens(1:12,0,jsp) + &
!                        tmpDeriv*ddensity_dpara(1:12,0)
!                     !NB: ddensity_dpara(12,0:lmax)
!                     !    dFlowDensPen_draddens(12,0:lmax,maxspecies))
!                  endif
!               endif
!               densityPrev=density(0)
!               densityGradPrev=densityGrad
!            enddo
!         enddo
!      endif
!
!      !Debug code:
!      if (FlowDensPen.gt.0d0) then
!        open(51,file='density_debug.dat')
!        isp=1
!        do jsp=1,maxspecies
!           do i=1,rNum
!              r=dble(i)*rSepn
!              call raddens(r,isp,jsp,density,ddensity_dpara)
!              write(51,*) r,density(0)
!           enddo
!        enddo
!        close(51)
!        stop
!      endif
!
!   end subroutine calcLowDensPen

!   Re insert the following to test the analytic derivatives of meamenergy:
!
!               print *,'analytic:'
!               print *,'dsummeamf_dmeamtau(1:lmax,1:maxspecies)=',dsummeamf_dmeamtau(1:lmax,1:maxspecies)
!               print *,'dsummeamf_dpara(1:12,0:lmax,1:maxspecies,1:maxspecies)=',dsummeamf_dpara(1:12,0:lmax,1:maxspecies,1:maxspecies)
!               print *,'dsummeamf_dmeame0=',dsummeamf_dmeame0
!               print *,'dsummeamf_dmeamrho0=',dsummeamf_dmeamrho0
!               print *,'dsummeamf_dmeamemb3=',dsummeamf_dmeamemb3
!               print *,'dsummeamf_dmeamemb4=',dsummeamf_dmeamemb4
!               print *,'dsummeam_paire_dpara=',dsummeam_paire_dpara
!               print *,'finished'
!
!               print *,'numerical:'
!
!               print *,'meamtau:'
!               do isp=1,maxspecies
!                  do ll=1,lmax
!                      meamtau(ll,isp)=meamtau(ll,isp)+disp
!                      call meamenergy
!                      meamtau(ll,isp)=meamtau(ll,isp)-disp
!                      print *,'denergy_meamtau(',ll,',',isp,')=',(energy-fitdata(idatapoint))/disp
!                  enddo
!               enddo
!
!               print *
!               print *,'denergy_dRaddens:' !dsummeamf_dpara(1:12,0:lmax,1:maxspecies,isp)
!               do jsp=1,maxspecies
!                  do isp=1,maxspecies
!                     do ll=0,lmax
!                        do ipara=1,4
!                           meamrhodecay(ipara,ll,isp,jsp)=meamrhodecay(ipara,ll,isp,jsp)+disp
!                           call meamenergy
!                           meamrhodecay(ipara,ll,isp,jsp)=meamrhodecay(ipara,ll,isp,jsp)-disp
!                           print *,'denergy_dRaddens(',ipara,',',ll,',',isp,',',jsp,')=',(energy-fitdata(idatapoint))/disp
!                        enddo
!                     enddo
!                  enddo
!               enddo
!
!               print *
!               print *,'denergy_dembfuncparas:'
!               do isp=1,maxspecies
!                  meame0(isp)=meame0(isp)+disp
!                  call meamenergy
!                  meame0(isp)=meame0(isp)-disp
!                  print *,'denergy_dmeame0(',isp,')=',(energy-fitdata(idatapoint))/disp
!               enddo
!               do isp=1,maxspecies
!                  meamrho0(isp)=meamrho0(isp)+disp
!                  call meamenergy
!                  meamrho0(isp)=meamrho0(isp)-disp
!                  print *,'denergy_dmeamrho0(',isp,')=',(energy-fitdata(idatapoint))/disp
!               enddo
!                do isp=1,maxspecies
!                  meamemb3(isp)=meamemb3(isp)+disp
!                  call meamenergy
!                  meamemb3(isp)=meamemb3(isp)-disp
!                  print *,'denergy_dmeamemb3(',isp,')=',(energy-fitdata(idatapoint))/disp
!               enddo
!                do isp=1,maxspecies
!                  meamemb4(isp)=meamemb4(isp)+disp
!                  call meamenergy
!                  meamemb4(isp)=meamemb4(isp)-disp
!                  print *,'denergy_dmeamemb4(',isp,')=',(energy-fitdata(idatapoint))/disp
!               enddo
!
!
!               print *
!               print *,'pair-potential:'
!               do ipara=1,4 !AnalyticDeriv
!                  do isp=1,maxspecies !AnalyticDeriv
!                     do jsp=isp,maxspecies !AnalyticDeriv
!                        pairpotparameter(ipara,isp,jsp)=pairpotparameter(ipara,isp,jsp)+disp
!                        call meamenergy
!                        pairpotparameter(ipara,isp,jsp)=pairpotparameter(ipara,isp,jsp)-disp
!                        print *,'dsummeam_paire_dpara(',ipara,isp,jsp,')=',(energy-fitdata(idatapoint))/disp
!                     enddo
!                  enddo
!               enddo
!
!               stop
