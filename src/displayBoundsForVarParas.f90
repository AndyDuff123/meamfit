subroutine displayBoundsForVarParas

    !--------------------------------------------------------------c
    !
    !     Write to standard output the limits which are to be used 
    !     to initialize the random starting values of the MEAM 
    !     parameters.
    !
    !     Called by: MEAMfit
    !     Calls: -
    !     Returns: -
    !     Files read: -
    !     Files written: -
    !
    !     Andrew Duff, 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization

    implicit none

    logical meamtauLimitsSame,meamtauRandParasSame
    integer i,j,ii,ll,isp,jsp,spc_cnt,minorderStr,maxorderStr
    integer nfreepTmp

    !cmin and cmax
    j=1
    isp=1
    jsp=1
    do i=1,m3
        if (freep(i).ge.2) then
            write(*,*) 'cmin(',j,',',isp,',',jsp,') ', &
                'to be randomly seeded'
        endif
        if (j.lt.m1) then
            j=j+1
        else
            if (isp.lt.m1) then
                j=1
                isp=isp+1
            else
                j=1
                isp=1
                jsp=jsp+1
            endif
        endif
    enddo
    j=1
    isp=1
    jsp=1
    do i=m3+1,2*m3
        if (freep(i).ge.2) then
            write(*,*) 'cmax(',j,',',isp,',',jsp,') ', &
                'to be randomly seeded'
        endif   
        if (j.lt.m1) then
            j=j+1   
        else        
            if (isp.lt.m1) then
                j=1 
                isp=isp+1
            else
                j=1
                isp=1
                jsp=jsp+1
            endif
        endif
    enddo

    !meamtau:
    !First check if all meamtau paras are to be randonly generated and that
    !their limits for random para gen are the same.
    if (lmax.gt.0) then

       if (randgenparas) then

          meamtauRandParasSame=.true.
          meamtauLimitsSame=.true.
          minorderStr=meamtau_minorder(1,1)
          maxorderStr=meamtau_maxorder(1,1)
          do ll=1,lmax
             do isp=1,maxspecies
                if (freep(2*m3+isp+(ll-1)*m1).lt.2) then
                   meamtauRandParasSame=.false.
                endif
                if ((meamtau_minorder(ll,isp).ne.minorderStr).or. &
                    (meamtau_maxorder(ll,isp).ne.maxorderStr)) then
                   meamtauLimitsSame=.false.
                endif
             enddo
          enddo

          if (meamtauRandParasSame.and.meamtauLimitsSame) then
             write(*,'(A19,I1,A5,I1,A1)') '    meamtau(isp=1-',maxspecies,',l=1-',lmax,')'
             write(*,'(A25,I2,A6,I2)') '       Can vary from: 10^', &
                meamtau_minorder(1,1),' -10^',meamtau_maxorder(1,1)
          else
             do ll=1,lmax
                 do isp=1,maxspecies
                     if (freep(2*m3+isp+(ll-1)*m1).ge.2) then
                         print *,'    meamtau(isp=',isp,',l=',ll,')'
                         write(*,'(A25,I2,A6,I2)') '       Can vary from: 10^', &
                             meamtau_minorder(ll,isp),' -10^',meamtau_maxorder(ll,isp)
                     endif
                 enddo
             enddo
          endif

       endif

    endif

    !electron density functions
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    !Check if we need to generate random paramaters for
                    !thi(isp,jsp,ll)
                    nfreepTmp=0
                    do i=1,12
                        if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i).ge.2) then
                            nfreepTmp=nfreepTmp+1
                        endif
                    enddo
                    write(*,'(A8,I1,A1,I1,A3,I1,A2)') &
                        '    thi(',isp,',',jsp,',l=',ll,'):'
                    if (typethi(isp,jsp).eq.1) then !Cubic form
                        !Read in parameters from 'dataforMEAMparagen'
                        !necessary
                        !to generate these parameters
                        write(*,*) '      Function: Sum over cubic terms'
                        write(*,'(A41,I2)') '       Number paras to randomly generate:',nfreepTmp
                        if (nfreepTmp.gt.0) then
                           if (holdcutoffs) then
                              write(*,'(A40)',advance='no') &
                                  '      Cutoffs, initially held fixed at:'
                              do i=1,ncutoffDens(isp,jsp)
                                  write(*,'(A1,F5.3)',advance='no') ' ', &
                                     cutoffDens(i,isp,jsp)
                                  if (i.lt.ncutoffDens(isp,jsp)) then
                                     write(*,'(A1)',advance='no') ','
                                  endif
                              enddo
                           else
                              write(*,'(A36,F5.3,A1,F5.3)') &
                                  '       Allowed cutoff radius range: ', &
                                  meamrhodecay_minradius(ll,isp,jsp),'-', &
                                  meamrhodecay_maxradius(ll,isp,jsp)
                           endif
                        endif
                        if (meamrhodecay_negvals(ll,isp,jsp).eq.1) then
                            write(*,*) '      Cutoffs and coefficients positive.'
                        elseif (meamrhodecay_negvals(ll,isp,jsp).eq.2) then
                            write(*,*) '      Cutoffs positive, coefficients positive or negative.'
                        else
                            print *,'      ERROR: meamrhodecay_negvals must be 1 or 2,'
                            print *,'      stopping.'
                            stop
                        endif

                        !For each coefficient which needs to be generated,
                        !read in
                        !min and max orders (10^..)
                        ii=1
                        do i=1,6
                            if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i-1).ge.2) &
                                then
                                write(*,'(A19,I2,A19,I2,A6,I2)') &
                                    '       Coefficient ',ii,' can vary from: 10^', &
                                    meamrhodecay_minorder(ii,ll,isp,jsp),' - 10^', &
                                    meamrhodecay_maxorder(ii,ll,isp,jsp)
                                ii=ii+1
                            endif
                        enddo
                    endif
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !embedding functions
    do isp=1,maxspecies
        do i=1,4
            if (freep(2*m3+lmax*m1+12*lm1*m2+isp+(i-1)*m1).ge.2) then
                if (embfuncRandType.eq.1) then
                 if (i.eq.1) then
                     write(*,'(A10,I1,A2)') '    E_emb(',isp,'):'
                     write(*,'(A29,F10.5,A3,F10.5)') &
                         '       meame0 can vary from: ', &
                         meame0_minorder(isp),' - ', &
                         meame0_maxorder(isp)
                 elseif (i.eq.2) then
                     write(*,'(A31,F10.5,A3,F10.5)') &
                         '       meamrho0 can vary from: ', &
                         meamrho0_minorder(isp),' - ', &
                         meamrho0_maxorder(isp)
                 elseif (i.eq.3) then
                     write(*,'(A31,F10.5,A3,F10.5)') &
                         '       meamemb3 can vary from: ', &
                         meamemb3_minorder(isp),' - ', &
                         meamemb3_maxorder(isp)
                 elseif (i.eq.4) then
                     write(*,'(A31,F10.5,A3,F10.5)') &
                         '       meamemb4 can vary from: ', &
                         meamemb4_minorder(isp),' - ', &
                         meamemb4_maxorder(isp)
                 endif
                else
                 if (i.eq.1) then
                     write(*,'(A10,I1,A2)') '    E_emb(',isp,'):'
                     write(*,'(A32,F10.5,A6,F10.5)') &
                         '       meame0 can vary from: 10^', &
                         meame0_minorder(isp),' - 10^', &
                         meame0_maxorder(isp)
                 elseif (i.eq.2) then
                     write(*,'(A34,F10.5,A6,F10.5)') &
                         '       meamrho0 can vary from: 10^', &
                         meamrho0_minorder(isp),' - 10^', &
                         meamrho0_maxorder(isp)
                 elseif (i.eq.3) then
                     write(*,'(A34,F10.5,A6,F10.5)') &
                         '       meamemb3 can vary from: 10^', &
                         meamemb3_minorder(isp),' - 10^', &
                         meamemb3_maxorder(isp)
                 elseif (i.eq.4) then
                     write(*,'(A34,F10.5,A6,F10.5)') &
                         '       meamemb4 can vary from: 10^', &
                         meamemb4_minorder(isp),' - 10^', &
                         meamemb4_maxorder(isp)
                 endif
                endif
            endif
        enddo
    enddo

    !pair-potentials
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                !Check if we need to generate random paramaters for
                !pairpot(isp,jsp)
                nfreepTmp=0
                do i=1,32
                    if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt) &
                        .ge.2) then
                        nfreepTmp=nfreepTmp+1
                    endif
                enddo
                write(*,'(A6,I1,A1,I1,A2)') &
                    '    V(',isp,',',jsp,'):'
                if (typepairpot(isp,jsp).eq.2) then
                    !Read in parameters from 'dataforMEAMparagen' necessary
                    !to
                    !generate these parameters
                    write(*,*) '      Function: Sum over cubic terms'
                    write(*,'(A41,I2)') '       Number paras to randomly generate:', &
                        nfreepTmp
                    if (nfreepTmp.gt.0) then
                       if (holdcutoffs) then
                          write(*,'(A40)',advance='no') &
                              '      Cutoffs, initially held fixed at:'
                          do i=1,ncutoffPairpot(isp,jsp)
                              write(*,'(A1,F5.3)',advance='no') ' ', &
                                 cutoffPairpot(i,isp,jsp)
                              if (i.lt.ncutoffPairpot(isp,jsp)) then
                                 write(*,'(A1)',advance='no') ','
                              endif
                          enddo
                       else
                          write(*,'(A36,F5.3,A1,F5.3)') &
                              '       Allowed cutoff radius range: ', &
                              pairpotparameter_minradius(isp,jsp),'-', &
                              pairpotparameter_maxradius(isp,jsp)
                       endif
                    endif
                    if (pairpotparameter_negvals(isp,jsp).eq.1) then
                        write(*,*) '      Cutoffs and coefficients positive.'
                    elseif (pairpotparameter_negvals(isp,jsp).eq.2) then
                        write(*,*) '      Cutoffs positive, coefficients positive or negative.'
                    else
                        print *,'      ERROR: pairpotparameter_negvals must be 1 or 2,'
                        print *,'      stopping.'
                        stop
                    endif
                    !For each coefficient which needs to be generated, read
                    !in min and max orders (10^..)
                    ii=1
                    do i=1,16
                        if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i-1+32*spc_cnt) &
                            .ge.2) then
                            write(*,'(A19,I2,A19,I2,A6,I2)') &
                                '       Coefficient ',ii,' can vary from: 10^', &
                                pairpotparameter_minorder(ii,isp,jsp),' - 10^', &
                                pairpotparameter_maxorder(ii,isp,jsp)
                            ii=ii+1
                        endif
                    enddo
                else
                    print *,'typepairpot=',typepairpot(isp,jsp), &
                        'not supported,stopping'
                    stop
                endif
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !enconst
    if (randgenparas) then
       do isp=1,maxspecies
           if (freep(m4+2*m2+isp).ge.2) then
               write(*,'(A12,I1,A20,F6.3,A6,F6.3)') &
                   '    enconst(',isp,') can vary from: 10^', &
                   enconst_minorder(isp),' - 10^',enconst_maxorder(isp)
           endif
       enddo
    endif

end subroutine displayBoundsForVarParas
