


module m_electrondensity !f_i_L_ij,rho_i_l

    !     Copyright (c) 2018, STFC

    !f_j_L_ij, computed in subroutine radialdensityfunction
    logical orthogElecDens
    real(8), allocatable:: fjlij(:,:,:),   & !L, site j, site i
        dfjlij_dpara(:,:,:,:), &             !n, L, site j, site i
        dfjlij_dxyz(:,:,:,:), &            !3, L, site j, site i
        d2fjlij_dxyz_dpara(:,:,:,:,:), &   !3, 12, L, site j, site i
        rhol(:,:),      &                    !L, site
        drhol_dpara(:,:,:,:), &              !n, L, jsp, site i
        drhol_dxyz(:,:,:,:), &                !3, site, L, site i
        d2rhol_dxyz_dpara(:,:,:,:,:,:), &       !3, site, 12, L, site i
        rho_i(:), &                          !site
        drhoi_dpara(:,:,:,:), &              !n, L, jsp, site i
        drhoi_dmeamtau(:,:,:), &             !L, jsp, site i
        drhoi_dxyz(:,:,:), &                 !3, site j, site i
        d2rhoi_dxyz_dmeamtau(:,:,:,:,:), &
        d2rhoi_dxyz_dpara(:,:,:,:,:,:)       !3, site j, 12, jsp, site i

    real(8), allocatable:: daux1_dxyz(:,:,:), daux1_dpara(:,:,:), daux2_dxyz(:,:,:,:), &
        daux2_dpara(:,:,:,:),daux2a_dxyz(:,:),daux2a_dpara(:,:),d2aux1_dxyz_dpara(:,:,:,:,:), &
        d2aux2_dxyz_dpara(:,:,:,:,:,:),d2aux2a_dxyz_dpara(:,:,:,:), &
        daux3_dxyz(:,:,:,:,:),daux3_dpara(:,:,:,:,:),d2aux3_dxyz_dpara(:,:,:,:,:,:,:), &
        daux3a_dxyz(:,:,:), daux3a_dpara(:,:,:), d2aux3a_dxyz_dpara(:,:,:,:,:)

end module m_electrondensity
