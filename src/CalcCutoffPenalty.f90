subroutine CalcCutoffPenalty

    !----------------------------------------------------------------------c
    !
    !     Currently un-used subroutine, but may be re-implemented again at 
    !     later date. Designed to apply a penalty to the optimization 
    !     function proportional to the square of the deviation of the 
    !     potential parameters from their starting values (originally to 
    !     try to control the cutoff radii, to stop them moving 
    !     uncontrollably beyond their limiting values).
    !
    !     Called by:     originally by 'fit_quality'
    !     Returns:       CutoffPenalty
    !     Files read:    -
    !     Files written: -
    !
    !     Andy Duff, Oct 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_optimization
    use m_generalinfo
    use m_meamparameters
    use m_atomproperties

    implicit none

    integer i,ip,isp,jsp,spc_cnt,ll

    CutoffPenalty=0d0
    CutoffPenCoeff=1d0 !Determines severity of penalty for deviation

    !Get contributions to CutoffPenalty from electron density cutoffs
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    do i=2,12,2
                       ip=2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i
                       CutoffPenalty=CutoffPenalty+CutoffPenCoeff*((p(ip)-p_orig(ip))**2)
                    enddo
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    
    !!Get contributions to CutoffPenalty from pair-potential cutoffs
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                do i=2,32,2
                   ip=2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt
                   CutoffPenalty=CutoffPenalty+CutoffPenCoeff*((p(ip)-p_orig(ip))**2)
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

end subroutine CalcCutoffPenalty
