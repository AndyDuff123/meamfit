subroutine initializemeam

    !--------------------------------------------------------------c
    !
    !     Initialize (initial) MEAM parameters either using a
    !     potential file provided by the user; a series of
    !     potential files in case of a continuation job; or set
    !     them to zero (later to be assigned random values) if
    !     they are to be initialized using parameters from the
    !     settings file. MEAM parameters are read both into the 
    !     work arrays: cmin, cmax, etc; and also into the p() 
    !     array.
    !
    !     Calls:         readmeamparam,variables_to_p,
    !                 p_to_variables
    !     Files read:    'startparameterfile' (filename provided
    !                 by user), potparas_best#n (n=1-10),
    !                 bestoptfuncs
    !     Returns:       cmin,cmax,meamtau,
    !                 meamrhodecay,meame0,meamrho0,meamemb3,
    !                 meamemb4,pairpotparameter,rs,rc,enconst,p()
    !
    !     Andrew Duff Jan 2015
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_atomproperties
    use m_filenames
    use m_generalinfo
    use m_optimization

    implicit none

    logical exist
    integer i,j,tmp
    character*1 tmp2
    character*4 char4
    character*20 string1
    character*80 filename,string
    

    if (verbosity.gt.1) then
       !print *
       !print *,'Potential initialization'
       !print *,'------------------------'
    endif

    if (.not.allocated(cmin)) then
        allocate( cmin(maxspecies,maxspecies,maxspecies), &
            cmax(maxspecies,maxspecies,maxspecies), &
            meamtau(1:lmax,maxspecies), &
            meamrhodecay(12,0:lmax,maxspecies,maxspecies), &
            meame0(maxspecies), &
            meamrho0(maxspecies), &
            meamemb3(maxspecies), &
            meamemb4(maxspecies), &
            pairpotparameter(32,maxspecies,maxspecies), &
            rs(maxspecies,maxspecies), &
            rc(maxspecies,maxspecies), &
            enconst(maxspecies)) !, &
            ! pairpotStr(splnNvals,maxspecies,maxspecies))
    endif

    cmin=0d0
    cmax=0d0
    meamtau=0d0
    meamrhodecay=0d0
    meame0=0d0
    meamrho0=0d0
    meamemb3=0d0
    meamemb4=0d0
    pairpotparameter=0d0
    rs=0d0
    rc=0d0
    enconst=0d0

    if (.not.allocated(p)) then
        allocate(p(np))
    endif

    p=0d0

    if (readpotfile.eqv..true.) then
       !if (verbosity.gt.1) print *,'Reading in potential parameters from ',trim(startparameterfile)
       call readmeamparam(startparameterfile) !Read into sensibly-named variables (meamrhodecay, etc)
       call variables_to_p !Now copy these variables into the p() array (necc. for CG optimizer)
    else
       if (contjob.eqv..true.) then
          ! Continuation job: Read in potentials from files
          allocate(p_saved(np,noptfuncstore),bestoptfuncs(noptfuncstore), &
              timeoptfuncHr(noptfuncstore),timeoptfuncMin(noptfuncstore))
          if (verbosity.gt.1) print *,'Reading in files from potparas_best#n, with #n=1,NOPTFUNCSTORE'
          open(50,file='bestoptfuncs')
          read(50,*) string
          if (verbosity.gt.1) print *,'string=',string
          if (string(1:3).ne.'Top') then
             print *,'ERROR: You do not yet have a full set of potparas_best files, stopping.'
             stop
          endif
          do i=1,noptfuncstore
             !Setup filename for each potential in turn
             filename="potparas_best"
             if (i.lt.10) then
                 write(string1,'(I1)') i
             elseif (i.lt.100) then
                 write(string1,'(I2)') i
             else
                 print *,'ERROR: more than 100 files set to save; code needs'
                 print *,'changing, STOPPING.'
                 stop
             endif
             filename=trim(filename)//trim(string1)

             !Read potential parameters from file
             print *,'starting with ',trim(adjustl(filename))
             call readmeamparam(trim(adjustl(filename)))
             call variables_to_p
             do j=1,np
                 p_saved(j,i)=p(j)
             enddo
             !Copy corresponding optimization function into bestoptfuncs
             read(50,*) char4,bestoptfuncs(i)
             timeoptfuncHr(i)=0
             timeoptfuncMin(i)=0
          enddo
          print *,'Bestopfuncs read in:'
          do i=1,noptfuncstore
             print *,i,': ',bestoptfuncs(i)
          enddo
       else
          !if (verbosity.gt.1) print *,'Potential parameters to be optimized read from settings file.'
          !Set-up dummy values for now, and then after the distance analysis,
          !assign values to the cut-off radii.
          call p_to_variables
       endif
    endif

end subroutine initializemeam
