subroutine allocateArrays

    !---------------------------------------------------------------c
    !
    !     Initializes arrays used to store all the parts necessary
    !     to compute the energies, forces and stress-tensors and
    !     their derivative w.r.t parameters and atomic positions.
    !     Also initialize arrays for storing the energies, forces
    !     and stress-tensors (and their derivatives) themselves.
    !     This array will be filled by 'meamenergy' and/or
    !     'meamforce' subroutines. Note that for single-shot
    !     calculation, the derivatives w.r.t parameters are not
    !     needed, but are calculated anyway, and therefore still
    !     need to be set up.
    !
    !     Andrew Duff
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_observables
    use m_meamparameters
    use m_atomproperties
    use m_geometry
    use m_datapoints
    use m_electronDensity
    use m_mpi_info
    use m_optimization

    implicit none

    integer iaux(1),iaux2,is

    !debug test
    iaux2=0
    do is=1,nstruct
       iaux=maxval( gn_neighbors(1:gn_inequivalentsites(is),is) )
       iaux2=max( iaux2, iaux(1) )
       !print *,'is=',is,' maxval(gn_nei...)=',iaux,' current maxneighbors (or iaux2, as it is called here)=',iaux2
    enddo
    if (iaux2.ne.maxneighbors) then
       print *,'iaux(1)=',iaux2
       print *,'maxneighbors=',maxneighbors
       print *,'...should be equal'
       print *,'They are not!'
       stop
    endif

    !debug test
    iaux=0
    do is=1,nstruct
       iaux=max( gn_inequivalentsites(is) , iaux(1) )
    enddo
    if (iaux(1).ne.maxsites) then
       print *,'iaux(1)=',iaux(1)
       print *,'maxsites=',maxsites
       print *,'...should be equal'
       print *,'They are not!'
       stop
    endif


    !---- Initialize arrays to store the parts used to compute the energies, forces and stress tensors ----

    !radial density arrays:
    allocate( fjlij(0:lmax,maxneighbors,maxsites), &
         dfjlij_dpara(12,0:lmax,maxneighbors,maxsites) )
    if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
       allocate( dfjlij_dxyz(1:3,0:lmax,maxneighbors,maxsites)  ,&
            d2fjlij_dxyz_dpara(1:3,12,0:lmax,maxneighbors,maxsites)  )
    endif

    !electron density arrays:
    allocate( rhol(0:lmax,maxsites), &
          drhol_dpara(12,0:lmax,1:maxspecies,maxsites) )
    if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
       allocate( drhol_dxyz(1:3,0:maxneighbors,0:lmax,maxsites)  , &
            d2rhol_dxyz_dpara(1:3,0:maxneighbors,12,0:lmax,1:maxspecies,maxsites)  )
    endif
    if (lmax.ge.0) then
       allocate( meam_t(1:lmax,maxsites), &
             dmeamt_dmeamtau(1:lmax,1:maxspecies,maxsites), &
             dmeamt_dpara(12,1:lmax,1:maxspecies,maxsites) )
       if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
          allocate( dmeamt_dxyz(3,0:maxneighbors,1:lmax,maxsites), &
                d2meamt_dxyz_dmeamtau(3,0:maxneighbors,1:lmax,1:maxspecies,maxsites), &
                d2meamt_dxyz_dpara(3,0:maxneighbors,1:lmax,12,1:maxspecies,maxsites) )
       endif
    endif
    if (lmax.ge.1) then
       allocate( daux1_dpara(3,1:12,1:maxspecies) )
       if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
          allocate( daux1_dxyz(3,0:maxneighbors,3), &
             d2aux1_dxyz_dpara(3,0:maxneighbors,3,12,maxspecies) )
       endif
    endif
    if (lmax.ge.2) then
       allocate( daux2_dpara(3,3,12,1:maxspecies), &
          daux2a_dpara(12,maxspecies) )
       if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
          allocate( daux2_dxyz(3,0:maxneighbors,3,3), &
             daux2a_dxyz(3,0:maxneighbors), &
             d2aux2_dxyz_dpara(3,0:maxneighbors,3,3,12,maxspecies), &
             d2aux2a_dxyz_dpara(3,0:maxneighbors,12,maxspecies) )
       endif
    endif
    if (lmax.ge.3) then
       allocate( daux3_dpara(3,3,3,12,1:maxspecies) )
       if (orthogElecDens) then
          allocate( daux3a_dpara(3,1:12,1:maxspecies) )
       endif
       if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
          allocate( daux3_dxyz(3,0:maxneighbors,3,3,3), &
             d2aux3_dxyz_dpara(3,0:maxneighbors,3,3,3,12,maxspecies))
          if (orthogElecDens) then
             allocate( daux3a_dxyz(3,0:maxneighbors,3), &
                d2aux3a_dxyz_dpara(3,0:maxneighbors,3,12,maxspecies) )
          endif
       endif
    endif

    !background density arrays:
    !print *,'allocating rho_i etc using size, maxsites=',maxsites
    allocate( rho_i(maxsites), &
       drhoi_dpara(12,0:lmax,1:maxspecies,maxsites) )
    if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
       allocate( drhoi_dxyz(3,0:maxneighbors,maxsites), &
          d2rhoi_dxyz_dpara(3,0:maxneighbors,12,0:lmax,maxspecies,maxsites) )
    endif
    if (lmax.ge.0) then
       allocate( drhoi_dmeamtau(1:lmax,1:maxspecies,maxsites))
       if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
          allocate( d2rhoi_dxyz_dmeamtau(3,0:maxneighbors,1:lmax,maxspecies,maxsites) )
       endif
    endif

    !embedding function:
    allocate( meam_f(maxsites), dmeamf_dpara(12,0:lmax,1:maxspecies,maxsites), &
              dmeamf_dmeame0(maxsites), &
              dmeamf_dmeamrho0(maxsites), &
              dmeamf_dmeamemb3(maxsites), &
              dmeamf_dmeamemb4(maxsites) )
    if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
       allocate( dmeamf_dxyz(1:3,0:maxneighbors,maxsites), &
                 d2meamf_dxyz_dpara(3,0:maxneighbors,12,0:lmax,maxspecies,maxsites), &
                 d2meamf_dxyz_dmeame0(3,0:maxneighbors,maxsites), &
                 d2meamf_dxyz_dmeamrho0(3,0:maxneighbors,maxsites), &
                 d2meamf_dxyz_dmeamemb3(3,0:maxneighbors,maxsites), &
                 d2meamf_dxyz_dmeamemb4(3,0:maxneighbors,maxsites) )
    endif

    if (lmax.gt.0) then
       allocate(dmeamf_dmeamtau(1:lmax,1:maxspecies,maxsites) )
       if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
          allocate(d2meamf_dxyz_dmeamtau(3,0:maxneighbors,1:lmax,maxspecies,maxsites) )
       endif
    endif

    !pair potential:
    allocate( meam_paire(maxsites) )
    if ((nFrcComp.gt.0).or.(nStrComp.gt.0)) then
       allocate( dmeam_paire_dxyz(1:3,0:maxneighbors,maxsites), &
                 d2meam_paire_dxyz_dpara(1:3,32,maxspecies,maxspecies,0:maxneighbors,maxsites) )
    endif
    !------------------------------------------------------------------------------------------------------


    !---- Initialize arrays for the observables themselves and their derivates w.r.t parameters and positions ----
    !Analytic derivatives of energy w.r.t parameters
    allocate(dsummeamf_dmeamtau(1:lmax,maxspecies),dsummeamf_dpara(12,0:lmax,maxspecies,maxspecies), &
        dsummeamf_dmeame0(maxspecies),dsummeamf_dmeamrho0(maxspecies),dsummeamf_dmeamemb3(maxspecies), &
        dsummeamf_dmeamemb4(maxspecies),dsummeam_paire_dpara(32,maxspecies,maxspecies), &
        dsummeam_paire_dpara_dummy(32,maxspecies,maxspecies))
    allocate(dsummeamf_dmeamtau_ref(9,1:lmax,maxspecies),dsummeamf_dpara_ref(9,12,0:lmax,maxspecies,maxspecies), &
        dsummeamf_dmeame0_ref(9,maxspecies),dsummeamf_dmeamrho0_ref(9,maxspecies),dsummeamf_dmeamemb3_ref(9,maxspecies), &
        dsummeamf_dmeamemb4_ref(9,maxspecies),dsummeam_paire_dpara_ref(9,32,maxspecies,maxspecies))
    !For interaction energies:
    allocate(dintEn_fit_dmeamtau(1:lmax,maxspecies),dintEn_fit_draddens(12,0:lmax,maxspecies), &
        dintEn_fit_dmeame0(maxspecies),dintEn_fit_dmeamrho0(maxspecies),dintEn_fit_dmeamemb3(maxspecies), &
        dintEn_fit_dmeamemb4(maxspecies),dintEn_fit_dpairpot(32,maxspecies,maxspecies))

    if ( ANY( computeForces.eqv..true. ) ) then
       !Forces and analytic derivatives of forces w.r.t parameters
       allocate(forces(3,maxval(gn_forces)))
       allocate(dforce_dpairpot(3,32,maxspecies,maxspecies,maxval(gn_forces)))
       allocate(dforce_draddens(3,12,0:lmax,maxspecies,maxval(gn_forces)))
       allocate(dforce_dmeamtau(3,1:lmax,maxspecies,maxval(gn_forces)))
       allocate(dforce_dmeame0(3,maxspecies,maxval(gn_forces)))
       allocate(dforce_dmeamrho0(3,maxspecies,maxval(gn_forces)))
       allocate(dforce_dmeamemb3(3,maxspecies,maxval(gn_forces)))
       allocate(dforce_dmeamemb4(3,maxspecies,maxval(gn_forces)))

       !Stress-Tensor and analytic derivatives w.r.t parameters
       allocate(dstressTensor_dpairpot(9,32,maxspecies,maxspecies))
       allocate(dstressTensor_draddens(9,12,0:lmax,maxspecies))
       allocate(dstressTensor_dmeamtau(9,1:lmax,maxspecies))
       allocate(dstressTensor_dmeame0(9,maxspecies))
       allocate(dstressTensor_dmeamrho0(9,maxspecies))
       allocate(dstressTensor_dmeamemb3(9,maxspecies))
       allocate(dstressTensor_dmeamemb4(9,maxspecies))
    endif
    !-------------------------------------------------------------------------------------------------------------

    call p_allocateArrays

end subroutine allocateArrays
