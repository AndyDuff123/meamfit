subroutine neighborhood(forcecalc)

    !----------------------------------------------------------------------c
    !
    !     Determine which atoms in the system are nearest neighbors (nn's)
    !     to at least one of the central atoms. Label each of these atoms
    !     from 1 to 'nnatoms' (the total number of such atoms), giving the
    !     central atoms the labels 1, ..., 'natoms' (Note: atoms are nn's
    !     if they are within 'p_rmax' of one another). Record the positions
    !     and species of these atoms in the arrays: 'xyz(1:3,1:nnatoms)'
    !     and 'species(1:nnatoms)'.
    !
    !     For each central atom, i, record which atoms are it's nn's in the
    !     array: 'neighborlist(1:n_neighbors(i),i)' (where 'n_neighbors(i)'
    !     is number of nearest neighbors of atom i). The elements of this
    !     array are just the labels defined in the previous paragraph.
    !
    !     For force-calculations: Note that the lattice has already been
    !     extended by the subroutine 'extendlattice' so that the
    !     atoms within sepn <= p_rmax of atoms in the original unit-cell
    !     are defined as inequivalent atoms (central atoms).
    !
    !     Called by:     initializestructuresandmeamparas
    !     Calls:         calculateshells
    !     Arguments:     natoms,nspecies,nat,crystalstrucfile,z,z2species,
    !                 coordinates,p_rmax,tr
    !     Returns:       species,xyz,nnatoms,n_neighbors,species,
    !                 neighborlist,n_inequivalentsites
    !     Files read:    filein
    !     Files written: crystalstrucfile
    !
    !     Marcel Sluiter, Feb 8 2006
    !     continued by Andy Duff, Dec, 2007
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_filenames
    use m_generalinfo
    use m_atomproperties
    use m_poscar
    use m_neighborlist
    use m_datapoints
    use m_optimization

    implicit none

    logical forcecalc
    integer i,it1,it2,it3,j,nshell,nnatoms_tot,atomnumber
    real(8) rmax2,xyztry1(3),xyztry2(3),xyztry(3),dxyz(3),dd,xy,xz,yz
    logical inlist

    if(allocated(xyz)) deallocate(xyz,species, &
        n_neighbors,neighborlist)

    n_inequivalentsites=natoms
    !Determine how many shells of unit cells should be included in
    !order that all nn's of the central atoms are included in the
    !calculation.
    if (forcecalc) then
        call calculateshells_forces(nshell,nnatoms_tot,p_rmax)
        !print *,'nshell after calculateshells_forces=',nshell
        !print *,'...nnatoms_tot=',nnatoms_tot
    else
        call calculateshells(nshell,nnatoms_tot,p_rmax)
    endif
    allocate(species(nnatoms_tot),xyz(3,nnatoms_tot), &
        n_neighbors(natoms),neighborlist(nnatoms_tot,natoms))

    if (outputLAMMPSposns.or.excludeCoulomb) then
        !Plot output positions files for LAMMPS
        write(50,*) 'Position data'
        write(50,*)
        write(50,*) natoms,' atoms'
        write(50,*) nspecies,' atom types'
        write(50,*)
        if ((tr(2,1).ne.0d0).or.(tr(3,1).ne.0d0).or.(tr(3,2).ne.0d0)) then
                print *,'ERROR: cannot create lammps tilt factors for lattice vectors because you'
                print *,'have none zero elements tr(2,1), tr(3,1) and/or tr(3,2), STOPPING.'
                stop
        endif
        write(50,*) '0.000000000 ',tr(1,1),'    xlo xhi'
        write(50,*) '0.000000000 ',tr(2,2),'    ylo yhi'
        write(50,*) '0.000000000 ',tr(3,3),'    zlo zhi'
        if ((tr(1,2).ne.0d0).or.(tr(1,3).ne.0d0).or.(tr(2,3).ne.0d0)) then
                xy=tr(1,2)
                xz=tr(1,3)
                yz=tr(2,3)
                write(50,'(3F20.15,A12)') xy,xz,yz,'    xy xz yz'
        endif
        write(50,*)
        write(50,*) 'Atoms'
        write(50,*)

    endif

    n_inequivalentsites = natoms
    do i=1,n_inequivalentsites

        species(i)=z2species(zz(i))
        xyz(1:3,i)=coordinates(1:3,i)
        if (outputLAMMPSposns) then
            !if ((forcecalc).and.(i.le.natoms_old)) then
            write(50,'(I5,I2,3F20.15)') i, species(i), xyz(1:3,i)
        elseif (excludeCoulomb) then
            !excludeCoulomb prints structure files to be read by LAMMPS with
            !atom_style=charge. it is necessary for a charge column in the
            !structure file. This can be 0, and is overruled by the set command
            !in the LAMMPS input file.
            write(50,'(I5,I2,I2,3F20.15)') i, species(i), 0, xyz(1:3,i)
        endif
    enddo

    if (forcecalc.eqv..false.) then

        rmax2=p_rmax**2

        !Determine which of the atoms in the trial unit cells are
        !within p_rmax of the atoms in the central unit cell.
        nnatoms=n_inequivalentsites
        n_neighbors=0
        do i=1,natoms
            do it1=-nshell,nshell
                xyztry1=xyz(1:3,i)+tr(1:3,1)*dble(it1)
                do it2=-nshell,nshell
                    xyztry2=xyztry1+tr(1:3,2)*dble(it2)
                    do it3=-nshell,nshell
                        xyztry=xyztry2+tr(1:3,3)*dble(it3)
                        inlist=(it1.eq.0.and.it2.eq.0.and.it3.eq.0)
                        atomnumber=i
                        do j=1,n_inequivalentsites
                            dxyz=xyztry-xyz(1:3,j)
                            dd=dxyz(1)**2+dxyz(2)**2+dxyz(3)**2
                            if(dd.le.rmax2) then
                                if(.not.inlist) then
                                    nnatoms=nnatoms+1 !Add to the list
                                    atomnumber=nnatoms
                                    xyz(1:3,atomnumber)=xyztry(1:3)
                                    species(atomnumber)=species(i)
                                    inlist=.true.
                                    !if (plot_crystal_struc) then
                                    !    if (species(atomnumber).eq.1) then
                                    !        write(50,*) 'Mg',xyztry
                                    !    elseif (species(atomnumber).eq.2) then
                                    !        write(50,*) 'Ca',xyztry
                                    !    elseif (species(atomnumber).eq.3) then
                                    !        write(50,*) 'P',xyztry
                                    !    else
                                    !        print *,'!',species(atomnumber)
                                    !        stop
                                    !    endif
                                    !endif
                                endif
                                if(atomnumber.ne.j) then
                                    n_neighbors(j)=n_neighbors(j)+1 !Update
                                    !neighborlist
                                    neighborlist(n_neighbors(j),j)=atomnumber
                                endif
                            endif
                        enddo
                    enddo
                enddo
            enddo
        enddo

    else

        rmax2=p_rmax**2
        !Determine which of the atoms in the trial unit cells are within
        !p_rmax of the inequivalent atoms.
        nnatoms=n_inequivalentsites
        n_neighbors=0
        do i=1,natoms_old
            do it1=-nshell,nshell
                xyztry1=coords_old(1:3,i)+tr(1:3,1)*dble(it1)
                do it2=-nshell,nshell
                    xyztry2=xyztry1+tr(1:3,2)*dble(it2)
                    do it3=-nshell,nshell
                        xyztry=xyztry2+tr(1:3,3)*dble(it3)
                        call checkatom(xyztry,inlist,atomnumber)
                        do j=1,n_inequivalentsites
                            dxyz=xyztry-xyz(1:3,j)
                            dd=dxyz(1)**2+dxyz(2)**2+dxyz(3)**2
                            if(dd.le.rmax2) then
                                if(.not.inlist) then
                                    nnatoms=nnatoms+1 !Add to the list
                                    atomnumber=nnatoms
                                    xyz(1:3,atomnumber)=xyztry(1:3)
                                    species(atomnumber)=species(i)
                                    inlist=.true.
                                    !if (plot_crystal_struc) then
                                    !    if (species(atomnumber).eq.1) then
                                    !        write(50,*) 'Mg',xyztry
                                    !    elseif (species(atomnumber).eq.2) then
                                    !        write(50,*) 'Ta',xyztry
                                    !    elseif (species(atomnumber).eq.3) then
                                    !        write(50,*) 'P',xyztry
                                    !    else
                                    !        print *,'!',species(atomnumber)
                                    !        stop
                                    !    endif
                                    !endif
                                endif
                                if(atomnumber.ne.j) then
                                    n_neighbors(j)=n_neighbors(j)+1 !Update
                                    !neighborlist
                                    neighborlist(n_neighbors(j),j)=atomnumber
                                endif
                            endif
                        enddo
                    enddo
                enddo
            enddo
        enddo

    endif

    !if (plot_crystal_struc) then
    !    !Plot the crystal structure
    !    close(unit=50)
    !    print *,'Finished plotting crystal structure.'
    !    print *,'Stopping.'
    !    stop
    !endif

end subroutine neighborhood
