
subroutine calcForces

    use m_geometry
    use m_meamparameters
    use m_observables
    use m_optimization
    use m_atomproperties

    implicit none

    integer iat,jat,kat,alph,jj,kk,isp,jsp,iiCoeff,iiCutoff,ip,l,iisp,jjsp

    !Calculate forces and their derivatives by summing up their 'by atom'
    !components
    do iat=1,gn_forces(istr)
       !print *,'iat=',iat,'/',gn_forces(istr)
       iisp=gspecies(iat,istr)

       ! jj=0 contributions
       ! ------------------

       do alph=1,3

          forces(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
          !print *,'dmeam_paire_dxyz(1:3,0,',iat,')=',dmeam_paire_dxyz(1:3,0,iat)
          !print *,'dmeamf_dxyz(1:3,0,',iat,')=',dmeamf_dxyz(1:3,0,iat)

          !dforce_dpairpot:
          do isp=1,maxspecies
             do jsp=isp,maxspecies
                do iiCoeff=1,noptpairpotCoeff(isp,jsp)
                   ip = ioptpairpotCoeff(iiCoeff,isp,jsp)
                   dforce_dpairpot(alph,ip,isp,jsp,iat)=d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,0,iat)
                enddo
                do iiCutoff=1,noptpairpotCutoff(isp,jsp)
                   ip = ioptpairpotCutoff(iiCutoff,isp,jsp)
                   dforce_dpairpot(alph,ip,isp,jsp,iat)=d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,0,iat)
                enddo
             enddo
          enddo

          !dforce_draddens:
          if (thiaccptindepndt.eqv..false.) then
             do isp=1,maxspecies
                do jsp=1,maxspecies
                   do l=0,lmax
                      do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                         ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                         dforce_draddens(alph,ip,l,jsp,iat)=d2meamf_dxyz_dpara(alph,0,ip,l,jsp,iat)
                      enddo
                      do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                         ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                         dforce_draddens(alph,ip,l,jsp,iat)=d2meamf_dxyz_dpara(alph,0,ip,l,jsp,iat)
                      enddo
                   enddo
                enddo
             enddo
          else
             do jsp=1,maxspecies
                do l=0,lmax
                   do iiCoeff=1,noptdensityCoeff(l,1,jsp)
                      ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
                      dforce_draddens(alph,ip,l,jsp,iat)=d2meamf_dxyz_dpara(alph,0,ip,l,jsp,iat)
                   enddo
                   do iiCutoff=1,noptdensityCutoff(l,1,jsp)
                      ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
                      dforce_draddens(alph,ip,l,jsp,iat)=d2meamf_dxyz_dpara(alph,0,ip,l,jsp,iat)
                   enddo
                enddo
             enddo
          endif

          !dforce_dmeamtau:
          if (lmax.gt.0) then
             do jsp=1,maxspecies
                do l=1,lmax
                   dforce_dmeamtau(alph,l,jsp,iat)=d2meamf_dxyz_dmeamtau(alph,0,l,jsp,iat)
                enddo
             enddo
          endif

          !dforce_d{emb paras}:
          do isp=1,maxspecies
             if (isp.eq.iisp) then
                dforce_dmeame0(alph,iisp,iat)=d2meamf_dxyz_dmeame0(alph,0,iat)
                dforce_dmeamrho0(alph,iisp,iat)=d2meamf_dxyz_dmeamrho0(alph,0,iat)
                dforce_dmeamemb3(alph,iisp,iat)=d2meamf_dxyz_dmeamemb3(alph,0,iat)
                dforce_dmeamemb4(alph,iisp,iat)=d2meamf_dxyz_dmeamemb4(alph,0,iat)
             else
                dforce_dmeame0(alph,isp,iat)=0d0
                dforce_dmeamrho0(alph,isp,iat)=0d0
                dforce_dmeamemb3(alph,isp,iat)=0d0
                dforce_dmeamemb4(alph,isp,iat)=0d0
             endif
          enddo 

       enddo

       ! jj>0 contributions
       ! ------------------

       do jj=1,gn_neighbors(iat,istr)
          jat=gneighborlist(jj,iat,istr)
          jjsp=gspecies(jat,istr)
          do kk=1,gn_neighbors(jat,istr)
             kat=gneighborlist(kk,jat,istr)
             if (kat.eq.iat) then
                do alph=1,3

                   forces(alph,iat)=forces(alph,iat)+ &
                      dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)

                   !dforce_dpairpot:
                   do isp=1,maxspecies
                      do jsp=isp,maxspecies
                         do iiCoeff=1,noptpairpotCoeff(isp,jsp)
                            ip = ioptpairpotCoeff(iiCoeff,isp,jsp)
                            dforce_dpairpot(alph,ip,isp,jsp,iat)=dforce_dpairpot(alph,ip,isp,jsp,iat)+d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,kk,jat)
                         enddo
                         do iiCutoff=1,noptpairpotCutoff(isp,jsp)
                            ip = ioptpairpotCutoff(iiCutoff,isp,jsp)
                            dforce_dpairpot(alph,ip,isp,jsp,iat)=dforce_dpairpot(alph,ip,isp,jsp,iat)+d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,kk,jat)
                         enddo
                      enddo
                   enddo

                   !dforce_draddens:
                   if (thiaccptindepndt.eqv..false.) then
                      do isp=1,maxspecies
                         do jsp=1,maxspecies
                            do l=0,lmax
                               do iiCoeff=1,noptdensityCoeff(l,isp,jsp)
                                  ip = ioptdensityCoeff(iiCoeff,l,isp,jsp)
                                  dforce_draddens(alph,ip,l,jsp,iat)=dforce_draddens(alph,ip,l,jsp,iat)+d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                               enddo
                               do iiCutoff=1,noptdensityCutoff(l,isp,jsp)
                                  ip = ioptdensityCutoff(iiCutoff,l,isp,jsp)
                                  dforce_draddens(alph,ip,l,jsp,iat)=dforce_draddens(alph,ip,l,jsp,iat)+d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                               enddo
                            enddo
                         enddo
                      enddo
                   else
                      do jsp=1,maxspecies
                         do l=0,lmax
                            do iiCoeff=1,noptdensityCoeff(l,1,jsp)
                               ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
                               dforce_draddens(alph,ip,l,jsp,iat)=dforce_draddens(alph,ip,l,jsp,iat)+d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                            enddo
                            do iiCutoff=1,noptdensityCutoff(l,1,jsp)
                               ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
                               dforce_draddens(alph,ip,l,jsp,iat)=dforce_draddens(alph,ip,l,jsp,iat)+d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                            enddo
                         enddo
                      enddo 
                   endif    

                   !dforce_dmeamtau:
                   do jsp=1,maxspecies
                      do l=1,lmax
                         dforce_dmeamtau(alph,l,jsp,iat)=dforce_dmeamtau(alph,l,jsp,iat)+d2meamf_dxyz_dmeamtau(alph,kk,l,jsp,jat)
                      enddo
                   enddo

                   !dforce_d{emb paras}:
                   do isp=1,maxspecies
                      if (isp.eq.jjsp) then
                         dforce_dmeame0(alph,isp,iat)=dforce_dmeame0(alph,isp,iat)+d2meamf_dxyz_dmeame0(alph,kk,jat)
                         dforce_dmeamrho0(alph,isp,iat)=dforce_dmeamrho0(alph,isp,iat)+d2meamf_dxyz_dmeamrho0(alph,kk,jat)
                         dforce_dmeamemb3(alph,isp,iat)=dforce_dmeamemb3(alph,isp,iat)+d2meamf_dxyz_dmeamemb3(alph,kk,jat)
                         dforce_dmeamemb4(alph,isp,iat)=dforce_dmeamemb4(alph,isp,iat)+d2meamf_dxyz_dmeamemb4(alph,kk,jat)
                      endif
                   enddo

                enddo
                exit
             endif
          enddo

       enddo
    enddo
    !print *,'forces='
    !do iat=1,gn_forces(istr)
    !   print *,forces(1:3,iat)
    !enddo

    !Forces are _minus_ the derivative of the energy w.r.t positions
    forces = -forces
    dforce_dpairpot =  -dforce_dpairpot  
    dforce_draddens =  -dforce_draddens  
    dforce_dmeamtau =  -dforce_dmeamtau  
    dforce_dmeame0 =   -dforce_dmeame0    
    dforce_dmeamrho0 = -dforce_dmeamrho0
    dforce_dmeamemb3 = -dforce_dmeamemb3
    dforce_dmeamemb4 = -dforce_dmeamemb4

end subroutine calcForces


subroutine calcStressTensor

    ! Andrew Ian Duff, Tom Mellan

    use m_geometry
    use m_meamparameters
    use m_observables
    use m_atomproperties
    use m_optimization

    implicit none

    integer i,jj,kk,iat,jat,kat,alph,isp,jsp,iiCoeff,iiCutoff,ip,l,jjsp

    stressTensor=0d0
    dstressTensor_dpairpot=0d0
    dstressTensor_draddens=0d0
    dstressTensor_dmeamtau=0d0
    dstressTensor_dmeame0=0d0
    dstressTensor_dmeamrho0=0d0
    dstressTensor_dmeamemb3=0d0
    dstressTensor_dmeamemb4=0d0

    do iat=1,gn_forces(istr)

       do jj=1,gn_neighbors(iat,istr)
          jat=gneighborlist(jj,iat,istr)
          jjsp=gspecies(jat,istr)
          do kk=1,gn_neighbors(jat,istr)
             kat=gneighborlist(kk,jat,istr)
             if (kat.eq.iat) then
                do alph=1,3
                   stressTensor(1+3*(alph-1)) = stressTensor(1+3*(alph-1)) - &
                      dxstr(jj,iat,0,0,istr) * (dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat))
                   stressTensor(2+3*(alph-1)) = stressTensor(2+3*(alph-1)) - &
                      dystr(jj,iat,0,0,istr) * (dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat))
                   stressTensor(3+3*(alph-1)) = stressTensor(3+3*(alph-1)) - &
                      dzstr(jj,iat,0,0,istr) * (dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat))
                   !print *,'stressTensors=',stressTensor(1+3*(alph-1)),stressTensor(2+3*(alph-1)),stressTensor(3+3*(alph-1))
                   !print *,'dystr(',jj,',',iat,',0,0,',istr,')=',dystr(jj,iat,0,0,istr)
                   !print *,'dmeam_paire_dxyz(',alph,',',kk,',',jat,')=',dmeam_paire_dxyz(alph,kk,jat)
                   !print *,'dmeamf_dxyz(',alph,',',kk,',',jat,')=',dmeamf_dxyz(alph,kk,jat)
                   !if (alph.eq.2) then
                   !   stop
                   !endif

                   !dstressTensor_dpairpot:
                   do isp=1,maxspecies
                      do jsp=isp,maxspecies
                         do iiCoeff=1,noptpairpotCoeff(isp,jsp)
                            ip = ioptpairpotCoeff(iiCoeff,isp,jsp)
                            dstressTensor_dpairpot(1+3*(alph-1),ip,isp,jsp) = dstressTensor_dpairpot(1+3*(alph-1),ip,isp,jsp) - &
                               dxstr(jj,iat,0,0,istr) * d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,kk,jat)
                            dstressTensor_dpairpot(2+3*(alph-1),ip,isp,jsp) = dstressTensor_dpairpot(2+3*(alph-1),ip,isp,jsp) - &
                               dystr(jj,iat,0,0,istr) * d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,kk,jat)
                            dstressTensor_dpairpot(3+3*(alph-1),ip,isp,jsp) = dstressTensor_dpairpot(3+3*(alph-1),ip,isp,jsp) - &
                               dzstr(jj,iat,0,0,istr) * d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,kk,jat)
                         enddo
                         do iiCutoff=1,noptpairpotCutoff(isp,jsp)
                            ip = ioptpairpotCutoff(iiCutoff,isp,jsp)
                            dstressTensor_dpairpot(1+3*(alph-1),ip,isp,jsp) = dstressTensor_dpairpot(1+3*(alph-1),ip,isp,jsp) - &
                               dxstr(jj,iat,0,0,istr) * d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,kk,jat)
                            dstressTensor_dpairpot(2+3*(alph-1),ip,isp,jsp) = dstressTensor_dpairpot(2+3*(alph-1),ip,isp,jsp) - &
                               dystr(jj,iat,0,0,istr) * d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,kk,jat)
                            dstressTensor_dpairpot(3+3*(alph-1),ip,isp,jsp) = dstressTensor_dpairpot(3+3*(alph-1),ip,isp,jsp) - &
                               dzstr(jj,iat,0,0,istr) * d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,kk,jat)
                         enddo
                      enddo
                   enddo

                   !dstressTensor_draddens:
                   if (thiaccptindepndt.eqv..false.) then
                      print *,'raddens acceptor dependance currently not supported (see meamforce.f90), STOPPING.'
                      stop
                   else
                      do jsp=1,maxspecies
                         do l=0,lmax
                            do iiCoeff=1,noptdensityCoeff(l,1,jsp)
                               ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
                               dstressTensor_draddens(1+3*(alph-1),ip,l,jsp) = dstressTensor_draddens(1+3*(alph-1),ip,l,jsp) - &
                                  dxstr(jj,iat,0,0,istr) * d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                               dstressTensor_draddens(2+3*(alph-1),ip,l,jsp) = dstressTensor_draddens(2+3*(alph-1),ip,l,jsp) - &
                                  dystr(jj,iat,0,0,istr) * d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                               dstressTensor_draddens(3+3*(alph-1),ip,l,jsp) = dstressTensor_draddens(3+3*(alph-1),ip,l,jsp) - &
                                  dzstr(jj,iat,0,0,istr) * d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                            enddo
                            do iiCutoff=1,noptdensityCutoff(l,1,jsp)
                               ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
                               dstressTensor_draddens(1+3*(alph-1),ip,l,jsp) = dstressTensor_draddens(1+3*(alph-1),ip,l,jsp) - &
                                  dxstr(jj,iat,0,0,istr) * d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                               dstressTensor_draddens(2+3*(alph-1),ip,l,jsp) = dstressTensor_draddens(2+3*(alph-1),ip,l,jsp) - &
                                  dystr(jj,iat,0,0,istr) * d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                               dstressTensor_draddens(3+3*(alph-1),ip,l,jsp) = dstressTensor_draddens(3+3*(alph-1),ip,l,jsp) - &
                                  dzstr(jj,iat,0,0,istr) * d2meamf_dxyz_dpara(alph,kk,ip,l,jsp,jat)
                            enddo
                         enddo
                      enddo
                   endif

                   !dstressTensor_dmeamtau:
                   if (lmax.gt.0) then
                      do jsp=1,maxspecies
                         do l=1,lmax
                            dstressTensor_dmeamtau(1+3*(alph-1),l,jsp) = dstressTensor_dmeamtau(1+3*(alph-1),l,jsp) - &
                               dxstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamtau(alph,kk,l,jsp,jat)
                            dstressTensor_dmeamtau(2+3*(alph-1),l,jsp) = dstressTensor_dmeamtau(2+3*(alph-1),l,jsp) - &
                               dystr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamtau(alph,kk,l,jsp,jat)
                            dstressTensor_dmeamtau(3+3*(alph-1),l,jsp) = dstressTensor_dmeamtau(3+3*(alph-1),l,jsp) - &
                               dzstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamtau(alph,kk,l,jsp,jat)
                         enddo
                      enddo
                   endif

                   !dstressTensor_d{emb_paras}:
                   dstressTensor_dmeame0(1+3*(alph-1),jjsp) = dstressTensor_dmeame0(1+3*(alph-1),jjsp) - &
                      dxstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeame0(alph,kk,jat)
                   dstressTensor_dmeame0(2+3*(alph-1),jjsp) = dstressTensor_dmeame0(2+3*(alph-1),jjsp) - &
                      dystr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeame0(alph,kk,jat)
                   dstressTensor_dmeame0(3+3*(alph-1),jjsp) = dstressTensor_dmeame0(3+3*(alph-1),jjsp) - &
                      dzstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeame0(alph,kk,jat)
                   dstressTensor_dmeamrho0(1+3*(alph-1),jjsp) = dstressTensor_dmeamrho0(1+3*(alph-1),jjsp) - &
                      dxstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamrho0(alph,kk,jat)
                   dstressTensor_dmeamrho0(2+3*(alph-1),jjsp) = dstressTensor_dmeamrho0(2+3*(alph-1),jjsp) - &
                      dystr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamrho0(alph,kk,jat)
                   dstressTensor_dmeamrho0(3+3*(alph-1),jjsp) = dstressTensor_dmeamrho0(3+3*(alph-1),jjsp) - &
                      dzstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamrho0(alph,kk,jat)
                   dstressTensor_dmeamemb3(1+3*(alph-1),jjsp) = dstressTensor_dmeamemb3(1+3*(alph-1),jjsp) - &
                      dxstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamemb3(alph,kk,jat)
                   dstressTensor_dmeamemb3(2+3*(alph-1),jjsp) = dstressTensor_dmeamemb3(2+3*(alph-1),jjsp) - &
                      dystr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamemb3(alph,kk,jat)
                   dstressTensor_dmeamemb3(3+3*(alph-1),jjsp) = dstressTensor_dmeamemb3(3+3*(alph-1),jjsp) - &
                      dzstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamemb3(alph,kk,jat)
                   dstressTensor_dmeamemb4(1+3*(alph-1),jjsp) = dstressTensor_dmeamemb4(1+3*(alph-1),jjsp) - &
                      dxstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamemb4(alph,kk,jat)
                   dstressTensor_dmeamemb4(2+3*(alph-1),jjsp) = dstressTensor_dmeamemb4(2+3*(alph-1),jjsp) - &
                      dystr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamemb4(alph,kk,jat)
                   dstressTensor_dmeamemb4(3+3*(alph-1),jjsp) = dstressTensor_dmeamemb4(3+3*(alph-1),jjsp) - &
                      dzstr(jj,iat,0,0,istr) * d2meamf_dxyz_dmeamemb4(alph,kk,jat)

                enddo
                exit
             endif
          enddo
       enddo

    enddo

    !Rescale using volume per atom:
    stressTensor = -stressTensor / g_vol(istr)

    dstressTensor_dpairpot = -dstressTensor_dpairpot / g_vol(istr)
    dstressTensor_draddens = -dstressTensor_draddens / g_vol(istr)
    dstressTensor_dmeamtau = -dstressTensor_dmeamtau / g_vol(istr)
    dstressTensor_dmeame0 = -dstressTensor_dmeame0 / g_vol(istr)
    dstressTensor_dmeamrho0 = -dstressTensor_dmeamrho0 / g_vol(istr)
    dstressTensor_dmeamemb3 = -dstressTensor_dmeamemb3 / g_vol(istr)
    dstressTensor_dmeamemb4 = -dstressTensor_dmeamemb4 / g_vol(istr)

end subroutine calcStressTensor


subroutine calcStressTensorNum(iat,Eperatom,Eperatom2,dist,stressTensorTmp)

    use m_geometry
    use m_meamparameters

    implicit none

    integer i,jj,kk,iat,jat,kat,alph
    real(8) Eperatom(maxneighbors),Eperatom2(maxneighbors),dist,derivEperatom
    real(8) stressTensorTmp(1:3),stressTensorTmp2(1:3)

    stressTensorTmp=0d0
    stressTensorTmp2=0d0

    do jj=1,gn_neighbors(iat,istr)
       derivEperatom = (Eperatom2(jj) - Eperatom(jj)) / dist
       stressTensorTmp(1) = stressTensorTmp(1) - &
           dxstr(jj,iat,0,0,istr) * derivEperatom
       stressTensorTmp(2) = stressTensorTmp(2) - &
           dystr(jj,iat,0,0,istr) * derivEperatom
       stressTensorTmp(3) = stressTensorTmp(3) - &
           dzstr(jj,iat,0,0,istr) * derivEperatom
    enddo

end subroutine calcStressTensorNum


subroutine energyForNumForce(iatom,energyTmp,Eperatom)

    !--------------------------------------------------------------c
    !
    !     Computes the components of the MEAM energy (for structure, 
    !     istr) necessary for computing the atomic forces and stress
    !     tensor components relating to atom, iatom. Only those
    !     embedding energies and pair-potentials which have changed 
    !     due to the atom being displaced need to be calculated.
    !
    !     Andy: if we want to use this subroutine again in future
    !     (e.g. to compute forces using FD) then we need to do the
    !     gxyz++ bit by hand. Previously, e.g., electrondensity()
    !     had parameters for the iatom and cart being displaced,
    !     and used look-up tables for the corresponding interatom
    !     seperations. Now, since we no longer have the parameters in
    !     the call, nor the look-up tables, we would have to
    !     preceed this call with the correct setting up of the
    !     atom positions.
    !
    !     Called by:     meamforce
    !     Calls:         readmeamparam,screening_ij,
    !                 radialdensityfunction,
    !                 radialdensityfunction_onlyiatom
    !                 electrondensity,
    !                 backgrounddensity,embeddingfunction,
    !                 pairpotential,distance
    !     Arguments:     gn_inequivalentsites,gspecies,
    !                 gn_neighbors,gneighborlist,screening
    !     Returns:       energyTmp,Eperatom
    !     Files read:    none
    !     Files written: none
    !
    !     Andrew Duff, 2016
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_geometry
    use m_screening   !S_ij
    use m_filenames
    use m_electrondensity!can remove
    use m_generalinfo
    use m_atomproperties !AnalyticDeriv

    implicit none

    integer i,j,jj,isp,jsp,iatom,f
    real(8) pairpot,energyTmp,SI_to_eVamg,summeamf
    real(8) Eperatom(maxneighbors)
    real(8) tmpArray(32,maxspecies,maxspecies)

    parameter(SI_to_eVamg=14.4375d0)

    energyTmp=0d0

    call screening_ij

    isp=gspecies(iatom,istr)
    meam_paire(iatom)=0d0
    do jj=1,gn_neighbors(iatom,istr)
        j=gneighborlist(jj,iatom,istr)
        jsp=gspecies(j,istr)
        rij=diststr(jj,iatom,iatom,0,istr)
        call radpairpot(isp,jsp,rij, &
            pairpot,tmpArray)
        meam_paire(iatom)=meam_paire(iatom)+pairpot* &
            screening(jj,iatom)
        Eperatom(jj)=0.5d0*pairpot*screening(jj,iatom)
    enddo
    energyTmp=energyTmp+meam_paire(iatom)

    call radialdensityfunction_onlyiatom(iatom)
    call electrondensity
    call backgrounddensity
    call embeddingfunction

    summeamf=0d0
    summeamf=summeamf+meam_f(iatom)
    do jj=1,gn_neighbors(iatom,istr)
        j=gneighborlist(jj,iatom,istr)
        summeamf=summeamf+meam_f(j)
        Eperatom(jj)=Eperatom(jj)+meam_f(j)
    enddo
    !print *,'summeamf=',summeamf,' using new meth'
    !stop

    !!Delete the following once we have convinced ourselves the above code works
    ! summeamf=0d0
    ! print *,gn_inequivalentsites(istr),gn_neighbors(iatom,istr)
    ! do i=1,gn_inequivalentsites(istr)
    !     summeamf=summeamf+meam_f(i)
    ! enddo
    !!----

    energyTmp = energyTmp + summeamf

end subroutine energyForNumForce


subroutine checkSignDensities

    !Scan electron densities for negative densities
    !
    !Andrew Ian Duff, STFC, Copyright 2017

    use m_generalinfo
    use m_objectiveFunction
    use m_atomproperties
    use m_meamparameters

    implicit none

    integer i,isp,jsp
    real(8) r,densityPrev,densityGrad,densityGradPrev,rSepn,tmpDeriv
    real(8) rij,density(0:lmax),ddensity_dpara(12,0:lmax)
    real(8), parameter:: rNum=100 !Number of radial points to check (excluding =0)

    rSepn=p_rmax/(dble(rNum))

    if (thiaccptindepndt.eqv..false.) then
       print *,'update code'
       stop
    else
       isp=1
       do jsp=1,maxspecies
          call raddens(0d0,isp,jsp,density,ddensity_dpara)
          if (density(0).lt.0d0) then
             negElecDens=.true.
             ! print *,'negative density for jsp=',jsp,' at r=0'
             ! print *,'   (density=',density(0),')'
          endif
          do i=1,rNum
             r=dble(i)*rSepn
             call raddens(r,isp,jsp,density,ddensity_dpara)
             if (density(0).lt.0d0) then
                negElecDens=.true.
                ! print *,'negative density for jsp=',jsp,' at r=',r
                ! print *,'   (density=',density(0),')'
             endif
          enddo
       enddo
    endif

    !   !Debug code:
    !   if (negElecDens) then
    !     print *,'negative electron density, writing to density_deb...'
    !     print *,'meamrhodecay(1:12,0,1,1:2)=',meamrhodecay(1:12,0,1,1:2)
    !     open(51,file='density_debug.dat')
    !     isp=1
    !     do jsp=1,maxspecies
    !        do i=1,rNum
    !           r=dble(i)*rSepn
    !           call raddens(r,isp,jsp,density,ddensity_dpara)
    !           write(51,*) r,density(0)
    !        enddo
    !     enddo
    !     close(51)
    !     stop
    !   else
    !     print *,'NO NEGATIVE DENSITY, ALL GOOD!'
    !   endif

end subroutine checkSignDensities




subroutine meamforce

    !------------------------------------------------------------c
    !
    !     Calculate the atomic forces for supercell istr (global 
    !     variables) if computeForcesTmp=1, and both atomic 
    !     forces and stress tensor if computeForcesTmp=2.
    !     Derivatives are calculated analytically. In addition
    !     a large quantity of debugging code is provided,
    !     commented out, to test analytic derivatives against
    !     numerical derivatives.
    !
    !     NOTE: where forces are being calculated gn_inequivalentsites
    !     is increased (with 'gn_forces' holding the original number
    !     of atoms as read from the DFT data file). 'meamforce'
    !     takes account for this, as is necessary even if just the
    !     energy is being calculated, therefore meamforce, and not
    !     meamenergy should be called for such structures even if just
    !     the energy is required.
    !
    !     Called by:     objectiveFunction
    !     Calls:         energyForNumForce
    !     Arguments:     iatom,gxyz
    !     Returns:       force
    !     Files read:    none
    !     Files written: none
    !
    !     Andrew Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------c

    use m_generalinfo
    use m_geometry
    use m_electrondensity
    use m_atomproperties
    use m_meamparameters
    use m_screening
    use m_optimization
    use m_observables
    use m_datapoints
    use m_objectiveFunction

    implicit none

    integer i,j,k,isp,iisp,jsp,jj,iat,jat,iatom,iaux(1),alph,kk,kat,ip,denom,iiCutoff,iiCoeff,l
    real(8) energy_prev,rms_error,energyTmp,energyTmp2,forceCut,summeam_paire_prev
    real(8) forcenew(3),Eperatom(maxneighbors),Eperatom2(maxneighbors)
    real(8) force(3),dforce_dpara_num(1:3),dforce_dmeamtau_num(1:3),dforce_dmeame0_num(1:3),dforce_dmeamrho0_num(1:3),dforce_dmeamemb3_num(1:3),dforce_dmeamemb4_num(1:3),stressTensorNum(9),stressTensor_prev(9), &
            stressTensorTmp(1:3),distComp(1:3),forces_prev(3,gn_inequivalentsites(istr))
    real(8), parameter:: dist=1d-8 !Distance of displacement used in computing finite difference derivatives w.r.t atomic positions
    real(8), parameter:: deltaPara=1d-8 !Change in parameter used in computing finite difference derivatives w.r.t potential parameters
    !real(8), allocatable:: meam_paire_prev(:)
    !real(8), allocatable:: dsummeam_paire_dpara_prev(:,:,:)
    !real(8), allocatable:: dmeam_paire_dxyz_prev(:,:,:)
    !real(8), allocatable:: d2meam_paire_dxyz_dpara_prev(:,:,:,:,:,:)
    !real(8), allocatable:: fjlij2(:,:,:)
    real(8), allocatable:: dfjlij_dxyz_prev(:,:,:,:)
    real(8), allocatable:: d2fjlij_dxyz_dpara_prev(:,:,:,:,:)
    !real(8), allocatable:: rhol_prev(:,:)
    !real(8), allocatable:: meam_t_prev(:,:)
    real(8), allocatable:: dmeamt_dpara_prev(:,:,:,:),dmeamt_dmeamtau_prev(:,:,:), &
            dmeamt_dxyz_prev(:,:,:,:),d2meamt_dxyz_dmeamtau_prev(:,:,:,:,:), &
            d2meamt_dxyz_dpara_prev(:,:,:,:,:,:)
    real(8), allocatable:: drhol_dxyz_prev(:,:,:,:)
    real(8), allocatable:: drhol_dpara_prev(:,:,:,:)
    real(8), allocatable:: d2rhol_dxyz_dpara_prev(:,:,:,:,:,:)
    real(8), allocatable:: rho_i_prev(:),drhoi_dxyz_prev(:,:,:),d2rhoi_dxyz_dmeamtau_prev(:,:,:,:,:),d2rhoi_dxyz_dpara_prev(:,:,:,:,:,:)
    real(8), allocatable:: meam_f_prev(:),dmeamf_dxyz_prev(:,:,:),dmeamf_dmeame0_prev(:),dmeamf_dmeamrho0_prev(:),dmeamf_dmeamemb3_prev(:),dmeamf_dmeamemb4_prev(:),dmeamf_dpara_prev(:,:,:,:),d2meamf_dxyz_dmeame0_prev(:,:,:),d2meamf_dxyz_dmeamrho0_prev(:,:,:),d2meamf_dxyz_dmeamemb3_prev(:,:,:),d2meamf_dxyz_dmeamemb4_prev(:,:,:),d2meamf_dxyz_dmeamtau_prev(:,:,:,:,:),d2meamf_dxyz_dpara_prev(:,:,:,:,:,:),dstressTensor_dpairpot_prev(:,:,:,:),dstressTensor_draddens_prev(:,:,:,:),dstressTensor_dmeamtau_prev(:,:,:),dstressTensor_dmeamrho0_prev(:,:),dstressTensor_dmeame0_prev(:,:),dstressTensor_dmeamemb3_prev(:,:),dstressTensor_dmeamemb4_prev(:,:)

    !Constituent properties and derivatives:
    !print *,'radialdensityfunctionForce:'
    if (useSplines.eqv..true.) then
       print *,'WARNING: Using splines for some or all electron densities - currently in testing phase'
       call radialdensityfunctionForceSpline
    else
       call radialdensityfunctionForce
    endif
    !If we are optimizing and want to avoid negative densities, check at this
    !point whether we can proceed
    !if ((noOpt.eqv..false.).and.(lowDensPen)) then
    if ((noOpt.eqv..false.).and.(negElecDensAllow.eqv..false.)) then
       call checkSignDensities
       if (negElecDens) then
          !print *,'negative electron density encountered'
          return
       endif
    endif
    !print *,'screening_ij:'
    call screening_ij
    !print *,'electrondensityForce:'
    call electrondensityForce
    if (negBackDens) then
       !print *,'negative background density encountered'
       return
    endif
    !print *,'backgrounddensityForce:'
    call backgrounddensityForce
    if ((gammaOutOfBounds).and.(noOpt.eqv..false.)) then
       !print *,'gamma either too small or too large'
       return
    endif
    !print *,'embeddingfunctionForce:'
    if (useSplines.eqv..true.) then
       print *,'WARNING: Using splines for some or all embedding functions - currently in testing phase'
       call embeddingfunctionForceSpline
    else
       call embeddingfunctionForce
    endif
    !print *,'pairpotentialForce:'
    if (useSplines.eqv..true.) then
       print *,'WARNING: Using splines for some or all pair-potentials - currently in testing phase'
       call pairpotentialForceSpline
    else
       call pairpotentialForce
    endif
    !Energy and forces and their derivatives:
    energy=summeam_paire+summeamf+enconst(1)

    !print *,'calcForces:'
    call calcForces
    if (weightsSt(istr).gt.0d0) then
       !Stress tensor and derivatives:
       !print *,'calcStressTensor:'
       call calcStressTensor
    endif


end subroutine meamforce


    ! - - - -    D E B U G    C O D E    F O R    T E S T I N G    A N A L Y T I C    D E R I V A T I V E S    - - - -
    ! To use this code, comment out the 'end subroutine' above, and comment in
    ! all the relevant lines of code below (including the 'end subroutine' at
    ! the bottom.


    !                                 /----------------------------------\
    !                                 |          Constituents            |
    !                                 | (including summeamf and pairpot) |
    !                                 \----------------------------------/


    !---------------------------------!
    ! Test pair potential derivatives !
    !---------------------------------!
    
    
    !   !---- Test dmeam_paire_dpara analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'Test dmeam_paire_dpara (analytic vs numerical):'
    !   print *,'--------------------------------------------'
    !   print *
    !   if(allocated(dsummeam_paire_dpara_prev)) deallocate(dsummeam_paire_dpara_prev)
    !   allocate(dsummeam_paire_dpara_prev(32,maxspecies,maxspecies))
    !   call pairpotentialForce
    !   summeam_paire_prev=summeam_paire
    !   dsummeam_paire_dpara_prev=dsummeam_paire_dpara
    !   rms_error=0d0
    !   denom=0
    !   do isp=1,maxspecies
    !      do jsp=isp,maxspecies
    !         do ip=1,4
    !            pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)+deltaPara
    !            call pairpotentialForce
    !            print *,'dsummeam_paire_dpara(ip=',ip,',isp=',isp,',jsp=',jsp,')=', &
    !                     (summeam_paire - summeam_paire_prev)/deltaPara
    !            rms_error=rms_error+&
    !    ( (summeam_paire - summeam_paire_prev)/deltaPara  &
    !      - dsummeam_paire_dpara_prev(ip,isp,jsp) )**2
    !            denom=denom+1
    !            if ( sqrt(( (summeam_paire - summeam_paire_prev)/deltaPara  &
    !      - dsummeam_paire_dpara_prev(ip,isp,jsp) )**2).gt.10d-3 ) then
    !               print *,'different to:',dsummeam_paire_dpara_prev(ip,isp,jsp)
    !               stop
    !            endif
    !            pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)-deltaPara
    !         enddo
    !      enddo
    !   enddo
    !   print *,'rms mean error:',sqrt(rms_error/dble(denom))
    !   stop
    !   !---------------------------------------------------------------------------------------------

    !   call pairpotentialForce
    !
    !   !---- Test dmeam_paire_dxyz analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'dmeam_paire_d{x,y,z} (analytic vs numerical):'
    !   print *,'--------------------------------------------'
    !   print *
    !   if(allocated(meam_paire_prev)) deallocate(meam_paire_prev,dmeam_paire_dxyz_prev)
    !   allocate( meam_paire_prev(gn_inequivalentsites(istr)),dmeam_paire_dxyz_prev(1:3,0:maxneighbors,gn_inequivalentsites(istr)))
    !   meam_paire_prev=meam_paire
    !   dmeam_paire_dxyz_prev=dmeam_paire_dxyz
    !   do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms

    !     do jj=0,gn_neighbors(iat,istr)
    !        if (jj.eq.0) then
    !           jat=iat !Onsite derivatives
    !        else
    !           jat=gneighborlist(jj,iat,istr) !Offsite derivatives
    !        endif

    !        do alph=1,3
    !           gxyz(alph,jat,istr)=gxyz(alph,jat,istr)+dist
    !           call setupnntables
    !           istr=1 !need to reset to one because setupnntables used in
    !           call pairpotentialForce
    !           gxyz(alph,jat,istr)=gxyz(alph,jat,istr)-dist
    !           call setupnntables
    !           istr=1 !need to reset to one because setupnntables used in
    !           write(*,'(A32,I1,A5,I3,A6,I4,A2,F25.15)') 'analytic dmeam_paire_dxyz(',alph,', jj=',jj,', iat=',iat,')=',dmeam_paire_dxyz_prev(alph,jj,iat)
    !           write(*,'(A11,F25.15)') '   numeric=',(meam_paire(iat)-meam_paire_prev(iat))/dist
    !          !  print *,meam_paire(iat),meam_paire_prev(iat),(meam_paire(iat)-meam_paire_prev(iat))/dist
    !          ! !
    !          ! print *,'now using pairpotential:'
    !          ! call pairpotential
    !          ! meam_paire_prev=meam_paire
    !          ! gxyz(alph,jat,istr)=gxyz(alph,jat,istr)+dist
    !          ! call setupnntables
    !          ! istr=1 !need to reset to one because setupnntables used in
    !          ! call pairpotential
    !          ! write(*,'(A32,I1,A5,I3,A6,I4,A2,F25.15)') 'analytic dmeam_paire_dxyz(',alph,', jj=',jj,',iat=',iat,')=',dmeam_paire_dxyz_prev(alph,jj,iat)
    !          ! write(*,'(A11,F25.15)') '   numeric=',(meam_paire(iat)-meam_paire_prev(iat))/dist
    !          ! print *,meam_paire(iat),meam_paire_prev(iat),(meam_paire(iat)-meam_paire_prev(iat))/dist
    !          ! stop
    !          ! !
    !           if ( sqrt( ( dmeam_paire_dxyz_prev(alph,jj,iat) - (meam_paire(iat)-meam_paire_prev(iat))/dist)**2).gt.10d-5 ) then
    !              print *,'error, analytic and numeric derivatives do not agree'
    !              stop
    !           endif
    !        enddo

    !     enddo

    !   enddo
    !   stop
    !   !-----------------------------------------------------------------------------------------

    ! print *,'analytic d2meam_paire_dxyz_dpara:'
    ! print *,'---------------------------------'

    ! do isp=1,maxspecies
    !    do jsp=isp,maxspecies
    !       do ip=1,4
    !          do iatom=1,gn_inequivalentsites(istr)
    !             do jj=0,gn_neighbors(iatom,istr)
    !                do alph=1,3
    !                   print *,'d2meam_paire_dxyz_dpara(alph=',alph,', ip=',ip,',isp=',isp,',jsp=',jsp,', jj=',jj,',iatom=',iatom,')=', &
    !                            d2meam_paire_dxyz_dpara(alph,ip,isp,jsp,jj,iatom)
    !                enddo
    !             enddo
    !          enddo
    !       enddo
    !    enddo
    ! enddo


    ! print *,'numerical d2meam_paire_dxyz_dpara:'
    ! print *,'----------------------------------'

    ! if(allocated(dmeam_paire_dxyz_prev)) deallocate(dmeam_paire_dxyz_prev)
    ! allocate(dmeam_paire_dxyz_prev(1:3,0:maxneighbors,gn_inequivalentsites(istr)))
    ! dmeam_paire_dxyz_prev=dmeam_paire_dxyz
    ! if(allocated(d2meam_paire_dxyz_dpara_prev)) deallocate(d2meam_paire_dxyz_dpara_prev)
    ! allocate(d2meam_paire_dxyz_dpara_prev(1:3,32,maxspecies,maxspecies,0:maxneighbors,gn_inequivalentsites(istr)))
    ! d2meam_paire_dxyz_dpara_prev=d2meam_paire_dxyz_dpara
    ! rms_error=0d0
    ! denom=0
    ! do isp=1,maxspecies
    !    do jsp=isp,maxspecies
    !       do ip=1,4
    !          pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)+0.000001d0
    !          call pairpotentialForce
    !          do iatom=1,gn_inequivalentsites(istr)
    !             do jj=0,gn_neighbors(iatom,istr)
    !                do alph=1,3
    !                   print *,'d2meam_paire_dxyz_dpara(alph=',alph,', ip=',ip,',isp=',isp,',jsp=',jsp,', jj=',jj,',iatom=',iatom,')=', &
    !                            (dmeam_paire_dxyz(alph,jj,iatom) - dmeam_paire_dxyz_prev(alph,jj,iatom))/0.000001d0
    !                   rms_error=rms_error+&
    !  (dmeam_paire_dxyz(alph,jj,iatom) - dmeam_paire_dxyz_prev(alph,jj,iatom))/0.000001d0  &
    !    d2meam_paire_dxyz_dpara_prev(alph,ip,isp,jsp,jj,iatom) )**2
    !                   denom=denom+1
    !                   if ( sqrt(( (dmeam_paire_dxyz(alph,jj,iatom) - &
    ! meam_paire_dxyz_prev(alph,jj,iatom))/0.000001d0  &
    !    d2meam_paire_dxyz_dpara_prev(alph,ip,isp,jsp,jj,iatom) )**2).gt.10d-5 ) then
    !                      print *,'different to:',d2meam_paire_dxyz_dpara_prev(alph,ip,isp,jsp,jj,iatom)
    !                      stop
    !                   endif
    !                enddo
    !             enddo
    !          enddo
    !          pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)-0.000001d0
    !       enddo
    !    enddo
    ! enddo
    ! print *,'rms mean error:',sqrt(rms_error/dble(denom))
    ! stop


    !---------------------------------!
    ! Test radial density derivatives !
    !---------------------------------!
   

    ! call radialdensityfunctionForce

    ! print *,'analytic d2fjlij/dpara_dxyz:'
    ! print *,'----------------------------'

    ! do o=0,lmax
    !    do ip=1,4
    !       do iatom=1,gn_inequivalentsites(istr)
    !          do jj=1,gn_neighbors(iatom,istr)
    !             do alph=1,3
    !                write(*,'(A24,I1,A5,I1,A3,I1,A5,I3,A7,I3,A2,F15.10)') 'd2fjlij_dxyz_dpara(alph=',alph,', ip=',ip, &
    !                     ',l=',o,', jj=',jj,',iatom=',iatom,')=', &
    !                     d2fjlij_dxyz_dpara(alph,ip,o,jj,iatom)
    !             enddo
    !          enddo
    !       enddo
    !    enddo
    ! enddo

    ! print *,'numerical d2fjlij/dpara_dxyz:'
    ! print *,'-----------------------------'

    ! if(allocated(dfjlij_dxyz_prev)) deallocate(dfjlij_dxyz_prev)
    ! allocate( dfjlij_dxyz_prev(1:3,0:lmax,maxneighbors,gn_inequivalentsites(istr)))
    ! dfjlij_dxyz_prev=dfjlij_dxyz
    ! if(allocated(d2fjlij_dxyz_dpara_prev)) deallocate(d2fjlij_dxyz_dpara_prev)
    ! allocate(d2fjlij_dxyz_dpara_prev(1:3,12,0:lmax,maxneighbors,gn_inequivalentsites(istr)))
    ! d2fjlij_dxyz_dpara_prev=d2fjlij_dxyz_dpara
    ! rms_error=0d0
    ! denom=0

    ! do o=0,lmax
    !    do ip=1,4
    !       meamrhodecay(ip,o,1,1)=meamrhodecay(ip,o,1,1)+0.000001d0
    !       meamrhodecay(ip,o,1,2)=meamrhodecay(ip,o,1,2)+0.000001d0
    !       call radialdensityfunctionForce
    !       do iatom=1,gn_inequivalentsites(istr)
    !          do jj=1,gn_neighbors(iatom,istr)
    !             do alph=1,3
    !                write(*,'(A24,I1,A5,I1,A3,I1,A5,I3,A7,I3,A2,F15.10)') 'd2fjlij_dxyz_dpara(alph=',alph,', ip=',ip, &
    !                     ',l=',o,', jj=',jj,',iatom=',iatom,')=', &
    !                     (dfjlij_dxyz(alph,o,jj,iatom) - &
    !                   dfjlij_dxyz_prev(alph,o,jj,iatom))/0.000001d0

    !                rms_error=rms_error+ (&
    !                   (dfjlij_dxyz(alph,o,jj,iatom) - &
    !                   dfjlij_dxyz_prev(alph,o,jj,iatom))/0.000001d0  &
    !                   - d2fjlij_dxyz_dpara_prev(alph,ip,o,jj,iatom) )**2
    !                denom=denom+1
    !               !if ( sqrt((  &
    !               !     (dfjlij_dxyz(alph,o,jj,iatom) - &
    !               !     dfjlij_dxyz_prev(alph,o,jj,iatom))/0.000001d0  &
    !               !       - d2fjlij_dxyz_dpara_prev(alph,ip,o,jj,iatom) &
    !               !        )**2).gt.10d-5 ) then
    !               !   print *,'different to:',d2fjlij_dxyz_dpara_prev(alph,ip,o,jj,iatom)
    !               !   stop
    !               !endif
    !             enddo
    !          enddo
    !       enddo
    !       meamrhodecay(ip,o,1,1)=meamrhodecay(ip,o,1,1)-0.000001d0
    !       meamrhodecay(ip,o,1,2)=meamrhodecay(ip,o,1,2)-0.000001d0
    !    enddo
    ! enddo
    ! print *,'rms mean error:',sqrt(rms_error/dble(denom))
    ! stop

    ! print *
    ! print *,'analytic dfjlij/dxyz:'
    ! print *,'---------------------'
    ! print *

    ! do iat=1,gn_inequivalentsites(istr)
    !    do jj=1,gn_neighbors(iat,istr)
    !       print *,'dfjlij_dxyz(1,l=1,jj,iat)=',dfjlij_dxyz(1,1,jj,iat)
    !       print *,'dfjlij_dxyz(2,l=1,jj,iat)=',dfjlij_dxyz(2,1,jj,iat)
    !       print *,'dfjlij_dxyz(3,l=1,jj,iat)=',dfjlij_dxyz(3,1,jj,iat)
    !    enddo
    ! enddo


    ! print *
    ! print *
    ! print *,'numerical dfjlij/dxyz:'
    ! print *,'----------------------'
    ! iaux=maxval(gn_neighbors(1:gn_inequivalentsites(istr),istr))
    ! allocate(fjlij2(0:lmax,iaux(1),gn_inequivalentsites(istr)))
    ! rms_error=0d0
    ! denom=0 
    !  !check radial density function derivs using numerical differentiation
    !  do iat=1,gn_inequivalentsites(istr)
    !     do jj=1,gn_neighbors(iat,istr)
    !        print *,jj,'/',gn_neighbors(iat,istr)
    !        jat=gneighborlist(jj,iat,istr)
    !        jsp=gspecies(jat,istr)
    !        rij=diststr(jj,iat,0,0,istr) !third from iatom to 0
    !        call raddens(rij,1,jsp,fjlij(0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat))
    !        !x
    !        gxyz(1,iat,istr)=gxyz(1,iat,istr)+dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in a loop
    !        rij=diststr(jj,iat,0,0,istr)
    !        call raddens(rij,1,jsp,fjlij2(0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat))
    !        gxyz(1,iat,istr)=gxyz(1,iat,istr)-dist
    !        call setupnntables
    !        istr=1
    !        print *,'dfjlij_dx(1,',jj,',',iat,')=',(fjlij2(1,jj,iat)-fjlij(1,jj,iat))/0.00000001d0
    !        !print *,'dfjlij_dx(0:lmax,',jj,',',iat,')=',(fjlij2(0:lmax,jj,iat)-fjlij(0:lmax,jj,iat))/0.00000001d0
    !        rms_error=rms_error+( (fjlij2(1,jj,iat)-fjlij(1,jj,iat))/0.00000001d0 - dfjlij_dxyz(1,1,jj,iat) )**2
    !        !y
    !        gxyz(2,iat,istr)=gxyz(2,iat,istr)+dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in a loop
    !        rij=diststr(jj,iat,0,0,istr)
    !        call raddens(rij,1,jsp,fjlij2(0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat))
    !        gxyz(2,iat,istr)=gxyz(2,iat,istr)-dist
    !        call setupnntables
    !        istr=1
    !        print *,'dfjlij_dy(1,',jj,',',iat,')=',(fjlij2(1,jj,iat)-fjlij(1,jj,iat))/0.00000001d0
    !        !print *,'dfjlij_dy(0:lmax,',jj,',',iat,')=',(fjlij2(0:lmax,jj,iat)-fjlij(0:lmax,jj,iat))/0.00000001d0
    !        rms_error=rms_error+( (fjlij2(1,jj,iat)-fjlij(1,jj,iat))/0.00000001d0 - dfjlij_dxyz(2,1,jj,iat) )**2
    !        !z
    !        gxyz(3,iat,istr)=gxyz(3,iat,istr)+dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in a loop
    !        rij=diststr(jj,iat,0,0,istr)
    !        call raddens(rij,1,jsp,fjlij2(0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat))
    !        gxyz(3,iat,istr)=gxyz(3,iat,istr)-dist
    !        call setupnntables
    !        istr=1
    !        print *,'dfjlij_dz(1,',jj,',',iat,')=',(fjlij2(1,jj,iat)-fjlij(1,jj,iat))/0.00000001d0
    !        !print *,'dfjlij_dz(0:lmax,',jj,',',iat,')=',(fjlij2(0:lmax,jj,iat)-fjlij(0:lmax,jj,iat))/0.00000001d0
    !        rms_error=rms_error+( (fjlij2(1,jj,iat)-fjlij(1,jj,iat))/0.00000001d0 - dfjlij_dxyz(3,1,jj,iat) )**2
    !        denom=denom+3

    !     enddo
    !  enddo
    !  print *,'rms mean error:',sqrt(rms_error/dble(denom))
    !  stop

    ! print *,'analytic dfjlij/dpara:'
    ! print *,'----------------------'

    ! !do o=0,lmax
    ! o=1
    !    do ip=1,4
    !       do iatom=1,gn_inequivalentsites(istr)
    !          do jj=1,gn_neighbors(iatom,istr)
    !             do alph=1,3
    !                write(*,'(A24,I1,A5,I1,A3,I1,A5,I3,A7,I3,A2,F15.10)') 'dfjlij_dpara(ip=',ip, &
    !                     ',l=',o,', jj=',jj,',iatom=',iatom,')=', dfjlij_dpara(ip,o,jj,iatom)
    !             enddo
    !          enddo
    !       enddo
    !    enddo
    ! !enddo


    !
    !   -------------------------------
    !   | Testing of rhol derivatives |
    !   -------------------------------
    !
    !
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !
    !---- Test drhol(l,iat)_dpara analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'drhol(l=0-3,iat)_dpara analytic derivative test:'
    !   print *,'------------------------------------------------'
    !   if(allocated(rhol_prev)) deallocate(rhol_prev)
    !   if(allocated(drhol_dpara_prev)) deallocate(drhol_dpara_prev)
    !   allocate(rhol_prev(0:lmax,gn_inequivalentsites(istr)))
    !   allocate(drhol_dpara_prev(12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)))
    !   call radialdensityfunction!Force
    !   call electrondensity(0,0)!Force
    !   rhol_prev=rhol
    !   drhol_dpara_prev=drhol_dpara
    !   do l=0,3

    !      print *
    !      print *,'drhol(l=',l,',iat)_dpara:'
    !      print *,'-------------------------'
    !      print *

    !      rms_error=0d0
    !      denom=0
    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !         do jsp=1,maxspecies

    !            do iiCoeff=1,noptdensityCoeff(l,1,jsp)
    !               ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
    !               write(*,'(A24,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic drhol_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',drhol_dpara_prev(ip,l,jsp,iat) 
    !               meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !               call radialdensityfunction!Force
    !               call electrondensity(0,0)!Force
    !               meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !               write(*,'(A11,F25.15)') '   numeric=',(rhol(l,iat)-rhol_prev(l,iat))/deltaPara
    !               if ( sqrt( ( drhol_dpara_prev(ip,l,jsp,iat) - (rhol(l,iat)-rhol_prev(l,iat))/deltaPara )**2).gt.10d-5 ) then
    !                  print *,'error, analytic and numeric derivatives do not agree'
    !                  write(*,'(A24,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic drhol_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',drhol_dpara(ip,l,jsp,iat)
    !                  write(*,'(A11,F25.15)') '   numeric=',(rhol(l,iat)-rhol_prev(l,iat))/deltaPara
    !                  stop
    !               endif
    !               rms_error=rms_error+( drhol_dpara(ip,l,jsp,iat) - (rhol(l,iat)-rhol_prev(l,iat))/deltaPara )**2
    !               denom=denom+1
    !            enddo

    !            do iiCutoff=1,noptdensityCutoff(l,1,jsp)
    !               ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
    !               write(*,'(A24,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic drhol_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',drhol_dpara(ip,l,jsp,iat)
    !               meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !               call radialdensityfunction!Force
    !               call electrondensity(0,0)!Force
    !               meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !               write(*,'(A11,F25.15)') '   numeric=',(rhol(l,iat)-rhol_prev(l,iat))/deltaPara
    !               if ( sqrt( ( drhol_dpara_prev(ip,l,jsp,iat) - (rhol(l,iat)-rhol_prev(l,iat))/deltaPara )**2).gt.10d-5 ) then
    !                  print *,'error, analytic and numeric derivatives do not agree'
    !                  write(*,'(A24,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic drhol_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',drhol_dpara(ip,l,jsp,iat)
    !                  write(*,'(A11,F25.15)') '   numeric=',(rhol(l,iat)-rhol_prev(l,iat))/deltaPara
    !                  stop
    !               endif
    !               rms_error=rms_error+( drhol_dpara(ip,l,jsp,iat) - (rhol(l,iat)-rhol_prev(l,iat))/deltaPara )**2
    !               denom=denom+1
    !            enddo

    !         enddo
    !      enddo
    !      print *
    !      print *,'rms error for l=',l,'=',sqrt(rms_error/dble(denom))

    !   enddo
    !-------------------------------------------------------------------------------------------


    !   !---- Test drhol(l,iat)_dxyz analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'drhol(l=0-3,iat)_dxyz analytic derivative test:'
    !   print *,'-----------------------------------------------'

    !   if(allocated(rhol_prev)) deallocate(rhol_prev)
    !   if(allocated(drhol_dxyz_prev)) deallocate(drhol_dxyz_prev)
    !   allocate(rhol_prev(0:lmax,gn_inequivalentsites(istr)))
    !   allocate(drhol_dxyz_prev(1:3,0:maxneighbors,0:lmax,gn_inequivalentsites(istr)))
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   rhol_prev=rhol
    !   drhol_dxyz_prev=drhol_dxyz
    !   do l=0,3    

    !      print *  
    !      print *,'drhol(l=',l,',iat)_dxyz:'
    !      print *,'------------------------'
    !      print *

    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms

    !        do jj=0,gn_neighbors(iat,istr)
    !           if (jj.eq.0) then
    !              jat=iat !Onsite derivatives
    !           else
    !              jat=gneighborlist(jj,iat,istr) !Offsite derivatives
    !           endif

    !           do alph=1,3
    !              gxyz(alph,jat,istr)=gxyz(alph,jat,istr)+dist
    !              call setupnntables
    !              istr=1 !need to reset to one because setupnntables used in
    !              call radialdensityfunctionForce
    !              call electrondensityForce
    !              gxyz(alph,jat,istr)=gxyz(alph,jat,istr)-dist
    !              call setupnntables
    !              istr=1 !need to reset to one because setupnntables used in
    !              write(*,'(A26,I1,A5,I3,A4,I1,A6,I4,A2,F25.15)') 'analytic drhol_dxyz(',alph,', jj=',jj,', l=',l,', iat=',iat,')=',drhol_dxyz(alph,jj,l,iat)
    !              write(*,'(A11,F25.15)') '   numeric=',(rhol(l,iat)-rhol_prev(l,iat))/dist
    !              if ( sqrt( ( drhol_dxyz(alph,jj,l,iat)-(rhol(l,iat)-rhol_prev(l,iat))/dist )**2).gt.10d-5 ) then
    !                 print *,'error, analytic and numeric derivatives do not agree'
    !                 stop
    !              endif
    !           enddo

    !        enddo

    !      enddo

    !      print *

    !   enddo
    !   stop
    !---------------------------------------------------------------------------------------------


    !   !---- Test d2rhol_dxyz_dpara(1:3,jj,ip,0,jsp,iatom) analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'d2rhol(l=0-3,iat)_dxyz_dpara analytic derivative test:'
    !   print *,'------------------------------------------------------'

    !   if(allocated(drhol_dxyz_prev)) deallocate(drhol_dxyz_prev)
    !   allocate(drhol_dxyz_prev(1:3,0:maxneighbors,0:lmax,gn_inequivalentsites(istr)))
    !   if(allocated(d2rhol_dxyz_dpara_prev)) deallocate(d2rhol_dxyz_dpara_prev)
    !   allocate(d2rhol_dxyz_dpara_prev(1:3,0:maxneighbors,12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)))
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   drhol_dxyz_prev=drhol_dxyz
    !   d2rhol_dxyz_dpara_prev=d2rhol_dxyz_dpara
    !   do l=0,3

    !      print *
    !      print *,'d2rhol(l=',l,',iat)_dpara_dxyz:'
    !      print *,'-------------------------------'
    !      print *

    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !         do jj=0,3 !<-- replace with: gn_neighbors(iatom,istr), to test all atoms
    !            isp=1
    !            do jsp=1,maxspecies
    !      ! iat=1
    !      !    jj=0
    !      !       isp=1
    !      !       jsp=1
    !               do iiCoeff=1,noptdensityCoeff(l,1,jsp)
    !                  ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
    !                  meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !                  call radialdensityfunctionForce
    !                  call electrondensityForce
    !                  meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !                  do alph=1,3
    !                     write(*,'(A31,I1,A1,I3,A1,I1,A1,I1,A1,I1,A1,I3,A2,F25.15)') 'analytic d2rhol_dxyz_dpara(',alph,',',jj,',',ip,',',l,',',jsp,',',iat,')=', d2rhol_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat)
    !                     write(*,'(A11,F25.15)') '   numeric=',(drhol_dxyz(alph,jj,l,iat) - &
    !                              drhol_dxyz_prev(alph,jj,l,iat))/deltaPara
    !                     if ( sqrt( ( d2rhol_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat) - (drhol_dxyz(alph,jj,l,iat)-drhol_dxyz_prev(alph,jj,l,iat))/deltaPara )**2).gt.10d-5 ) then
    !                        print *,'error, analytic and numeric derivatives do not agree'
    !                        stop
    !                     endif
    !                  enddo
    !               enddo
    !               do iiCutoff=1,noptdensityCutoff(l,1,jsp)
    !                  ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
    !                  meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !                  call radialdensityfunctionForce
    !                  call electrondensityForce
    !                  meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !                  do alph=1,3
    !                     write(*,'(A31,I1,A1,I3,A1,I1,A1,I1,A1,I1,A1,I3,A2,F25.15)') 'analytic d2rhol_dxyz_dpara(',alph,',',jj,',',ip,',',l,',',jsp,',',iat,')=', d2rhol_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat)
    !                     write(*,'(A11,F25.15)') ' numeric=',(drhol_dxyz(alph,jj,l,iat) - &
    !                              drhol_dxyz_prev(alph,jj,l,iat))/deltaPara
    !                     if ( sqrt( ( d2rhol_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat) - (drhol_dxyz(alph,jj,l,iat)-drhol_dxyz_prev(alph,jj,l,iat))/deltaPara)**2).gt.10d-5 ) then
    !                        print *,'error, analytic and numeric derivatives do not agree'
    !                        write(*,'(A31,I1,A1,I3,A1,I1,A1,I1,A1,I1,A1,I3,A2,F25.15)') 'analytic d2rhol_dxyz_dpara(',alph,',',jj,',',ip,',',l,',',jsp,',',iat,')=', d2rhol_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat)
    !                        write(*,'(A11,F25.15)') ' numeric=',(drhol_dxyz(alph,jj,l,iat) - &
    !                              drhol_dxyz_prev(alph,jj,l,iat))/deltaPara
    !                        stop
    !                     endif
    !                  enddo
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !--------------------------------------------------------------------------------------------------------------------

    !
    !
    !   ---------------------------------
    !   | Testing of meam_t derivatives |
    !   ---------------------------------
    !
    !
    !   !---- Test dmeam_t_dpara analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'dmeam_t(l=0-3,iat)_dpara analytic derivative test:'
    !   print *,'--------------------------------------------------'
    !   if(allocated(meam_t_prev)) deallocate(meam_t_prev)
    !   if(allocated(dmeamt_dpara_prev)) deallocate(dmeamt_dpara_prev)
    !   allocate(meam_t_prev(1:lmax,gn_inequivalentsites(istr)))
    !   allocate(dmeamt_dpara_prev(12,1:lmax,1:maxspecies,gn_inequivalentsites(istr)))
    !   call radialdensityfunction!Force
    !   call electrondensity(0,0)!Force
    !   meam_t_prev=meam_t
    !   dmeamt_dpara_prev=dmeamt_dpara
    !   print *,dmeamt_dpara_prev(1,1,1,1)
    !   do l=1,3 !1,3

    !      print *
    !      print *,'dmeamt(l=',l,',iat)_dpara:'
    !      print *,'--------------------------'
    !      print *

    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !         do jsp=1,maxspecies

    !            do iiCoeff=1,noptdensityCoeff(l,1,jsp)
    !               ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
    !               meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)+deltaPara
    !               call radialdensityfunction!Force
    !               call electrondensity(0,0)!Force
    !               meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)-deltaPara
    !               print *,'meam_t prev=',meam_t_prev(l,iat),', meam_t=',meam_t(l,iat)
    !               write(*,'(A25,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic dmeamt_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',dmeamt_dpara(ip,l,jsp,iat)
    !               write(*,'(A11,F25.15)') '   numeric=',(meam_t(l,iat)-meam_t_prev(l,iat))/deltaPara
    !               if ( sqrt( ( dmeamt_dpara_prev(ip,l,jsp,iat) - (meam_t(l,iat)-meam_t_prev(l,iat))/deltaPara )**2).gt.10d-5 ) then
    !                  print *,'error, analytic and numeric derivatives do not agree'
    !                  call radialdensityfunction
    !                  call electrondensity(0,0)
    !                  meam_t_prev=meam_t
    !                  meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)+deltaPara
    !                  call radialdensityfunction
    !                  call electrondensity(0,0)
    !                  meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)-deltaPara
    !                  print *,'from non-force routines'
    !                  write(*,'(A25,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic dmeamt_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',dmeamt_dpara(ip,l,jsp,iat)
    !                  print *,'meam_t prev=',meam_t_prev(l,iat),', meam_t=',meam_t(l,iat)
    !                  write(*,'(A11,F25.15)') 'numeric=',(meam_t(l,iat)-meam_t_prev(l,iat))/deltaPara
    !                  stop
    !               endif
    !            enddo

    !            do iiCutoff=1,noptdensityCutoff(l,1,jsp)
    !               ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
    !               meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)+deltaPara
    !               call radialdensityfunction!Force
    !               call electrondensity(0,0)!Force
    !               meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)-deltaPara
    !               print *,'meam_t prev=',meam_t_prev(l,iat),', meam_t=',meam_t(l,iat)
    !               write(*,'(A25,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic dmeamt_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',dmeamt_dpara(ip,l,jsp,iat)
    !               write(*,'(A11,F25.15)') '   numeric=',(meam_t(l,iat)-meam_t_prev(l,iat))/deltaPara
    !               if ( sqrt( ( dmeamt_dpara_prev(ip,l,jsp,iat) - (meam_t(l,iat)-meam_t_prev(l,iat))/deltaPara )**2).gt.10d-5 ) then
    !                  print *,'error, analytic and numeric derivatives do not agree'
    !                  stop
    !               endif
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   !stop
    !   !-----------------------------------------------------------------------------------------


    !   !---- Test dmeamt_dmeamtau analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'dmeam_t(l=0-3,iat)_dmeamtau(l,jsp) analytic derivative test:'
    !   print *,'------------------------------------------------------------'
    !   if(allocated(meam_t_prev)) deallocate(meam_t_prev)
    !   if(allocated(dmeamt_dmeamtau_prev)) deallocate(dmeamt_dmeamtau_prev)
    !   allocate(meam_t_prev(1:lmax,gn_inequivalentsites(istr)))
    !   allocate(dmeamt_dmeamtau_prev(1:lmax,1:maxspecies,gn_inequivalentsites(istr)))
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   meam_t_prev=meam_t
    !   dmeamt_dmeamtau_prev=dmeamt_dmeamtau
    !   do l=1,3 !1,3

    !      print *
    !      print *,'dmeamt(l=',l,',iat)_dmeamtau(l,jsp):'
    !      print *,'------------------------------------'
    !      print *

    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !         do jsp=1,maxspecies
    !            meamtau(l,jsp)=meamtau(l,jsp)+deltaPara
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            meamtau(l,jsp)=meamtau(l,jsp)-deltaPara
    !            write(*,'(A27,I1,A6,I1,A6,I1,A2,F25.15)') 'analytic dmeamt_dmeamtau(l=',l,', jsp=',jsp,', iat=',iat,')=',dmeamt_dmeamtau_prev(l,jsp,iat)
    !            write(*,'(A11,F25.15)') '   numeric=',(meam_t(l,iat)-meam_t_prev(l,iat))/deltaPara
    !            if ( sqrt( ( dmeamt_dmeamtau_prev(l,jsp,iat) - (meam_t(l,iat)-meam_t_prev(l,iat))/deltaPara )**2).gt.10d-5 ) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !   !-------------------------------------------------------------------------------------------

    !   !---- Test dmeam_t_dxyz analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'dmeam_t(l=0-3,iat)_dxyz analytic derivative test:'
    !   print *,'-------------------------------------------------'
    !   if(allocated(meam_t_prev)) deallocate(meam_t_prev)
    !   if(allocated(dmeamt_dxyz_prev)) deallocate(dmeamt_dxyz_prev)
    !   allocate(meam_t_prev(1:lmax,gn_inequivalentsites(istr)))
    !   allocate(dmeamt_dxyz_prev(1:3,0:maxneighbors,1:lmax,gn_inequivalentsites(istr)))
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   meam_t_prev=meam_t
    !   dmeamt_dxyz_prev=dmeamt_dxyz
    !   do l=1,3
    !   
    !      print *
    !      print *,'dmeam_t(l=',l,',iat)_dxyz:'
    !      print *,'--------------------------'
    !      print *
    !     
    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms

    !        do jj=0,3  !<-- replace with: gn_neighbors(iat,istr), to test all nns
    !           if (jj.eq.0) then
    !              jat=iat !Onsite derivatives
    !           else
    !              jat=gneighborlist(jj,iat,istr) !Offsite derivatives
    !           endif

    !           do alph=1,3
    !              gxyz(alph,jat,istr)=gxyz(alph,jat,istr)+dist
    !              call setupnntables
    !              istr=1 !need to reset to one because setupnntables used in
    !              call radialdensityfunctionForce
    !              call electrondensityForce
    !              gxyz(alph,jat,istr)=gxyz(alph,jat,istr)-dist
    !              call setupnntables
    !              istr=1 !need to reset to one because setupnntables used in
    !              write(*,'(A26,I1,A5,I3,A4,I1,A6,I4,A2,F25.15)') 'analytic dmeamt_dxyz(',alph,', jj=',jj,', l=',l,', iat=',iat,')=',dmeamt_dxyz(alph,jj,l,iat)
    !              write(*,'(A11,F25.15)') '   numeric=',(meam_t(l,iat)-meam_t_prev(l,iat))/dist
    !              if ( sqrt( (dmeamt_dxyz(alph,jj,l,iat)-(meam_t(l,iat)-meam_t_prev(l,iat))/dist )**2).gt.10d-5 ) then
    !                 print *,'error, analytic and numeric derivatives do not agree'
    !                 stop
    !              endif
    !           enddo
    !        enddo

    !      enddo
    !   enddo
    !   stop
    !   !----------------------------------------------------------------------------------------


    !   !---- Test dmeam_t_dxyz_dmeamtau analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'d2meam_t(l=0-3,iat)_dxyz_dmeamtau analytic derivative test:'
    !   print *,'-----------------------------------------------------------'
    !   if(allocated(dmeamt_dxyz_prev)) deallocate(dmeamt_dxyz_prev)
    !   if(allocated(d2meamt_dxyz_dmeamtau_prev)) deallocate(d2meamt_dxyz_dmeamtau_prev)
    !   allocate(dmeamt_dxyz_prev(1:3,0:maxneighbors,1:lmax,gn_inequivalentsites(istr)))
    !   allocate(d2meamt_dxyz_dmeamtau_prev(1:3,0:maxneighbors,1:lmax,1:maxspecies,gn_inequivalentsites(istr)))
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   dmeamt_dxyz_prev=dmeamt_dxyz
    !   d2meamt_dxyz_dmeamtau_prev=d2meamt_dxyz_dmeamtau
    !   do l=1,3
    !      print *
    !      print *,'d2meam_t(l=',l,',iat)_dxyz_dmeamtau:'
    !      print *,'------------------------------------'
    !      print *
    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !         do jj=0,3  !<-- replace with: gn_neighbors(iat,istr), to test all nns
    !            do alph=1,3
    !               do jsp=1,maxspecies
    !                  meamtau(l,jsp)=meamtau(l,jsp)+deltaPara
    !                  call radialdensityfunctionForce
    !                  call electrondensityForce
    !                  meamtau(l,jsp)=meamtau(l,jsp)-deltaPara
    !                  write(*,'(A36,I1,A5,I4,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic d2meamt_dxyz_dmeamtau(a1ph=', &
    !                     alph,', jj=',jj,', l=',l,', jsp=',jsp,', iat=',iat,')=',d2meamt_dxyz_dmeamtau_prev(alph,jj,l,jsp,iat)
    !                  write(*,'(A11,F25.15)') '   numeric=',(dmeamt_dxyz(alph,jj,l,iat)-dmeamt_dxyz_prev(alph,jj,l,iat))/deltaPara
    !                  if ( sqrt( ( d2meamt_dxyz_dmeamtau_prev(alph,jj,l,jsp,iat) - &
    !                  (dmeamt_dxyz(alph,jj,l,iat)-dmeamt_dxyz_prev(alph,jj,l,iat))/deltaPara )**2).gt.10d-5 ) then
    !                     print *,'error, analytic and numeric derivatives do not agree'
    !                     stop
    !                  endif
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !   !----------------------------------------------------------------------------------------


    !   !---- Test dmeam_t_dxyz_dpara analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'d2meam_t(l=0-3,iat)_dxyz_dpara analytic derivative test:'
    !   print *,'--------------------------------------------------------'
    !   if(allocated(dmeamt_dxyz_prev)) deallocate(dmeamt_dxyz_prev)
    !   if(allocated(d2meamt_dxyz_dpara_prev)) deallocate(d2meamt_dxyz_dpara_prev)
    !   allocate(dmeamt_dxyz_prev(1:3,0:maxneighbors,1:lmax,gn_inequivalentsites(istr)))
    !   allocate(d2meamt_dxyz_dpara_prev(1:3,0:maxneighbors,1:lmax,12,1:maxspecies,gn_inequivalentsites(istr)))
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   dmeamt_dxyz_prev=dmeamt_dxyz
    !   d2meamt_dxyz_dpara_prev=d2meamt_dxyz_dpara
    !   do l=1,3
    !      print *
    !      print *,'d2meam_t(l=',l,',iat)_dxyz_dpara:'
    !      print *,'---------------------------------'
    !      print *
    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !         do jj=0,3  !<-- replace with: gn_neighbors(iat,istr), to test all nns
    !            do alph=1,3
    !               do jsp=1,maxspecies
    !                  do iiCoeff=1,noptdensityCoeff(l,1,jsp)
    !                     ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
    !                     meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)+deltaPara
    !                     call radialdensityfunctionForce
    !                     call electrondensityForce
    !                     meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)-deltaPara
    !                     write(*,'(A37,I1,A5,I4,A4,I1,A5,I2,A6,I1,A6,I4,A2,F25.15)') 'analytic d2meamt_dxyz_dpara(a1ph=', &
    !                        alph,', jj=',jj,', l=',l,', ip=',ip,', jsp=',jsp,', iat=',iat,')=',d2meamt_dxyz_dpara_prev(alph,jj,l,ip,jsp,iat)
    !                     write(*,'(A11,F25.15)') '   numeric=',(dmeamt_dxyz(alph,jj,l,iat)-dmeamt_dxyz_prev(alph,jj,l,iat))/deltaPara
    !                     if ( sqrt( ( d2meamt_dxyz_dpara_prev(alph,jj,l,ip,jsp,iat) - &
    !                     (dmeamt_dxyz(alph,jj,l,iat)-dmeamt_dxyz_prev(alph,jj,l,iat))/deltaPara )**2).gt.10d-5 ) then
    !                        print *,'error, analytic and numeric derivatives do not agree'
    !                        stop
    !                     endif
    !                  enddo
    !                  do iiCutoff=1,noptdensityCutoff(l,1,jsp)
    !                     ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
    !                     meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)+deltaPara
    !                     call radialdensityfunctionForce
    !                     call electrondensityForce
    !                     meamrhodecay(ip,0,1,jsp)=meamrhodecay(ip,0,1,jsp)-deltaPara
    !                     write(*,'(A37,I1,A5,I4,A4,I1,A5,I2,A6,I1,A6,I4,A2,F25.15)') 'analytic d2meamt_dxyz_dpara(a1ph=', &
    !                        alph,', jj=',jj,', l=',l,', ip=',ip,', jsp=',jsp,', iat=',iat,')=',d2meamt_dxyz_dpara_prev(alph,jj,l,ip,jsp,iat)
    !                     write(*,'(A11,F25.15)') '   numeric=',(dmeamt_dxyz(alph,jj,l,iat)-dmeamt_dxyz_prev(alph,jj,l,iat))/deltaPara
    !                     if ( sqrt( ( d2meamt_dxyz_dpara_prev(alph,jj,l,ip,jsp,iat) - &
    !                     (dmeamt_dxyz(alph,jj,l,iat)-dmeamt_dxyz_prev(alph,jj,l,iat))/deltaPara )**2).gt.10d-5 ) then
    !                        print *,'error, analytic and numeric derivatives do not agree'
    !                        stop
    !                     endif
    !                  enddo
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !   !----------------------------------------------------------------------------------------


    !-----------------------------------!
    ! Test electron density derivatives !
    !-----------------------------------!


    !call radialdensityfunctionForce
    !call electrondensityForce
    !call backgrounddensityForce
    
    !   print *,'drho_i_dpara:'
    !   print *,'-------------'
    !   print *
    !   if(allocated(rho_i_prev)) deallocate(rho_i_prev)
    !   allocate( rho_i_prev(gn_inequivalentsites(istr)) )

    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   rho_i_prev=rho_i

    !   iat=60
    !   l=1
    !   jsp=1
    !   ip=1
    !   print *,'analytic:',drhoi_dpara(ip,l,jsp,iat)
    !   meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !   print *,'numerical:',(rho_i(iat)-rho_i_prev(iat))/deltaPara
    !   print *


    !   print *,'drho_i_d{x,y,z} (analytic vs numerical):'
    !   print *,'----------------------------------------'
    !   print *
    !   if(allocated(rho_i_prev)) deallocate(rho_i_prev)
    !   allocate( rho_i_prev(gn_inequivalentsites(istr)) )
    !   
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   rho_i_prev=rho_i
   
    !   do iat=1,gn_inequivalentsites(istr)

    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      do jj=0,gn_neighbors(iat,istr)
    !         do alph=1,3
    !            print *,'drhoi_dxyz(',alph,',',jj,',',iat,')=',drhoi_dxyz(alph,jj,iat)
    !         enddo
    !      enddo

    !      isp=gspecies(iat,istr)

    !      !First compute numerical derivative w.r.t onsite atomic coordinates:
    !      !x
    !      gxyz(1,iat,istr)=gxyz(1,iat,istr)+dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      print *,'rho_i(',iat,')=',rho_i(iat),' (istr=',istr,')'
    !      print *,'drho_i(iat=',iat,')_dx(iat=',iat,')=',(rho_i(iat)-rho_i_prev(iat))/dist
    !      gxyz(1,iat,istr)=gxyz(1,iat,istr)-dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in

    !      !y
    !      gxyz(2,iat,istr)=gxyz(2,iat,istr)+dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      print *,'drho_i(iat=',iat,')_dy(iat=',iat,')=',(rho_i(iat)-rho_i_prev(iat))/dist
    !      gxyz(2,iat,istr)=gxyz(2,iat,istr)-dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in

    !      !z
    !      gxyz(3,iat,istr)=gxyz(3,iat,istr)+dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      print *,'drho_i(iat=',iat,')_dz(iat=',iat,')=',(rho_i(iat)-rho_i_prev(iat))/dist
    !      gxyz(3,iat,istr)=gxyz(3,iat,istr)-dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in

    !      !now compute deriviates of rhol(l=1,i) w.r.t other atomic coordinates
    !      do jj=1,gn_neighbors(iat,istr)
    !        jj=1
    !        jat=gneighborlist(jj,iat,istr)

    !        !x
    !        gxyz(1,jat,istr)=gxyz(1,jat,istr)+dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in
    !        call radialdensityfunctionForce
    !        call electrondensityForce
    !        call backgrounddensityForce
    !        print *,'drho_i(iat=',iat,')_dx(iat=',jat,')=',(rho_i(iat)-rho_i_prev(iat))/dist
    !        gxyz(1,jat,istr)=gxyz(1,jat,istr)-dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in

    !        !y
    !        gxyz(2,jat,istr)=gxyz(2,jat,istr)+dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in
    !        call radialdensityfunctionForce
    !        call electrondensityForce
    !        call backgrounddensityForce
    !        print *,'drho_i(iat=',iat,')_dy(iat=',jat,')=',(rho_i(iat)-rho_i_prev(iat))/dist
    !        gxyz(2,jat,istr)=gxyz(2,jat,istr)-dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in

    !        !z
    !        gxyz(3,jat,istr)=gxyz(3,jat,istr)+dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in
    !        call radialdensityfunctionForce
    !        call electrondensityForce
    !        call backgrounddensityForce
    !        print *,'drho_i(iat=',iat,')_dz(iat=',jat,')=',(rho_i(iat)-rho_i_prev(iat))/dist
    !        gxyz(3,jat,istr)=gxyz(3,jat,istr)-dist
    !        call setupnntables
    !        istr=1 !need to reset to one because setupnntables used in

    !     enddo

    !     stop

    !   enddo
    !   stop


    !---- Test d2rho_i_dxyz_dmeamtau analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'d2rho_i(l=0-3,iat)_dxyz_dmeamtau analytic derivative test:'
    !   print *,'----------------------------------------------------------'
    !   if(allocated(drhoi_dxyz_prev)) deallocate(drhoi_dxyz_prev)
    !   if(allocated(d2rhoi_dxyz_dmeamtau_prev)) deallocate(d2rhoi_dxyz_dmeamtau_prev)
    !   allocate(drhoi_dxyz_prev(1:3,0:maxneighbors,gn_inequivalentsites(istr)))
    !   allocate(d2rhoi_dxyz_dmeamtau_prev(1:3,0:maxneighbors,1:lmax,1:maxspecies,gn_inequivalentsites(istr)))
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   drhoi_dxyz_prev=drhoi_dxyz
    !   d2rhoi_dxyz_dmeamtau_prev=d2rhoi_dxyz_dmeamtau
    !   do l=1,3
    !      print *
    !      print *,'d2rho_i(l=',l,',iat)_dxyz_dmeamtau:'
    !      print *,'-----------------------------------'
    !      print *
    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !         do jj=0,3  !<-- replace with: gn_neighbors(iat,istr), to test all nns
    !            do alph=1,3
    !               do jsp=1,maxspecies
    !                  meamtau(l,jsp)=meamtau(l,jsp)+deltaPara
    !                  call radialdensityfunctionForce
    !                  call electrondensityForce
    !                  call backgrounddensityForce
    !                  meamtau(l,jsp)=meamtau(l,jsp)-deltaPara
    !                  write(*,'(A35,I1,A5,I4,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic d2rhoi_dxyz_dmeamtau(a1ph=', &
    !                     alph,', jj=',jj,', l=',l,', jsp=',jsp,', iat=',iat,')=',d2rhoi_dxyz_dmeamtau_prev(alph,jj,l,jsp,iat)
    !                  write(*,'(A11,F25.15)') '   numeric=',(drhoi_dxyz(alph,jj,iat)-drhoi_dxyz_prev(alph,jj,iat))/deltaPara
    !                  if ( sqrt( ( d2rhoi_dxyz_dmeamtau_prev(alph,jj,l,jsp,iat) - &
    !                     (drhoi_dxyz(alph,jj,iat)-drhoi_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5 ) then
    !                     print *,'error, analytic and numeric derivatives do not agree'
    !                     stop
    !                  endif
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop


    !   !---- Test d2rho_i_dxyz_dpara analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'d2rho_i(l=0-3,iat)_dxyz_dpara analytic derivative test:'
    !   print *,'-------------------------------------------------------'
    !   if(allocated(drhoi_dxyz_prev)) deallocate(drhoi_dxyz_prev)
    !   if(allocated(d2rhoi_dxyz_dpara_prev)) deallocate(d2rhoi_dxyz_dpara_prev)
    !   allocate(drhoi_dxyz_prev(1:3,0:maxneighbors,gn_inequivalentsites(istr)))
    !   allocate(d2rhoi_dxyz_dpara_prev(1:3,0:maxneighbors,1:12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)))
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   !print *,'undisplaced:'
    !   call backgrounddensityForce
    !   drhoi_dxyz_prev=drhoi_dxyz
    !   d2rhoi_dxyz_dpara_prev=d2rhoi_dxyz_dpara
    !   do l=0,3
    !      print *
    !      print *,'d2rho_i(l=',l,',iat)_dxyz_dpara:'
    !      print *,'--------------------------------'
    !      print *
    !      do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !         do jj=0,3  !<-- replace with: gn_neighbors(iat,istr), to test all nns
    !            do alph=1,3
    !               do jsp=1,maxspecies
    !                  do iiCoeff=1,noptdensityCoeff(l,1,jsp)
    !                     ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
    !                     meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !                     call radialdensityfunctionForce
    !                     call electrondensityForce
    !                     !print *,'meamrhodecay displaced by ',deltaPara,':'
    !                     call backgrounddensityForce
    !                     meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !                     write(*,'(A32,I1,A5,I4,A5,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic d2rhoi_dxyz_dpara(a1ph=', &
    !                        alph,', jj=',jj,', ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',d2rhoi_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat)
    !                     write(*,'(A11,F25.15)') '   numeric=',(drhoi_dxyz(alph,jj,iat)-drhoi_dxyz_prev(alph,jj,iat))/deltaPara
    !                     if ( sqrt( ( d2rhoi_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat) - &
    !                        (drhoi_dxyz(alph,jj,iat)-drhoi_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5 ) then
    !                        print *,'error, analytic and numeric derivatives do not agree'
    !                        stop
    !                     endif
    !                  enddo
    !                  do iiCutoff=1,noptdensityCutoff(l,1,jsp)
    !                     ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
    !                     meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !                     call radialdensityfunctionForce
    !                     call electrondensityForce
    !                     !print *,'meamrhodecay displaced by ',deltaPara,':'
    !                     call backgrounddensityForce
    !                     meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !                     write(*,'(A32,I1,A5,I4,A5,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic d2rhoi_dxyz_dpara(a1ph=', &
    !                        alph,', jj=',jj,', ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',d2rhoi_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat)
    !                     write(*,'(A11,F25.15)') '   numeric=',(drhoi_dxyz(alph,jj,iat)-drhoi_dxyz_prev(alph,jj,iat))/deltaPara
    !                     if ( sqrt( ( d2rhoi_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat) - &
    !                        (drhoi_dxyz(alph,jj,iat)-drhoi_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5 ) then
    !                        print *,'error, analytic and numeric derivatives do not agree'
    !                        stop
    !                     endif
    !                  enddo
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop


    !-------------------------------------!
    ! Test embedding function derivatives !
    !-------------------------------------!


    ! call radialdensityfunctionForce
    ! call electrondensityForce
    ! call backgrounddensityForce
    ! call embeddingfunctionForce

    !   !---- Test dmeamf_i_dxyz analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'dmeamf_i_d{x,y,z} (analytic vs numerical):'
    !   print *,'------------------------------------------'
    !   print *
    !   if(allocated(meam_f_prev)) deallocate(meam_f_prev,dmeamf_dxyz_prev)
    !   allocate( meam_f_prev(gn_inequivalentsites(istr)),dmeamf_dxyz_prev(1:3,0:maxneighbors,gn_inequivalentsites(istr)))
    !   meam_f_prev=meam_f
    !   dmeamf_dxyz_prev=dmeamf_dxyz
    !   do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms

    !     do jj=0,gn_neighbors(iat,istr)
    !        if (jj.eq.0) then
    !           jat=iat !Onsite derivatives
    !        else
    !           jat=gneighborlist(jj,iat,istr) !Offsite derivatives
    !        endif

    !        do alph=1,3
    !           gxyz(alph,jat,istr)=gxyz(alph,jat,istr)+dist
    !           call setupnntables
    !           istr=1 !need to reset to one because setupnntables used in
    !           call radialdensityfunctionForce
    !           call electrondensityForce
    !           call backgrounddensityForce
    !           call embeddingfunctionForce
    !           gxyz(alph,jat,istr)=gxyz(alph,jat,istr)-dist
    !           call setupnntables
    !           istr=1 !need to reset to one because setupnntables used in
    !           write(*,'(A28,I1,A5,I3,A6,I4,A2,F25.15)') 'analytic dmeam_f_dxyz(',alph,', jj=',jj,', iat=',iat,')=',dmeamf_dxyz_prev(alph,jj,iat)
    !           write(*,'(A11,F25.15)') '   numeric=',(meam_f(iat)-meam_f_prev(iat))/dist
    !           if ( sqrt( ( dmeamf_dxyz_prev(alph,jj,iat) - (meam_f(iat)-meam_f_prev(iat))/dist)**2).gt.10d-5 ) then
    !              print *,'error, analytic and numeric derivatives do not agree'
    !              stop
    !           endif
    !        enddo

    !     enddo
    !    
    !   enddo
    !   !stop
    !   !-----------------------------------------------------------------------------------------


    !   !---- Test dmeamf_i_dmeame0, etc, analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'dmeamf_i_dmeam{e0,rho0,emb3,emb4} (analytic vs numerical):'
    !   print *,'----------------------------------------------------------'
    !   print * 
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   if(allocated(meam_f_prev)) deallocate(meam_f_prev)
    !   allocate(meam_f_prev(gn_inequivalentsites(istr)))
    !   if(allocated(dmeamf_dmeame0_prev)) deallocate(dmeamf_dmeame0_prev,dmeamf_dmeamrho0_prev,d2meamf_dxyz_dmeamemb3_prev,d2meamf_dxyz_dmeamemb4_prev)
    !   allocate(dmeamf_dmeame0_prev(gn_inequivalentsites(istr)),dmeamf_dmeamrho0_prev(gn_inequivalentsites(istr)),dmeamf_dmeamemb3_prev(gn_inequivalentsites(istr)),dmeamf_dmeamemb4_prev(gn_inequivalentsites(istr)))
    !   meam_f_prev=meam_f
    !   dmeamf_dmeame0_prev=dmeamf_dmeame0
    !   dmeamf_dmeamrho0_prev=dmeamf_dmeamrho0
    !   dmeamf_dmeamemb3_prev=dmeamf_dmeamemb3
    !   dmeamf_dmeamemb4_prev=dmeamf_dmeamemb4
    !   do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !      isp=gspecies(iat,istr)
    !      ! ---- meame0 ----
    !      meame0(isp)=meame0(isp)+deltaPara
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      call embeddingfunctionForce
    !      meame0(isp)=meame0(isp)-deltaPara
    !      write(*,'(A28,I6,A2,F25.15)') 'analytic dmeamf_dmeame0(iat=',iat,')=',dmeamf_dmeame0_prev(iat)
    !      write(*,'(A11,F25.15)') '   numeric=',(meam_f(iat)-meam_f_prev(iat))/deltaPara
    !      if ( sqrt( ( dmeamf_dmeame0_prev(iat) - (meam_f(iat)-meam_f_prev(iat))/deltaPara)**2).gt.10d-5 ) then
    !         print *,'error, analytic and numeric derivatives do not agree'
    !         stop
    !      endif
    !      ! ---- meamrho0 ----
    !      meamrho0(isp)=meamrho0(isp)+deltaPara
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      call embeddingfunctionForce
    !      meamrho0(isp)=meamrho0(isp)-deltaPara
    !      write(*,'(A30,I6,A2,F25.15)') 'analytic dmeamf_dmeamrho0(iat=',iat,')=',dmeamf_dmeamrho0_prev(iat)
    !      write(*,'(A11,F25.15)') '   numeric=',(meam_f(iat)-meam_f_prev(iat))/deltaPara
    !      if ( sqrt( ( dmeamf_dmeamrho0_prev(iat) - (meam_f(iat)-meam_f_prev(iat))/deltaPara)**2).gt.10d-5 ) then
    !         print *,'error, analytic and numeric derivatives do not agree'
    !         stop
    !      endif
    !      ! ---- meamemb3 ----
    !      meamemb3(isp)=meamemb3(isp)+deltaPara
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      call embeddingfunctionForce
    !      meamemb3(isp)=meamemb3(isp)-deltaPara
    !      write(*,'(A30,I6,A2,F25.15)') 'analytic dmeamf_dmeamemb3(iat=',iat,')=',dmeamf_dmeamemb3_prev(iat)
    !      write(*,'(A11,F25.15)') '   numeric=',(meam_f(iat)-meam_f_prev(iat))/deltaPara
    !      if ( sqrt( ( dmeamf_dmeamemb3_prev(iat) - (meam_f(iat)-meam_f_prev(iat))/deltaPara)**2).gt.10d-5 ) then
    !         print *,'error, analytic and numeric derivatives do not agree'
    !         stop
    !      endif
    !      ! ---- meamemb4 ----
    !      meamemb4(isp)=meamemb4(isp)+deltaPara
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      call embeddingfunctionForce
    !      meamemb4(isp)=meamemb4(isp)-deltaPara
    !      write(*,'(A30,I6,A2,F25.15)') 'analytic dmeamf_dmeamemb4(iat=',iat,')=',dmeamf_dmeamemb4_prev(iat)
    !      write(*,'(A11,F25.15)') '   numeric=',(meam_f(iat)-meam_f_prev(iat))/deltaPara
    !      if ( sqrt( ( dmeamf_dmeamemb4_prev(iat) - (meam_f(iat)-meam_f_prev(iat))/deltaPara)**2).gt.10d-5 ) then
    !         print *,'error, analytic and numeric derivatives do not agree'
    !         stop
    !      endif
    !   enddo
    !   !stop
    !   !--------------------------------------------------------------------------------------------------


    !   !---- Test dmeamf_i_dpara analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'dmeamf_i_dpara (analytic vs numerical):'
    !   print *,'---------------------------------------'
    !   print *
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   if(allocated(meam_f_prev)) deallocate(meam_f_prev)
    !   allocate(meam_f_prev(gn_inequivalentsites(istr)))
    !   allocate(dmeamf_dpara_prev(12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)))
    !   meam_f_prev=meam_f
    !   dmeamf_dpara_prev=dmeamf_dpara
    !   do iat=1,3!   !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !      print *,'iat=',iat,'/',gn_forces(istr)
    !      isp=gspecies(iat,istr)
    !      do jsp=1,maxspecies
    !         do l=0,lmax
    !            do iiCoeff=1,noptdensityCoeff(l,1,jsp)
    !               ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
    !               meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !               call radialdensityfunctionForce
    !               call electrondensityForce
    !               call backgrounddensityForce
    !               call embeddingfunctionForce
    !               meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !               write(*,'(A33,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic dmeam_f_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',dmeamf_dpara_prev(ip,l,jsp,iat)
    !               write(*,'(A11,F25.15)') '   numeric=',(meam_f(iat)-meam_f_prev(iat))/deltaPara
    !               if ( sqrt( ( dmeamf_dpara_prev(ip,l,jsp,iat) - (meam_f(iat)-meam_f_prev(iat))/deltaPara)**2).gt.10d-5 ) then
    !                  print *,'error, analytic and numeric derivatives do not agree'
    !                  stop
    !               endif
    !            enddo
    !            do iiCutoff=1,noptdensityCutoff(l,1,jsp)
    !               ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
    !               meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !               call radialdensityfunctionForce
    !               call electrondensityForce
    !               call backgrounddensityForce
    !               call embeddingfunctionForce
    !               meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !               write(*,'(A33,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic dmeam_f_dpara(ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',dmeamf_dpara_prev(ip,l,jsp,iat)
    !               write(*,'(A11,F25.15)') '   numeric=',(meam_f(iat)-meam_f_prev(iat))/deltaPara
    !               if ( sqrt( ( dmeamf_dpara_prev(ip,l,jsp,iat) - (meam_f(iat)-meam_f_prev(iat))/deltaPara)**2).gt.10d-5 ) then
    !                  print *,'error, analytic and numeric derivatives do not agree'
    !                  stop  
    !               endif 
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !   !------------------------------------------------------------------------------------------


    !   !---- Test d2meamf_i_dxyz_dmeame0, etc, analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'d2meamf_i_d{x,y,z}_dmeam{e0,rho0,emb3,emb4} (analytic vs numerical):'
    !   print *,'--------------------------------------------------------------------'
    !   print * 
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   if(allocated(dmeamf_dxyz_prev)) deallocate(dmeamf_dxyz_prev,d2meamf_dxyz_dmeame0_prev,d2meamf_dxyz_dmeamrho0_prev,d2meamf_dxyz_dmeamemb3_prev,d2meamf_dxyz_dmeamemb4_prev)
    !   allocate(dmeamf_dxyz_prev(1:3,0:maxneighbors,gn_inequivalentsites(istr)),d2meamf_dxyz_dmeame0_prev(3,0:maxneighbors,gn_inequivalentsites(istr)),d2meamf_dxyz_dmeamrho0_prev(3,0:maxneighbors,gn_inequivalentsites(istr)),d2meamf_dxyz_dmeamemb3_prev(3,0:maxneighbors,gn_inequivalentsites(istr)),d2meamf_dxyz_dmeamemb4_prev(3,0:maxneighbors,gn_inequivalentsites(istr)))
    !   dmeamf_dxyz_prev=dmeamf_dxyz
    !   d2meamf_dxyz_dmeame0_prev=d2meamf_dxyz_dmeame0
    !   d2meamf_dxyz_dmeamrho0_prev=d2meamf_dxyz_dmeamrho0
    !   d2meamf_dxyz_dmeamemb3_prev=d2meamf_dxyz_dmeamemb3
    !   d2meamf_dxyz_dmeamemb4_prev=d2meamf_dxyz_dmeamemb4
    !   do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !      isp=gspecies(iat,istr)
    !      do jj=0,3 !<-- replace with: gn_neighbors(iat,istr), to test all atoms
    !         do alph=1,3
    !            ! ---- meame0 ----
    !            meame0(isp)=meame0(isp)+deltaPara
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            meame0(isp)=meame0(isp)-deltaPara
    !            write(*,'(A37,I1,A5,I3,A6,I4,A2,F25.15)') 'analytic d2meam_f_dxyz_dmeame0(',alph,', jj=',jj,', iat=',iat,')=',d2meamf_dxyz_dmeame0_prev(alph,jj,iat)
    !            write(*,'(A11,F25.15)') '   numeric=',(dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara
    !            if ( sqrt( ( d2meamf_dxyz_dmeame0_prev(alph,jj,iat) - (dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5 ) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !            ! ---- meamrho0 ----
    !            meamrho0(isp)=meamrho0(isp)+deltaPara
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            meamrho0(isp)=meamrho0(isp)-deltaPara
    !            write(*,'(A39,I1,A5,I3,A6,I4,A2,F25.15)') 'analytic d2meam_f_dxyz_dmeamrho0(',alph,', jj=',jj,', iat=',iat,')=',d2meamf_dxyz_dmeamrho0_prev(alph,jj,iat)
    !            write(*,'(A11,F25.15)') '   numeric=',(dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara
    !            if ( sqrt( ( d2meamf_dxyz_dmeamrho0_prev(alph,jj,iat) - (dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !            ! ---- meamemb3 ----
    !            meamemb3(isp)=meamemb3(isp)+deltaPara
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            meamemb3(isp)=meamemb3(isp)-deltaPara
    !            write(*,'(A39,I1,A5,I3,A6,I4,A2,F25.15)') 'analytic d2meam_f_dxyz_dmeamemb3(',alph,', jj=',jj,', iat=',iat,')=',d2meamf_dxyz_dmeamemb3_prev(alph,jj,iat)
    !            write(*,'(A11,F25.15)') '   numeric=',(dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara
    !            if ( sqrt( ( d2meamf_dxyz_dmeamemb3_prev(alph,jj,iat) - (dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !            ! ---- meamemb4 ----
    !            meamemb4(isp)=meamemb4(isp)+deltaPara
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            meamemb4(isp)=meamemb4(isp)-deltaPara
    !            write(*,'(A39,I1,A5,I3,A6,I4,A2,F25.15)') 'analytic d2meam_f_dxyz_dmeamemb4(',alph,', jj=',jj,', iat=',iat,')=',d2meamf_dxyz_dmeamemb4_prev(alph,jj,iat)
    !            write(*,'(A11,F25.15)') '   numeric=',(dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara
    !            if ( sqrt( ( d2meamf_dxyz_dmeamemb4_prev(alph,jj,iat) - (dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !   !--------------------------------------------------------------------------------------------------------


    !   !---- Test d2meamf_i_dxyz_dmeamtau analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'d2meamf_i_d{x,y,z}_dmeamtau (analytic vs numerical):'
    !   print *,'----------------------------------------------------'
    !   print *
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   if(allocated(dmeamf_dxyz_prev)) deallocate(dmeamf_dxyz_prev,d2meamf_dxyz_dmeamtau_prev)
    !   allocate(dmeamf_dxyz_prev(1:3,0:maxneighbors,gn_inequivalentsites(istr)),d2meamf_dxyz_dmeamtau_prev(3,0:maxneighbors,1:lmax,maxspecies,gn_inequivalentsites(istr)))
    !   dmeamf_dxyz_prev=dmeamf_dxyz
    !   d2meamf_dxyz_dmeamtau_prev=d2meamf_dxyz_dmeamtau
    !   do iat=1,3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !      isp=gspecies(iat,istr)
    !      do jsp=1,maxspecies
    !         do l=1,3
    !            do jj=0,3 !<-- replace with: gn_neighbors(iat,istr), to test all atoms
    !               do alph=1,3
    !                  meamtau(l,jsp)=meamtau(l,jsp)+deltaPara
    !                  call radialdensityfunctionForce
    !                  call electrondensityForce
    !                  call backgrounddensityForce
    !                  call embeddingfunctionForce
    !                  meamtau(l,jsp)=meamtau(l,jsp)-deltaPara
    !                  write(*,'(A39,I1,A5,I3,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic d2meam_f_dxyz_dmeamtau(',alph,', jj=',jj,', l=',l,', jsp=',jsp,', iat=',iat,')=',d2meamf_dxyz_dmeamtau_prev(alph,jj,l,jsp,iat)
    !                  write(*,'(A11,F25.15)') '   numeric=',(dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara
    !                  if ( sqrt( ( d2meamf_dxyz_dmeamtau_prev(alph,jj,l,jsp,iat) - (dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5 ) then
    !                     print *,'error, analytic and numeric derivatives do not agree'
    !                     stop
    !                  endif
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !   !---------------------------------------------------------------------------------------------------


    !   !---- Test d2meamf_i_dxyz_dpara analytic derivatives (by comparing to numerical derivatives) ----
    !   print *,'d2meamf_i_d{x,y,z}_dpara (analytic vs numerical):'
    !   print *,'-------------------------------------------------'
    !   print *
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   if(allocated(dmeamf_dxyz_prev)) deallocate(dmeamf_dxyz_prev)
    !   allocate(dmeamf_dxyz_prev(1:3,0:maxneighbors,gn_inequivalentsites(istr)),d2meamf_dxyz_dpara_prev(3,0:maxneighbors,12,0:lmax,maxspecies,gn_inequivalentsites(istr)))
    !   dmeamf_dxyz_prev=dmeamf_dxyz
    !   d2meamf_dxyz_dpara_prev=d2meamf_dxyz_dpara
    !   do iat=1,gn_forces(istr) !3  !<-- replace with: gn_inequivalentsites(istr), to test all atoms
    !      print *,'iat=',iat,'/',gn_forces(istr)
    !      isp=gspecies(iat,istr)
    !      do jsp=1,maxspecies
    !         do l=1,3
    !            do iiCoeff=1,noptdensityCoeff(l,1,jsp)
    !               ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
    !               do jj=0,gn_neighbors(iat,istr)  ! 3 !<-- replace with: gn_neighbors(iat,istr), to test all atoms
    !                  do alph=1,3
    !                     meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !                     call radialdensityfunctionForce
    !                     call electrondensityForce
    !                     call backgrounddensityForce
    !                     call embeddingfunctionForce
    !                     meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !                     write(*,'(A36,I1,A5,I3,A5,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic d2meam_f_dxyz_dpara(',alph,', jj=',jj,', ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',d2meamf_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat)
    !                     write(*,'(A11,F25.15)') '   numeric=',(dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara
    !                     if ( sqrt( ( d2meamf_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat) - (dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5 ) then
    !                        print *,'error, analytic and numeric derivatives do not agree'
    !                        stop
    !                     endif
    !                  enddo
    !               enddo
    !            enddo
    !            do iiCutoff=1,noptdensityCutoff(l,1,jsp)
    !               ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
    !               do jj=0,3 !<-- replace with: gn_neighbors(iat,istr), to test all atoms
    !                  do alph=1,3
    !                     meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !                     call radialdensityfunctionForce
    !                     call electrondensityForce
    !                     call backgrounddensityForce
    !                     call embeddingfunctionForce
    !                     meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !                     write(*,'(A36,I1,A5,I3,A5,I2,A4,I1,A6,I1,A6,I4,A2,F25.15)') 'analytic d2meam_f_dxyz_dpara(',alph,', jj=',jj,', ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,')=',d2meamf_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat)
    !                     write(*,'(A11,F25.15)') '   numeric=',(dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara
    !                     if ( sqrt( ( d2meamf_dxyz_dpara_prev(alph,jj,ip,l,jsp,iat) - (dmeamf_dxyz(alph,jj,iat)-dmeamf_dxyz_prev(alph,jj,iat))/deltaPara)**2).gt.10d-5 ) then
    !                        print *,'error, analytic and numeric derivatives do not agree'
    !                        stop
    !                     endif
    !                  enddo
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !   !------------------------------------------------------------------------------------------------


    !                                             /-----------\
    !                                             |  Forces   |
    !                                             \-----------/


    !   !---- Test forces ----
    !   print *,'test forces'
    !   print *,'-----------'

    !   ! call pairpotential
    !   ! call radialdensityfunction
    !   ! call electrondensity(0,0)
    !   ! call backgrounddensity
    !   ! call embeddingfunction
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce

    !   energyTmp=0d0
    !   do iat=1,gn_inequivalentsites(istr)
    !      energyTmp=energyTmp+meam_paire(iat)+meam_f(iat)
    !   enddo

    !   rms_error=0d0
    !   do iat=1,gn_forces(istr)

    !      do alph=1,3

    !         gxyz(alph,iat,istr)=gxyz(alph,iat,istr)+dist
    !         call setupnntables
    !         istr=1 !need to reset to one because setupnntables used in
    !         ! call pairpotential
    !         ! call radialdensityfunction
    !         ! call electrondensity(0,0)
    !         ! call backgrounddensity
    !         ! call embeddingfunction
    !         call pairpotentialForce
    !         call radialdensityfunctionForce
    !         call electrondensityForce
    !         call backgrounddensityForce
    !         call embeddingfunctionForce

    !         energyTmp2=0d0
    !         do jat=1,gn_inequivalentsites(istr)
    !            energyTmp2=energyTmp2+meam_paire(jat)+meam_f(jat)
    !         enddo
    !         force(alph)=(energyTmp2-energyTmp)/dist
    !         !To remove digits after the precision of the numerical differentiation:
    !         !call rmforcenoise(energyTmp,energyTmp2,dist,force(alph),forceCut)
    !         !force(alph)=forceCut
    !         !print *,'dE/dx_',iat,' =',(energyTmp2-energyTmp)/dist
    !         gxyz(alph,iat,istr)=gxyz(alph,iat,istr)-dist
    !         call setupnntables
    !         istr=1 !need to reset to one because setupnntables used in
    !   
    !         write(*,'(A7,I1,A6,I4,A2,F15.10)') 'dE/d{x_',alph,', iat=',iat,'}=',forces(alph,iat)
    !         write(*,'(A13,F15.10)') '   numerical=',force(alph)
    !         !print *,'   ( =',energyTmp2,'-',energyTmp,'/',dist,')'
    !         !if ( sqrt( ( forces(alph,iat) - force(alph))**2).gt.10d-5 ) then
    !         !   print *,'error, analytic and numeric derivatives do not agree'
    !         !   stop
    !         !endif
    !   
    !         rms_error=rms_error+(force(alph)-forces(alph,iat))**2

    !      enddo

    !   enddo
    !   print *,'rms error on force components=',sqrt(rms_error/(3d0*gn_inequivalentsites(istr)))
    !   stop
    !   !----------------------


    !   ! ---- Test dforce_dpairpotpara ----
    !   print *,'Test dforce_dpairpotpara against numerical. Analytic values shown:'
    !   print *,'------------------------------------------------------------------' 
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   do iat=1,gn_forces(istr)
    !      do alph=1,3
    !         forces_prev(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !      enddo
    !      do jj=1,gn_neighbors(iat,istr)
    !         jat=gneighborlist(jj,iat,istr)
    !         do kk=1,gn_neighbors(jat,istr)
    !            kat=gneighborlist(kk,jat,istr)
    !            if (kat.eq.iat) then
    !               do alph=1,3
    !                  forces_prev(alph,iat)=forces_prev(alph,iat)+ &
    !                     dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !               enddo
    !               exit
    !            endif
    !         enddo
    !      enddo
    !   enddo
    !   do iat=1,gn_forces(istr)
    !      do isp=1,maxspecies
    !         do jsp=isp,maxspecies
    !            do ip=1,4
    !               do alph=1,3
    !                  pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)+deltaPara

    !                  !Calculate force using displaced parameters
    !                  call pairpotentialForce
    !                  call radialdensityfunctionForce
    !                  call electrondensityForce
    !                  call backgrounddensityForce
    !                  call embeddingfunctionForce
    !                  forces(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !                  do jj=1,gn_neighbors(iat,istr)
    !                     jat=gneighborlist(jj,iat,istr)
    !                     do kk=1,gn_neighbors(jat,istr)
    !                        kat=gneighborlist(kk,jat,istr)
    !                        if (kat.eq.iat) then
    !                           forces(alph,iat)=forces(alph,iat)+ &
    !                              dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !                           exit
    !                        endif
    !                     enddo
    !                  enddo

    !                  dforce_dpara_num(alph)=(forces(alph,iat)-forces_prev(alph,iat))/deltaPara
    !                  pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)-deltaPara
    !                  write(*,'(A21,I1,A5,I2,A6,I1,A6,I1,A6,I4,A2,F15.10)') 'dF_dpairpotpara(alph=',alph,', ip=',ip,', isp=',isp,', jsp=',jsp,', iat=',iat,'}=',dforce_dpairpot(alph,ip,isp,jsp,iat)
    !                  if ( sqrt( ( dforce_dpairpot(alph,ip,isp,jsp,iat) - dforce_dpara_num(alph))**2).gt.10d-3 ) then
    !                     print *,'error, analytic and numeric derivatives do not agree'
    !                     write(*,'(A13,F15.10)') '   numerical=',dforce_dpara_num(alph)
    !                     stop
    !                  endif
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   stop
    !   !----------------------------------


    !   !---- Test dforce_draddenspara ----
    !   print *,'Test dforce_draddenspara'
    !   print *,'------------------------'

    !   rms_error=0d0
    !   denom=0
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   do iat=1,gn_forces(istr)
    !      do alph=1,3
    !         forces_prev(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !      enddo
    !      do jj=1,gn_neighbors(iat,istr)
    !         jat=gneighborlist(jj,iat,istr)
    !         do kk=1,gn_neighbors(jat,istr)
    !            kat=gneighborlist(kk,jat,istr)
    !            if (kat.eq.iat) then
    !               do alph=1,3
    !                  forces_prev(alph,iat)=forces_prev(alph,iat)+ &
    !                     dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !               enddo
    !               exit
    !            endif
    !         enddo
    !      enddo
    !   enddo
    !   do iat=1,gn_forces(istr)
    !      do jsp=1,maxspecies
    !         do ip=1,4
    !            do l=0,3
    !               do alph=1,3
    !                  meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara

    !                  !Calculate force using displaced parameters
    !                  call pairpotentialForce
    !                  call radialdensityfunctionForce
    !                  call electrondensityForce
    !                  call backgrounddensityForce
    !                  call embeddingfunctionForce
    !                  forces(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !                  do jj=1,gn_neighbors(iat,istr)
    !                     jat=gneighborlist(jj,iat,istr)
    !                     do kk=1,gn_neighbors(jat,istr)
    !                        kat=gneighborlist(kk,jat,istr)
    !                        if (kat.eq.iat) then
    !                           forces(alph,iat)=forces(alph,iat)+ &
    !                              dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !                           exit
    !                        endif
    !                     enddo
    !                  enddo
    !                  dforce_dpara_num(alph)=(forces(alph,iat)-forces_prev(alph,iat))/deltaPara
    !                  meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !                  write(*,'(A21,I1,A5,I2,A4,I1,A6,I1,A6,I4,A2,F15.10)') 'dF_draddenspara(alph=',alph,', ip=',ip,', l=',l,', jsp=',jsp,', iat=',iat,'}=',dforce_draddens(alph,ip,l,jsp,iat)
    !                  write(*,'(A13,F15.10)') '   numerical=',dforce_dpara_num(alph)
    !                  if ( sqrt( (dforce_draddens(alph,ip,l,jsp,iat) - dforce_dpara_num(alph))**2).gt.10d-4 ) then
    !                     print *,'error, analytic and numeric derivatives do not agree'
    !                     stop
    !                  endif
    !                  rms_error=rms_error+(dforce_draddens(alph,ip,l,jsp,iat) - dforce_dpara_num(alph))**2
    !                  denom=denom+1
    !               enddo
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   print *,'rms error=',sqrt(rms_error/denom)
    !   stop
    !   !----------------------------------


    !   !---- Test dforce_dmeamtau ----
    !   print *,'Test dforce_dmeamtau'
    !   print *,'--------------------'
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   do iat=1,gn_forces(istr)
    !      do alph=1,3
    !         forces_prev(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !      enddo
    !      do jj=1,gn_neighbors(iat,istr)
    !         jat=gneighborlist(jj,iat,istr)
    !         do kk=1,gn_neighbors(jat,istr)
    !            kat=gneighborlist(kk,jat,istr)
    !            if (kat.eq.iat) then
    !               do alph=1,3
    !                  forces_prev(alph,iat)=forces_prev(alph,iat)+ &
    !                     dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !               enddo
    !               exit
    !            endif
    !         enddo
    !      enddo
    !   enddo
    !   rms_error=0d0
    !   denom=0
    !   do iat=1,gn_forces(istr)
    !      do jsp=1,maxspecies
    !         do l=1,3
    !            do alph=1,3
    !               meamtau(l,jsp)=meamtau(l,jsp)+deltaPara

    !               !Calculate force using displaced parameters
    !               call pairpotentialForce
    !               call radialdensityfunctionForce
    !               call electrondensityForce
    !               call backgrounddensityForce
    !               call embeddingfunctionForce
    !               forces(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !               do jj=1,gn_neighbors(iat,istr)
    !                  jat=gneighborlist(jj,iat,istr)
    !                  do kk=1,gn_neighbors(jat,istr)
    !                     kat=gneighborlist(kk,jat,istr)
    !                     if (kat.eq.iat) then
    !                        forces(alph,iat)=forces(alph,iat)+ &
    !                           dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !                        exit
    !                     endif
    !                  enddo
    !               enddo
    !               dforce_dmeamtau_num(alph)=(forces(alph,iat)-forces_prev(alph,iat))/deltaPara
    !               meamtau(l,jsp)=meamtau(l,jsp)-deltaPara
    !               write(*,'(A17,I1,A4,I1,A6,I1,A6,I4,A2,F15.10)') 'dF_dmeamtau(alph=',alph,', l=',l,', jsp=',jsp,', iat=',iat,'}=',dforce_dmeamtau(alph,l,jsp,iat)
    !               write(*,'(A13,F15.10)') '   numerical=',dforce_dmeamtau_num(alph)
    !               if ( sqrt( (dforce_dmeamtau(alph,l,jsp,iat) - dforce_dmeamtau_num(alph))**2).gt.10d-4 ) then
    !                  print *,'error, analytic and numeric derivatives do not agree'
    !                  stop
    !               endif
    !               rms_error=rms_error+(dforce_dmeamtau(alph,l,jsp,iat) - dforce_dmeamtau_num(alph))**2
    !               denom=denom+1

    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   print *,'rms_error=',sqrt(rms_error/denom)
    !   stop
    !   !------------------------------


    !   !---- Test dforce_d{embedding function parameters} ----
    !   !including: d2meamf_dxyz_dmeame0,d2meamf_dxyz_dmeamrho0,d2meamf_dxyz_dmeamemb3,d2meamf_dxyz_dmeamemb4
    !   print *,'Test dforce_d{embedding function parameters}'
    !   print *,'--------------------------------------------'
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   do iat=1,gn_forces(istr)
    !      do alph=1,3
    !         forces_prev(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !      enddo
    !      do jj=1,gn_neighbors(iat,istr)
    !         jat=gneighborlist(jj,iat,istr)
    !         do kk=1,gn_neighbors(jat,istr)
    !            kat=gneighborlist(kk,jat,istr)
    !            if (kat.eq.iat) then
    !               do alph=1,3
    !                  forces_prev(alph,iat)=forces_prev(alph,iat)+ &
    !                     dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !               enddo
    !               exit
    !            endif
    !         enddo
    !      enddo
    !   enddo
    !   rms_error=0d0
    !   denom=0
    !   do iat=1,gn_forces(istr)
    !      do jsp=1,maxspecies
    !         do alph=1,3
    !            !Calculate force using displaced parameters
    !            !meame0:
    !            meame0(jsp)=meame0(jsp)+deltaPara
    !            call pairpotentialForce
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            forces(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !            do jj=1,gn_neighbors(iat,istr)
    !               jat=gneighborlist(jj,iat,istr)
    !               do kk=1,gn_neighbors(jat,istr)
    !                  kat=gneighborlist(kk,jat,istr)
    !                  if (kat.eq.iat) then
    !                     forces(alph,iat)=forces(alph,iat)+ &
    !                        dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !                     exit
    !                  endif
    !               enddo
    !            enddo
    !            dforce_dmeame0_num(alph)=(forces(alph,iat)-forces_prev(alph,iat))/deltaPara
    !            meame0(jsp)=meame0(jsp)-deltaPara
    !            write(*,'(A16,I1,A6,I1,A6,I4,A2,F15.10)') 'dF_dmeame0(alph=',alph,', jsp=',jsp,', iat=',iat,'}=',dforce_dmeame0(alph,jsp,iat)
    !            write(*,'(A13,F15.10)') '   numerical=',dforce_dmeame0_num(alph)
    !            if ( sqrt( (dforce_dmeame0(alph,jsp,iat) - dforce_dmeame0_num(alph))**2).gt.10d-4 ) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !            rms_error=rms_error+(dforce_dmeame0(alph,jsp,iat) - dforce_dmeame0_num(alph))**2
    !            denom=denom+1
    !            !meamrho0:
    !            meamrho0(jsp)=meamrho0(jsp)+deltaPara
    !            call pairpotentialForce
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            forces(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !            do jj=1,gn_neighbors(iat,istr)
    !               jat=gneighborlist(jj,iat,istr)
    !               do kk=1,gn_neighbors(jat,istr)
    !                  kat=gneighborlist(kk,jat,istr)
    !                  if (kat.eq.iat) then
    !                     forces(alph,iat)=forces(alph,iat)+ &
    !                        dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !                     exit
    !                  endif
    !               enddo
    !            enddo
    !            dforce_dmeamrho0_num(alph)=(forces(alph,iat)-forces_prev(alph,iat))/deltaPara
    !            meamrho0(jsp)=meamrho0(jsp)-deltaPara
    !            write(*,'(A18,I1,A6,I1,A6,I4,A2,F25.15)') 'dF_dmeamrho0(alph=',alph,', jsp=',jsp,', iat=',iat,'}=',dforce_dmeamrho0(alph,jsp,iat)
    !            write(*,'(A13,F25.15)') '   numerical=',dforce_dmeamrho0_num(alph)
    !            if ( sqrt( (dforce_dmeamrho0(alph,jsp,iat) - dforce_dmeamrho0_num(alph))**2).gt.10d-4 ) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !            rms_error=rms_error+(dforce_dmeamrho0(alph,jsp,iat) - dforce_dmeamrho0_num(alph))**2
    !            denom=denom+1
    !            !meamemb3:
    !            meamemb3(jsp)=meamemb3(jsp)+deltaPara
    !            call pairpotentialForce
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            forces(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !            do jj=1,gn_neighbors(iat,istr)
    !               jat=gneighborlist(jj,iat,istr)
    !               do kk=1,gn_neighbors(jat,istr)
    !                  kat=gneighborlist(kk,jat,istr)
    !                  if (kat.eq.iat) then
    !                     forces(alph,iat)=forces(alph,iat)+ &
    !                        dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !                     exit
    !                  endif
    !               enddo
    !            enddo
    !            dforce_dmeamemb3_num(alph)=(forces(alph,iat)-forces_prev(alph,iat))/deltaPara
    !            meamemb3(jsp)=meamemb3(jsp)-deltaPara
    !            write(*,'(A18,I1,A6,I1,A6,I4,A2,F25.15)') 'dF_dmeamemb3(alph=',alph,', jsp=',jsp,', iat=',iat,'}=',dforce_dmeamemb3(alph,jsp,iat)
    !            write(*,'(A13,F25.15)') '   numerical=',dforce_dmeamemb3_num(alph)
    !            if ( sqrt( (dforce_dmeamemb3(alph,jsp,iat) - dforce_dmeamemb3_num(alph))**2).gt.10d-4 ) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !            rms_error=rms_error+(dforce_dmeamemb3(alph,jsp,iat) - dforce_dmeamemb3_num(alph))**2
    !            denom=denom+1
    !            !meamemb4:
    !            meamemb4(jsp)=meamemb4(jsp)+deltaPara
    !            call pairpotentialForce
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            forces(alph,iat)=dmeam_paire_dxyz(alph,0,iat)+dmeamf_dxyz(alph,0,iat)
    !            do jj=1,gn_neighbors(iat,istr)
    !               jat=gneighborlist(jj,iat,istr)
    !               do kk=1,gn_neighbors(jat,istr)
    !                  kat=gneighborlist(kk,jat,istr)
    !                  if (kat.eq.iat) then
    !                     forces(alph,iat)=forces(alph,iat)+ &
    !                        dmeam_paire_dxyz(alph,kk,jat)+dmeamf_dxyz(alph,kk,jat)
    !                     exit
    !                  endif
    !               enddo
    !            enddo
    !            dforce_dmeamemb4_num(alph)=(forces(alph,iat)-forces_prev(alph,iat))/deltaPara
    !            meamemb4(jsp)=meamemb4(jsp)-deltaPara
    !            write(*,'(A18,I1,A6,I1,A6,I4,A2,F25.15)') 'dF_dmeamemb4(alph=',alph,', jsp=',jsp,', iat=',iat,'}=',dforce_dmeamemb4(alph,jsp,iat)
    !            write(*,'(A13,F25.15)') '   numerical=',dforce_dmeamemb4_num(alph)
    !            if ( sqrt( (dforce_dmeamemb4(alph,jsp,iat) - dforce_dmeamemb4_num(alph))**2).gt.10d-4 ) then
    !               print *,'error, analytic and numeric derivatives do not agree'
    !               stop
    !            endif
    !            rms_error=rms_error+(dforce_dmeamemb4(alph,jsp,iat) - dforce_dmeamemb4_num(alph))**2
    !            denom=denom+1
    !         enddo
    !      enddo
    !   enddo
    !   print *,'rms_error=',sqrt(rms_error/denom)
    !   stop
    !--------------------------------------------------------------


    !                                         /------------------\
    !                                         |   Stress tensor  |
    !                                         \------------------/


    !   !---- Test stress tensor calculated from analytic derivative forces ----
    !   !     by comparing against finite difference approach
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   call calcStressTensor
    !   print *,'stressTensor(1:9), analytic=',stressTensor(1:9)
    !   stressTensorNum=0d0
    !   do iatom=1,gn_forces(istr)
    !      !The radial densities between atoms i and j (where i.ne.iatom and
    !      !j.ne.iatom) need only be calculated once. For simplicity,
    !      !calculate the radial densities for all atoms, and then later
    !      !when we move atom iatom, just recalculate the radial densities
    !      !involving atom iatom
    !      call radialdensityfunctionForce
    !      !Evaluate meam energy for structure 'istr' with atoms in undisplaced positions
    !      call energyForNumForce(iatom,energyTmp,Eperatom)

    !      !Displace atom 'iatom' in the x-direction to evaluate force and stress tensor
    !      gxyz(1,iatom,istr)=gxyz(1,iatom,istr)+dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      call energyForNumForce(iatom,energyTmp2,Eperatom2)
    !      gxyz(1,iatom,istr)=gxyz(1,iatom,istr)-dist !Return the atom to it's
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      call calcStressTensorNum(iatom,Eperatom,Eperatom2,dist,stressTensorTmp)
    !      do i=1,3
    !         stressTensorNum(i)=stressTensorNum(i)+stressTensorTmp(i)
    !      enddo

    !      !y component
    !      gxyz(2,iatom,istr)=gxyz(2,iatom,istr)+dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      call energyForNumForce(iatom,energyTmp2,Eperatom2)
    !      gxyz(2,iatom,istr)=gxyz(2,iatom,istr)-dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      endif
    !      call calcStressTensorNum(iatom,Eperatom,Eperatom2,dist,stressTensorTmp)
    !      do i=1,3
    !         stressTensorNum(i+3)=stressTensorNum(i+3)+stressTensorTmp(i)
    !      enddo

    !      !z component
    !      gxyz(3,iatom,istr)=gxyz(3,iatom,istr)+dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      call energyForNumForce(iatom,energyTmp2,Eperatom2)
    !      gxyz(3,iatom,istr)=gxyz(3,iatom,istr)-dist
    !      call setupnntables
    !      istr=1 !need to reset to one because setupnntables used in
    !      endif
    !      call calcStressTensorNum(iatom,Eperatom,Eperatom2,dist,stressTensorTmp)
    !      do i=1,3
    !         stressTensorNum(i+6)=stressTensorNum(i+6)+stressTensorTmp(i)
    !      enddo
    !   enddo
    !   print *,'stressTensor(1:9), numeric=',stressTensorNum(1:9)
    !   !---------------------------------------------------------------------
    !   !stop


    !   !---- Test dstresstensor_dpairpot ----
    !   rms_error=0d0
    !   denom=0
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   call calcStressTensor
    !   stressTensor_prev=stressTensor
    !   if (allocated(dstressTensor_dpairpot_prev)) deallocate(dstressTensor_dpairpot_prev)
    !   allocate(dstressTensor_dpairpot_prev(9,32,maxspecies,maxspecies))
    !   dstressTensor_dpairpot_prev=dstressTensor_dpairpot
    !   do isp=1,maxspecies
    !      do jsp=isp,maxspecies
    !         do iiCoeff=1,noptpairpotCoeff(isp,jsp)
    !            ip = ioptpairpotCoeff(iiCoeff,isp,jsp)
    !            pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)+deltaPara
    !            call pairpotentialForce
    !            call calcStressTensor
    !            pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)-deltaPara
    !            do alph=1,9
    !               print *,'dstressTensor_dpairpot(',alph,',',ip,',',isp,',',jsp,')^num = ',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !               if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !                  dstressTensor_dpairpot_prev(alph,ip,isp,jsp) )**2 ).gt.5d-3 ) then
    !                  print *,'Error, disagree, analytic=',dstressTensor_dpairpot_prev(alph,ip,isp,jsp)
    !               endif
    !               rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !                  dstressTensor_dpairpot_prev(alph,ip,isp,jsp) )**2
    !               denom=denom+1
    !            enddo
    !         enddo
    !         do iiCutoff=1,noptpairpotCutoff(isp,jsp)
    !            ip = ioptpairpotCutoff(iiCutoff,isp,jsp)
    !            pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)+deltaPara
    !            call pairpotentialForce
    !            call calcStressTensor
    !            pairpotparameter(ip,isp,jsp)=pairpotparameter(ip,isp,jsp)-deltaPara
    !            do alph=1,9
    !               print *,'dstressTensor_dpairpot(',alph,',',ip,',',isp,',',jsp,')^num = ',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !               if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !                  dstressTensor_dpairpot_prev(alph,ip,isp,jsp) )**2 ).gt.5d-3 ) then
    !                  print *,'Error, disagree, analytic=',dstressTensor_dpairpot_prev(alph,ip,isp,jsp)
    !               endif
    !               rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !                  dstressTensor_dpairpot_prev(alph,ip,isp,jsp) )**2
    !               denom=denom+1
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   print *,'rms_error=',sqrt(rms_error/denom)
    !   stop
    !   !--------------------------------------


    !   !---- Test dstresstensor_draddens ----
    !   rms_error=0d0
    !   denom=0
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   call calcStressTensor
    !   stressTensor_prev=stressTensor
    !   if (allocated(dstressTensor_draddens_prev)) deallocate(dstressTensor_draddens_prev)
    !   allocate(dstressTensor_draddens_prev(9,32,0:lmax,maxspecies))
    !   dstressTensor_draddens_prev=dstressTensor_draddens
    !   do jsp=1,maxspecies
    !      do l=0,lmax
    !         do iiCoeff=1,noptdensityCoeff(l,1,jsp)
    !            ip = ioptdensityCoeff(iiCoeff,l,1,jsp)
    !            meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            call calcStressTensor
    !            meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !            do alph=1,9
    !               print *,'dstressTensor_draddens(',alph,',',ip,',',l,',',jsp,')^num =',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !               if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !                  dstressTensor_draddens_prev(alph,ip,l,jsp) )**2 ).gt.5d-3 ) then
    !                  print *,'Error, disagree, analytic=',dstressTensor_draddens_prev(alph,ip,l,jsp)
    !               endif
    !               rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !                  dstressTensor_draddens_prev(alph,ip,l,jsp) )**2
    !               denom=denom+1
    !            enddo
    !         enddo
    !         do iiCutoff=1,noptdensityCutoff(l,1,jsp)
    !            ip = ioptdensityCutoff(iiCutoff,l,1,jsp)
    !            meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)+deltaPara
    !            call radialdensityfunctionForce
    !            call electrondensityForce
    !            call backgrounddensityForce
    !            call embeddingfunctionForce
    !            call calcStressTensor
    !            meamrhodecay(ip,l,1,jsp)=meamrhodecay(ip,l,1,jsp)-deltaPara
    !            do alph=1,9
    !               print *,'dstressTensor_draddens(',alph,',',ip,',',l,',',jsp,')^num =',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !               if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !                  dstressTensor_draddens_prev(alph,ip,l,jsp) )**2 ).gt.5d-3 ) then
    !                  print *,'Error, disagree, analytic=',dstressTensor_draddens_prev(alph,ip,l,jsp)
    !               endif
    !               rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !                  dstressTensor_draddens_prev(alph,ip,l,jsp) )**2
    !               denom=denom+1
    !            enddo
    !         enddo
    !      enddo
    !   enddo
    !   print *,'rms_error=',sqrt(rms_error/denom)
    !   stop
    !   !--------------------------------------


    !   !---- Test dstresstensor_dmeamtau ----
    !   rms_error=0d0
    !   denom=0
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   call calcStressTensor
    !   stressTensor_prev=stressTensor
    !   if (allocated(dstressTensor_dmeamtau_prev)) deallocate(dstressTensor_dmeamtau_prev)
    !   allocate(dstressTensor_dmeamtau_prev(9,1:lmax,maxspecies))
    !   dstressTensor_dmeamtau_prev=dstressTensor_dmeamtau
    !   do jsp=1,maxspecies
    !      do l=1,lmax
    !         meamtau(l,jsp)=meamtau(l,jsp)+deltaPara
    !         call radialdensityfunctionForce
    !         call electrondensityForce
    !         call backgrounddensityForce
    !         call embeddingfunctionForce
    !         call calcStressTensor
    !         meamtau(l,jsp)=meamtau(l,jsp)-deltaPara
    !         do alph=1,9
    !            print *,'dstressTensor_dmeamtau(',alph,',',l,',',jsp,')^num =',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !            if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !               dstressTensor_dmeamtau_prev(alph,l,jsp) )**2 ).gt.5d-3 ) then
    !               print *,'Error, disagree, analytic=',dstressTensor_dmeamtau_prev(alph,l,jsp)
    !            endif
    !            rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !               dstressTensor_dmeamtau_prev(alph,l,jsp) )**2
    !            denom=denom+1
    !         enddo
    !      enddo
    !   enddo
    !   print *,'rms_error=',sqrt(rms_error/denom)
    !   stop
    !   !--------------------------------------


    !   !---- Test dstresstensor_dmeam{emb} ----
    !   rms_error=0d0
    !   denom=0
    !   call pairpotentialForce
    !   call radialdensityfunctionForce
    !   call electrondensityForce
    !   call backgrounddensityForce
    !   call embeddingfunctionForce
    !   call calcStressTensor
    !   stressTensor_prev=stressTensor
    !   if (allocated(dstressTensor_dmeame0_prev)) deallocate(dstressTensor_dmeame0_prev, &
    !         dstressTensor_dmeamrho0_prev,dstressTensor_dmeamemb3_prev,dstressTensor_dmeamemb4_prev)
    !   allocate(dstressTensor_dmeame0_prev(9,maxspecies),dstressTensor_dmeamrho0_prev(9,maxspecies),&
    !         dstressTensor_dmeamemb3_prev(9,maxspecies),dstressTensor_dmeamemb4_prev(9,maxspecies))
    !   dstressTensor_dmeame0_prev=dstressTensor_dmeame0
    !   dstressTensor_dmeamrho0_prev=dstressTensor_dmeamrho0
    !   dstressTensor_dmeamemb3_prev=dstressTensor_dmeamemb3
    !   dstressTensor_dmeamemb4_prev=dstressTensor_dmeamemb4
    !   do jsp=1,maxspecies
    !      meame0(jsp)=meame0(jsp)+deltaPara
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      call embeddingfunctionForce
    !      call calcStressTensor
    !      meame0(jsp)=meame0(jsp)-deltaPara
    !      do alph=1,9
    !         print *,'dstressTensor_dmeame0(',alph,',',jsp,')^num =',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !         if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !            dstressTensor_dmeame0_prev(alph,jsp) )**2 ).gt.5d-3 ) then
    !            print *,'Error, disagree, analytic=',dstressTensor_dmeame0_prev(alph,jsp)
    !         endif
    !         rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !            dstressTensor_dmeame0_prev(alph,jsp) )**2
    !         denom=denom+1
    !      enddo
    !      meamrho0(jsp)=meamrho0(jsp)+deltaPara
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      call embeddingfunctionForce
    !      call calcStressTensor
    !      meamrho0(jsp)=meamrho0(jsp)-deltaPara
    !      do alph=1,9
    !         print *,'dstressTensor_dmeamrho0(',alph,',',jsp,')^num=',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !         if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !            dstressTensor_dmeamrho0_prev(alph,jsp) )**2 ).gt.5d-3 ) then
    !            print *,'Error, disagree, analytic=',dstressTensor_dmeamrho0_prev(alph,jsp)
    !         endif
    !         rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !            dstressTensor_dmeamrho0_prev(alph,jsp) )**2
    !         denom=denom+1
    !      enddo
    !      meamemb3(jsp)=meamemb3(jsp)+deltaPara
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      call embeddingfunctionForce
    !      call calcStressTensor
    !      meamemb3(jsp)=meamemb3(jsp)-deltaPara
    !      do alph=1,9
    !         print *,'dstressTensor_dmeamemb3(',alph,',',jsp,')^num=',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !         if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !            dstressTensor_dmeamemb3_prev(alph,jsp) )**2 ).gt.5d-3 ) then
    !            print *,'Error, disagree, analytic=',dstressTensor_dmeamemb3_prev(alph,jsp)
    !         endif
    !         rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !            dstressTensor_dmeamemb3_prev(alph,jsp) )**2
    !         denom=denom+1
    !      enddo
    !      meamemb4(jsp)=meamemb4(jsp)+deltaPara
    !      call radialdensityfunctionForce
    !      call electrondensityForce
    !      call backgrounddensityForce
    !      call embeddingfunctionForce
    !      call calcStressTensor
    !      meamemb4(jsp)=meamemb4(jsp)-deltaPara
    !      do alph=1,9
    !         print *,'dstressTensor_dmeamemb4(',alph,',',jsp,')^num=',(stressTensor(alph) - stressTensor_prev(alph))/deltaPara
    !         if ( sqrt(( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !            dstressTensor_dmeamemb4_prev(alph,jsp) )**2 ).gt.5d-3 ) then
    !            print *,'Error, disagree, analytic=',dstressTensor_dmeamemb4_prev(alph,jsp)
    !         endif
    !         rms_error=rms_error+( (stressTensor(alph) - stressTensor_prev(alph))/deltaPara - &
    !            dstressTensor_dmeamemb4_prev(alph,jsp) )**2
    !         denom=denom+1
    !      enddo
    !   enddo
    !   print *,'rms_error=',sqrt(rms_error/denom)
    !   stop

    !print *,'stressTensor=',stressTensor
    !stop

    !end subroutine meamforce


