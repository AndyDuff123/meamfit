subroutine initParaLimits

    !--------------------------------------------------------------c
    !
    !     Use values of 'maxspecies' and 'lmax' to set up the    
    !     variables to be used to reference the potential parameter
    !     array, p().
    !
    !     Called by:     readsettings
    !     Calls:         -
    !     Returns:       lm1, m1, m2, m3, m4
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_filenames
    use m_meamparameters
    use m_atomproperties

    implicit none

    lm1=lmax+1
    m1=maxspecies
    m2=m1*maxspecies
    m3=m2*maxspecies
    m4=2*m3+(4+lmax)*m1+32*m2+12*lm1*m2

end subroutine initParaLimits
