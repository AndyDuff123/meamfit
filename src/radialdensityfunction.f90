subroutine radialdensityfunction

    !----------------------------------------------------------------c
    !
    !     Fills the module m_radialdensity with the f_j_L_ij
    !     parameters. NOTE: I use f_j_L_ij = rhoatnucleus_j *
    !     exp( -decay(i,j,L)*r_ij )
    !
    !     Called by:     meamenergy
    !     Calls:         distance,radialcutofffunction
    !     Arguments:     istr,gn_inequivalentsites,gn_neighbors,
    !                 lmax,gspecies,gn_neighbors,gneighborlist,rs,
    !                 rc,meamrhodecay
    !     Returns:       fjlij
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff and Marcel Sluiter 2006-2016
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------c

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity   !f_j_L_ij

    implicit none

    integer i,j,jj,nni,iaux(1),isp,jsp,n,o

    !  if(allocated(fjlij)) deallocate(fjlij,dfjlij_dpara)
    !  iaux=maxval(gn_neighbors(1:gn_inequivalentsites(istr),istr))
    !  allocate(fjlij(0:lmax,iaux(1),gn_inequivalentsites(istr)), &
    !           dfjlij_dpara(12,0:lmax,iaux(1),gn_inequivalentsites(istr))  )

    do i=1,gn_inequivalentsites(istr)
        isp=1
        if (thiaccptindepndt.eqv..false.) isp=gspecies(i,istr)
        nni=gn_neighbors(i,istr)

        do jj=1,nni
            j=gneighborlist(jj,i,istr)
            jsp=gspecies(j,istr)
            rij=diststr(jj,i,0,0,istr)
            !            rij=distance(i,j)
            !           if (rij.ne.diststr(jj,i,0,0,istr)) then
            !              print *,'rij does not match the stored value'
            !              print *,'istr=',istr,', i=',i,',j=',j
            !              print *,'rij calc=',rij,', stored=',
            !     +    diststr(jj,i,0,0,istr)
            !              stop
            !           endif
            call raddens(rij,isp,jsp,fjlij(0:lmax,jj,i),dfjlij_dpara(1:12,0:lmax,jj,i))
           !if ((i.eq.1).and.(jj.le.10)) then
           !print *,'i=',i,', jj=',jj,',dfjlij_dpara(1:4,l=0)=',dfjlij_dpara(1:4,0,jj,i)
           !print *,'                   dfjlij_dpara(1:4,l=1)=',dfjlij_dpara(1:4,1,jj,i)
           !print *,'                   dfjlij_dpara(1:4,l=1)=',dfjlij_dpara(1:4,2,jj,i)
           !print *,'                   dfjlij_dpara(1:4,l=1)=',dfjlij_dpara(1:4,3,jj,i)
           !endif
        enddo

    enddo


    ! print *,'numerical gradients'
    !     do i=1,gn_inequivalentsites(istr)
    !     isp=1
    !     if (thiaccptindepndt.eqv..false.) isp=gspecies(i,istr)
    !     nni=gn_neighbors(i,istr)

    !     do jj=1,nni
    !         j=gneighborlist(jj,i,istr)
    !         jsp=gspecies(j,istr)
    !         rij=diststr(jj,i,0,0,istr)
    !         !            rij=distance(i,j)
    !         !           if (rij.ne.diststr(jj,i,0,0,istr)) then
    !         !              print *,'rij does not match the stored value'
    !         !              print *,'istr=',istr,', i=',i,',j=',j
    !         !              print *,'rij calc=',rij,', stored=',
    !         !     +    diststr(jj,i,0,0,istr)
    !         !              stop
    !         !           endif
    !         call raddens(rij,isp,jsp,fjlij(0:lmax,jj,i),dfjlij_dpara(1:12,0:lmax,jj,i))
    !         do o=0,3
    !            do k=1,4
    !               meamrhodecay(k,o,isp,jsp)=meamrhodecay(k,o,isp,jsp)+0.000001d0
    !               call raddens(rij,isp,jsp,fjlij2(0:lmax,jj,i),dfjlij_dpara(1:12,0:lmax,jj,i))
    !               meamrhodecay(k,o,isp,jsp)=meamrhodecay(k,o,isp,jsp)-0.000001d0
    !               if ((i.eq.1).and.(jj.le.10)) then
    !                  print *,'dfjlij_dpara(',k,',',o,',',jj,',',i,')=',(fjlij2(o,jj,i)-fjlij(o,jj,i))/0.000001d0
    !               endif
    !            enddo
    !         enddo

    !     enddo

    ! enddo

    ! stop

end subroutine radialdensityfunction



subroutine radialdensityfunctionSpline

    !----------------------------------------------------------------c
    !
    !     Fills the module m_radialdensity with the f_j_L_ij
    !     parameters. NOTE: I use f_j_L_ij = rhoatnucleus_j *
    !     exp( -decay(i,j,L)*r_ij )
    !
    !     Called by:     meamenergy
    !     Calls:         distance,radialcutofffunction
    !     Arguments:     istr,gn_inequivalentsites,gn_neighbors,
    !                 lmax,gspecies,gn_neighbors,gneighborlist,rs,
    !                 rc,meamrhodecay
    !     Returns:       fjlij
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff and Marcel Sluiter 2006-2016
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------c

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity   !f_j_L_ij

    implicit none

    integer iat,j,jj,iaux(1),isp,jsp,n,o

    do iat=1,gn_inequivalentsites(istr)

        isp=1
        if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
        do jj=1,gn_neighbors(iat,istr)
            j=gneighborlist(jj,iat,istr)
            jsp=gspecies(j,istr)
            rij=diststr(jj,iat,0,0,istr)
            if (useSpline1PartFns(jsp).eqv..false.) then
               call raddens(rij,isp,jsp,fjlij(0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat))
            else
               call raddensSpline(rij,isp,jsp,fjlij(0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat))
               ! !Test:
               ! print *,'fjlij(0:lmax,jj,iat)=',fjlij(0:lmax,jj,iat),' (numerical)'
               ! call raddens(rij,isp,jsp,fjlij(0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat))
               ! print *,'fjlij(0:lmax,jj,iat)=',fjlij(0:lmax,jj,iat),'(analytic)'
               ! stop
            endif

        enddo

    enddo

end subroutine radialdensityfunctionSpline



subroutine radialdensityfunctionForce

    !     Copyright (c) 2018, STFC

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity   !f_j_L_ij

    implicit none

    integer i,j,jj,nni,iaux(1),isp,jsp,iat,jat
    real(8) distComp(1:3)

    !  if(allocated(fjlij)) deallocate(fjlij)
    !  if(allocated(dfjlij_dxyz)) deallocate(dfjlij_dxyz)
    !  if(allocated(dfjlij_dpara)) deallocate(dfjlij_dpara)
    !  if(allocated(d2fjlij_dxyz_dpara)) deallocate(d2fjlij_dxyz_dpara)
    !  iaux=maxval(gn_neighbors(1:gn_inequivalentsites(istr),istr))
    !  allocate( fjlij(0:lmax,iaux(1),gn_inequivalentsites(istr)), &
    !       dfjlij_dxyz(1:3,0:lmax,iaux(1),gn_inequivalentsites(istr))  , & 
    !       dfjlij_dpara(12,0:lmax,iaux(1),gn_inequivalentsites(istr)), &
    !       d2fjlij_dxyz_dpara(1:3,12,0:lmax,iaux(1),gn_inequivalentsites(istr))  )

    dfjlij_dxyz=0d0
    d2fjlij_dxyz_dpara=0d0

    do iat=1,gn_inequivalentsites(istr)
          !print *,'iat=',iat
       isp=1
       if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)

       do jj=1,gn_neighbors(iat,istr)
             !print *,'jj=',jj
          jat=gneighborlist(jj,iat,istr)
          jsp=gspecies(jat,istr)

          rij=diststr(jj,iat,0,0,istr)
          !Change the dx, dy etc later to come from one array with 1:3 index
          distComp(1)=dxstr(jj,iat,0,0,istr)
          distComp(2)=dystr(jj,iat,0,0,istr)
          distComp(3)=dzstr(jj,iat,0,0,istr) 

          call raddensForce(rij,distComp,isp,jsp,fjlij(0:lmax,jj,iat),dfjlij_dxyz(1:3,0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat),d2fjlij_dxyz_dpara(1:3,1:12,0:lmax,jj,iat))
          !print *,'dfjlij_dxyz(1:3,0:lmax,',jj,',',iat,')=',dfjlij_dxyz(1:3,0:lmax,jj,iat)
       enddo

    enddo
    
end subroutine radialdensityfunctionForce


subroutine radialdensityfunctionForceSpline

    !     Copyright (c) 2018, STFC

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity   !f_j_L_ij

    implicit none

    integer i,j,jj,nni,iaux(1),isp,jsp,iat,jat
    real(8) distComp(1:3)

    dfjlij_dxyz=0d0
    d2fjlij_dxyz_dpara=0d0

    do iat=1,gn_inequivalentsites(istr)
          !print *,'iat=',iat
       isp=1
       if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)

       do jj=1,gn_neighbors(iat,istr)
             !print *,'jj=',jj
          jat=gneighborlist(jj,iat,istr)
          jsp=gspecies(jat,istr)

          rij=diststr(jj,iat,0,0,istr)
          !Change the dx, dy etc later to come from one array with 1:3 index
          distComp(1)=dxstr(jj,iat,0,0,istr)
          distComp(2)=dystr(jj,iat,0,0,istr)
          distComp(3)=dzstr(jj,iat,0,0,istr) 

          if (useSpline1PartFns(jsp).eqv..false.) then
            call raddensForce(rij,distComp,isp,jsp,fjlij(0:lmax,jj,iat),dfjlij_dxyz(1:3,0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat),d2fjlij_dxyz_dpara(1:3,1:12,0:lmax,jj,iat))
          else
             call raddensForceSpline(rij,distComp,isp,jsp,fjlij(0:lmax,jj,iat),dfjlij_dxyz(1:3,0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat),d2fjlij_dxyz_dpara(1:3,1:12,0:lmax,jj,iat))
             ! !Test:
             ! print *,'fjlij(0:lmax,jj,iat)=',fjlij(0:lmax,jj,iat),', dfjlij_dxyz(1:3,0:lmax,jj,iat)=',dfjlij_dxyz(1:3,0:lmax,jj,iat),' (numerical)'
             ! call raddensForce(rij,distComp,isp,jsp,fjlij(0:lmax,jj,iat),dfjlij_dxyz(1:3,0:lmax,jj,iat),dfjlij_dpara(1:12,0:lmax,jj,iat),d2fjlij_dxyz_dpara(1:3,1:12,0:lmax,jj,iat))
             ! print *,'fjlij(0:lmax,jj,iat)=',fjlij(0:lmax,jj,iat),', dfjlij_dxyz(1:3,0:lmax,jj,iat)=',dfjlij_dxyz(1:3,0:lmax,jj,iat),'(analytic)'
             ! stop
          endif

       enddo

    enddo
    
end subroutine radialdensityfunctionForceSpline

