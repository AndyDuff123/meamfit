subroutine getlargestbkgrnddens

    !----------------------------------------------------------------------c
    !
    !     Determines the largest value of background density (rho)
    !     encountered across all structures.
    !
    !     Called by:     optimizeparameters, fit_quality
    !     Returns:       largestrho
    !     Files read:    -
    !     Files written: -
    !
    !     Andy Duff, Oct 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c
 

    use m_electrondensity
    use m_geometry
    use m_plotfiles

    implicit none

    integer i

    largestrho=0d0
    do i=1,gn_inequivalentsites(istr)
        largestrho=MAX(largestrho,rho_i(i))
    enddo

end subroutine getlargestbkgrnddens
