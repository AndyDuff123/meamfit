function pairpotAcklandFeFe(distance)

    !     Copyright (c) 2018, STFC

    implicit none

    real(8) distance,pairpotAcklandFeFe,cutoff

    if (distance.lt.1d0) then
        pairpotAcklandFeFe=( 9734.2365892908d0 / distance ) * &
            ( 0.18180d0*exp(-2.8616724320005d1*distance) & !E+01
            + 0.50990d0*exp(-8.4267310396064d0*distance) &
            + 0.28020d0*exp(-3.6030244464156d0*distance) &
            + 0.02817d0*exp(-1.8028536321603d0*distance) )
    else
        pairpotAcklandFeFe=exp( 7.4122709384068d0 - &
            0.64180690713367d0*distance - &
            2.6043547961722d0*(distance**2) + &
            0.6262539393123d0*(distance**3) ) * &
            cutoff(distance,1d0,2.05d0) &
            - 27.444805994228d0 * ((2.2d0 - distance) **3) * &
            cutoff(distance,2.05d0,2.2d0) &
            + 15.738054058489d0 * ((2.3d0 - distance) **3) * &
            cutoff(distance,2.05d0,2.3d0) &
            + 2.2077118733936d0 * ((2.4d0 - distance) **3) * &
            cutoff(distance,2.05d0,2.4d0) &
            - 2.4989799053251d0 * ((2.5d0 - distance) **3) * &
            cutoff(distance,2.05d0,2.5d0) &
            + 4.2099676494795d0 * ((2.6d0 - distance) **3) * &
            cutoff(distance,2.05d0,2.6d0) &
            - 0.77361294129713d0 * ((2.7d0 - distance) **3) * &
            cutoff(distance,2.05d0,2.7d0) &
            + 0.80656414937789d0  * ((2.8d0 - distance) **3) * &
            cutoff(distance,2.05d0,2.8d0) &
            - 2.3194358924605d0  * ((3.d0 - distance) **3) * &
            cutoff(distance,2.05d0,3.d0) &
            + 2.6577406128280d0  * ((3.3d0 - distance) **3) * &
            cutoff(distance,2.05d0,3.3d0) &
            - 1.0260416933564d0  * ((3.7d0 - distance) **3) * &
            cutoff(distance,2.05d0,3.7d0) &
            + 0.35018615891957d0  * ((4.2d0 - distance) **3) * &
            cutoff(distance,2.05d0,4.2d0) &
            - 0.058531821042271d0  * ((4.7d0 - distance) **3)* &
            cutoff(distance,2.05d0,4.7d0) &
            - 0.0030458824556234d0  * &
            ((5.3d0 - distance) **3)* &
            cutoff(distance,2.05d0,5.3d0)
    endif

end function pairpotAcklandFeFe
