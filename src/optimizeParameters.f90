subroutine optimizeParameters(nfreep)

    !----------------------------------------------------------------------c
    !
    !      Main optimization loop: randomly seed MEAM parameters; optimize
    !      using conjugate-gradient; use genetic algortihm to seed new
    !      starting points; repeat until optimization function small enough.
    !
    !      Andrew Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_datapoints
    use m_filenames
    use m_optimization
    use m_generalinfo
    use m_atomproperties
    use m_plotfiles
    use m_geometry
    use m_meamparameters
    use m_objectiveFunction

    implicit none

    logical exitRandomSampling
    integer i,j,k,ip,ipara,iter,nfreep,nf, &
        timeLocOptStart,timeLocOptFinish,timeLocOptFull, &
        hrs,mins,secs,nGuesses_Tot,nGuesses_Suc
    real(8) func,timeLocOpt,timeFullCG,timePhase2,lowestoptfunc
    real(8), allocatable:: popt(:)

    !Alocate arrays for objective function analytic derivatives
    allocate(dF_dmeamtau(1:lmax,maxspecies))
    allocate(dF_draddens(12,0:lmax,maxspecies))
    allocate(dF_dmeame0(maxspecies),dF_dmeamrho0(maxspecies), &
        dF_dmeamemb3(maxspecies),dF_dmeamemb4(maxspecies))
    allocate(dF_dpairpot(32,maxspecies,maxspecies))
    allocate(dF_denconst(maxspecies))
    allocate(dF_dpara(np))
    !Also for objective function components
    allocate(dFen_dmeamtau(1:lmax,maxspecies))
    allocate(dFen_draddens(12,0:lmax,maxspecies))
    allocate(dFen_dmeame0(maxspecies),dFen_dmeamrho0(maxspecies), &
        dFen_dmeamemb3(maxspecies),dFen_dmeamemb4(maxspecies))
    allocate(dFen_dpairpot(32,maxspecies,maxspecies))
    allocate(dFen_denconst(maxspecies))
    if (objFuncType.eq.2) then
       !The following are for Tom's objective function
       allocate(dFen2_dmeamtau(1:lmax,maxspecies))
       allocate(dFen2_draddens(12,0:lmax,maxspecies))
       allocate(dFen2_dmeame0(maxspecies),dFen2_dmeamrho0(maxspecies), &
           dFen2_dmeamemb3(maxspecies),dFen2_dmeamemb4(maxspecies))
       allocate(dFen2_dpairpot(32,maxspecies,maxspecies))
    endif

    if (lmax.gt.0) then
       allocate(dFfrc_dmeamtau(1:lmax,maxspecies))
    endif
    allocate(dFfrc_draddens(12,0:lmax,maxspecies))
    allocate(dFfrc_dmeame0(maxspecies),dFfrc_dmeamrho0(maxspecies), &
        dFfrc_dmeamemb3(maxspecies),dFfrc_dmeamemb4(maxspecies))
    allocate(dFfrc_dpairpot(32,maxspecies,maxspecies))
    if (lmax.gt.0) then
       allocate(dFstr_dmeamtau(1:lmax,maxspecies))
    endif
    allocate(dFstr_draddens(12,0:lmax,maxspecies))
    allocate(dFstr_dmeame0(maxspecies),dFstr_dmeamrho0(maxspecies), &
        dFstr_dmeamemb3(maxspecies),dFstr_dmeamemb4(maxspecies))
    allocate(dFstr_dpairpot(32,maxspecies,maxspecies))
    allocate(dFcutoffPen_draddens(12,0:lmax,maxspecies), &
        dFcutoffPen_dpairpot(32,maxspecies,maxspecies)) !, &
        !dFlowDensPen_draddens(12,0:lmax,maxspecies))

    allocate(positivep(np),p_orig(np),p_best(np))
    if (contjob.eqv..false.) then
       allocate(p_saved(np,noptfuncstore))
    endif

    !Set up positivep array (determines those parameters which must remain positive)
    call setuppositivep
    if (holdcutoffs.and.(noOpt.eqv..false.)) then
      !Adjust freep values to hold cutoffs fixed for first part of optimization
      call holdcutoffparas
    endif
    allocate(popt(nfreep))
    !Set up the arrays controlling which analytic derivatives are to be calculated
    call initialize_noptp
    open(62,file='ObjFuncValsLocOpt.dat')

    nextHourRec=outputVsTime !The next hour that we want to record parameters etc.
    open(64,file='bestoptfuncsVsTime')
    write(64,*) 'Hours | objective function'

    !Main optimization loop
    do

       !---- Random sampling phase ----
       if (randgenparas) then

          !Select a starting point: to begin with using selective random sampling; once
          !n_optfunc potentials have been optimized by using the genetic algorithm
          lowestoptfunc=100000d0
          optcomplete=.false.
          adjustedStopTime=.false.
          write(*,'(A37)') ' Random potential parameters sampling'
          nGuesses_Tot=0
          nGuesses_Suc=0 ! Those guesses which do not result in NaN, etc (see below)
          do
             call generaterandomstartparas !Randomly initialize starting potential parameters
             nGuesses_Tot=nGuesses_Tot+1
             if (nGuesses_Tot.gt.1000000) then
                print *,' > 1000000 random parameter sets sampled without achieving ',nGuesses_Rnd,'/',nGuesses_GA,' finite valued objective functions, STOPPING'
                stop
             endif
             if ((n_optfunc.gt.noptfuncstore).and.(genAlgo)) then
                !Once we have full set of parameters, start genetic algorithm:
                !print *,'Mix potential from existing potentials; with mutations.'
                call mixPotential
             endif

             call objectiveFunction(.false.)

             !debug code to check that F=NaN has been caught earlier
             if ((F.ge.0d0).or.(F.lt.0d0)) then
                !all is well
             else
                print *,'how can F be NaN here?!'
                stop
             endif

             ! Check there are no numerical problems with F, or out-of-bounds issues
             exitRandomSampling=.false.
             if ((finiteF.eqv..true.).and.((negElecDens.eqv..false.).or.(negElecDensAllow.eqv..true.)) &
                 .and.(gammaOutOfBounds.eqv..false.).and.(negBackDens.eqv..false.)) then
                nGuesses_Suc=nGuesses_Suc+1
                if (n_optfunc.le.noptfuncstore) then
                  ! Random sampling phase
                  write(*,'(A58,G20.12,A17,G20.12,A2,G20.12,A1)') &
                    ' Random potential parameters sampling... (current optfunc:', &
                    F,', lowest optfunc:',lowestoptfunc,')'
                  ! Once we have clearer the requisite number of minimum guesses, the next set of 'best' parameters is taken as
                  ! the starting point for local minimization
                  if ( ( nGuesses_Suc.gt.nGuesses_Rnd ).and. (lowestoptfunc.lt.100000d0) ) then
                     exitRandomSampling=.true.
                  endif
                else
                  ! Genetic algortihm phase
                  write(*,'(A58,G20.12,A17,G20.12,A2,G20.12,A1)') &
                    ' Random potential parameters sampling... (current optfunc:', &
                    F,', lowest optfunc:',lowestoptfunc,')'
                  if ( ( nGuesses_Suc.gt.nGuesses_GA ).and. (lowestoptfunc.lt.100000d0 ) ) then
                     exitRandomSampling=.true.
                  endif
                endif
                if (F.lt.lowestoptfunc) then
                   lowestoptfunc=F
                   ! Save the parameters so that we can retrieve them after the random phase has completed
                   call variables_to_p
                   p_best=p
                endif
                if (exitRandomSampling.eqv..true.) then
                   exit
                endif
             else

                if (((negElecDens.eqv..true.).and.(negElecDensAllow.eqv..false.)).or.(negBackDens.eqv..true.).or.(gammaOutOfBounds.eqv..true.)) then
                   !Return to sumsl with the kill command (nf=-1), note, cutoffs out of bounds will instead try shorter step length (nf=0)
                   if (debug) then
                      print *,'Objective function evaluation encountered problem:'
                      if ((negElecDens.eqv..true.).and.(negElecDensAllow.eqv..false.)) then
                         print *,'   Negative electron density occured'
                      endif
                      if (negBackDens) then
                         print *,'   Negative background density occured'
                      endif
                      if (gammaOutOfBounds) then
                         print *,'   Gamma exceeded bounds'
                      endif
                   endif
                endif

             endif

          enddo
          print *,'Completed random sampling phase'
          print *,'(nGuesses_Suc=',nGuesses_Suc,'/nGuesses_Tot=',nGuesses_Tot,')'

       endif
       !-------------------------------


       !---- Perform optimization of objective function ----
       !  call variables_to_p !Move parameters to the p() array
       p=p_best
       call getTime(timeLocOptStart)
       Fprev=0d0
       Fprev2=0d0 ! we use these to test for stop condition for optimization, namely when the objective function decreases by less than a threshold value (1 to begin with). This test is evaluated over two subsequent iterations, I.e. we test F-Fprev and Fprev-Fprev2. The threshold decreases as difference between best and worst objective functions in score-table reduces
       if (quasiNewton.eqv..true.) then
          call quasiNewtonWrap(nfreep,iter) !quasi-Newton wrapper subroutine
       else
          call CG_Wrap(nfreep,iter) !CG wrapper subroutine
       endif
       !----------------------------------------------------

       call displaytime(6,hrs,mins,secs)

       if (finiteF.and.((negElecDens.eqv..false.).or.(negElecDensAllow.eqv..true.)).and.(cutoffOutOfBounds.eqv..false.)) then

          print *,'Optimization succeeded, F=',F
          call savePotential(nfreep,popt,hrs,mins,secs)
          !Calculate time taken for local optimization (as well as for a full optimization):
          call getTime(timeLocOptFinish)
          timeLocOpt=dble(timeLocOptFinish-timeLocOptStart)/3600d0 !Time for the last local optimization
          !Use this to compute the time needed for a full 'maxfuncevals' number of function evaluations
          timeLocOptFull=timeLocOpt*dble(maxfuncevals/iter)

       else

          print *,'Optimization failed: F cannot be evaluated'
          if (finiteF.eqv..false.) then
             print *,'   (F=NaN)'
          endif
          if ((negElecDens.eqv..true.).and.(negElecDensAllow.eqv..false.)) then
             print *,'   (negitive electron density)'
          endif
          if (cutoffOutOfBounds) then
             print *,'   (cutoff/s out of bounds)'
          endif
          if (randgenparas.eqv..false.) then
             optComplete=.false.
             print *,'No random parameter generation, so quitting.'
             return
          endif

       endif

       print *

       !Check total time stop condition:
       if (hrs.ge.stopTime) then
          print *,'Number of hours elapsed: ',hrs,' greater than STOPTIME=',stopTime
          exit
       endif
       !Check convergence stop condition:
       if (n_optfunc.gt.2) then
          if ( (n_optfunc.gt.noptfuncstore).and.(bestoptfuncs(noptfuncstore)- &
                bestoptfuncs(1)).lt.optAcc  ) then
             print *,'Best and worst optimization functions within ',optAcc
             exit
          endif
       endif

       ! !Check if deltaOlimit needs reducing
       ! if (deltaOlimit==1d0) then
       !    if ( (n_optfunc.gt.noptfuncstore).and.(bestoptfuncs(noptfuncstore)- &
       !             bestoptfuncs(1)).lt.1d0 ) then
       !       print *,'Difference between best and worst optimization functions = ',(bestoptfuncs(noptfuncstore)- &
       !             bestoptfuncs(1)),' < 1'
       !       deltaOlimit=0.001d0
       !       print *,'...reducing deltaOlimit to: ',deltaOlimit
       !    endif
       ! elseif (deltaOlimit==0.001d0) then
       !    if ( (n_optfunc.gt.noptfuncstore).and.(bestoptfuncs(noptfuncstore)- &
       !             bestoptfuncs(1)).lt.0.1d0 ) then
       !       print *,'Difference between best and worst optimization functions = ',(bestoptfuncs(noptfuncstore)- &
       !             bestoptfuncs(1)),' < 0.1'
       !       deltaOlimit=1d-10
       !       print *,'...reducing deltaOlimit to: ',deltaOlimit
       !    endif
       ! endif
       !Try a continuous scaling approach : deltaOlimit = 0.001*(wrst-best)^3
       ! if (n_optfunc.gt.noptfuncstore) then
       !    deltaOlimit=0.001d0*((bestoptfuncs(noptfuncstore)-bestoptfuncs(1))**3)
       !    print *,'obj func of worst-best potential=',bestoptfuncs(noptfuncstore)-bestoptfuncs(1)
       !    print *,'deltaOlimit=',deltaOlimit
       !    if (deltaOlimit.gt.1d0) then
       !       print *,'... > 1, limiting to deltaOlimit=1'
       !       deltaOlimit=1d0
       !    elseif (deltaOlimit.lt.1d-15) then
       !       print *,'... < 1d-15, limiting to deltaOlimit=1d-15' ! Not sure exactly what value this should be, but in practise doens't matter because 
       !       deltaOlimit=1d-15                                   ! other convergence criteria of the local optimizer will kick in before this... 
       !    endif
       ! endif
       !Try a continuous scaling approach : deltaOlimit = 0.001*(wrst-best)^3
       if (n_optfunc.gt.noptfuncstore) then
          deltaOlimit=0.3d0*(10d0**(-2.5d0/(bestoptfuncs(noptfuncstore)-bestoptfuncs(1))))
          !if (n_optfunc.gt.noptfuncstore) then
          !   deltaOlimit=(bestoptfuncs(noptfuncstore)-bestoptfuncs(1))/(10d0*dble(noptfuncstore))
          !else
          !   deltaOlimit=1d0
          !endif
          print *,'obj func of worst-best potential=',bestoptfuncs(noptfuncstore)-bestoptfuncs(1)
          print *,'deltaOlimit=',deltaOlimit
          if (deltaOlimit.gt.1d0) then
             print *,'... > 1, limiting to deltaOlimit=1'
             deltaOlimit=1d0
          elseif (deltaOlimit.lt.1d-15) then
             print *,'... < 1d-15, limiting to deltaOlimit=1d-15' ! Not sure exactly what value this should be, but in practise doens't matter because 
             deltaOlimit=1d-15                                   ! other convergence criteria of the local optimizer will kick in before this... 
          endif
       endif



       if ((iter.gt.50).and.(stopTime.gt.0).and.(adjustedStopTime.eqv..false.).and.hardStop) then

          !Adjust stopTime, if hard stop has been selected, so that the code
          !finishes _before_ the STOPTIME specified in the settings file
          print *,'Time for a full local optimization opt: ',timeLocOptFull,'hrs'

          if (holdcutoffs.eqv..false.) then
             !Time<stopTime is only checked in between local optimizations, so need to
             !subtract off the time taken for a full local opt from stopTime
             stopTime=stopTime-timeLocOptFull-5d0/60d0 !add five minutes buffer-time
          else
             !Check that there will be enough time after phase 1 to reoptimize all
             !'noptfuncstore' potentials (inc. extra local optimization and five minute
             !buffer time, as above). If not, adjust noptfuncstore.
             if (stopTime.lt.dble(2*noptfuncstore+2)*timeLocOptFull+5d0/60d0) then
                !Need to reduce noptfuncstore to ensure both phases have time to complete
                noptfuncstore=INT( ( (stopTime-5d0/60d0)/timeLocOptFull &
                                      -2 ) / 2 )
                print *,'Insufficient time to complete both phases in the holdcutoffs'
                print *,'optimization mode, therefore reducing noptfuncstore to:',noptfuncstore
             endif
             !Adjust stopTime (for phase 1) so that afterwards, there is
             !sufficient time for phase 2 to complete.
             ![ the following needs adjusting to account for the case where a full
             !set of potentials has not been accrued in phase 1 ( < noptfuncstore ) ]
             timePhase2=dble(noptfuncstore+1)*timeLocOptFull+5d0/60d0
             print *,'   -> prediction for time of phase 2: ',timePhase2,'hrs'
             stopTime=stopTime-timePhase2
             print *,'   -> new STOPTIME for phase 1:',stopTime
          endif

          adjustedStopTime=.true.

       endif

       !Since we are now going back to the random-gen/mutation phase, need to
       !make sure that we reset any of those parameters which AREN'T randomly
       !regenerated but ARE allowed to optimize (=1 in the 'parastoopt' tag).
       !(note, p_orig are initialized in the 'Wrap' subroutines) 
       do i=1,np
          p(i)=p_orig(i)
       enddo
       call p_to_variables

    enddo !Main optimization loop

    print *
    if (holdcutoffs.eqv..false.) then
       print *,'Commencing with full local optimization of best potential'
       do i=1,np
           p(i)=p_saved(i,1)
       enddo
       Fprev=0d0
       Fprev2=0d0
       maxfuncevals=10000
       deltaOlimit=1d-20 ! overly small to make sure this doesn't cause optimization to end prematurely
       if (quasiNewton.eqv..true.) then
          call quasiNewtonWrap(nfreep,iter) !quasi-Newton wrapper subroutine
       else
          call CG_Wrap(nfreep,iter) !CG wrapper subroutine
       endif
       call displaytime(6,hrs,mins,secs)
       if (finiteF.and.((negElecDens.eqv..false.).or.(negElecDensAllow.eqv..true.)).and.(cutoffOutOfBounds.eqv..false.)) then
          print *,'Optimization succeeded, F=',F
          call savePotential(nfreep,popt,hrs,mins,secs)
       else
          print *,'Optimization failed: returning to saved potential pre-optimization'
          if (finiteF.eqv..false.) then
             print *,'   (F=NaN)'
          endif
          if ((negElecDens.eqv..true.).and.(negElecDensAllow.eqv..false.)) then
             print *,'   (negitive electron density)'
          endif
          if (cutoffOutOfBounds) then
             print *,'   (cutoff/s out of bounds)'
          endif
       endif


       print *,' ::: Optimization completed :::'
       return
       ! MPI: this return statement used to be a stop statement.
       ! It was changed, because when the program exists 'p_finalize' should be called
    else

       print *,' ::: Initial stage optimization-- with fixed cutoffs--completed :::'

       call setupstage2freep !Adjust freep so that cutoffs are now also allowed to relax
       !Update nfreep (and relevant arrays) accordingly
       nfreep=0
       do i=1,np
          if ((freep(i).eq.1).or.(freep(i).eq.2)) then
             nfreep=nfreep+1
          endif
       enddo
       deallocate(popt)
       allocate(popt(nfreep))
       !Adjust the arrays controlling which analytic derivatives are to be calculated
       deallocate(optmeamtau,noptpairpotCoeff,noptpairpotCutoff, &
            ioptpairpotCoeff,ioptpairpotCutoff, &
            noptdensityCoeff,noptdensityCutoff, &
            ioptdensityCoeff,ioptdensityCutoff)
       call initialize_noptp

     ! !
     ! !
     ! maxfuncevals=5000
     ! !
     ! ! <- temporary here

       !Loop through each saved potential and re-optimize (note, the upper limit
       !is not 'noptfuncstore', since a full set of parameters may not have been
       !acquired, instead use n_optfunc-1, since n_optfunc is the index of the
       !next potential to be optimized)
       do i=1,n_optfunc-1
          print *,i,'/',n_optfunc-1,':'
          do j=1,np
             p(j)=p_saved(j,i)
          enddo
          !Now remove this potential from the list by shuffling up the rest
          if (i.lt.n_optfunc) then
             do j=i,n_optfunc-2
                 bestoptfuncs(j)=bestoptfuncs(j+1)
                 timeoptfuncHr(j)=timeoptfuncHr(j+1)
                 timeoptfuncMin(j)=timeoptfuncMin(j+1)
                 do k=1,np
                     p_saved(k,j)=p_saved(k,j+1)
                 enddo
             enddo
          endif
          n_optfunc=n_optfunc-1 !And reduce list size by one (list will expand again
                                !once we have relaxed the current potential).
          !Reoptimize the potential
          if (quasiNewton.eqv..true.) then
             call quasiNewtonWrap(nfreep,iter) !quasi-Newton wrapper subroutine
          else
             call CG_Wrap(nfreep,iter) !CG wrapper subroutine
          endif
          print *,'n_optfunc at end of loop:',n_optfunc
          call savePotential(nfreep,popt,hrs,mins,secs)
       enddo

    endif
    print *

    close(62)
    close(64)

end subroutine optimizeParameters





subroutine quasiNewtonWrap(nfreep,iter)

    use m_objectiveFunction
    use m_optimization

    implicit none

    integer i,ip,nfreep,iter

    !Variables for quasi Newton optimizer:
    integer lv
    integer uip(1)
    integer, parameter :: liv=60
    integer iv(liv)
    real(8) d_smsno(nfreep),urp(nfreep,3),popt(nfreep)
    real(8), allocatable:: vv(:)

    external objFuncWrap,objFuncDerivWrap

    allocate(dF_dpopt(nfreep),dF_dpoptStore(nfreep,100000))
    lv=71+nfreep*(nfreep+15)/2 + 500
    allocate(vv(lv))

    print *
    print *,'Optimizing parameters using quasi Newton algorithm'
    print *,'--------------------------------------------------'
    print *

    !Store initial values of parameters so they can be returned to
    !their original units once optimization is complete
    do i=1,np
       p_orig(i)=p(i)
    enddo

    !Set up 'popt', the array containing the (rescaled) potential
    !parameters for use in the quasi Newton optimizer
    ip=0
    do i=1,np
       if ((freep(i).eq.1).or.(freep(i).eq.2)) then
          ip=ip+1
          if (p_orig(i).ne.0d0) then
             popt(ip)=p(i)*(poptStrt/p_orig(i))
          else
             popt(ip)=p(i)
          endif
       endif
    enddo

    !---- Set up variables necessary for call to Davison optimizer ----
    d_smsno=1d0
    call deflt(2, iv, liv, lv, vv) !Initializes many variables automatically
    iv(1)=12 !fresh start; already read defaults (with changes below)
    !Stopping criteria
    iv(17)=maxfuncevals !max no. function evals
    iv(18)=10000 !max no. itns
    iv(7)=1d-14 !x-convergence
    iv(8)=1d-14 !relative function convergence
    iv(19)=1 !no iterations between outputing to screen
    iv(20)=0 !don't print non default values
    iv(22)=0 !don't print final x and d
    iv(23)=0 !summary statistics
    iv(24)=0 !don't print initial x and d
    vv(32) = optdiff
    vv(42) = optfunc_err  !Relative error in calculating optfunc (important for gradient calc)
    !-------------------------------------------------------------

    !---- call quasi Newton optimizer ----
    if (analyticDerivs.eqv..true.) then
       call sumsl(nfreep,d_smsno,popt,objFuncWrap,objFuncDerivWrap,iv,liv,lv,vv,uip,urp,objFuncWrap)
    else
       call smsno(nfreep,d_smsno,popt,objFuncWrap,iv,liv,lv,vv,uip,urp,objFuncWrap)
    endif
    iter=iv(6) !Number of function evaluations made
    !-------------------------------------

    !Check if optimizer succeeded. Respectively: F=NaN at 1st itn; reached
    !maxfunceval but no convergence; reached iteration limit but no convergence
    !(the first of  these can only happen for a continuation job and not for
    !random sampling, since for the minimizer to start the objective function
    !must be finite)
    if (iv(1).eq.11) then
       print *,'Quasi-Newton optimizer failed: stopped due to nf=-1 signal from objFuncWrap'
    elseif (iv(1).eq.63) then
       print *,'Quasi-Newton optimizer failed: objective function blew up on first iteration'
    elseif (iv(1).eq.9) then
       print *,'Quasi-Newton optimizer warning: reached maxfunceval but incomplete convergence'
    elseif (iv(1).eq.10) then
       print *,'Quasi-Newton optimizer warning: reached iteration limit but incomplete convergence'
    endif

    !CODE TO ADD BACK
    if (((negElecDens.eqv..true.).and.(negElecDensAllow.eqv..false.)).or.(negBackDens.eqv..true.).or.(gammaOutOfBounds.eqv..true.)) then
       print *,'   (further details from objective function evaluator:'
       if ((negElecDens.eqv..true.).and.(negElecDensAllow.eqv..false.)) then
          print *,'      Negative electron density occured)'
       endif
       if (negBackDens) then
          print *,'      Negative background density occured)'
       endif
       if (gammaOutOfBounds) then
          print *,'      Gamma exceeded bounds)'
       endif
    endif

    deallocate(dF_dpopt,dF_dpoptStore,vv)

end subroutine quasiNewtonWrap



subroutine CG_Wrap(nfreep,iter)

    use m_objectiveFunction
    use m_optimization

    implicit none

    logical signflipped(np)
    integer i,ip,ipara,nfreep,iter,nf
    !Variables for CG_PLUS:
    logical finish_CGPLUS
    integer iprint_CGPLUS(2),iflag_CGPLUS,irest_CGPLUS, &
        method_CGPLUS
    real(8) eps_CGPLUS,dF_dpopt_norm
    real(8) d_CGPLUS(nfreep),gold_CGPLUS(nfreep),w_CGPLUS(nfreep),popt(nfreep)

    allocate(dF_dpopt(nfreep))

    print *
    print *,'Optimizing parameters using conjugate gradient algorithm'
    print *,'---------------------------------------------------------'
    print *

    iprint_CGPLUS(1)=1
    iprint_CGPLUS(2)=0
    eps_CGPLUS=1d-5
    irest_CGPLUS=1
    method_CGPLUS=1
    finish_CGPLUS=.false.

    !CG outer loop to reset popt
    !(optional - I haven't decided yet if necessary)
    !do iterOuter=1,1000

       iflag_CGPLUS=0 !Signals new CG optimization

       !Store initial values of parameters so they can be returned to their
       !original units once CG optimization is complete
       do i=1,np
           p_orig(i)=p(i)
       enddo

       !Set up 'popt', the array containing the (rescaled) potential parameters
       !for
       !use in the CG optimizer
       ip=0
       do i=1,np
           if ((freep(i).eq.1).or.(freep(i).eq.2)) then
               ip=ip+1
               if (p_orig(i).ne.0d0) then
                   popt(ip)=p(i)*(poptStrt/p_orig(i))
               else
                   popt(ip)=p(i)
               endif
           endif
       enddo

       !CG inner loop to optimize objective function w.r.t popt
       iter=1
       do

          call objectiveFunction(.false.)
          if ((negElecDens.and.negElecDensAllow.eqv..false.).or.negBackDens.or.gammaOutOfBounds) then
             print *,'negative density or too small gamma  encountered - exiting CG optimization'
             exit
          endif

          !Convert derivatives of F to dF_dpara
          call dFdmeam_to_dFdpara
          !Convert dF_dpara to dF_dpopt (I.e., just w.r.t paras being optimized)
          !and compute norm of dF_dpopt
          if (np.ge.1) then
             ip=0
             dF_dpopt_norm=0d0
             do ipara=1,np
                 if ( freep(ipara).ge.1 ) then
                     ip=ip+1
                     dF_dpopt(ip)=dF_dpara(ipara)*(p_orig(ipara)/poptStrt)
                     dF_dpopt_norm=dF_dpopt_norm+dF_dpopt(ip)**2
                 endif
             enddo
          endif
          dF_dpopt_norm=sqrt(dF_dpopt_norm/dble(ip))

          call CGFAM(nfreep,popt,F,dF_dpopt,d_CGPLUS,gold_CGPLUS, &!
                     iprint_CGPLUS,eps_CGPLUS,w_CGPLUS,iflag_CGPLUS, &
                     irest_CGPLUS,method_CGPLUS,finish_CGPLUS)

          if (iflag_CGPLUS.eq.-1) then
             !CG optimization failed
             exit
          endif

          !Convert popt() to p()
          if (np.ge.1) then
             ip=0
             do ipara=1,np
                 if ( (freep(ipara).eq.1).or.(freep(ipara).eq.2) ) then
                     ip=ip+1
                     if (p_orig(ipara).ne.0d0) then
                         p(ipara)=popt(ip)*(p_orig(ipara)/poptStrt)
                     else
                         p(ipara)=popt(ip)
                     endif
                 endif
             enddo
          endif
          !Ensure meam parameters have the correct signs and are in the correct
          !range
          call parachange1(signflipped,nf)
          !  call setupsplines !Prepare splines for pair-potentials and
          !                    !radial densities
          !Convert variables in p() to the descriptive-named variables
          !(pairpotparameter, etc), as used by meamenergy and meamforce
          !subroutines
          call p_to_variables
          !print *,'p=',p
          !print *,'positivep=',positivep

          iter=iter+1
          !Exit loop only when CGPLUS completes one of its own 'iterations'
          !print *,'iter=',iter,' iflag=',iflag_CGPLUS
          if ((iter.ge.maxfuncevals).and.(iflag_CGPLUS.eq.2)) then
             exit
          endif
       enddo

       if (iflag_CGPLUS.eq.-1) then
          print *,'CG optimization failed.'
          print *
          print *,'  F=',F,' dF_dpopt_norm=',dF_dpopt_norm
          print *,'  and p=',p
          !exit
       else
          print *,'CG optimization succeeded.'
          print *
          print *,'  F=',F,' dF_dpopt_norm=',dF_dpopt_norm
          print *,'  and p=',p
       endif
     !    print *,'Completed CG inner loop, readjusting popt'
    !enddo

    deallocate(dF_dpopt)

end subroutine CG_Wrap

! Old code:

! Temporary setup of array for testing purposes ----
! Turned out not to be faster than sums over cubic terms!!!
! do isp=1,maxspecies
!   do jsp=1,maxspecies
!      !print *,'isp=',isp,', jsp=',jsp
!      if (jsp.ge.isp) then
!         !print *,'i going form 1 to ',splnNvals
!         do i=1,splnNvals
!            sepn=cutoffMin+dble(i-1)*(cutoffMax-cutoffMin)/dble(splnNvals-1)
!            pairpot=0d0
!            do j=1,31,2
!                if (sepn.le.pairpotparameter(j+1,isp,jsp)) then
!                    pairpot=pairpot +
!                    pairpotparameter(j,isp,jsp) * &
!                        ((pairpotparameter(j+1,isp,jsp)-sepn)**3)
!                endif
!            enddo
!            pairpotStr(i,isp,jsp)=pairpot
!            !if
!            ((i.le.10).or.(i.ge.(splnNvals-10)).and.(isp.eq.1).and.(jsp.eq.1))
!            then
!            !   write(81,*) sepn,pairpotStr(i,isp,jsp)
!            !endif
!         enddo
!      endif
!   enddo
! enddo
! stop
! print *,'finished spline init'
! --------------------------------------------------
