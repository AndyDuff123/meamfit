subroutine writeBoundsForRandParas

    !--------------------------------------------------------------c
    !
    !     Reads in the limits which are to be used to initialize
    !     the random starting values of the MEAM parameters from
    !     the 'dataforMEAMparagen' file.
    !
    !     Called by:
    !     Calls:
    !     Arguments:
    !     Returns:
    !     Files read:
    !     Files written:
    !
    !     Andy Duff
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization

    implicit none

    integer i,j,ii,ll,isp,jsp,spc_cnt
    character*80 string
    !Variables just used for reading in start_meam_parameters file:
    integer nfreepTmp

    open(unit=1,file=trim(settingsfile),status="old",position="append",action='write')

    write(1,*) "RandParasManual"

    !meamtau
    do ll=1,lmax
        do isp=1,maxspecies
            if (freep(2*m3+isp+(ll-1)*m1).ge.2) then
                write(1,*) meamtau_minorder(ll,isp)
                write(1,*) meamtau_maxorder(ll,isp)
            endif
        enddo
    enddo

    !electron density functions
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    !Check if we need to generate random paramaters for
                    !thi(isp,jsp,ll)
                    nfreepTmp=0
                    do i=1,12
                        if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i).ge.2) then
                            nfreepTmp=nfreepTmp+1
                        endif
                    enddo
                    if (nfreepTmp.gt.0) then
                        if (typethi(isp,jsp).eq.1) then !Cubic form
                            !Read in parameters from 'dataforMEAMparagen'
                            !necessary
                            !to generate these parameters
                            write(1,*) meamrhodecay_maxradius(ll,isp,jsp)
                            write(1,*) meamrhodecay_minradius(ll,isp,jsp)
                            write(1,*) meamrhodecay_negvals(ll,isp,jsp)

                            !For each coefficient which needs to be generated,
                            !read in
                            !min and max orders (10^..)
                            ii=1
                            do i=1,6
                                if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i-1).ge.2) &
                                    then
                                    write(1,*) meamrhodecay_minorder(ii,ll,isp,jsp)
                                    write(1,*) meamrhodecay_maxorder(ii,ll,isp,jsp)
                                    ii=ii+1
                                endif
                            enddo
                        endif
                    endif
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !embedding functions
    do isp=1,maxspecies
        do i=1,4
            if (freep(2*m3+lmax*m1+12*lm1*m2+isp+(i-1)*m1).ge.2) then
                if (i.eq.1) then
                    write(1,*) meame0_minorder(isp)
                    write(1,*) meame0_maxorder(isp)
                elseif (i.eq.2) then
                    write(1,*) meamrho0_minorder(isp)
                    write(1,*) meamrho0_maxorder(isp)
                elseif (i.eq.3) then
                    write(1,*) meamemb3_minorder(isp)
                    write(1,*) meamemb3_maxorder(isp)
                elseif (i.eq.4) then
                    write(1,*) meamemb4_minorder(isp)
                    write(1,*) meamemb4_maxorder(isp)
                endif
            endif
        enddo
    enddo

    !pair-potentials
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                !Check if we need to generate random paramaters for
                !pairpot(isp,jsp)
                nfreeppairpot(isp,jsp)=0
                do i=1,32
                    if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt) &
                        .ge.2) then
                        nfreeppairpot(isp,jsp)=nfreeppairpot(isp,jsp)+1
                    endif
                enddo
                if (nfreeppairpot(isp,jsp).gt.0) then
                    if (typepairpot(isp,jsp).eq.2) then
                        !Read in parameters from 'dataforMEAMparagen' necessary
                        !to
                        !generate these parameters
                        write(1,*) pairpotparameter_maxradius(isp,jsp)
                        write(1,*) pairpotparameter_minradius(isp,jsp)
                        write(1,*) pairpotparameter_negvals(isp,jsp)
                        if ((pairpotparameter_negvals(isp,jsp).lt.1).or. &
                            (pairpotparameter_negvals(isp,jsp).gt.3)) then
                            print *,'      ERROR: you must specify 1, 2 or 3,'
                            print *,'      stopping.'
                            stop
                        endif
                        !For each coefficient which needs to be generated, read
                        !in min and max orders (10^..)
                        ii=1
                        do i=1,16
                            if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i-1+32*spc_cnt) &
                                .ge.2) then
                                write(1,*) pairpotparameter_minorder(ii,isp,jsp)
                                write(1,*) pairpotparameter_maxorder(ii,isp,jsp)
                                ii=ii+1
                            endif
                        enddo
                    else
                        print *,'typepairpot=',typepairpot(isp,jsp), &
                            'not supported,stopping'
                        stop
                    endif
                endif
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !enconst
    do isp=1,maxspecies
        if (freep(m4+2*m2+isp).ge.2) then
            write(1,*) enconst_minorder(isp)
            write(1,*) enconst_maxorder(isp)
        endif
    enddo

    close(1)

end subroutine writeBoundsForRandParas
