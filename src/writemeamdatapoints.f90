subroutine writemeamdatapoints(outch,extraOutput)

    !---------------------------------------------------------------------c
    !
    !     Prints meam and vasp energies and forces to the screen and to
    !     the log file.
    !
    !     Called by:     program MEAMfit
    !     Calls:         -
    !     Arguments:     ndatapoints,truedata,bestfitdata
    !     Returns:       -
    !     Files read:    -
    !     Files written: -
    !
    !     Andy Duff, Feb 2008.
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------------c

    use m_datapoints
    use m_optimization
    use m_geometry
    use m_filenames
    use m_atomproperties

    implicit none

    logical extraOutput
    integer idatapoint,i,j,previous,outch,iRef,nRef
    integer ref_datapoint(9),ref_strucnum(9)
    real(8) intEn_fit,intEn_true

    !See 'objective function' for description of following variables
    ref_datapoint(1)=1
    ref_strucnum(1)=1
    ref_datapoint(2:9)=0
    ref_strucnum(2:9)=0

    nRef=1     !Number of reference energies
    idatapoint=1
    previous=0

    do i=1,nstruct

        if (refenergy(i).ge.1) then
           ref_datapoint(refenergy(i))=idatapoint
           ref_strucnum(refenergy(i))=i
           !print *,'refenergy(',i,')=',refenergy(i),' current nRef=',nRef,', increasing to...:'
           nRef=MAX(nRef,refenergy(i))
           !print *,'for istruct=',i,', i_{refenergy}=',ref_datapoint(1),', refenergy=',fitdata(ref_datapoint(1))
        endif

        if (weightsEn(i).ne.0d0) then
            if (extraOutput.eqv..false.) then
                if (previous.ne.1) then
                    write(outch,*)
                    write(outch,*) 'Energies:'
                    write(outch,*)
                    write(outch,*) 'Structure                     fitdata', &
                        '                    truedata'
                    write(outch,*) '----------------------------------------', &
                        '-----------------------------------------------'
                    previous=1
                endif
                if (useRef) then
                   if (refenergy(i).eq.0) then
                      if (nRef.eq.1) then
                         write(outch,'(A1,A35,F25.15,A2,F25.15)') ' ',strucnames(i), &
                             (fitdata(idatapoint)-fitdata(ref_datapoint(1))),'  ',(truedata(idatapoint)-truedata(ref_datapoint(1)))
                      !   write(50,*) weightsEn(i),'  ',(fitdata(idatapoint)-fitdata(ref_datapoint(1))),'  ',(truedata(idatapoint)-truedata(ref_datapoint(1))) !23_10_2020
                      else
                         intEn_fit=fitdata(idatapoint)
                         intEn_true=truedata(idatapoint)
                         do iRef=1,nRef
                            intEn_fit=intEn_fit+weightsEn(ref_strucnum(iRef))*fitdata(ref_datapoint(iRef))
                            intEn_true=intEn_true+weightsEn(ref_strucnum(iRef))*truedata(ref_datapoint(iRef))
                         enddo
                         write(outch,'(A1,A35,F25.15,A2,F25.15)') ' ',strucnames(i), &
                             intEn_fit,'  ',intEn_true
                      endif
                   endif
                else
                   write(outch,'(A1,A35,F25.15,A2,F25.15)') ' ',strucnames(i), &
                       fitdata(idatapoint),' ',truedata(idatapoint)
                endif
            else
                if (previous.ne.1) then
                    write(outch,'(A16,A59,A57,A28)') '% id     fitdata', &
               '            truedata            pair-pot part              ', &
                        'emb-func part            smallest sepn      ..per species', &
                        ' (e.g. (1,1); (1,2); (2,2) )'
                    previous=1
                endif
                if (useRef) then
                   if (nRef.eq.1) then
                      write(outch, & 
                   '(I5,A3,F17.12,A2,F17.12,A2,F22.15,A2,F25.15,A2,F10.5,A2)',advance="no") &
                          i,'   ',(fitdata(idatapoint)-fitdata(ref_datapoint(1))),'  ',(truedata(idatapoint)-truedata(ref_datapoint(1))), &
                          '  ',summeam_paireStr(idatapoint),'  ',summeamfStr(idatapoint),'  ', &
                          smallestsepnStr(i),'  '
                   endif
                else
                   write(outch, &
                '(I5,A3,F17.12,A2,F17.12,A2,F22.15,A2,F25.15,A2,F10.5,A2)',advance="no") &
                       i,'   ',fitdata(idatapoint),' ',truedata(idatapoint), &
                       '  ',summeam_paireStr(idatapoint),'  ',summeamfStr(idatapoint),' ', &
                       smallestsepnStr(i),'  '
                endif
                do j=1,smlsepnperspcStr_numEntries
                    write(outch,'(A1,F10.5)',advance="no") ' ',smlsepnperspcStr(j,i)
                enddo
                write(outch,*)
            endif
            idatapoint=idatapoint+1
        endif
        if (weightsFr(i).gt.0d0) then
            idatapoint=idatapoint+3*gn_forces(i)
        endif
        if (weightsSt(i).gt.0d0) then
            idatapoint=idatapoint+6
        endif
    enddo

    ! Print forces and/or stress tensor
    idatapoint=1
    previous=0
    do i=1,nstruct

        if (weightsEn(i).ne.0d0) then
            idatapoint=idatapoint+1
        endif

        !Place the header if we have not yet reported any forces or stress-tensors
        if (((weightsFr(i).gt.0d0).or.(weightsSt(i).gt.0d0)).and.(previous.eq.0)) then
            write(outch,*)
            write(outch,*) 'Forces and/or stress tensors:'
            write(outch,*)
            write(outch,*) 'Structure                     fitdata', &
                '                    truedata'
            write(outch,*) '----------------------------------------', &
                '-------------------------------------'
            previous=1
        endif

        if ((weightsFr(i).gt.0d0).or.(weightsSt(i).gt.0d0)) then
            write(outch,*)
            write(outch,'(A1,A20)') ' ',strucnames(i)
        endif

        if (weightsFr(i).gt.0d0) then
            open(78,file='forces') !save forces for use in vasp_langevin

            do j=1,gn_forces(i)
                if (optforce(j,i).eqv..true.) then
                    if (previous.eq.1) then
                       write(outch,*)
                       write(outch,'(A27,F10.5,F10.5,F10.5,F10.5,F10.5,F10.5)') &
                           ' Forces:                   ', &
                           fitdata(idatapoint),fitdata(idatapoint+1),fitdata(idatapoint+2), &
                           truedata(idatapoint),truedata(idatapoint+1),truedata(idatapoint+2)
                       previous=2
                    !  write(51,*) weightsFr(i),fitdata(idatapoint),truedata(idatapoint) !23_10_2020
                    !  write(51,*) weightsFr(i),fitdata(idatapoint+1),truedata(idatapoint+1) !23_10_2020
                    !  write(51,*) weightsFr(i),fitdata(idatapoint+2),truedata(idatapoint+2) !23_10_2020
                    else
                       write(outch,'(A27,F10.5,F10.5,F10.5,F10.5,F10.5,F10.5)') &
                           '                           ', &
                           fitdata(idatapoint),fitdata(idatapoint+1),fitdata(idatapoint+2), &
                           truedata(idatapoint),truedata(idatapoint+1),truedata(idatapoint+2)
                    !  write(51,*) weightsFr(i),fitdata(idatapoint),truedata(idatapoint) !23_10_2020
                    !  write(51,*) weightsFr(i),fitdata(idatapoint+1),truedata(idatapoint+1) !23_10_2020
                    !  write(51,*) weightsFr(i),fitdata(idatapoint+2),truedata(idatapoint+2) !23_10_2020
                    endif
                    write(78,'(E14.6,E14.6,E14.6)') &
                        fitdata(idatapoint),fitdata(idatapoint+1),fitdata(idatapoint+2)
                endif
                idatapoint=idatapoint+3
            enddo
            close(78)
        endif
        if (weightsSt(i).gt.0d0) then
            write(outch,*)
            write(outch,'(A27,F10.5,F10.5,F10.5,F10.5,F10.5,F10.5)') &
                            ' Stress tensor:            ', &
                            fitdata(idatapoint),fitdata(idatapoint+1),fitdata(idatapoint+2), &
                            truedata(idatapoint),truedata(idatapoint+1),truedata(idatapoint+2)
            write(outch,'(A27,F10.5,F10.5,F10.5,F10.5,F10.5,F10.5)') &
                            '                           ', &
                            fitdata(idatapoint+3),fitdata(idatapoint+4),fitdata(idatapoint+5), &
                            truedata(idatapoint+3),truedata(idatapoint+4),truedata(idatapoint+5)
!            write(52,*) weightsSt(i),fitdata(idatapoint),truedata(idatapoint) !23_10_2020
!            write(52,*) weightsSt(i),fitdata(idatapoint+1),truedata(idatapoint+1) !23_10_2020
!            write(52,*) weightsSt(i),fitdata(idatapoint+2),truedata(idatapoint+2) !23_10_2020
!            write(52,*) weightsSt(i),fitdata(idatapoint+3),truedata(idatapoint+3) !23_10_2020
!            write(52,*) weightsSt(i),fitdata(idatapoint+4),truedata(idatapoint+4) !23_10_2020
!            write(52,*) weightsSt(i),fitdata(idatapoint+5),truedata(idatapoint+5) !23_10_2020
            idatapoint=idatapoint+6
        endif
        if (previous.eq.2) then
            previous=1
        endif
    enddo

end subroutine writemeamdatapoints
