subroutine plotCutoffs

    !---------------------------------------------------------------c
    !
    !     Plots the cutoffs to the atomic density and pair- 
    !     potentials to files, so that they cen be plotted alongside
    !     the potential functions.
    !
    !     Called by:     -
    !     Calls:         -
    !     Arguments:     -
    !     Returns:       -
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_filenames
    use m_meamparameters
    use m_optimization
    use m_atomproperties
    use m_generalinfo

    implicit none

    integer i,j,k,l,m

    j=1
    k=0
    l=1
    m=1
    do i=2*m3+lmax*m1+1,2*m3+lmax*m1+12*lm1*m2
        meamrhodecay(j,k,l,m)=p(i)
        if (j.lt.12) then
            j=j+1
        elseif (k.lt.lmax) then
            j=1
            k=k+1
        elseif (m.lt.maxspecies) then
            j=1
            k=0
            m=m+1
        elseif (l.lt.maxspecies) then
            j=1
            k=0
            l=l+1
            m=1
        endif
    enddo

    j=1
    if (maxspecies.eq.1) then
        do i=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4
            pairpotparameter(j,1,1)=p(i)
            j=j+1
        enddo
    else
        k=1
        l=1
        do i=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4
            pairpotparameter(j,k,l)=p(i)
            if (j.lt.32) then
                j=j+1
            elseif (l.lt.maxspecies) then
                j=1
                l=l+1
            elseif (k.lt.maxspecies) then
                j=1
                l=1
                k=k+1
            endif
        enddo
    endif

end subroutine plotCutoffs
