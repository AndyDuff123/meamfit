
subroutine initializestruc_fromarrays(filenmr)

    !--------------------------------------------------------------c
    !
    !     Read in the set of vasprun.xml or POSCAR files, process 
    !     them, and store geometrical info in the module 
    !     'm_geometry'. If filenmr=0 then all files are processed, 
    !     otherwise only vasprun.xml/POSCAR file number 'filenmr' 
    !     is treated.
    !
    !     For filenmr=0, vasprun.xml/POSCAR files are read three 
    !     times, whereas for filenmr>0 only two passes are used 
    !     (and for only a single vasprun.xml/POSCAR file). The 
    !     first pass (only for filenmr=0) determines the total 
    !     number of species across all vasprun.xml/POSCAR files, 
    !     maxspecies, and constructs the array z2species, for 
    !     which the Zth element contains the order in which the 
    !     atomic number Z first appeared in the vasprun.xml/
    !     POSCAR files (and this order is henceforth refered to 
    !     as the 'species'). E.g., for a vasprun.xml file
    !     containing two species, C (Z=12) and Fe (Z=26). C is 
    !     first to appear and so we have z2species(12)=1, and 
    !     next is Fe, so that z2species(26)=2. In this way, the 
    !     z2species array maps the atomic number z to the species 
    !     number. In this example maxspecies=2.
    !
    !     The next two passes are used for both filenmr=0 and for
    !     filenmr>0, but in the latter only a single vasprun.xml/
    !     POSCAR file is processed. In the second pass, we read in 
    !     the vasprun.xml/POSCAR files again and extend the 
    !     lattice where neccesary (for cases where force 
    !     optimisation is specified) and determine the largest
    !     values of n_inequivalentsites, etc (so that we can
    !     define array sizes). Then we can read in for the third
    !     time and fill the gn_neighbors arrays, etc.
    !
    !     Notes for programmer:
    !     When 'filenmr' is non-zero, only the neighborhood of the 
    !     specified structure number is determined. However, the 
    !     arrays are still re-allocated and given sizes spanning 
    !     the full range of structure files, with array elements
    !     elements corresponding to files .ne. filenmr set to 
    !     zero. This turned out to be the tidiest way of
    !     implementing the 'filenmr' switch.
    !
    !     Written by Andrew Duff.
    !
    !     Called by:     program MEAMfit
    !     Calls:         extendlattice,neighborhood
    !     Returns:       maxnnatoms,maxspecies,z2species,
    !                 gn_inequivalentsites,gspecies,gn_neighbors,
    !                 gneighborlist,gxyz
    !     Files read:    targetfiles
    !     Files written: crystalstrucfile
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_filenames
    use m_poscar
    use m_neighborlist
    use m_geometry
    use m_atomproperties
    use m_datapoints
    use m_meamparameters
    use m_optimization
    use m_generalinfo

    implicit none
    integer i,j,ns,nshell, &
        iaux(1),filenmr,istrucInFile_prev,err

    integer zz_tmp(natoms)
    real(8) coords_tmp(1:3,natoms)

    !Store old coordinates and z values. This is so that after 'extendlattice'
    !(which changes coordinates and z values) we can return to the original
    !supercell
    norigatoms=natoms
    do i=1,natoms
        coords_tmp(1:3,i)=coordinates(1:3,i)
        zz_tmp(i)=zz(i)
    enddo

    if (verbosity.gt.1) then
       print *,'Preparing to initialize structures...'
    endif

    if (filenmr.eq.0) then
        if (verbosity.gt.1) print *,'preparing first pass'
        !First pass:
        z2species=0
        maxspecies=0
        do i=1,nstruct
            if (verbosity.gt.1) print *,'i=',i,'nspecies=',nspecies
            !Find all atomic species
            do j=1,nspecies
                if (verbosity.gt.1) print *,'z(j)=',z(j)
                if (verbosity.gt.1) print *,'z2species(z(',j,'))=',z2species(z(j))
                if(z2species(z(j)).eq.0) then
                    maxspecies=maxspecies+1
                    if (maxspecies.gt.10) then
                       print *,'ERROR: maxspecies>10. Please re-size speciestoZ'
                       print *,'array and then re-run.'
                       stop
                    endif
                    if (verbosity.gt.1) print *,'i=',i,' j=',j,' z(',j,')=',z(j),', maxspecies=',maxspecies
                    z2species(z(j))=maxspecies
                    speciestoZ(maxspecies)=mod(z(j),112)
                    if (verbosity.gt.1) print *,'z2species(z(',j,')=',z2species(z(j)),', speciestoZ(',maxspecies,')=',speciestoZ(maxspecies)
                endif
            enddo
            istrucInFile_prev=istrucInFile(i)
        enddo
        !stop
        if (verbosity.gt.1) print *,'preparing for second pass'
        !Second pass:
        maxsites=0
        maxneighbors=0
        maxnnatoms=0
        do i=1,nstruct
            if (verbosity.gt.1) print *,i,'/',nstruct
           !print *,'targetfiles=',targetfiles(i),', prev=',targetfile_prev
           !print *,'istrucInFile(i)=',istrucInFile(i),' istrucInFile_prev+1=',istrucInFile_prev+1
            n_inequivalentsites=natoms
            nshell=0
            if (verbosity.gt.1) print *,'n_inequivalentsites=',n_inequivalentsites
            if (computeForces(i)) then
              if (verbosity.gt.1) print *,'extending...'
                !Need to make atoms in adjacent cells to the central cell
                !inequivalent sites, so that when the force calculation
                !takes place, these atoms are included in the energy
                !calculation.
                call extendlattice_fromarrays(coords_tmp,zz_tmp)
            endif
            !Find all neighbors
            if (verbosity.gt.1) print *,'finding neighbours'
            call neighborhood(computeForces(i))
            maxsites=max(maxsites,n_inequivalentsites)
            iaux=maxval(n_neighbors)
            maxneighbors=max(maxneighbors,iaux(1))
            maxnnatoms=max(maxnnatoms,nnatoms)
            istrucInFile_prev=istrucInFile(i)

            if (computeForces(i)) then
               !Restore old natoms, etc (post 'extendlattice'...)
               natoms=norigatoms
               deallocate (zz,coordinates)
               allocate (zz(natoms),coordinates(3,natoms))
               do j=1,natoms
                   coordinates(1:3,j)=coords_tmp(1:3,j)
                   zz(j)=zz_tmp(j)
               enddo
            endif

        enddo
        if (verbosity.gt.1) print *,'completed second pass'
    elseif (filenmr.gt.0) then

        print *,'ERROR, filenmr>0 in initializestruc... , STOPPING.'
        stop

    endif

    !(Re)allocate relevant arrays (note: we allocate arrays
    !of size nstruct, even if filenmr>0, I.e. if we just need
    !the neighborhood of one structure. This is to avoid having to
    !rewrite other subroutines, which assume such a form for these
    !arrays).
    if (allocated(gn_inequivalentsites)) &
        deallocate(gn_inequivalentsites)
    if (allocated(gspecies)) deallocate(gspecies)
    if (allocated(gn_neighbors)) deallocate(gn_neighbors)
    if (allocated(gneighborlist)) deallocate(gneighborlist)
    if (allocated(gxyz)) deallocate(gxyz)
    if (allocated(gn_forces)) deallocate(gn_forces)
    if (allocated(optforce)) deallocate(optforce)
    if (allocated(diststr)) deallocate(diststr)
    if (allocated(dist2str)) deallocate(dist2str)
    if (allocated(dist3str)) deallocate(dist3str)
    if (allocated(dxstr)) deallocate(dxstr)
    if (allocated(dystr)) deallocate(dystr)
    if (allocated(dzstr)) deallocate(dzstr)
    allocate( gn_inequivalentsites(nstruct), &
        gspecies(maxnnatoms,nstruct), &
        optforce(maxsites,nstruct), &
        gn_neighbors(maxsites,nstruct), &
        gneighborlist(maxneighbors,maxsites,nstruct), &
        gxyz(3,maxnnatoms,nstruct), &
        gn_forces(nstruct) )
    allocate( diststr(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dist2str(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dist3str(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dxstr(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dystr(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dzstr(maxneighbors,maxsites,0:0,0:0,nstruct),stat=err)
    if (.not.allocated(gxyz_backup)) then
        allocate(gxyz_backup(3,maxnnatoms,nstruct))
        !This should just be allocated once and then left alone
    endif

    !Third pass:
    if (verbosity.gt.1) print *,'starting third pass'
    if (filenmr.eq.0) then

        do i=1,nstruct
            if (verbosity.gt.1) print *,'i=',i
            n_inequivalentsites=natoms
            nshell=0
            if (computeForces(i)) then
                !Need to make atoms in adjacent cells to the central cell
                !inequivalent sites, so that when the force calculation
                !takes place, these atoms are included in the energy
                !calculation.
                call extendlattice_fromarrays(coords_tmp,zz_tmp)
            endif
            call neighborhood(computeForces(i))
          ! if (i.eq.2) then
          !    print *,'n_neighbors(1)=',n_neighbors(1)
          !     print *,'for istr=2'
          !     stop
          !  endif

            ns=n_inequivalentsites
            gn_inequivalentsites(i)=ns
            !            print *,'istr=',i,', gn_inequivalentsites=',
            !     +          gn_inequivalentsites(i)
            if (verbosity.gt.1) print *,'computeForces(',i,')=',computeForces(i)
            if (computeForces(i)) then
                gn_forces(i)=norigatoms
            endif
            gn_neighbors(1:ns,i)=n_neighbors(1:ns)
            do j=1,ns
                gneighborlist(1:n_neighbors(j),j,i)= &
                    neighborlist(1:n_neighbors(j),j)
            enddo
            do j=1,nnatoms
                !print *,'j=',j,' species(j)=',species(j)
                gxyz(1:3,j,i)=xyz(1:3,j)!ERROR: here the second index is
                !the nearest neighbor number
                gspecies(j,i)=species(j)
            enddo
            istrucInFile_prev=istrucInFile(i)

            if (computeForces(i)) then
               !Restore old natoms, etc (post 'extendlattice'...)
               natoms=norigatoms
               deallocate (zz,coordinates)
               allocate (zz(natoms),coordinates(3,natoms))
               do j=1,natoms
                   coordinates(1:3,j)=coords_tmp(1:3,j)
                   zz(j)=zz_tmp(j)
               enddo
            endif

        enddo
    else

        print *,'ERROR, filenmr>0 in initializestruc... , STOPPING.'
        stop

    endif
    close(1)

    if(allocated(z)) deallocate(z,coordinates)!nat
    if(allocated(xyz)) deallocate(xyz,species, &
        n_neighbors,neighborlist)

end subroutine initializestruc_fromarrays
