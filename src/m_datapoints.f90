
module m_datapoints

    !     Copyright (c) 2018, STFC

    !Input energy or force data (e.g. from DFT)
    integer nEn,nFrcComp,nStrComp
    logical fitLargeForces
    logical, allocatable:: freeenergy(:),computeForces(:),computeForces_backup(:)
    integer, allocatable:: rlxstruc(:),refenergy(:)
    integer ndatapoints,ndatapointsTrue       !no. of datapoints in file (the first includes in the count also atoms for 
                                  !which force-fitting has been turned off by selective force fitting, and is used to size 
                                  !arrays; the latter excludes these, providing a true value of the number of fitting 
                                  !points, and is used to compute no. fitting points per parameters.
    real(8) V1,V2,V3,e1,e2,e3,e4,e5,e6,e7,e8,e9, &
        gam4,gam5,gam6,gam7,gam8,gam9, &
        forcemagnitudepower,excludeforcemax,excludeforcemin, &  !MS2020:modification for L0&L1 norm force fitting
        V(21)
    real(8), allocatable:: truedata(:) !size: ndatapoints
    logical, allocatable:: optforce(:,:) !size: maxatoms,nposcars
    real(8), parameter :: kB2eVperAng3=0.00062415091259d0 !used to convert vasprun stress tensors

end module m_datapoints
