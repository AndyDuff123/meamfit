subroutine holdcutoffparas

    !--------------------------------------------------------------c
    !
    !     Set radial cut-off parameters not to optimize, and store
    !     previous values for reinstatement later.
    !
    !     Called by: MEAMfit
    !     Calls: -
    !     Returns: freep
    !     Files read: -
    !     Files written: -
    !
    !     Andy Duff, Oct 2016.
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization

    implicit none

    !logical sorted
    !integer iseed
    integer i,j,ll,isp,jsp,spc_cnt

    allocate(freep_backup(np))

    print *,"Fixing cutoffs for first stage of optimization"
    print *

    !---- electron density functions ----
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    do i=1,ncutoffDens(isp,jsp)
                        freep_backup(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i)= &
                               freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i)
                        freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i)=0
                    enddo
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    !------------------------------------


    !---- pair-potentials ----
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                if (nfreeppairpot(isp,jsp).gt.0) then
                    if (typepairpot(isp,jsp).eq.2) then
                        do i=1,ncutoffPairpot(isp,jsp)
                            freep_backup(2*m3+(4+lmax)*m1+12*lm1*m2+2*i+32*spc_cnt)= &
                                   freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i+32*spc_cnt)
                            freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i+32*spc_cnt)=0
                        enddo
                    endif
                endif
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    !--------------------------------------------------------


end subroutine holdcutoffparas
