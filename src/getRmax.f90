subroutine getRmax

    !----------------------------------------------------------------------c
    !
    !     Set-up the maximum radius to be considered subsequently. If
    !     cutoffs are to be optimized, this will be set either to 
    !     CUTOFFMAX, or if a potential file is provided by the user and
    !     some of the (fixed) cutoffs in this file are larger, then it will
    !     be set to this instead. If cutoffs are not to be optimized,
    !     it will either be set to the maximum cutoff in the potential
    !     input file, or if none is provided, it will be set to CUTOFFMAX
    !     (for the purposes of plotting the separation histogram).
    !
    !     Called by:     MEAMfit
    !     Calls:         -
    !     Returns:       p_rmax
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_meamparameters
    use m_generalinfo
    use m_atomproperties
    use m_optimization
    use m_filenames

    implicit none

    integer i,j,k,isp,jsp,ll,spc_cnt
    real(8) rlargest

    if (readpotfile.eqv..false.) then
       !If no input potential file has been supplied the maximum radius to be
       !considered is just that given by CUTOFF_MAX
       if (verbosity.gt.1) then
          print *,'No potential parameters file supplied, therefore setting'
          print *,'p_rmax=CUTOFF_MAX (=',cutoffMax,')'
       endif
       p_rmax=cutoffMax
    endif

    !Check to see if any of the cut-off radii are being optimized, and keep
    !track of the largest of these.
    !First check the electron densities:
    cutoffopt=.false.
    spc_cnt=0
    rlargest=0d0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    do i=2,12,2
                        if ( (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i).eq.1).or. &
                             (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i).eq.2) ) then
                            cutoffopt=.true.
                        endif
                        if (meamrhodecay(i,ll,isp,jsp).gt.rlargest) then
                           rlargest=meamrhodecay(i,ll,isp,jsp)
                        endif
                    enddo
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    !Next the pair-potentials:
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                do i=2,32,2
                    if ( (freep(2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt).eq.1).or. &
                         (freep(2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt).eq.2) ) then
                        cutoffopt=.true.
                    endif
                    if (pairpotparameter(i,isp,jsp).gt.rlargest) then
                       rlargest=pairpotparameter(i,isp,jsp)
                    endif
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    if (verbosity.gt.1) then
       print *,'cutoffopt=',cutoffopt,', so...'
    endif
    if ((cutoffopt.eqv..false.).and.(readpotfile.eqv..true.)) then
       !In this case p_rmax can be set equal to the maximum radius used in the
       !start_meam_parameters file
       if (verbosity.gt.1) then
          print *,'Largest cutoff in start_meam_parameters=',rlargest
          print *,'Since these are not being optimized, setting p_rmax=',rlargest
       endif
       p_rmax=rlargest
      !cutoffMax=rlargest !Even though we are not optimizing, this needs to be
      !          !set to the largest cut-off to avoid the cut-offs being changed
    else
       if (cutoffMax.gt.rlargest) then
          if (verbosity.gt.1) then
             print *,'cutoffMax > largest cut-off in start_meam_parameters, therefore'
             print *,'setting p_rmax=',cutoffMax
          endif
          p_rmax=cutoffMax
       else
          if (verbosity.gt.1) then
             print *,'cutoffMax < largest cut-off in start_meam_parameters, therefore'
             print *,'setting p_rmax=',rlargest
          endif
          p_rmax=rlargest
       endif
    endif

end subroutine getRmax
